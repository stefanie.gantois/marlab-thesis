﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CableController : MonoBehaviour
{
    public GameObject cable, probetip, probebody;

    private void Start()
    {
    }

    private void Update()
    {
        PlaceCable();
    }


    public void PlaceCable()
    {
        cable.SetActive(true);
        Vector3 positionbody = probebody.transform.position;
        Vector3 positiontip = probetip.transform.position;

        //Interpolate the position of the cable between tip and body
        Vector3 positioncable = Vector3.Lerp(positionbody, positiontip, 0.5f);
        cable.transform.position = positioncable;

        //Orientation
        cable.transform.up = positiontip - positionbody;

        //scale the cable 
        Vector3 scale = cable.transform.localScale;
        scale.y = Vector3.Distance(positionbody, positiontip) / 2.0f;
        cable.transform.localScale = scale;

        /*
        Debug.Log("position cable" + positioncable);
        Debug.Log("position box" + positionbody);
        Debug.Log("position probe" + positiontip);
        Debug.Log("scale" + scale);
        */
        //cable.transform.LookAt(probe.transform.localPosition);
    }

    public void DissapearCable()
    {
        cable.SetActive(false);
    }

}
