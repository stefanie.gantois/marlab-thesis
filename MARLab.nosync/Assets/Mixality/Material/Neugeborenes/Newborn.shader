﻿/*The Subsurface scattering portion of this shader was derived from https://www.alanzucconi.com/2017/08/30/fast-subsurface-scattering-2/
*/

Shader "Mixality/Newborn"
{
    Properties
    {
        _MainTex ("Healthy (RGB)", 2D) = "white" {}
        _PaleTex("Pale (RGB)", 2D) = "white" {}
        _BlueTex("Blue (RGB)", 2D) = "white" {}
        _MetalGlossiness ("Metallic Smoothness", 2D) = "white" {}
        _AmbientOcclusion("Ambient Occlusion", 2D) = "white" {}
        _Mask("Mouth Area Mask (G)",2D) = "white"{}
        _LocalThickness("Thickness",2D) = "white"{}
        _Oxygen("Oxygen", Range(0,1)) = 1.0
        _MouthArea("Mouth Area Factor", Range(0,1)) = 1.0
        _Scale("SSS_Scale",Range(0,1)) = 1.0
        _Power("SSS_Power",Range(0,1)) = 1.0
        _Distortion("SSS_Distortion",Range(0,1)) = 1.0
        _SSS_Color("SSS Color",Color) = (1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf StandardTranslucent fullforwardshadows
        #include "UnityPBSLighting.cginc"

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _PaleTex;
        sampler2D _BlueTex;
        sampler2D _MetalGlossiness;
        sampler2D _AmbientOcclusion;
        sampler2D _LocalThickness;
        sampler2D _Mask;

        struct Input
        {
            float2 uv_MainTex;
        };
        half _Oxygen;
        half _MouthArea;
        float _thickness;
        float _Power = 1;
        float _Distortion = 1;
        float _Scale;
        fixed3 _SSS_Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)
        
        inline fixed4 LightingStandardTranslucent(SurfaceOutputStandard s, fixed3 viewDir, UnityGI gi)
        {
            // Original colour
            fixed4 pbr = LightingStandard(s, viewDir, gi);

            // --- Translucency ---
            float3 L = gi.light.dir;
            float3 V = viewDir;
            float3 N = s.Normal;

            float3 H = normalize(L + N *  _Distortion);
            float I = pow(saturate(dot(V, -H)), _Power) * _Scale* _thickness;

            // Final add
            pbr.rgb = pbr.rgb + gi.light.color * _SSS_Color * I;
            return pbr;
        }
        void LightingStandardTranslucent_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi)
        {
            LightingStandard_GI(s, data, gi);
        }
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 color_healthy = tex2D (_MainTex, IN.uv_MainTex);
            fixed4 color_pale = tex2D(_PaleTex, IN.uv_MainTex);
            fixed4 color_blue = tex2D(_BlueTex, IN.uv_MainTex);
            float mask = tex2D(_Mask, IN.uv_MainTex).r;
            fixed4 blend;
            if (_Oxygen > 0.5f) {
                blend = lerp(color_pale, color_healthy, (_Oxygen - 0.5f) * 2.0);
            }
            else {
                blend = lerp(color_blue, color_pale, _Oxygen * 2.0);
            }

            blend = lerp(blend,color_healthy ,clamp(_MouthArea-mask,0.0,1.0));

            o.Albedo = blend.rgb;
            o.Metallic = tex2D(_MetalGlossiness, IN.uv_MainTex).r ;
            o.Smoothness =  tex2D(_MetalGlossiness, IN.uv_MainTex).a;
            o.Occlusion = tex2D(_AmbientOcclusion, IN.uv_MainTex).r;

            _thickness = 1.0- tex2D(_LocalThickness, IN.uv_MainTex).r;
        }
        ENDCG
    }
    FallBack "Diffuse"
}

