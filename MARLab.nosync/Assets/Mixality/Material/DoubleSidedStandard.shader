﻿Shader "CustomExtended/DoubleSidedStandard"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _MetalGlossiness("Metallic Smoothness", 2D) = "white" {}
        _Normal("Normal Map", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGINCLUDE
            // Whatever you write inside here
            // will be included in every pass
            sampler2D _MainTex;

            struct Input
            {
                float2 uv_MainTex;
            };

            sampler2D _MetalGlossiness;
            sampler2D _Normal;
            fixed4 _Color;
            void surf(Input IN, inout SurfaceOutputStandard o)
            {
                // Albedo comes from a texture tinted by color
                fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;


                o.Albedo = c.rgb;
                o.Metallic = tex2D(_MetalGlossiness, IN.uv_MainTex).r;
                o.Smoothness = tex2D(_MetalGlossiness, IN.uv_MainTex).a;
                o.Alpha = c.a;
                o.Normal = UnpackNormal( tex2D(_Normal, IN.uv_MainTex));
            }
        ENDCG

        CGPROGRAM
            // This is the actual pass
            #pragma surface surf Standard fullforwardshadows
            #pragma target 3.0
        ENDCG

        Cull Front
        CGPROGRAM
            #pragma surface surf Standard fullforwardshadows vertex:vert
            #pragma target 3.0
            void vert(inout appdata_full v)
            {
                // Here we are making the surface look
                // the opposite direction
                v.normal = -v.normal;
            }
        ENDCG
    }
    FallBack "Diffuse"
}
