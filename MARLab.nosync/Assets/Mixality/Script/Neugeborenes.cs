﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Neugeborenes : MonoBehaviour
{
    Animator anim;
    [SerializeField] float blendingTime = 0f;
    int layer_base = 0,layer_POMOverride = 3,layer_facialOverride = 5;
    string state_active = "Lebhaft", state_calm = "Ruhig", state_weak = "Schwach", state_still = "Sniffing";

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    private void OnEnable()
    {
        StartCoroutine(Blink());
    }

    /// <summary>
    /// Steuert die Atemfrequenz und Tiefe des Neugeborenen
    /// </summary>
    /// <param name="frequency"></param>
    /// <param name="depth"></param>
    public void SetBreath(float frequency, float depth)
    {
        anim.SetFloat("AtemzugProS", frequency);
        Debug.Log("depth: " + depth);
        anim.SetLayerWeight(1, depth);
    }
    /// <summary>
    /// Steuert das Grundverhalten des Neugeborenen. 0 = keine Bewegung bis 3 = lebhaftes Verhalten.
    /// </summary>
    /// <param name="state"></param>
    public void SetBehaviour(int state)
    {
        switch (state)
        {
            //completely still
            case 0:
                anim.CrossFadeInFixedTime(state_still, blendingTime, layer_base);
                break;
            //weak
            case 1:
                anim.CrossFadeInFixedTime(state_weak, blendingTime, layer_base);
                break;
            //calm
            case 2:
                anim.CrossFadeInFixedTime(state_calm, blendingTime, layer_base);
                break;
            //active
            case 3:
                anim.CrossFadeInFixedTime(state_active, blendingTime, layer_base);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Kontrolliert ob die Pose für das Pulsoxymeter zusätzlich gehalten werden soll
    /// </summary>
    /// <param name="isPOM"></param>
    public void SetStaticPOM(bool isPOM)
    {
        if(isPOM) anim.SetLayerWeight(layer_POMOverride,1f);
        else anim.SetLayerWeight(layer_POMOverride, 0f);
    }

    /// <summary>
    /// Die Facial Animationen müssen in einigen Fällen manuell überschireben werden, da sie nicht mit Masken gesteuert werden können. (z.B. die Interaktion "Trocknen" hat keine Facial animationen, und überschreibt daher die basis animation mit Null. Um das zu umgehen, diese Funktion mit interaction = false aufrufen)
    /// </summary>
    /// <param name="interaction"></param>
    public void SetFacialSource(bool interaction)
    {
        if(interaction) anim.SetLayerWeight(layer_facialOverride, 0f);
        else anim.SetLayerWeight(layer_facialOverride, 1f);
    }

    IEnumerator Blink()
    {
        int r = Random.Range(3, 17);
        yield return new WaitForSeconds(r);
        anim.Play("Blinzeln", 5);
        StartCoroutine(Blink());

    }
}
