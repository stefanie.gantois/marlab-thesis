﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class AvatarIndicator : MonoBehaviour
{
    [SerializeField] Transform hipPiece,groundPiece;
    [SerializeField] Vector3 groundNormal = Vector3.up;
    [SerializeField] float groundHeight = 0;
    [SerializeField][Tooltip("Time it takes until Ground Indicator catches up")] float smoothTime = 0.1f;

    [SerializeField] SkinnedMeshRenderer render;
    [SerializeField] bool setRandomColorAtEnable;
    Color color;

    //Cache
    Vector3 c_position, ref_position, c_forward, ref_forward;

    private void OnEnable()
    {
        if (setRandomColorAtEnable) SetHue(Random.Range(0f, 1f));
        SetUpMotion();
    }

    private void Update()
    {
        Motion();
    }

    void SetUpMotion()
    {
        c_position = Vector3.ProjectOnPlane(hipPiece.position, Vector3.up) + (Vector3.up * groundHeight);
        c_forward = hipPiece.forward;
    }
    void Motion()
    {
        if (groundPiece == null || hipPiece == null) enabled = false;

        if (smoothTime < 0.01) StraightMotion();
        else DampedMotion();
    } 
    void DampedMotion()
    {
        c_position = Vector3.SmoothDamp(c_position, Vector3.ProjectOnPlane(hipPiece.position, Vector3.up), ref ref_position, smoothTime);

        groundPiece.position = c_position + (Vector3.up * groundHeight);

        c_forward = Vector3.SmoothDamp(c_forward, hipPiece.forward, ref ref_forward, smoothTime);
        groundPiece.rotation = Quaternion.LookRotation(groundNormal.normalized, c_forward);
    }
    void StraightMotion()
    {
        groundPiece.position = Vector3.ProjectOnPlane(hipPiece.position, Vector3.up) + (Vector3.up * groundHeight);
        groundPiece.rotation = Quaternion.LookRotation(groundNormal.normalized, hipPiece.forward);
    }

    public void SetHue(float hue)
    {
        color = Color.HSVToRGB(hue, 0.5f, 0.5f);
        color.a = 0f;
        render.material.color = color;
    }
}
