﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Beispielklasse zur Steuerung der Reanimationseinheit im Editor
/// </summary>
public class ReanimationseinheitInspektorController : MonoBehaviour
{
    public Reanimationseinheit rea;
    [SerializeField] bool abs, beatmung, gas;
    [SerializeField] float sauerstoffkonzentration = 21f, saugleistung = 0f, frequenz = 20f, peep = 0.5f, druckbegrenzung = 0.5f, flow1 = 10f, flow2 = 0f,
        nd_absaugung = 0f, nd_beatmung = 0f, nd_gas1 = 0f, nd_gas2 = 0f;
    [SerializeField] string timer = "", temp = "", other = "";

    [SerializeField] bool bell, vorwaermen, manuell, haut, led_over37, alarme, C, F, netz, b1, b2;

    [SerializeField] bool fallschutzFront, fallschutzLeft, fallschutzRight;

    [SerializeField] bool lampe;

    void Update()
    {
        rea.toggle_absaugung.Toggle(abs);
        rea.toggle_beatmung.Toggle(beatmung);
        rea.toggle_gasversorgung.Toggle(gas);

        rea.drehknopf_Sauerstoffkonzentration.SetValue(sauerstoffkonzentration);
        rea.drehknopf_Druckbegrenzung.SetValue(druckbegrenzung);
        rea.drehknopf_flow1.SetValue(flow1);
        rea.drehknopf_flow2.SetValue(flow2);
        rea.drehknopf_Frequenz.SetValue(frequenz);
        rea.drehknopf_Peep.SetValue(peep);
        rea.drehknopf_Saugleistung.SetValue(saugleistung);

        rea.nadel_absaugung.SetValue(nd_absaugung);
        rea.nadel_beatmung.SetValue(nd_beatmung);
        rea.nadel_gasversorgung1.SetValue(nd_gas1);
        rea.nadel_gasversorgung2.SetValue(nd_gas2);

        rea.SetTempText(temp);
        rea.SetTimerText(timer);
        rea.SetOtherText(other);

        rea.led_bell.Toggle(bell);
        rea.led_vorwaermen.Toggle(vorwaermen);
        rea.led_manuell.Toggle(manuell);
        rea.led_haut.Toggle(haut);
        rea.led_37.Toggle(led_over37);
        rea.led_alarme.Toggle(alarme);
        rea.led_C.Toggle(C);
        rea.led_F.Toggle(F);

        rea.but_netz.Toggle(netz);
        rea.but_1.Toggle(b1);
        rea.but_2.Toggle(b2);

        rea.SetSidebar(fallschutzFront, fallschutzLeft, fallschutzRight);

        rea.SetLight(lampe);
    }
}
