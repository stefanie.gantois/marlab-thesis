﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Beispielklasse zur Steuerung des Patientenbetts im Editor
/// </summary>
public class PatientenbettInspektorController : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 1f)] float hoehe = 1f;
    [SerializeField] [Range(-1f, 1f)] float balance = 0f;

    [SerializeField]
    [Range(0f, 1f)] float rueckenlehne = 0f;
    [SerializeField] [Range(0f, 1f)] float beinlehne = 0f;


    [SerializeField]
    bool fallschutz_kopfLinks;
    [SerializeField] bool fallschutz_kopfRechts, fallschutz_fussLinks, fallschutz_fussRechts;

    public Patientenbett patientenbett;

    void Update()
    {
        patientenbett.hoehe = hoehe;
        patientenbett.balance = balance;
        patientenbett.rueckenlehne = rueckenlehne;
        patientenbett.beinlehne = beinlehne;
       patientenbett.ToggleProtection(fallschutz_kopfLinks,fallschutz_kopfRechts, fallschutz_fussLinks, fallschutz_fussRechts, false);
    }
}
