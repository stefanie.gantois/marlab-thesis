﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
/// <summary>
/// Beispielklasse zur Steuerung des Neugeborenen im Editor
/// </summary>
public class NeugeborenesInspectorController : MonoBehaviour
{
    [Tooltip("Atemzüge pro Sekunde")] [Range(0f, 3f)] public float atemFrequenz = 1f;
    [SerializeField] [Tooltip("Relativer Effekt der Atem-Animation")] [Range(0f, 1f)] public float atemTiefe = 1f;
    [SerializeField] [Tooltip("Aktivität")] [Range(0, 3)] public int aktivitaet = 3;
    [SerializeField] [Tooltip("Ist das Kind mit Pulsoxymeter verbunden")] bool pulsOxymetrie = false;
    [SerializeField] [Tooltip("Sollen die Blendshapes aus den Interaktionsanimationen benutzt werden?")] bool facialAusInteraktionen = true;
    [SerializeField] [Tooltip("Schaltet durch die Animationen")] InteractionTypes interaktion;

    //cache
    int act = -1;
    InteractionTypes inter = InteractionTypes.AmbAufsetzen;

    public Neugeborenes newborn;
    public ScenarioInteractionController rea;
    
    private void Start()
    {
        newborn.SetBreath(atemFrequenz,atemTiefe);
        /*newborn = GetComponentInChildren<Neugeborenes>();
        rea = GetComponentInChildren<ScenarioInteractionController>();
        */
    }
    /*private void Update()
    {
        newborn.SetBreath(atemFrequenz, atemTiefe);
        newborn.SetStaticPOM(pulsOxymetrie);
        newborn.SetFacialSource(facialAusInteraktionen);
        if (aktivitaet != act) newborn.SetBehaviour(aktivitaet);
        if (interaktion != inter) rea.TriggerInteraction(interaktion);

        act = aktivitaet;
        inter = interaktion;
    }*/

    public void SetBreathingFrequency(int frequency)
    {
        atemFrequenz = (float)frequency / 60;
        newborn.SetBreath(atemFrequenz, atemTiefe);
    }

    public void SetBreathDepth(int depth)
    {
        atemTiefe = (float) (depth * 0.1);
        newborn.SetBreath(atemFrequenz, atemTiefe);
    }

    public void SetActivity(int activity)
    {
        newborn.SetBehaviour(activity);
    }

    public void SetPom(bool pom)
    {
        newborn.SetStaticPOM(pom);
    }
    
    public void StartInteraction(string interaction)
    {
        Debug.Log(interaction);
        switch (interaction)
        {
            case "Trocknen":
                rea.TriggerInteraction(InteractionTypes.Trocknen);
                break;
            case "AtmungAbhoeren":
                rea.TriggerInteraction(InteractionTypes.AtmungAbhoeren);
                break;
            case "Esmarch":
                rea.TriggerInteraction(InteractionTypes.Esmarch);
                break;
            case "Esmarch_Handgriff":
                rea.TriggerInteraction(InteractionTypes.Esmarch_Handgriff);
                break;
            case "CGriff":
                rea.TriggerInteraction(InteractionTypes.CGriff);
                break;
            case "MundAbsaugen":
                rea.TriggerInteraction(InteractionTypes.MundAbsaugen);
                break;
            case "NaseAbsaugen":
                rea.TriggerInteraction(InteractionTypes.NaseAbsaugen);
                break;
            case "AmbZusammenstecken":
                rea.TriggerInteraction(InteractionTypes.AmbZusammenstecken);
                break;
            case "BeatmungInitial":
                rea.TriggerInteraction(InteractionTypes.BeatmungInitial);
                break;
            case "Beatmung1s":
                rea.TriggerInteraction(InteractionTypes.Beatmung1s);
                break;
            case "HerzdruckMassage":
                rea.TriggerInteraction(InteractionTypes.HerzdruckMassage);
                break;
            case "HerzAbhoeren":
                rea.TriggerInteraction(InteractionTypes.HerzAbhoeren);
                break;
            case "PulsoxymetrieKleben":
                rea.TriggerInteraction(InteractionTypes.PulsoxymetrieKleben);
                break;
            case "FolieAbziehen":
                rea.TriggerInteraction(InteractionTypes.FolieAbziehen);
                break;
            case "MundAbsaugenRea":
                rea.TriggerInteraction(InteractionTypes.MundAbsaugenRea);
                break;
            case "NaseAbsaugenRea":
                rea.TriggerInteraction(InteractionTypes.NaseAbsaugenRea);
                break;
        }
    }
}
