﻿
using UnityEngine;

public class ThreeDToggle : MonoBehaviour
{
    [SerializeField] float rotation_On;
    [SerializeField] float rotation_Off;
    [SerializeField] Vector3 axis;
    [SerializeField] float flipTime = 1f;

    //cache
    float time;
    bool cachevalue;

    Quaternion origRot = Quaternion.identity;
    Quaternion targetRot = Quaternion.identity;

    /// <summary>
    /// Zustand des Toggles setzen. "instant" benutzen, um interpolation zu überspringen
    /// </summary>
    /// <param name="state"></param>
    /// <param name="instant"></param>
    public void Toggle(bool state, bool instant = false)
    {
        if (cachevalue == state &! instant) return;

        if (state) targetRot = Quaternion.AngleAxis(rotation_On, axis);
        else targetRot = Quaternion.AngleAxis(rotation_Off, axis);


        cachevalue = state;
        if (instant)
        {
            transform.localRotation = targetRot;
            return;
        }
        time = 0f;
        origRot = transform.localRotation;
    }

    private void LateUpdate()
    {
        Lerp();
    }
    void Lerp()
    {
        if (time > flipTime) return;
        transform.localRotation = Quaternion.Lerp(origRot, targetRot, time/flipTime);
        time += Time.deltaTime;
    }
}
