﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knob : MonoBehaviour
{
    [SerializeField] float minValue = 0f, maxValue = 1f;
    [SerializeField] float minRotation, maxRotation;
    [SerializeField] Vector3 axis;
    [SerializeField] float lerpTime = 0.1f;

    Quaternion targetRot;
    float cacheValue;

    /// <summary>
    /// Zielwert für die Drehung setzen. "instant" benutzen um die Interpolation zu überspringen
    /// </summary>
    /// <param name="value"></param>
    /// <param name="instant"></param>
    public void SetValue(float value, bool instant = false)
    {
        if (Mathf.Approximately(value, cacheValue) && !instant) return;

        targetRot = Quaternion.AngleAxis(Mathf.Lerp(minRotation,maxRotation,     Mathf.InverseLerp(minValue,maxValue,value))     , axis);
        cacheValue = value;

        if (instant) transform.localRotation = targetRot;
    }

    private void LateUpdate()
    {
        Lerp();
    }
    void Lerp()
    {
        transform.localRotation = Quaternion.Slerp(transform.localRotation, targetRot, lerpTime);
    }
}
