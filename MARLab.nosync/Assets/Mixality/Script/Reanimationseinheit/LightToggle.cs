﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LightToggle : MonoBehaviour
{
    [SerializeField] Color on, off;
    [SerializeField] Image image;
    [SerializeField] TextMeshProUGUI text;

    //cache
    bool cachevalue;
    private void OnEnable()
    {
        image = GetComponent<Image>();
        text = GetComponent<TextMeshProUGUI>();
    }
    public void Toggle(bool state, bool instant = true)
    {
        if (cachevalue == state &! instant) return;

        Color col;
        if (state) col = on;
        else col = off;

        if (image) image.color = col;
        if (text) text.color = col;

        cachevalue = state;
    }

}
