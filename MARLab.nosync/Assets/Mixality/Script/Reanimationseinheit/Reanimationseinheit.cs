﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Reanimationseinheit : MonoBehaviour
{
    [SerializeField] [Header("Objektreferenzen")]
    public ThreeDToggle
        toggle_absaugung;
    public ThreeDToggle
        toggle_beatmung,
        toggle_gasversorgung;
    public Knob
        drehknopf_Sauerstoffkonzentration,
        drehknopf_Saugleistung,
        drehknopf_Frequenz,
        drehknopf_Peep,
        drehknopf_Druckbegrenzung,
        drehknopf_flow1,
        drehknopf_flow2,
        nadel_absaugung,
        nadel_beatmung,
        nadel_gasversorgung1,
        nadel_gasversorgung2;
    [SerializeField] TextMeshProUGUI
        text_timer,
        text_temperature,
        text_3;
    public LightToggle
        led_bell,
        led_vorwaermen,
        led_manuell,
        led_haut,
        led_37,
        led_alarme,
        led_C,
        led_F;
    public LightButton
        but_netz,
        but_1,
        but_2;

    [SerializeField]
    [Header("Animation Layer")]
    int layer_front = 1;
    [SerializeField]
    int layer_left = 2,
        layer_right = 3;
    MeshRenderer rea_renderer;

    [SerializeField] [Header("Startwerte")]
    bool tg_absaugung;
    [SerializeField]
    bool beatmung,
        gasversorgung;
    
    [Space]
    [SerializeField]
    float sauerstoffkonzentration = 21f,
        saugleistung = 0f,
        frequenz = 20f,
        peep = 0.5f,
        druckbegrenzung = 0.5f,
        flow1 = 10f,
        flow2 = 0f,
        nd_absaugung = 0f,
        nd_beatmung = 0f,
        nd_gas1 = 0f,
        nd_gas2 = 0f;
    [Space]
    [SerializeField] string timer = "",
        temp = "",
        other = "";
    [SerializeField]
    bool
        bell,
        vorwaermen,
        manuell,
        haut,
        led_over37,
        alarme,
        C,
        F,
        netz,
        b1,
        b2,
        fallschutz_front,
        fallschutz_left,
        fallschutz_right;
    
    [SerializeField] bool lampe = false;

    Animator anim;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        rea_renderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        toggle_absaugung.Toggle(tg_absaugung, true);
        toggle_beatmung.Toggle(beatmung, true);
        toggle_gasversorgung.Toggle(gasversorgung, true);

        drehknopf_Sauerstoffkonzentration.SetValue(sauerstoffkonzentration, true);
        drehknopf_Druckbegrenzung.SetValue(druckbegrenzung, true);
        drehknopf_flow1.SetValue(flow1, true);
        drehknopf_flow2.SetValue(flow2, true);
        drehknopf_Frequenz.SetValue(frequenz, true);
        drehknopf_Peep.SetValue(peep, true);
        drehknopf_Saugleistung.SetValue(saugleistung, true);

        nadel_absaugung.SetValue(nd_absaugung, true);
        nadel_beatmung.SetValue(nd_beatmung, true);
        nadel_gasversorgung1.SetValue(nd_gas1, true);
        nadel_gasversorgung2.SetValue(nd_gas2, true);

        text_timer.text = timer;
        text_temperature.text = temp;
        text_3.text = other;

        led_bell.Toggle(bell);
        led_vorwaermen.Toggle(vorwaermen);
        led_manuell.Toggle(manuell);
        led_haut.Toggle(haut);
        led_37.Toggle(led_over37);
        led_alarme.Toggle(alarme);
        led_C.Toggle(C);
        led_F.Toggle(F);

        but_netz.Toggle(netz,true);
        but_1.Toggle(b1,true);
        but_2.Toggle(b2,true);

        SetLight(lampe, true);
        SetSidebar(fallschutz_front,fallschutz_left, fallschutz_right,true);
    }

    /// <summary>
    /// Eintrag für das Timer Feld im Panel. Format sollte 00:00 entsprechen.
    /// </summary>
    /// <param name="text"></param>
    public void SetTimerText(string text)
    {
        text_timer.text = text;
    }
    /// <summary>
    /// Eintrag für das Temparaturfeld im Panel. Nummern sollten für die Lesbarkeit mit einem Sonderzeichen oder einem Leerzeichen getrennt sein
    /// </summary>
    /// <param name="text"></param>
    public void SetTempText(string text)
    {
        text_temperature.text = text;
    }

    /// <summary>
    /// Eintrag für die unterste LED Zeile im Panel. Nummern sollten für die Lesbarkeit  mit einem Sonderzeichen oder einem Leerzeichen getrennt sein
    /// </summary>
    /// <param name="text"></param>
    public void SetOtherText(string text)
    {
        text_3.text = text;
    }

    /// <summary>
    /// Kontrolliert die Fallschutz Barrieren. Für die einzelnen Objekte gilt "true" = ausgefahren.
    /// </summary>
    /// <param name="front"></param>
    /// <param name="left"></param>
    /// <param name="right"></param>
    public void SetSidebar(bool front, bool left, bool right, bool force = false)
    {
        anim.SetBool("Front", front);
        anim.SetBool("Left", left);
        anim.SetBool("Right", right);

        if (force)
        {
            ForceSidebar(front, layer_front);
            ForceSidebar(left, layer_left);
            ForceSidebar(right, layer_right);
        }
    }
    void ForceSidebar(bool state, int layer)
    {
        string name = "Runter";
        if (state) name = "Hoch";
        anim.Play(name, layer, 1f);
    }
 
    /// <summary>
    /// Kontrolliert die Lampe über die Emission Rate des Materials
    /// </summary>
    /// <param name="on"></param>
    /// <param name="force"></param>
    public void SetLight(bool on, bool force = false)
    {
        if (on == lampe &!force) return;
        if(on) rea_renderer.materials[1].EnableKeyword("_EMISSION");
        else rea_renderer.materials[1].DisableKeyword("_EMISSION");
        lampe = on;
    }
}