﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LightButton : MonoBehaviour
{
    [SerializeField] Material on, off;
    MeshRenderer render;

    //cache
    bool cachevalue;

    private void OnEnable()
    {
        render = GetComponent<MeshRenderer>();
    }
    /// <summary>
    /// Zustand des Knopfes setzen. "force" benutzen, um aktuellen Zustand zu ignorieren.
    /// </summary>
    /// <param name="state"></param>
    /// <param name="force"></param>
    public void Toggle(bool state, bool force = true)
    {
        if (cachevalue == state &! force) return;

        Material mat;
        if (state) mat = on;
        else mat = off;

        if (render)
        {
            render.material = mat;
        }

        cachevalue = state;
    }

}
