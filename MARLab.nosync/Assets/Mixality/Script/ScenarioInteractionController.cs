﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioInteractionController : MonoBehaviour
{
    ReaInteraction[] interactions;
    void Awake()
    {
        interactions = GetComponentsInChildren<ReaInteraction>();
    }

    public void TriggerInteraction(InteractionTypes interaction)
    {
        foreach (var inter in interactions)
        {
            inter.PlayInteraction(interaction);
        }
    }
}
