﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class Patientenbett : MonoBehaviour
{
    Animator anim;


    [SerializeField]
    [Header("Motor")]
    float maxSpeed = 2f;
    [SerializeField] float smoothTime = 0.7f;

    [SerializeField]
    [Header("Zielwerte Höhe")]
    [Range(0f, 1f)] public float hoehe = 1f;
    [SerializeField] [Range(-1f, 1f)] public float balance = 0f;

    [SerializeField]
    [Header("Zielwerte Lehne")]
    [Range(0f, 1f)] public float rueckenlehne = 0f;
    [SerializeField] [Range(0f, 1f)] public float beinlehne = 0f;

    [SerializeField] [Header("Zielwerte Fallschutz")]
    bool fallschutz_kopfLinks;
    [SerializeField] bool fallschutz_kopfRechts, fallschutz_fussLinks, fallschutz_fussRechts;

    [SerializeField] Animator linkedAvatar;
    [SerializeField] Transform node_patient;

    //Must fit with the animator
    int protectionLayer_headLeft = 3, protectionLayer_footLeft = 4, protectionLayer_headRight = 5, protectionLayer_footRight = 6;

    //cache
    float c_head, c_foot, ref_head, ref_foot,
        c_back, c_knee, ref_back, ref_knee;

    private void OnEnable()
    {
        anim = GetComponent<Animator>();
        if(anim == null)
        {
            enabled = false;
            return;
        }
        ForceHeight(hoehe, balance);
        ForceLeanPose(rueckenlehne, beinlehne);
        ToggleProtection(fallschutz_kopfLinks, fallschutz_fussLinks, fallschutz_kopfRechts, fallschutz_fussRechts, true);

        if (linkedAvatar == null) return;
        linkedAvatar.gameObject.transform.SetParent(node_patient);
        linkedAvatar.gameObject.transform.localPosition = Vector3.zero;
        linkedAvatar.gameObject.transform.localRotation = Quaternion.identity;
        anim.SetFloat("Decke", 1f);
        
    }

    private void Update()
    {
        LerpHeight();
        LerpLean();

        //Testing
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ToggleProtection(true, false, true, false, false);
        }
    }

    /// <summary>
    /// Smoothly sets the height of the bed
    /// </summary>
    void LerpHeight()
    {
        float height_head = hoehe * (1 + Mathf.Clamp(balance, -1f, 0f));
        float height_foot = hoehe * (1 - Mathf.Clamp(balance, 0f, 1f));

        c_head = Mathf.SmoothDamp(c_head, height_head, ref ref_head, smoothTime, maxSpeed);
        c_foot = Mathf.SmoothDamp(c_foot, height_foot, ref ref_foot, smoothTime, maxSpeed);

        anim.SetFloat("Liegeflaeche_Kopfseite", c_head);
        anim.SetFloat("Liegeflaeche_Fussseite", c_foot);
    }

    /// <summary>
    /// Smoothly sets the lean setting
    /// </summary>
    public void LerpLean()
    {
        c_back = Mathf.SmoothDamp(c_back, rueckenlehne, ref ref_back, smoothTime, maxSpeed);
        c_knee = Mathf.SmoothDamp(c_knee, beinlehne, ref ref_knee, smoothTime, maxSpeed);

        anim.SetFloat("Kopflehne", c_back);
        anim.SetFloat("Fusslehne", c_knee);

        if (linkedAvatar == null) return;
        linkedAvatar.SetFloat("Rueckenlehne", c_back);
        linkedAvatar.SetFloat("Fusslehne", c_knee);
    }

    /// <summary>
    /// Force the height of the bed to a point instantly
    /// </summary>
    /// <param name="p_height"></param>
    /// <param name="p_balance"></param>
    public void ForceHeight(float p_height, float p_balance)
    {
        hoehe = p_height;
        balance = p_balance;

        float height_head = hoehe * (1 + Mathf.Clamp(balance, -1f, 0f));
        float height_foot = hoehe * (1 - Mathf.Clamp(balance, 0f, 1f));

        c_head = height_head;
        c_foot = height_foot;

        anim.SetFloat("Liegeflaeche_Kopfseite", c_head);
        anim.SetFloat("Liegeflaeche_Fussseite", c_foot);
    }

    /// <summary>
    /// Force the lean pose of the bed instantly
    /// </summary>
    /// <param name="p_back"></param>
    /// <param name="p_knee"></param>
    public void ForceLeanPose(float p_back, float p_knee)
    {
        rueckenlehne = p_back;
        beinlehne = p_knee;
        c_back = p_back;
        c_knee = p_knee;

        anim.SetFloat("Kopflehne", c_back);
        anim.SetFloat("Fusslehne", c_knee);

        if (linkedAvatar == null) return;
        linkedAvatar.SetFloat("Rueckenlehne", c_back);
        linkedAvatar.SetFloat("Fusslehne", c_knee);
    }

    /// <summary>
    /// Access the separate protection Panels, use force if only the final state should be displayed
    /// </summary>
    /// <param name="id"></param>
    /// <param name="up"></param>
    /// <param name="forceState"></param>
    public void SetProtection(int id, bool up, bool forceState = false)
    {
        switch (id)
        {
            case 3:
                if (up == fallschutz_kopfLinks & !forceState) return;
                fallschutz_kopfLinks = up;
                break;
            case 4:
                if (up == fallschutz_fussLinks & !forceState) return;
                fallschutz_fussLinks = up;
                break;
            case 5:
                if (up == fallschutz_kopfRechts & !forceState) return;
                fallschutz_kopfRechts = up;
                break;
            case 6:
                if (up == fallschutz_fussRechts & !forceState) return;
                fallschutz_fussRechts = up;
                break;
            default:
                break;
        }

        float time = 0f;
        if (forceState) time = 0.999f;
        if (up) anim.Play("Hoch", id, time);
        else anim.Play("Runter", id, time);
    }
    /// <summary>
    /// Shorthand to access all Protection panels, use force if only the final state should be displayed
    /// </summary>
    /// <param name="hL"></param>
    /// <param name="fL"></param>
    /// <param name="hR"></param>
    /// <param name="fR"></param>
    /// <param name="forceState"></param>
    public void ToggleProtection(bool hL, bool fL, bool hR, bool fR, bool forceState = false)
    {
            SetProtection(protectionLayer_headLeft, hL, forceState);

            SetProtection(protectionLayer_headRight, hR, forceState);

            SetProtection(protectionLayer_footLeft, fL, forceState);

            SetProtection(protectionLayer_footRight, fR, forceState);
    }
}
