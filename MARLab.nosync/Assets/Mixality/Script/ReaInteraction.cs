﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaInteraction : MonoBehaviour
{
    public float blendingTime = 0f;
    public int animLayer = 0;

    Animator anim;
    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void PlayInteraction(InteractionTypes interaction) {
        string state = interaction.ToString() ;
        anim.CrossFadeInFixedTime(state, blendingTime, animLayer);
    }
}
public enum InteractionTypes
{
    Null = 0,
    Esmarch_Handgriff = 1,
    CGriff = 2,
    Esmarch = 3,
    Trocknen = 4,
    MundAbsaugen = 5,
    NaseAbsaugen = 6,
    AmbZusammenstecken = 7,
    AmbAufsetzen = 8,
    BeatmungInitial = 9,
    Beatmung1s = 10,
    HerzdruckMassage = 11,
    AtmungAbhoeren = 12,
    HerzAbhoeren = 13,
    PulsoxymetrieKleben = 14,
    FolieAbziehen = 15,
    MundAbsaugenRea = 16,
    NaseAbsaugenRea = 17
}
