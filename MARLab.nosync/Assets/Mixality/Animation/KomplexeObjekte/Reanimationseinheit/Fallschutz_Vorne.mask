%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Fallschutz_Vorne
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Drehknopf_01_Sauerstoff
    m_Weight: 0
  - m_Path: Drehknopf_02_Saugleistung
    m_Weight: 0
  - m_Path: Drehknopf_03_Frequenz
    m_Weight: 0
  - m_Path: Drehknopf_04_Peep
    m_Weight: 0
  - m_Path: Drehknopf_05_Druckbegrenzung
    m_Weight: 0
  - m_Path: Drehknopf_06_Flow1
    m_Weight: 0
  - m_Path: Drehknopf_07_Flow2
    m_Weight: 0
  - m_Path: DruckKnopf
    m_Weight: 0
  - m_Path: DruckKnopf.001
    m_Weight: 0
  - m_Path: Fallschutz_Hinten
    m_Weight: 0
  - m_Path: Fallschutz_L
    m_Weight: 0
  - m_Path: Fallschutz_R
    m_Weight: 0
  - m_Path: Fallschutz_Vorne
    m_Weight: 1
  - m_Path: Lamoe_Glas
    m_Weight: 0
  - m_Path: Nadel_01_Absaugung
    m_Weight: 0
  - m_Path: Nadel_02_Beatmung
    m_Weight: 0
  - m_Path: Nadel_03_Gas1
    m_Weight: 0
  - m_Path: Nadel_04_Gas2
    m_Weight: 0
  - m_Path: Netzschalter_Knopf
    m_Weight: 0
  - m_Path: Rad
    m_Weight: 0
  - m_Path: Rad.001
    m_Weight: 0
  - m_Path: Rad.002
    m_Weight: 0
  - m_Path: Rad.003
    m_Weight: 0
  - m_Path: Toggle_Schalter
    m_Weight: 0
  - m_Path: Toggle_Schalter.002
    m_Weight: 0
  - m_Path: Toggle_Schalter.003
    m_Weight: 0
