﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteWasteInteraction : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject vinegar;
    public GameObject vinegarWaste;

    // Start is called before the first frame update
    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    // Update is called once per frame
    void Update()
    {
        if (Statemachine.Instance.stoptTitrating)
        {
            gameObject.SetActive(true);
        }
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "ErlenStand":
                vinegar.SetActive(false);
                vinegarWaste.SetActive(true);
                Statemachine.Instance.thrownAway[3] = true;
                Statemachine.Instance.doneCleanUp();
                break;
        }
    }
}
