﻿using System;
using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;

public class ARVersionController : MonoBehaviour
{
    int number;
    bool isParsable;

    // Start is called before the first frame update
    void Start()
    {
        isParsable = Int32.TryParse(ApplicationSettings.idCode, out number);
    }

    // Update is called once per frame
    void Update()
    {
        if (isParsable)
        {
            if (number % 2 == 0)
            {
                gameObject.SetActive(false);
            }
        }
        
    }
}
