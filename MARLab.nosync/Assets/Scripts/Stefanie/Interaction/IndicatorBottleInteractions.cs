﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorBottleInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject PhenolPrefab;
    public GameObject MethylPrefab;
    public GameObject TopDelete;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void Interact()
    {
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "PhenoDropper":
                putPhenolTop();
                break;
            case "MethylDropper":
                putMethylTop();
                break;
        }
    }

    public void putPhenolTop()
    {
        if (TopDelete.activeInHierarchy)
        {
            TopDelete.SetActive(false);
        } else
        {
            GameObject topObject = GameObject.Find("PhenolDropper(Clone)");
            topObject.SetActive(false);
        }

        GameObject top = Instantiate(PhenolPrefab, gameObject.transform);
        gameObject.GetComponent<IndicatorBottleInteractions>().TopDelete = top;
    }

    public void putMethylTop()
    {
        if (TopDelete.activeInHierarchy)
        {
            TopDelete.SetActive(false);
        }
        else
        {
            GameObject topObject = GameObject.Find("MethylDropper(Clone)");
            topObject.SetActive(false);
        }

        GameObject top = Instantiate(MethylPrefab, gameObject.transform);
        gameObject.GetComponent<IndicatorBottleInteractions>().TopDelete = top;
    }
}
