﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiquidController : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject Vinegar1;
    public GameObject Wash;
    public GameObject Flask;
    public GameObject Analyte;

    // Start is called before the first frame update
    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
        Vinegar1.SetActive(true);
        Wash.SetActive(true);
        Flask.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
