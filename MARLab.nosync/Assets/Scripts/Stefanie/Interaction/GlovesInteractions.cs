﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlovesInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject speechBubble;


    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
        //Liquid.SetActive(true);
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
        }
    }

    public void Interact()
    {
        StartCoroutine(BubbleCoroutine());
    }

    IEnumerator BubbleCoroutine()
    {
        speechBubble.SetActive(true);

        yield return new WaitForSeconds(3.0f);

        speechBubble.SetActive(false);
    }
}
