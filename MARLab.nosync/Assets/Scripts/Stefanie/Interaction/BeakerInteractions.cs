﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeakerInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject LiquidBeaker;
    public GameObject PouringPrefab;
    public GameObject titrant;
    public GameObject titrantPouringBack;

    private bool enter = false;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;

            case "Titrant":
                if (enter == false)
                {
                    StartCoroutine(AnimationCoroutine());
                }
                NameDict.InstanceNameDict.putString("BeakerEmpty", "Beaker with titrant");
                break;
        }
    }

    IEnumerator AnimationCoroutine()
    {
        enter = true;
        
        Vector3 pos = gameObject.transform.position + new Vector3((float)0.274, (float)0.1342934, 0);
        Vector3 rot = new Vector3(0, 0, (float)79.29501);
        GameObject prefab = Instantiate(PouringPrefab, pos, Quaternion.Euler(rot));

        titrant.SetActive(false);

        yield return new WaitForSeconds(4.0f);
        prefab.SetActive(false);
        Vector3 startposition = gameObject.transform.position + new Vector3((float)-0.099, (float)0.002, (float)-0.0821);
        prefab.transform.position = startposition;
        prefab.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        prefab.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        titrantPouringBack.SetActive(true);
        gameObject.GetComponent<Interaction.ObjectInteractionHandler>().isGrabbable = true;
        enter = false;

    }
}
