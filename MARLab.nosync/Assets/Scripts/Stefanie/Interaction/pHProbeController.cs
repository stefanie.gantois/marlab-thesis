﻿using System;
using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;

public class pHProbeController : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject Title;
    public GameObject Number;
    public GameObject Callibrated;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateVisibility();
        UpdateNumber();
    }

    private void UpdateNumber()
    {
        Number.GetComponent<TextMesh>().text = pHCalculator.calculator.getPH().ToString("n2");
    }

    private void UpdateVisibility()
    {
        if (Statemachine.Instance.titrating)
        {
            Title.SetActive(true);
            Number.SetActive(true);
            Callibrated.SetActive(false);
        }
        else
        {
            Title.SetActive(false);
            Number.SetActive(false);
            Callibrated.SetActive(true);
        }
    }
}
