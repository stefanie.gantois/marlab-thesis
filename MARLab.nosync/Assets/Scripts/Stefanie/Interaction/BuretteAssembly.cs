﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;

public class BuretteAssembly : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject FunnelDelete;
    public GameObject FunnelAdd;
    public GameObject PouringWash;
    public GameObject PouringTitrant;
    public GameObject Wash;
    public GameObject titrant;
    public GameObject TitrantPouringBack;
    public GameObject system;

    private bool enter = false;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    private void Update()
    {
        if (Statemachine.Instance.stoptTitrating)
        {
            gameObject.GetComponent<ObjectInteractionHandler>().isGrabbable = true;
        } else
        {
            gameObject.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
        }
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;

            case "Funnel":
                FunnelDelete.SetActive(false);
                FunnelAdd.SetActive(true);
                break;

            case "Wash":
                if (enter == false)
                {
                    StartCoroutine(WashCoroutine());
                }
                break;

            case "BeakerEmpty":
                if (enter == false)
                {
                    StartCoroutine(TitrantCoroutine());
                }
                break;
        }
    }

    IEnumerator WashCoroutine()
    {
        enter = true;
        
        Vector3 pos = gameObject.transform.position + new Vector3((float)-0.000799969, (float)0.139, (float)0.2429001);
        Vector3 rot = new Vector3(-75, 0, 0);
        GameObject pouringPrefab = Instantiate(PouringWash, pos, Quaternion.Euler(rot));

        Wash.SetActive(false);

        yield return new WaitForSeconds(3.0f);

        pouringPrefab.SetActive(false);
        Vector3 startposition = gameObject.transform.position + new Vector3((float)0.2066, (float)-0.737, (float)0.094);
        //GameObject back = Instantiate(PouringWash, startposition, Quaternion.Euler(new Vector3(0, 50, 0)));
        pouringPrefab.transform.position = startposition;
        pouringPrefab.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));

        enter = false;

    }

    IEnumerator TitrantCoroutine()
    {
        enter = true;
        
        Vector3 pos = gameObject.transform.position + new Vector3((float)-0.003700003, (float)0.28, (float)-0.1404);
        Vector3 rot = new Vector3(0, 90, 75);
        GameObject pouringPrefab = Instantiate(PouringTitrant, pos, Quaternion.Euler(rot));

        titrant.SetActive(false);

        yield return new WaitForSeconds(7.0f);
        pouringPrefab.SetActive(false);
        Vector3 startposition = gameObject.transform.position + new Vector3((float)-0.06570004, (float)-0.739, (float)-0.2434);
        pouringPrefab.transform.position = startposition;
        pouringPrefab.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        pouringPrefab.transform.GetChild(0).GetChild(3).gameObject.SetActive(false);
        TitrantPouringBack.SetActive(true);
        enter = false;
    }
}
