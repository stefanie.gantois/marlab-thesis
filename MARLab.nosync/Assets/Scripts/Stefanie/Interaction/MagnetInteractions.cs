﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;

    public GameObject Magnet;

    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Erlenmeyer":
                gameObject.SetActive(false);
                Magnet.SetActive(true);
                break;
        }
    }
}
