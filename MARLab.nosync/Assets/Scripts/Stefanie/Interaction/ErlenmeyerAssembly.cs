﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErlenmeyerAssembly : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject Liquid;
    public GameObject magnet;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
        //Liquid.SetActive(true);
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Beaker":
                Liquid.SetActive(true);
                gameObject.GetComponent<Interaction.ObjectInteractionHandler>().isGrabbable = true;
                break;
            case "PhenoDropper":
                addIndicator("phenolphthalein");
                break;
            case "MethylDropper":
                addIndicator("methylorange");
                break;
            case "Magnet":
                magnet.SetActive(true);
                break;
        }
    }

    public void addIndicator(string indicator)
    {
        //Liquid.SetActive(true);
        
        switch (indicator)
        {
            case "phenolphthalein":
                //Liquid.GetComponent<MeshRenderer>().material = phenoWater;
                gameObject.GetComponentInChildren<Indicator.ColorDecider>().indicator = 2;
                pHCalculator.calculator.setInd(2);
        break;
            case "methylorange":
                //Liquid.GetComponent<MeshRenderer>().material = methylWater;
                gameObject.GetComponentInChildren<Indicator.ColorDecider>().indicator = 1;
                pHCalculator.calculator.setInd(1);
        break;
        }
        
    }
}
