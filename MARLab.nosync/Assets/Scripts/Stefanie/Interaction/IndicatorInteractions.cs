﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Interaction;

public class IndicatorInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    private GameObject Bottle;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Phenolphthalein":
                break;
        }
    }
}
