﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitrantInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject top;
    public GameObject Liquid;
    

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
        //Liquid.SetActive(true);
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
        }
    }

    public void Interact()
    {
        top.SetActive(false);
    }

}
