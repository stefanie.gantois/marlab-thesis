﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropperInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject liquid;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Beaker":
                liquid.SetActive(true);
                break;
        }
    }
}
