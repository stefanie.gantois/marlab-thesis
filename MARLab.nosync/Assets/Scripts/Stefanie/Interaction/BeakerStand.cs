﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeakerStand : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public List<GameObject> probesDelete = new List<GameObject>();
    public List<GameObject> probesAdd = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "ProbeTip":
                probesDelete[0].SetActive(false);
                probesDelete[1].SetActive(false);
                probesAdd[0].SetActive(true);
                probesAdd[1].SetActive(true);
                ResultsLibrary.InstanceLib.addResult();
                break;
        }
    }
}
