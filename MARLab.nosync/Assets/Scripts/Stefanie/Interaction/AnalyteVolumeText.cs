﻿using System.Collections;
using System.Collections.Generic;
using LiquidVolumeFX;
using Static;
using UnityEngine;

public class AnalyteVolumeText : MonoBehaviour
{
    private GameObject arSessionOrigin;

    public GameObject LiquidPouring;
    public GameObject Text;

    // Start is called before the first frame update
    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    // Update is called once per frame
    void Update()
    {
        UpdateVisibility();
        UpdateText();
        UpdatePosition();
    }

    private void UpdatePosition()
    {
        if (Statemachine.Instance.titrating)
        {
            LiquidVolume lv = Text.GetComponentInParent<LiquidVolume>();
            Vector3 pos = Text.transform.position;
            pos.y = (float)lv.liquidSurfaceYPosition;
            Text.transform.position = pos;
        }
    }

    private void UpdateText()
    {
        if (Statemachine.Instance.titrating)
        {
            //float vb = (float)LiquidPouring.GetComponent<LiquidLevelPouringSync>().vb;
            float v = (float)(pHCalculator.calculator.getVb() + pHCalculator.calculator.getVa());
            Text.GetComponent<TextMesh>().text = v.ToString();
        }
    }

    private void UpdateVisibility()
    {
        if (Statemachine.Instance.titrating && Statemachine.Instance.ARVersion(ApplicationSettings.idCode))
        {
            Text.SetActive(true);
        }
        else
        {
            Text.SetActive(false);
        }
    }
}
