﻿using System.Collections;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace LiquidVolumeFX
{

    public class LiquidLevelPouringSync : MonoBehaviour
    {

        public float fillSpeed = 0.0002f;
        public float sinkFactor = 0.1f;
        public double vb = 0;
        private double va;
        private double total;
        LiquidVolume lv;
        Rigidbody rb;

        private GlobalAudioController globalAudioController;
        public PlayableSounds DropSound;

        public GameObject Pouring;

        void Start()
        {
            rb = GetComponent<Rigidbody>();
            lv = transform.parent.GetComponent<LiquidVolume>();
            va = pHCalculator.calculator.getVa();
            fillSpeed = (float)(0.03 / 250);
            UpdateColliderPos();
            globalAudioController = GameObject.Find("Sounds").GetComponent<GlobalAudioController>();

        }

        void OnParticleCollision(GameObject other)
        {
            LiquidVolume pouringLiq = Pouring.GetComponentInChildren<LiquidVolume>();
            if (gameObject.layer == LayerMask.NameToLayer("TitrationLevel"))
            {
                vb += 0.03;
                pHCalculator.calculator.setVb(vb);
                total = va + vb;
                if (lv.level < 1f)
                {
                    lv.level += fillSpeed;
                    pouringLiq.level -= Convert.ToSingle(6.5*fillSpeed); //0.00078
                }
            }
            else if (gameObject.layer == LayerMask.NameToLayer("TitrantLevel"))
            {
                if (lv.level < 1f)
                {
                    lv.level += fillSpeed*35;
                    pouringLiq.level -= fillSpeed*80;
                }
            }
            else
            {
                if (lv.level < 1f)
                {
                    lv.level += fillSpeed * 75;
                    pouringLiq.level -= fillSpeed * 50;
                }
            }

            UpdateColliderPos();
            //globalAudioController.playSound(DropSound);

        }

        void UpdateColliderPos()
        {
            Vector3 pos = new Vector3(transform.position.x, lv.liquidSurfaceYPosition - transform.localScale.y * 0.5f - sinkFactor, transform.position.z);
            rb.position = pos;
            //Debug.Log("position is: " + pos);
            if (lv.level >= 1f)
            {
                transform.localRotation = Quaternion.Euler(Random.value * 30 - 15, Random.value * 30 - 15, Random.value * 30 - 15);
            }
            else
            {
                transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
        }


    }
}
