﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandAssembly : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject ErlenmeyerDelete;
    public GameObject ErlenmeyerAdd;
    public GameObject BuretteDelete;
    public GameObject BuretteAdd;
    public GameObject FunnelDelete;
    public GameObject FunnelAdd;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Erlenmeyer":
                ErlenmeyerAdd.transform.parent = gameObject.transform;
                ErlenmeyerDelete.SetActive(false);
                int indicator = ErlenmeyerDelete.GetComponentInChildren<Indicator.ColorDecider>().indicator;
                ErlenmeyerAdd.GetComponentInChildren<Indicator.ColorDecider>().indicator = indicator;
                ErlenmeyerAdd.SetActive(true);
                break;

            case "Burette":
                //BuretteAdd.transform.parent = gameObject.transform;
                BuretteDelete.SetActive(false);
                BuretteAdd.SetActive(true);
                break;

            case "Funnel":
                //FlunnelDelete.transform.parent = gameObject.transform;
                FunnelDelete.SetActive(false);
                FunnelAdd.SetActive(true);
                break;
        }
    }
}
