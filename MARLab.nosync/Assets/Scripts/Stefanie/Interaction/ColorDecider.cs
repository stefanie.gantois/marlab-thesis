﻿using System.Collections;
using System.Collections.Generic;
using LiquidVolumeFX;
using UnityEngine;

namespace Indicator
{
    public class ColorDecider : MonoBehaviour
    {

        public int indicator = 0; //0 is geen, 1 is methyl orange, 2 is phenophtalein
        private int oldInd = 0;
        public float pH = (float)2.4;
        private float pHold = (float)2.4;

        // Start is called before the first frame update
        void Start()
        {
            pH = pHCalculator.calculator.getPH();
        }

        // Update is called once per frame
        void Update()
        {
            pH = pHCalculator.calculator.getPH();

            switch (indicator)
            {
                case 0:
                    RenderNone();
                    break;
                case 1:
                    RenderMethyl();
                    break;
                case 2:
                    RenderPhenolph();
                    break;
            }
        }

        void RenderNone()
        {
            //gewoon de standaard vanop scherm
        }

        void RenderMethyl()
        {
            float color = 0;

            if (pH > 5)
            {
                color = (float)(200.0 / 255);
            }
            else if (pH >= 3)
            {
                color = (float)((pH - 3) / 2.0);
            }
            if (pH != pHold || indicator != oldInd)
            {
                LiquidVolume lv = gameObject.GetComponent<LiquidVolume>();
                lv.liquidColor1 = new Color(1, color, 0, 1);
                lv.liquidColor2 = new Color(1, 1, 0, 1);
                //Debug.Log("ph is " + pH + ", and ind " + indicator + ", color " + gameObject.GetComponent<LiquidVolume>().liquidColor1);
                pHold = pH;
                oldInd = indicator;
            }
            
        }

        void RenderPhenolph()
        {
            float color = 0;

            if (pH > 10)
            {
                color = 1;
            }
            else if (pH >= 8)
            {
                color = (float)((pH - 8) / 2.0);
            }
            if (pH != pHold || indicator != oldInd)
            {

                LiquidVolume lv = gameObject.GetComponent<LiquidVolume>();
                lv.liquidColor1 = new Color(1, (float)(50.0 / 255), 1, color);
                lv.liquidColor2 = new Color(1, 1, 1, (float)(75.0 / 255));
                //Debug.Log("ph is " + pH + ", and ind " + indicator + ", color " + gameObject.GetComponent<LiquidVolume>().liquidColor1);
                pHold = pH;
                oldInd = indicator;

            }
        }

    }
}
