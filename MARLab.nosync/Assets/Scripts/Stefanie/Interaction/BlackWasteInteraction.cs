﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackWasteInteraction : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject emptyTitrant;
    public GameObject emptyBeaker;
    public GameObject burette;
    public GameObject emptyBurette;

    public GameObject vinegar;
    public GameObject vinegarWaste;

    // Start is called before the first frame update
    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    // Update is called once per frame
    void Update()
    {
        if (Statemachine.Instance.stoptTitrating)
        {
            gameObject.SetActive(true);
        }
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "TitrantPouring":
                GameObject pouringTit = GameObject.Find("TitrantBack");
                pouringTit.SetActive(false);
                Destroy(pouringTit);
                emptyTitrant.SetActive(true);
                Statemachine.Instance.thrownAway[0] = true;
                Statemachine.Instance.doneCleanUp();
                break;
            case "BeakerFilled":
                GameObject beakerTit = GameObject.Find("BeakerBack");
                beakerTit.SetActive(false);
                Destroy(beakerTit);
                emptyBeaker.SetActive(true);
                Statemachine.Instance.thrownAway[1] = true;
                Statemachine.Instance.doneCleanUp();
                break;
            case "BuretteStand":
                burette.SetActive(false);
                emptyBurette.SetActive(true);
                Statemachine.Instance.thrownAway[2] = true;
                Statemachine.Instance.doneCleanUp();
                break;
            case "ErlenStand":
                vinegar.SetActive(false);
                vinegarWaste.SetActive(true);
                Statemachine.Instance.thrownAway[3] = true;
                Statemachine.Instance.doneCleanUp();
                break;
        }
    }
}
