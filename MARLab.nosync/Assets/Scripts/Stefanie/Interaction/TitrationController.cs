﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitrationController : MonoBehaviour
    
{
    public GameObject Erlenmeyer;
    public GameObject Fluid;
    public GameObject pouringSystem;
    public GameObject blackWaste;
    public GameObject whiteWaste;

    void Start()
    {
        pouringSystem.SetActive(false);
    }

    void Update()
    {
        if (Statemachine.Instance.stoptTitrating && !Statemachine.Instance.questioning && Statemachine.Instance.stateNumber >= 95)
        {
            blackWaste.SetActive(true);
            whiteWaste.SetActive(true);
        }
    }

    public void CombineWith(string objectName)
    {
        
    }

    public void Interact()
    {
        
    }

    public void ManipulateErlenmeyerFluid(int milliliters)
    {
        Vector3 fluidScale = Fluid.transform.localScale;
        pouringSystem.SetActive(true);
        var em = pouringSystem.GetComponent<ParticleSystem>().emission;
        switch (milliliters)
        {
            case 0:
                fluidScale = new Vector3(fluidScale.x, 1, fluidScale.z);
                em.rateOverTime = 0;
                ResultsLibrary.InstanceLib.addResult();
                break;
            case 1:
                fluidScale = new Vector3(fluidScale.x, (float)1.1, fluidScale.z);
                em.rateOverTime = 10; 
                break;
            case 2:
                fluidScale = new Vector3(fluidScale.x, (float)1.2, fluidScale.z);
                em.rateOverTime = 25;
                break;
            case 3:
                fluidScale = new Vector3(fluidScale.x, (float)1.3, fluidScale.z);
                em.rateOverTime = 40;
                break;
            case 4:
                fluidScale = new Vector3(fluidScale.x, (float)1.4, fluidScale.z);
                em.rateOverTime = 60;
                break;
            case 5:
                fluidScale = new Vector3(fluidScale.x, (float)1.5, fluidScale.z);
                em.rateOverTime = 80;
                break;
        }

        Fluid.transform.localScale = fluidScale;
    }
}
