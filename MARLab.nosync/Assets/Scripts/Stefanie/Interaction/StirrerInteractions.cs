﻿using System.Collections;
using System.Collections.Generic;
using LiquidVolumeFX;
using UnityEngine;

public class StirrerInteractions : MonoBehaviour
{
    private GameObject arSessionOrigin;

    public GameObject ErlenmeyerDelete;
    public GameObject ErlenmeyerAdd;

    void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void Interact()
    {
        LiquidVolume lv = ErlenmeyerAdd.transform.GetChild(0).GetChild(2).gameObject.GetComponent<LiquidVolume>();
        lv.turbulence1 = (float)0.5;
        lv.turbulence2 = (float)0.5;
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Erlenmeyer":
                ErlenmeyerAdd.transform.parent = gameObject.transform;
                ErlenmeyerDelete.SetActive(false);
                int indicator = ErlenmeyerDelete.GetComponentInChildren<Indicator.ColorDecider>().indicator;
                ErlenmeyerAdd.GetComponentInChildren<Indicator.ColorDecider>().indicator = indicator;
                ErlenmeyerAdd.SetActive(true);
                break;
        }
    }
}
