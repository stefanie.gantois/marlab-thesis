﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pHCalculator 
{
    private double pH = (float)2.4;
    private double Va = 10; //ml
    private double Vb = 0; //ml
    private int indicator = 0; //1 is methyl orange, 2 is phenol
    private Color color = new Color(0, 0, 0);
    Dictionary<double, double> conversion = new Dictionary<double, double>();

    private double ka = 1.74 * Math.Pow(10, -5);

    public static pHCalculator calculator = new pHCalculator();

    private pHCalculator()
    {
        conversion.Add(0.00, 2.40);
        conversion.Add(0.06, 2.50);
        conversion.Add(0.09, 2.55);
        conversion.Add(0.12, 2.60);
        conversion.Add(0.15, 2.70);
        conversion.Add(0.18, 2.75);
        conversion.Add(0.21, 2.80);
        conversion.Add(0.24, 2.85);
        conversion.Add(0.27, 2.90);
        conversion.Add(0.33, 2.95);
        conversion.Add(0.36, 3.00);
        conversion.Add(0.42, 3.05);
        conversion.Add(0.45, 3.10);
        conversion.Add(0.51, 3.15);
        conversion.Add(0.57, 3.20);
        conversion.Add(0.66, 3.25);
        conversion.Add(0.72, 3.30);
        conversion.Add(0.81, 3.35);
        conversion.Add(0.90, 3.40);
        conversion.Add(1.02, 3.45);
        conversion.Add(1.14, 3.50);
        conversion.Add(1.26, 3.55);
        conversion.Add(1.41, 3.60);
        conversion.Add(1.56, 3.65);
        conversion.Add(1.74, 3.70);
        conversion.Add(1.95, 3.75);
        conversion.Add(2.16, 3.80);
        conversion.Add(2.37, 3.85);
        conversion.Add(2.64, 3.90);
        conversion.Add(2.91, 3.95);
        conversion.Add(3.21, 4.00);
        conversion.Add(3.54, 4.05);
        conversion.Add(3.90, 4.10);
        conversion.Add(4.29, 4.15);
        conversion.Add(4.68, 4.20);
        conversion.Add(5.13, 4.25);
        conversion.Add(5.58, 4.30);
        conversion.Add(6.06, 4.35);
        conversion.Add(6.60, 4.40);
        conversion.Add(7.14, 4.45);
        conversion.Add(7.68, 4.50);
        conversion.Add(8.28, 4.55);
        conversion.Add(8.85, 4.60);
        conversion.Add(9.48, 4.65);
        conversion.Add(10.08, 4.70);
        conversion.Add(10.71, 4.75);
        conversion.Add(11.34, 4.80);
        conversion.Add(11.94, 4.85);
        conversion.Add(12.57, 4.90);
        conversion.Add(13.17, 4.95);
        conversion.Add(13.74, 5.00);
        conversion.Add(14.31, 5.05);
        conversion.Add(14.88, 5.10);
        conversion.Add(15.39, 5.15);
        conversion.Add(15.90, 5.20);
        conversion.Add(16.35, 5.25);
        conversion.Add(16.80, 5.30);
        conversion.Add(17.22, 5.35);
        conversion.Add(17.61, 5.40);
        conversion.Add(18.00, 5.45);
        conversion.Add(18.33, 5.50);
        conversion.Add(18.63, 5.55);
        conversion.Add(18.93, 5.60);
        conversion.Add(19.20, 5.65);
        conversion.Add(19.44, 5.70);
        conversion.Add(19.65, 5.75);
        conversion.Add(19.86, 5.80);
        conversion.Add(20.04, 5.85);
        conversion.Add(20.19, 5.90);
        conversion.Add(20.34, 5.95);
        conversion.Add(20.49, 6.00);
        conversion.Add(20.61, 6.05);
        conversion.Add(20.70, 6.10);
        conversion.Add(20.82, 6.15);
        conversion.Add(20.91, 6.20);
        conversion.Add(20.97, 6.25);
        conversion.Add(21.06, 6.30);
        conversion.Add(21.12, 6.35);
        conversion.Add(21.18, 6.40);
        conversion.Add(21.21, 6.45);
        conversion.Add(21.27, 6.50);
        conversion.Add(21.30, 6.55);
        conversion.Add(21.36, 6.60);
        conversion.Add(21.39, 6.65);
        conversion.Add(21.42, 6.70);
        conversion.Add(21.45, 6.80);
        conversion.Add(21.48, 6.85);
        conversion.Add(21.51, 6.95);
        conversion.Add(21.54, 7.05);
        conversion.Add(21.57, 7.20);
        conversion.Add(21.60, 7.45);
        conversion.Add(21.63, 8.05);
        conversion.Add(21.66, 10.50);
        conversion.Add(21.69, 10.85);
        conversion.Add(21.72, 11.10);
        conversion.Add(21.75, 11.20);
        conversion.Add(21.78, 11.30);
        conversion.Add(21.81, 11.40);
        conversion.Add(21.84, 11.45);
        conversion.Add(21.87, 11.55);
        conversion.Add(21.90, 11.60);
        conversion.Add(21.93, 11.65);
        conversion.Add(21.99, 11.70);
        conversion.Add(22.02, 11.75);
        conversion.Add(22.05, 11.80);
        conversion.Add(22.11, 11.85);
        conversion.Add(22.17, 11.90);
        conversion.Add(22.23, 11.95);
        conversion.Add(22.29, 12.00);
        conversion.Add(22.38, 12.05);
        conversion.Add(22.47, 12.10);
        conversion.Add(22.56, 12.15);
        conversion.Add(22.68, 12.20);
        conversion.Add(22.83, 12.25);
        conversion.Add(22.98, 12.30);
        conversion.Add(23.13, 12.35);
        conversion.Add(23.34, 12.40);
        conversion.Add(23.55, 12.45);
        conversion.Add(23.79, 12.50);
        conversion.Add(24.06, 12.55);
        conversion.Add(24.39, 12.60);
        conversion.Add(24.75, 12.65);
        conversion.Add(25.17, 12.70);
        conversion.Add(25.65, 12.75);
        conversion.Add(26.22, 12.80);
        conversion.Add(26.88, 12.85);
        conversion.Add(27.63, 12.90);
        conversion.Add(28.53, 12.95);
        conversion.Add(29.55, 13.00);
        conversion.Add(30.81, 13.05);
        conversion.Add(32.31, 13.10);
        conversion.Add(34.11, 13.15);
        conversion.Add(36.33, 13.20);
        conversion.Add(39.12, 13.25);
        conversion.Add(42.66, 13.30);
        conversion.Add(47.31, 13.35);
        conversion.Add(53.61, 13.40);
        conversion.Add(62.52, 13.45);
        conversion.Add(76.11, 13.50);
        conversion.Add(98.97, 13.55);
        conversion.Add(145.29, 13.60);
        conversion.Add(286.74, 13.65);


        /*
        conversion.Add(0.20,2.8);
        conversion.Add(0.25,2.8);
        conversion.Add(0.30,2.9);
        conversion.Add(0.35,3.0);
        conversion.Add(0.45,3.1);
        conversion.Add(0.60,3.2);
        conversion.Add(0.75,3.3);
        conversion.Add(0.90,3.4);
        conversion.Add(1.15,3.5);
        conversion.Add(1.40,3.6);
        conversion.Add(1.75,3.7);
        conversion.Add(2.15,3.8);
        conversion.Add(2.65,3.9);
        conversion.Add(3.20,4.0);
        conversion.Add(3.90,4.1);
        conversion.Add(4.70,4.2);
        conversion.Add(5.60,4.3);
        conversion.Add(6.60,4.4);
        conversion.Add(7.70,4.5);
        conversion.Add(8.85,4.6);
        conversion.Add(10.10,4.7);
        conversion.Add(11.35,4.8);
        conversion.Add(12.55,4.9);
        conversion.Add(13.75,5.0);
        conversion.Add(14.85,5.1);
        conversion.Add(15.90,5.2);
        conversion.Add(16.80,5.3);
        conversion.Add(17.65,5.4);
        conversion.Add(18.35,5.5);
        conversion.Add(18.95,5.6);
        conversion.Add(19.45,5.7);
        conversion.Add(19.85,5.8);
        conversion.Add(20.20,5.9);
        conversion.Add(20.50,6.0);
        conversion.Add(20.70,6.10);
        conversion.Add(20.90,6.2);
        conversion.Add(21.05,6.3);
        conversion.Add(21.20,6.4);
        conversion.Add(21.25,6.5);
        conversion.Add(21.35,6.6);
        conversion.Add(21.40,6.7);
        conversion.Add(21.45,6.8);
        conversion.Add(21.50,6.9);
        conversion.Add(21.55,7.1);
        conversion.Add(21.60,7.5);
        conversion.Add(21.65,10.3);
        conversion.Add(21.70,11.0);
        conversion.Add(21.75,11.2);
        conversion.Add(21.80,11.4);
        conversion.Add(21.85,11.5);
        conversion.Add(21.90,11.6);
        conversion.Add(21.95,11.7);
        conversion.Add(22.05,11.8);
        conversion.Add(22.15,11.9);
        conversion.Add(22.30,12.0);
        conversion.Add(22.50,12.1);
        conversion.Add(22.70,12.2);
        conversion.Add(23.00,12.3);
        conversion.Add(23.35,12.4);
        conversion.Add(23.80,12.5);
        conversion.Add(24.40,12.6);
        conversion.Add(25.20,12.7);
        conversion.Add(26.25,12.8);
        conversion.Add(27.65,12.9);
        conversion.Add(29.55,13.0);
        conversion.Add(32.30,13.1);
        conversion.Add(36.35,13.2);
        conversion.Add(42.65,13.3);
        conversion.Add(53.60,13.4);
        conversion.Add(76.08,13.5);
        */
    }

    public float getPH()
    {
        return Convert.ToSingle(pH);
    }

    public double getVa()
    {
        return Va;
    }

    public float getVb()
    {
        return Convert.ToSingle(Vb);
    }

    public void updatePH()
    {
        if (conversion.ContainsKey(Vb)) {
            pH = conversion[Vb];
        } else
        {
            pH = pH;
        }
    }

    public void setVb(double vb)
    {
        Vb = Math.Round(vb,2);
        updatePH();
    }

    public void setInd(int i)
    {
        indicator = i;
    }

    public int getInd()
    {
        return indicator;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor()
    {
        color = new Color(0, 0, 0);
        if(indicator == 1)
        {
            color = new Color(1,0,0,1);
            if (pH > 5)
            {
                color = new Color(1,(float)(200.0 / 255),0,1);
            }
            else if (pH >= 3)
            {
                color = new Color(1,(float)((pH - 3) / 2.0),0,1);
            }
        }
        if(indicator == 2)
        {
            color = new Color((float)0.6, (float)0.6, (float)0.6, (float)0.8);
            if (pH > 10)
            {
                color = new Color(1, (float)(50.0 / 255), 1, 1);
            }
            else if (pH >= 8)
            {
                color = new Color(1, (float)(50.0 / 255), 1,(float)((pH - 7) / 2.0));
            }
        }
    }

    public float calculateHA()
    {
        double hPlus = Math.Pow(10, -pH);
        double ha = hPlus / (hPlus + ka);
        return Convert.ToSingle(ha);
    }

    public float calculateAMin()
    {
        double hPlus = Math.Pow(10, -pH);
        double aMin = ka / (hPlus + ka);
        return Convert.ToSingle(aMin);
    }

    public void Reset()
    {
        calculator = new pHCalculator();
    }

}