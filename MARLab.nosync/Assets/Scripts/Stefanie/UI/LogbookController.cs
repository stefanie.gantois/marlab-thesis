﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using Static;
using UI;

public class LogbookController : MonoBehaviour
{
    public GameObject textObject;
    public GameObject resultLine;
    public GameObject volumeLine;
    public GameObject phLine;
    public Transform content;
    public TMP_Text indicatorMaterial;
    private List<Toggle> toggles = new List<Toggle>();

    private Transform procedureP;
    private Transform resultsP;
    private Transform anaP;
    private Transform MCP;

    public List<GameObject> mcQuestions = new List<GameObject>();
    public int nbQuestions = 0;
    public GameObject questionController;

    // Start is called before the first frame update
    void Start()
    {
        procedureP = content.GetChild(3);
        resultsP = content.GetChild(4);
        anaP = content.GetChild(5);
        MCP = content.GetChild(6);
        setDate();
        //setReaction(); not on startup
        setProcedure();
        setStructure();
    }

    // Update is called once per frame
    void Update()
    {
        updateProcedure();
        updateResults();
        setIndicatorMaterial();
        if (questionController != null)
        {
            nbQuestions = questionController.GetComponent<QuestionsController>().questionsDone;
        }
        addMC();
    }

    public void UpdateVisibility()
    {
        GameObject child = gameObject.transform.GetChild(0).gameObject;
        if (child.activeInHierarchy)
        {
            //LogController.controller.printJson(7, Time.time);
            child.SetActive(false);
            AnalyticsResult result = Analytics.CustomEvent("CloseLogbook",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
            //Debug.Log("analytic result: " + result);
        } else
        {
            //LogController.controller.printJson(4,Time.time);
            child.SetActive(true);
            AnalyticsResult result = Analytics.CustomEvent("OpenLogbook",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode }
                }
            );
            //Debug.Log("analytic result: " + result);
        }
    }

    public void setDate()
    {
        string date = DateTime.Now.ToString("d/MM/yy");
        content.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = date;
    }

    public void setReaction()
    {
        content.GetChild(1).GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "CH<sub>3</sub>COOH + NaOH <-> CH<sub>3</sub>COONa + H<sub>2</sub>O";
    }

    public void setIndicatorMaterial()
    {
        switch (pHCalculator.calculator.getInd())
        {
            case 0:
                indicatorMaterial.text = "- Indicator: ";
                break;
            case 1:
                indicatorMaterial.text = "- Indicator: <b>methyl orange</b>";
                break;
            case 2:
                indicatorMaterial.text = "- Indicator: <b>phenolphthalein</b>";
                break;
        }
    }

    public void setProcedure()
    {
        for (int i = 0; i < InstructionDict.InstanceInstructionDict.steps.Count; i++)
        {
            GameObject line = Instantiate(textObject, procedureP, false);
            Vector3 pos = line.GetComponent<RectTransform>().localPosition;
            line.GetComponent<RectTransform>().localPosition = new Vector3 (pos.x, pos.y - i * 100, pos.z);
            line.GetComponent<TextMeshProUGUI>().text = InstructionDict.InstanceInstructionDict.lookupStep(i);
            Toggle tog = line.GetComponentInChildren<Toggle>();
            tog.isOn = InstructionDict.InstanceInstructionDict.lookupDone(i);
            toggles.Add(tog);
        }
    }

    public void updateProcedure()
    {
        for (int i = 0; i < toggles.Count; i++)
        {
            toggles[i].isOn = InstructionDict.InstanceInstructionDict.lookupDone(i);
        }
    }

    public void addResults(int i)
    {
        if (i > 0)
        {
            GameObject line = Instantiate(resultLine, resultsP.GetChild(1).GetChild(3), false);
            Vector3 pos1 = line.GetComponent<RectTransform>().localPosition;
            line.GetComponent<RectTransform>().localPosition = new Vector3(pos1.x, pos1.y + 75 - i * 75, pos1.z);
        }

        GameObject VolumeText = Instantiate(volumeLine, resultsP.GetChild(1).GetChild(2), false);
        Vector3 pos2 = VolumeText.GetComponent<RectTransform>().localPosition;
        VolumeText.GetComponent<RectTransform>().localPosition = new Vector3(pos2.x, pos2.y - i * 75, pos2.z);
        VolumeText.GetComponent<TextMeshProUGUI>().text = ResultsLibrary.InstanceLib.storedVolume[i].ToString();

        GameObject phText = Instantiate(phLine, resultsP.GetChild(1).GetChild(2), false);
        Vector3 pos3 = phText.GetComponent<RectTransform>().localPosition;
        phText.GetComponent<RectTransform>().localPosition = new Vector3(pos3.x, pos3.y - i * 75, pos3.z);
        phText.GetComponent<TextMeshProUGUI>().text = ResultsLibrary.InstanceLib.storedPH[i].ToString();
    }

    private void updateResults()
    {
        //check if new lines were added;
        int totalResults = ResultsLibrary.InstanceLib.storedVolume.Count;
        int currentResults = resultsP.GetChild(1).GetChild(2).childCount/2;
        if (currentResults < totalResults)
        {
            RectTransform box = resultsP.GetChild(1).gameObject.GetComponent<RectTransform>();
            Vector2 size = box.sizeDelta;
            size.y += 75;
            box.sizeDelta = size;
            RectTransform rectRes = resultsP.gameObject.GetComponent<RectTransform>();
            Vector2 resSize = rectRes.sizeDelta;
            resSize.y += 75;
            rectRes.sizeDelta = resSize;

            RectTransform rectContent = content.gameObject.GetComponent<RectTransform>();
            Vector2 sizeContent = rectContent.sizeDelta;
            sizeContent.y += 75;
            rectContent.sizeDelta = sizeContent;

            setStructure();
            addResults(currentResults);
        }
    }

    public void addMC()
    {
        RectTransform rectMC = MCP.gameObject.GetComponent<RectTransform>();
        Vector2 mcSize = rectMC.sizeDelta;
        mcSize.y = 250 + nbQuestions * 150;
        rectMC.sizeDelta = mcSize;
        for(int i = 0; i < nbQuestions; i++)
        {
            mcQuestions[i].SetActive(true);
        }
    }

    public void setStructure()
    {
        // ---- Results -----
        Vector3 posProc = content.GetChild(3).localPosition;
        int count = InstructionDict.InstanceInstructionDict.steps.Count + 1;
        Vector3 posRes = new Vector3(posProc.x, posProc.y - 50 - count * 100, posProc.z);
        content.GetChild(4).localPosition = posRes;

        // ---- Analyse ----
        RectTransform rectRes = resultsP.gameObject.GetComponent<RectTransform>();
        Vector2 sizeRes = rectRes.sizeDelta;
        Vector3 posAna = new Vector3(posRes.x, posRes.y - sizeRes.y, posRes.z);
        content.GetChild(5).localPosition = posAna;

        // ---- MC questions ----
        RectTransform rectAna = anaP.gameObject.GetComponent<RectTransform>();
        Vector2 sizeAna = rectAna.sizeDelta;
        Vector3 posMC = new Vector3(posAna.x, posAna.y - sizeAna.y, posAna.z);
        //Vector3 posMC = posAna;
        content.GetChild(6).localPosition = posMC;

        // ---- end ----
        RectTransform rectMC = MCP.gameObject.GetComponent<RectTransform>();
        Vector2 sizeMC = rectMC.sizeDelta;

        // ---- Lines ----
        content.GetChild(7).GetChild(3).position = resultsP.position;
        content.GetChild(7).GetChild(4).position = anaP.position;
        content.GetChild(7).GetChild(5).position = MCP.position;
    }

    
}
