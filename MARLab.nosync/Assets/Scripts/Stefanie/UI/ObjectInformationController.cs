﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interaction;
using TMPro;
using UnityEngine;

namespace UI
{
    public class ObjectInformationController : MonoBehaviour
    {
        public InteractionController interactionController;
        public GameObject ObjectName;
        public TMP_Text NameText;
        public GameObject PhenolColorRange;
        public GameObject MethylColorRange;
        public GameObject BackgroundColorRange;
        public GameObject closeIndicator;
        public GameObject GrabbedName;
        public TMP_Text GrabbedNameText;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            updatePhenolVisibility();
            updateMethylVisibility();
            updateNameVisibility();
            //updateGrabbedNameVisibility();
        }

        private void updateNameVisibility()
        {
            if (interactionController.isSelectingObject && !Statemachine.Instance.questioning && !Statemachine.Instance.introducing)
            {
                ObjectName.SetActive(true);
                string name = interactionController.selectedObject.GetComponent<AR_Interactable>().InteractableName;
                NameText.text = NameDict.InstanceNameDict.lookupString(name);

            } else
            {
                ObjectName.SetActive(false);
            }
        }

        public void updatePhenolVisibility( )
        {
            GameObject panel = GameObject.Find("ObjectInformation").transform.GetChild(0).gameObject;
            if (Statemachine.Instance.showPhenol == true)
            {
                PhenolColorRange.SetActive(true);
                panel.SetActive(true);
            } else
            {
                PhenolColorRange.SetActive(false);
                panel.SetActive(false);
            }
        }

        public void updateMethylVisibility()
        {
            if (Statemachine.Instance.showMethyl == true)
            {
                MethylColorRange.SetActive(true);
                BackgroundColorRange.SetActive(true);
            }
            else
            {
                MethylColorRange.SetActive(false);
                BackgroundColorRange.SetActive(false);
            }
        }

        public void closeIndicators()
        {
            Statemachine.Instance.showMethyl = false;
            Statemachine.Instance.showPhenol = false;
        }

        private void updateGrabbedNameVisibility()
        {
            if (interactionController.isGrabbingObject)
            {
                //Debug.Log("object selected: " + interactionController.selectedObject.name);
                GrabbedName.SetActive(true);
                //GrabbedNameText.text = interactionController.grabbedObject.GetComponent<AR_Interactable>().InteractableName;
                string name = interactionController.grabbedObject.GetComponent<AR_Interactable>().InteractableName;
                GrabbedNameText.text = NameDict.InstanceNameDict.lookupString(name);
            }
            else
            {
                GrabbedName.SetActive(false);
            }
        }
    }
}
