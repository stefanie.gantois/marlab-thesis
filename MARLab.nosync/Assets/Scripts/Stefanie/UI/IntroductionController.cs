﻿using System;
using System.Collections;
using System.Collections.Generic;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class IntroductionController : MonoBehaviour
    {
        private int number = 0;
        public GameObject introObject;
        public TMP_Text text;
        private List<String> entries = new List<String>();
        private bool closed = true;

        // Start is called before the first frame update
        void Start()
        {
            entries.Add("Welcome to the lab. Let me guide you through your first experiment. First, can you solve these questions?");
            entries.Add("Good job! Now we are ready to start. Try to assemble the setup by using the two buttons below me.");
            entries.Add("You have correctly put all the materials together. Now we are ready to start titrating! Use the slider to add titrant to the analyte.");
            entries.Add("Now we can analyse the titration. Can you answer the following questions?");
            entries.Add("Woohow, you are almost done! All that remains is cleaning your desk.");
            entries.Add("Well done! You are all done. Thank you for playing!");
        }

        // Update is called once per frame
        void Update()
        {
            updateVisibility();
        }

        private void updateVisibility()
        {
            if (Statemachine.Instance.startIntro)
            {
                introObject.SetActive(true);
                Statemachine.Instance.startIntro = false;
                Statemachine.Instance.introducing = true;
                text.text = entries[number];
                closed = false;
                StartCoroutine(ShowIntro());
            }
        }
        IEnumerator ShowIntro()
        {
            yield return new WaitForSeconds(7);
            if (!closed)
            {
                EndIntro();
            }
            
        }

        public void EndIntro()
        {
            closed = true;
            introObject.SetActive(false);
            Statemachine.Instance.introducing = false;
            if (number == 0 || number == 3)
            {
                Statemachine.Instance.questioning = true;
                Statemachine.Instance.startQuestioning = true;
                number++;
            }
            else
            {
                if (number < entries.Count - 1) number++;
            }
        }
    }
}
