﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReviewController : MonoBehaviour
{
    public static ReviewController controller = new ReviewController();

    public bool showReview;
    public bool enable;

    public GameObject tempLogbook;
    public GameObject tempGraph;

    private ReviewController()
    {
        showReview = false;
    }

    public void SetShow(bool rev)
    {
        showReview = rev;
    }

    public bool GetShow()
    {
        return showReview;
    }

    public void setTemps(GameObject logbook, GameObject graph)
    {
        GameObject beforeLog = GameObject.Find("Logbook(Clone)");
        if (beforeLog != null)
        {
            Destroy(beforeLog);
        }
        GameObject beforeGraph = GameObject.Find("ContentGraph(Clone)");
        if (beforeGraph != null)
        {
            Destroy(beforeGraph);
        }

        tempLogbook = Instantiate(logbook);
        DontDestroyOnLoad(tempLogbook);

        tempGraph = Instantiate(graph);
        DontDestroyOnLoad(tempGraph);

    }

    public void enableReview()
    {
        enable = true;
    }

    public bool getEnable()
    {
        return enable;
    }

    public void Reset()
    {
        controller = new ReviewController();
    }
}
