﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;
using Static;

public class GraphController : MonoBehaviour
{
    
    public GameObject LineV;
    public GameObject LineH;
    public GameObject TickV;
    public GameObject TickH;
    public GameObject Dot;
    private float vInterval;
    private float hInterval;
    public GameObject container;
    public GameObject content;

    public GameObject dissociationBars;
    public GameObject contentBase;
    public GameObject contentAcid;
    public GameObject contentConj;
    public GameObject contentBeaker;

    public GameObject secondSlider;
    public GameObject comparingDiss;
    public GameObject firstContentAcid;
    public GameObject firstContentConj;
    public GameObject secContentAcid;
    public GameObject secContentConj;

    private int selectedDot;
    private int secondSelectedDot;

    public GameObject pointInfo;

    public bool movedSlider;

    private List<GameObject> dots = new List<GameObject>();

    void Start()
    {
        RectTransform container = content.GetComponent<RectTransform>();
        Vector2 containerSize = container.sizeDelta;
        vInterval = Convert.ToSingle(containerSize.x / 46);
        hInterval = Convert.ToSingle(containerSize.y / 14);
        CreateAxes();
    }

    void Update()
    {
        UpdatePoints();
        UpdateIndicator();
        setSlider();
    }

    public void UpdateVisibility()
    {
        GameObject child = gameObject.transform.GetChild(0).gameObject;

        if (child.activeInHierarchy)
        {
            child.SetActive(false);
            AnalyticsResult result = Analytics.CustomEvent("CloseGraph",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
            //Debug.Log("analytic result: " + result);
        }
        else
        {
            child.SetActive(true);
            AnalyticsResult result = Analytics.CustomEvent("OpenGraph",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
            //Debug.Log("analytic result: " + result);
        }
    }

    public void UpdateIndicator()
    {
        if (pHCalculator.calculator.getInd() > 0)
        {
            int ind = pHCalculator.calculator.getInd();
            content.transform.GetChild(ind).gameObject.SetActive(true);
        }
    }

    public void CreateAxes()
    {
        Transform xAxis = content.transform.GetChild(3);
        Transform yAxis = content.transform.GetChild(4);
       
        for (int i = 1; i < 46; i++)
        {
            GameObject vLine = Instantiate(LineV, xAxis, false);
            Vector3 vPos = vLine.transform.localPosition;
            vPos.x += i * vInterval;
            vLine.transform.localPosition = vPos;

            if (i % 2 == 0)
            {
                GameObject vTick = Instantiate(TickV, xAxis, false);
                Vector3 vtPos = vTick.transform.localPosition;
                vtPos.x += i * vInterval;
                vTick.transform.localPosition = vtPos;
                vTick.GetComponentInChildren<TextMeshProUGUI>().text = i.ToString();
            }
        }
        for (int i = 1; i < 15; i++)
        {
            GameObject hLine = Instantiate(LineH, yAxis, false);
            Vector3 hPos = hLine.transform.localPosition;
            hPos.y += i * hInterval;
            hLine.transform.localPosition = hPos;

            GameObject hTick = Instantiate(TickH, yAxis, false);
            Vector3 htPos = hTick.transform.localPosition;
            htPos.y += i * hInterval;
            hTick.transform.localPosition = htPos;
            hTick.GetComponentInChildren<TextMeshProUGUI>().text = i.ToString();
        }
    }

    public void CreatePoint(double vb, double ph, int index)
    {
        Transform dotsGroup = content.transform.GetChild(5);
        GameObject dotI = Instantiate(Dot, dotsGroup, false);
        Vector2 dotPos = dotI.transform.localPosition;
        dotPos.x += Convert.ToSingle(vb) * vInterval;
        dotPos.y += Convert.ToSingle(ph) * hInterval;
        dotI.transform.localPosition = dotPos;
        dotI.GetComponent<Image>().color = ResultsLibrary.InstanceLib.colorPoints[index];
        dots.Add(dotI);
    }

    public void UpdatePoints()
    {
        int totalResults = ResultsLibrary.InstanceLib.storedVolume.Count;
        int currentDots = dots.Count;
        if (currentDots < totalResults)
        {
            double vb = ResultsLibrary.InstanceLib.storedVolume[currentDots];
            double ph = ResultsLibrary.InstanceLib.storedPH[currentDots];
            CreatePoint(vb,ph,currentDots);
        }
    }

    public void ShowClosestPoint()
    {
        //Debug.Log("in show closest");
        GameObject slider = content.transform.GetChild(6).gameObject;
        double vb = slider.GetComponent<Slider>().value;
        //Debug.Log("bvb is " + vb);
        int index = 1000;
        float min = 1000;
        for(int i = 0; i < ResultsLibrary.InstanceLib.storedVolume.Count;i++)
        {
            float volume = ResultsLibrary.InstanceLib.storedVolume[i];
            float distance = Math.Abs(Convert.ToSingle(vb) - volume);
            if (distance < min)
            {
                min = distance;
                index = i;
            }
        }
        if (dots.Count > 0)
        {
            dots[selectedDot].transform.GetChild(0).gameObject.SetActive(false);
            RectTransform dotRect = dots[selectedDot].GetComponent<RectTransform>();
            Vector2 dotSize = dotRect.sizeDelta;
            dotSize.Set(25, 25);
            dotRect.sizeDelta = dotSize;

            if (index < dots.Count)
            {
                selectedDot = index;
                dots[selectedDot].transform.GetChild(0).gameObject.SetActive(true);
                dotRect = dots[selectedDot].GetComponent<RectTransform>();
                dotSize = dotRect.sizeDelta;
                dotSize.Set(30, 30);
                dotRect.sizeDelta = dotSize;
                setBars();
                if (comparingDiss.activeInHierarchy)
                {
                    setComparingBars();
                }
                else
                {
                    pointInfo.SetActive(true);
                    pointInfo.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = "Added volume: " + ResultsLibrary.InstanceLib.storedVolume[selectedDot].ToString() + " mL";
                    pointInfo.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = "pH: " + ResultsLibrary.InstanceLib.storedPH[selectedDot].ToString();
                }
            }
        }
        
    }

    public void setSlider()
    {
        if (!movedSlider)
        {
            GameObject slider = content.transform.GetChild(6).gameObject;
            int currentDots = dots.Count;
            if (currentDots > 0)
            {
                float vb = ResultsLibrary.InstanceLib.storedVolume[currentDots-1];
                slider.GetComponent<Slider>().SetValueWithoutNotify(vb);
                ShowClosestPoint();
            }
        }
    }

    public void setMovedSlider()
    {
        movedSlider = true;
    }

    private void setBars()
    {
        RectTransform rBase = contentBase.GetComponent<RectTransform>();
        Vector2 sizeBase = rBase.sizeDelta;
        RectTransform rAcid = contentAcid.GetComponent<RectTransform>();
        Vector2 sizeAcid = rAcid.sizeDelta;
        RectTransform rConj = contentConj.GetComponent<RectTransform>();
        Vector2 sizeConj = rConj.sizeDelta;
        RectTransform rBeaker = contentBeaker.GetComponent<RectTransform>();
        Vector2 sizeBeaker = rBeaker.sizeDelta;

        GameObject dot = dots[selectedDot];
        float vb = ResultsLibrary.InstanceLib.storedVolume[selectedDot];
        float va = Convert.ToSingle(pHCalculator.calculator.getVa());
        float ha = ResultsLibrary.InstanceLib.ha[selectedDot];
        float aMin = ResultsLibrary.InstanceLib.aMin[selectedDot];
        Color color = dot.GetComponent<Image>().color;

        sizeBase.y = (float)(vb * 240 / 100);
        sizeBeaker.y = (float)((vb + va) * 240 / 100);
        sizeAcid.y = 300 * ha;
        sizeConj.y = 300 * aMin;

        rBase.sizeDelta = sizeBase;
        rBeaker.sizeDelta = sizeBeaker;
        rAcid.sizeDelta = sizeAcid;
        rConj.sizeDelta = sizeConj;
        contentBeaker.GetComponent<Image>().color = color;

        dissociationBars.transform.GetChild(1).GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = (vb + va).ToString();
        dissociationBars.transform.GetChild(2).GetChild(3).gameObject.GetComponent<TextMeshProUGUI>().text = vb.ToString();
    }

    public void showBothIndicators(bool show)
    {
        content.transform.GetChild(1).gameObject.SetActive(show);
        content.transform.GetChild(2).gameObject.SetActive(show);
        AnalyticsResult result = Analytics.CustomEvent("ShowIndicatorsGraph",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
        Debug.Log("analytic result: " + result);
    }

    public void showBothSliders(bool show)
    {
        secondSlider.SetActive(show);
        comparingDiss.SetActive(show);
        AnalyticsResult result = Analytics.CustomEvent("ShowCompareGraph",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
        Debug.Log("analytic result: " + result);
    }

    public void showInformation(bool show)
    {
        //UpdateIndicator();
        dissociationBars.SetActive(show);
        AnalyticsResult result = Analytics.CustomEvent("ShowInformationGraph",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
        Debug.Log("analytic result: " + result);

    }

    public void ShowSecondClosestPoint()
    {
        GameObject slider = secondSlider;
        double vb = slider.GetComponent<Slider>().value;
        int index = 10000;
        float min = 10000;
        for (int i = 0; i < ResultsLibrary.InstanceLib.storedVolume.Count; i++)
        {
            float volume = ResultsLibrary.InstanceLib.storedVolume[i];
            float distance = Math.Abs(Convert.ToSingle(vb) - volume);
            if (distance < min)
            {
                min = distance;
                index = i;
            }
        }
        if (dots.Count > 0)
        {
            dots[secondSelectedDot].transform.GetChild(0).gameObject.SetActive(false);
            RectTransform dotRect = dots[secondSelectedDot].GetComponent<RectTransform>();
            Vector2 dotSize = dotRect.sizeDelta;
            dotSize.Set(25, 25);
            dotRect.sizeDelta = dotSize;

            if (index < dots.Count)
            {
                secondSelectedDot = index;
                dots[secondSelectedDot].transform.GetChild(0).gameObject.SetActive(true);
                dotRect = dots[secondSelectedDot].GetComponent<RectTransform>();
                dotSize = dotRect.sizeDelta;
                dotSize.Set(30, 30);
                dotRect.sizeDelta = dotSize;
                setComparingBars();
            }
        }
    }

    private void setComparingBars()
    {
        RectTransform rAcid = firstContentAcid.GetComponent<RectTransform>();
        Vector2 sizeAcid = rAcid.sizeDelta;
        RectTransform rConj = firstContentConj.GetComponent<RectTransform>();
        Vector2 sizeConj = rConj.sizeDelta;
        RectTransform rAcid2 = secContentAcid.GetComponent<RectTransform>();
        Vector2 sizeAcid2 = rAcid2.sizeDelta;
        RectTransform rConj2 = secContentConj.GetComponent<RectTransform>();
        Vector2 sizeConj2 = rConj2.sizeDelta;

        GameObject dot = dots[selectedDot];
        float ha = ResultsLibrary.InstanceLib.ha[selectedDot];
        float aMin = ResultsLibrary.InstanceLib.aMin[selectedDot];
        GameObject dot2 = dots[secondSelectedDot];
        float ha2 = ResultsLibrary.InstanceLib.ha[secondSelectedDot];
        float aMin2 = ResultsLibrary.InstanceLib.aMin[secondSelectedDot];

        sizeAcid.y = 300 * ha;
        sizeConj.y = 300 * aMin;
        sizeAcid2.y = 300 * ha2;
        sizeConj2.y = 300 * aMin2;

        rAcid.sizeDelta = sizeAcid;
        rConj.sizeDelta = sizeConj;
        rAcid2.sizeDelta = sizeAcid2;
        rConj2.sizeDelta = sizeConj2;
    }
}
