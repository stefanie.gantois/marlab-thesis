﻿using System.Collections.Specialized;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using Static;

public class InstructionDict : MonoBehaviour
{
    public List<string> steps = new List<string>();
    public OrderedDictionary storedSteps = new OrderedDictionary();
    public List<string> hints = new List<string>();

    public static InstructionDict InstanceInstructionDict = new InstructionDict();

    private InstructionDict()
    {
        steps.Add("Put on protective material");
        storedSteps.Add("Put on protective material", false);
        steps.Add("Attach the burette");
        storedSteps.Add("Attach the burette", false);
        steps.Add("Rinse the burette with distilled water");
        storedSteps.Add("Rinse the burette with distilled water", false);
        steps.Add("Put the funnel on the burette");
        storedSteps.Add("Put the funnel on the burette", false);
        steps.Add("Fill an empty beaker with the titrant");
        storedSteps.Add("Fill an empty beaker with the titrant", false);
        steps.Add("Pour titrant into burette with the beaker");
        storedSteps.Add("Pour titrant into burette with the beaker", false);
        steps.Add("Read the burette from top of the miniscus");
        storedSteps.Add("Read the burette from top of the miniscus", false);
        steps.Add("Put away the funnel");
        storedSteps.Add("Put away the funnel", false);
        steps.Add("Add an indicator to the analyte");
        storedSteps.Add("Add an indicator to the analyte", false);
        steps.Add("Put the magnet of the magnetic stirrer into the analyte");
        storedSteps.Add("Put the magnet of the magnetic stirrer into the analyte", false);
        steps.Add("Put the analyte onto the magnetic stirrer");
        storedSteps.Add("Put the analyte onto the magnetic stirrer", false);
        steps.Add("Put the electrode of the pH probe into the analyte");
        storedSteps.Add("Put the electrode of the pH probe into the analyte", false);
        steps.Add("Start stirring the analyte");
        storedSteps.Add("Start stirring the analyte", false);
        steps.Add("Start titrating and record the pH");
        storedSteps.Add("Start titrating and record pH", false);
        steps.Add("Clean up your desk.");
        storedSteps.Add("Clean up your desk", false);

        hints.Add("You need to wear protective gloves and glasses when working in the lab.");
        hints.Add("You need to attach the burette to the stand, before you can rinse and/or fill it.");
        hints.Add("You have to rinse the burette to make sure no other chemicals are present in the burette.");
        hints.Add("You should use a funnel, to pour the titrant into the burette.");
        hints.Add("Open the flask to pour the titrant into an empty beaker");
        hints.Add("Use a beaker to pour the liquid into the burette");
        hints.Add("You need to know the volume of the titrant in the burette before titrating.");
        hints.Add("You need to remove the funnel before titrating. The extra weight of the funnel could inlfuence the behaviour of the burette.");
        hints.Add("You need to choose an indicator, but choose wisely! Add the indicator by using the top.");
        hints.Add("You need to add a magnet into the analyte, such that the magnetic stirrer will stir the analyte. The magnet can be found on the stirrer.");
        hints.Add("You need to put the analyte onto the magnetic stirrer, such that you can stir the analyte during the titration.");
        hints.Add("Put the electrode of the pH probe into the analyte to measure the pH. Be carefull not to touch the magnet!");
        hints.Add("You need to start stirring the analyte before titrating. This way the analyte and indicator are fully mixed.");
        hints.Add("You need to titrate by using the slider.");
        hints.Add("Clean up your desk by throwing away the liquids in the correct container.");
    }

    public bool lookupDone(int toLookUp)
    {
        bool done = false;
        if (toLookUp < storedSteps.Count)
        {
            done = (bool)storedSteps[toLookUp];
        }
        return done;
    }

    public string lookupStep(int toLookUp)
    {
        string result = "";
        if (toLookUp < steps.Count)
        {
            result = (string)steps[toLookUp];
        }
        return result;
    }

    public void putDone(int toUpdate)
    {
        if (toUpdate < storedSteps.Count)
        {
            storedSteps[toUpdate] = true;
        }
        AnalyticsResult result = Analytics.CustomEvent("DoneProcess",
            new Dictionary<string, object>
                {
                    { "Step",steps[toUpdate]},
                    { "id", ApplicationSettings.idCode }
                });
        Debug.Log("analytic result: " + result);
    }

    public void setAssistanceInstruction()
    {
        
        for (int i = 0; i < steps.Count; i++)
        {
            if (!(bool)storedSteps[i])
            {
                Statemachine.Instance.instruction = steps[i];
                break;
            }
            if (i == steps.Count-1)
            {
                Statemachine.Instance.instruction = steps[i];
            }
            if (Statemachine.Instance.titrating)
            {
                Statemachine.Instance.instruction = steps[steps.Count-2];
            }
        }
    }

    public string lookUpHint (int hint)
    {
        if(hint < hints.Count)
        {
            return hints[hint];
        }
        return "";
    }

    public int getFirstToDoHint ()
    {
        if (Statemachine.Instance.titrating)
        {
            return hints.Count - 2;
        }
        for (int i = 0; i < steps.Count; i++)
        {
            if (!(bool)storedSteps[i])
            {
                return i;
            }
        }
        return hints.Count;
    }
    public void Reset()
    {
        InstanceInstructionDict = new InstructionDict();
    }
}
