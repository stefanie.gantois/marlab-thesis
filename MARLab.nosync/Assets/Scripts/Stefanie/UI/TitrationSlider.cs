﻿using Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TitrationSlider : MonoBehaviour
    {
        public InteractionController interactionController;
        public Others.PrefabSpawningController controller;
        public GameObject slider;
        public GameObject DirectInfoBoxCont;
        
        public void ChangeTitrationValue()
        {
            controller.getSpawnedObject().GetComponent<TitrationController>().ManipulateErlenmeyerFluid((int)slider.GetComponent<Slider>().value);
        }

        public void StopTitration()
        {
            if (ResultsLibrary.InstanceLib.storedPH[ResultsLibrary.InstanceLib.storedPH.Count - 1] > 9)
            {
                Statemachine.Instance.titrating = false;
                Statemachine.Instance.startIntro = true;
                Statemachine.Instance.stoptTitrating = true;
            } else
            {
                DirectInfoBoxCont.GetComponent<DirectInfoBoxController>().ShowTitrationInfobox();
                Statemachine.Instance.errorTotal++;
            }
            /*
            Statemachine.Instance.startQuestioning = true;
            Statemachine.Instance.questioning = true;
            */
        }
    }
}

