﻿using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;
using UnityEngine.Analytics;

public class ResultsLibrary : MonoBehaviour
{
    public List<float> storedVolume = new List<float>();
    public List<float> storedPH = new List<float>();
    public List<Color> colorPoints = new List<Color>();
    public List<float> ha = new List<float>();
    public List<float> aMin = new List<float>();

    public static ResultsLibrary InstanceLib = new ResultsLibrary();

    private ResultsLibrary()
    {
    }

    public void addResult()
    {
        storedVolume.Add(pHCalculator.calculator.getVb());
        storedPH.Add(pHCalculator.calculator.getPH());
        pHCalculator.calculator.setColor();
        colorPoints.Add(pHCalculator.calculator.getColor());
        ha.Add(pHCalculator.calculator.calculateHA());
        aMin.Add(pHCalculator.calculator.calculateAMin());
        /*
        AnalyticsResult result = Analytics.CustomEvent("AddPoint",
                new Dictionary<string, object>
                {
                    { "volume" , storedVolume[storedVolume.Count-1] },
                    { "pH" , storedPH[storedPH.Count-1] },
                    { "id", ApplicationSettings.idCode}
                }
            );
        Debug.Log("analytic result: " + result);
        */
    }

    public void Reset()
    {
        InstanceLib = new ResultsLibrary();
    }

}
