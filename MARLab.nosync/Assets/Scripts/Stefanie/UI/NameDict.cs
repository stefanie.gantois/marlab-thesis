﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 
public class NameDict : MonoBehaviour
{
    Dictionary<string, string> storedStrings = new Dictionary<string, string>();

    public static NameDict InstanceNameDict = new NameDict();

    private NameDict()
    {
        storedStrings.Add("Erlenmeyer", "Beaker with analyte");
        storedStrings.Add("Funnel", "Funnel");
        storedStrings.Add("Wash", "Distilled Water");
        storedStrings.Add("Phenolphthalein", "Phenolphthalein");
        storedStrings.Add("MethylOrange", "Methyl Orange");
        storedStrings.Add("PhenoDropper", "Phenolphthalein");
        storedStrings.Add("MethylDropper", "Methyl Orange");
        storedStrings.Add("Burette", "Burette");
        storedStrings.Add("Stand", "Stand");
        storedStrings.Add("ErlenStand", "Erlenmeyer");
        storedStrings.Add("BuretteStand", "Burette");
        storedStrings.Add("FunnelStand", "Funnel");
        storedStrings.Add("BeakerEmpty", "Empty beaker");
        storedStrings.Add("BeakerFilled", "Beaker with titrant");
        storedStrings.Add("Titrant", "Volumetric flask with titrant");
        storedStrings.Add("TitrantPouring", "Volumetric flask with titrant");
        storedStrings.Add("Stirrer", "Magnetic Stirrer");
        storedStrings.Add("ProbeTip", "pH probe electrode");
        storedStrings.Add("ProbeBody", "Probe");
        storedStrings.Add("Magnet","Magnet");
        storedStrings.Add("Handschuhbox", "Glove Box");
        storedStrings.Add("Glasses", "Safety Glasses");
        storedStrings.Add("Graph", "Titration Dashboard");
        storedStrings.Add("JerryCanWhite", "Acid inorganic liquid waste container");
        storedStrings.Add("JerryCanBlack", "Alkaline inorganic waste container");
        storedStrings.Add("TitrantWaste", "Volumetric flask to be washed");
        storedStrings.Add("BeakerWaste", "Beaker to be washed");
        storedStrings.Add("BuretteWash", "Burette to be washed");
        storedStrings.Add("VinegarWashed", "Beaker to be washed");
    }

    public string lookupString(string toLookUp)
    {
        string result = "no text in dictionary found";
        if (storedStrings.ContainsKey(toLookUp))
        {
            result = storedStrings[toLookUp];
        }
        return result;
    }

    public void putString(string name, string toPut)
    {
        storedStrings[name] = toPut;
    }
}
