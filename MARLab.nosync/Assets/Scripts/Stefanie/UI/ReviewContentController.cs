﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Analytics;
using Static;

public class ReviewContentController : MonoBehaviour
{
    public Button LogbookButton;
    public Button GraphButton;
    public GameObject logbookContainer;
    public GameObject graphContainer;

    private int lastSelected;

    void Start()
    {
        instantiateOverviews();
        LogbookButton.Select();
        showLogbookReview();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void goBackHome()
    {
        ReviewController.controller.SetShow(false);
        SceneManager.LoadScene("Scenes/Menu", LoadSceneMode.Single);
    }

    public void showLogbookReview()
    {
        logbookContainer.SetActive(true);
        graphContainer.SetActive(false);
        setColors(LogbookButton, GraphButton);
        AnalyticsResult result = Analytics.CustomEvent("ShowLogbookReview",
            new Dictionary<string, object>
            {
                    { "id", ApplicationSettings.idCode}
            }
        );
        //Debug.Log("analytic result: " + result);
        //Debug.Log("id: " + ApplicationSettings.idCode);
    }

    public void showGraphReview()
    {
        graphContainer.SetActive(true);
        logbookContainer.SetActive(false);
        setColors(GraphButton, LogbookButton);
        AnalyticsResult result = Analytics.CustomEvent("ShowGraphReview",
            new Dictionary<string, object>
            {
                    { "id", ApplicationSettings.idCode}
            }
        );
        //Debug.Log("analytic result: " + result);
        //Debug.Log("id: " + ApplicationSettings.idCode);
    }

    public void instantiateOverviews()
    {
        if (ReviewController.controller.tempLogbook != null && ReviewController.controller.tempGraph != null)
        {
            //delete the first element in each container
            GameObject revLog = Instantiate(ReviewController.controller.tempLogbook, logbookContainer.transform);
            revLog.SetActive(true);
            revLog.transform.GetChild(0).gameObject.SetActive(true);
            Destroy(revLog.transform.GetChild(0).GetChild(0).gameObject);
            Destroy(revLog.transform.GetChild(0).GetChild(1).gameObject);
            Destroy(revLog.transform.GetChild(0).GetChild(3).gameObject);

            GameObject revGraph = Instantiate(ReviewController.controller.tempGraph, graphContainer.transform);
            revGraph.SetActive(true);
            revGraph.transform.GetChild(0).gameObject.SetActive(true);
            Destroy(revGraph.transform.GetChild(0).GetChild(0).gameObject);
            Destroy(revGraph.transform.GetChild(0).GetChild(1).gameObject);
            Destroy(revGraph.transform.GetChild(0).GetChild(3).gameObject);
            revGraph.GetComponent<GraphController>().setMovedSlider();
        }
    }

    private void setColors(Button selected, Button deselected)
    {
        var tempColor = selected.colors;
        tempColor.normalColor = new Color((float)(255.0 / 255), (float)(255.0 / 255), (float)(255.0 / 255), (float)(255.0 / 255));
        selected.colors = tempColor;
        selected.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = new Color((float)(54.0 / 255), (float)(90.0 / 255), (float)(126.0 / 255), (float)(255.0 / 255));

        var tempColor2 = deselected.colors;
        tempColor2.normalColor = new Color((float)(54.0 / 255), (float)(90.0 / 255), (float)(126.0 / 255), (float)(255.0 / 255));
        deselected.colors = tempColor2;
        deselected.gameObject.GetComponentInChildren<TextMeshProUGUI>().color = new Color((float)(255.0 / 255), (float)(255.0 / 255), (float)(255.0 / 255), (float)(255.0 / 255));
    }
}
