﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interaction;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;

namespace UI
{
    public class InfoboxController : MonoBehaviour
    {
        public GameObject infobox;
        public TMP_Text infotext;
        public GameObject questionMark;
        private float lastShown;
        private bool shown = false;
        private float duration;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            
        }

        public void UpdateVisibilityInfobox()
        {
            if (shown)
            {
                gameObject.SetActive(false);
                questionMark.SetActive(true);
                infobox.SetActive(false);
                shown = false;
            } else
            {
                int hintIndex = InstructionDict.InstanceInstructionDict.getFirstToDoHint();
                string text = InstructionDict.InstanceInstructionDict.lookUpHint(hintIndex);
                infotext.text = text;
                infobox.SetActive(true);

                shown = true;

                gameObject.SetActive(true);
                questionMark.SetActive(false);

                AnalyticsResult result = Analytics.CustomEvent("ShowHint",
                new Dictionary<string, object>
                {
                    { "hintNumber",  hintIndex},
                    { "stateNumber", Statemachine.Instance.stateNumber},
                    { "id", ApplicationSettings.idCode}
                }
            );
                //Debug.Log("analytic result: " + result);

                StartCoroutine(ShownCoroutine());
            }
                
        }

        private IEnumerator ShownCoroutine()
        {
            yield return new WaitForSeconds(10);

            infobox.SetActive(false);
            shown = false;
            gameObject.SetActive(false);
            questionMark.SetActive(true);
        }
    }
}
