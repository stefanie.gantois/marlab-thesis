﻿using System;
using System.Collections;
using System.Collections.Generic;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

namespace UI
{
    public class QuestionsController : MonoBehaviour
    {

        public GameObject QuestionsObject;
        public TMP_Text question;
        public List<Button> answers;
        public List<AnswerEntry> answerEntries = new List<AnswerEntry>();
        public GameObject ExplanationObject;
        public Color colorNormal;
        public Color colorDisabled;
        public int num;
        private bool correct = false;
        public Button cross;
        public GameObject Logbook;
        public int questionsDone = 0;

        public PlayableSounds CorrectSound;
        public PlayableSounds ErrorSound;

        private GlobalAudioController globalAudioController;

        // Start is called before the first frame update
        void Start()
        {
            //initButtons();
            //QuestionsObject.SetActive(true);
            ExplanationObject.SetActive(false);
            answerEntries.Add(new AnswerEntry("2HNO<sub>3</sub> + Ca(OH)<sub>2</sub> <-> Ca(NO<sub>3</sub>)<sub>2</sub> + 2H<sub>2</sub>O", false));
            answerEntries.Add(new AnswerEntry("HCl + NaOH <-> NaCl + H<sub>2</sub>O", false));
            answerEntries.Add(new AnswerEntry("CH<sub>3</sub>COOH + NaOH <-> CH<sub>3</sub>COONa + H<sub>2</sub>O", true));

            answerEntries.Add(new AnswerEntry("stronger", false));
            answerEntries.Add(new AnswerEntry("weaker", true));
            answerEntries.Add(new AnswerEntry("more neutral", false));

            answerEntries.Add(new AnswerEntry("Ka = [CH<sub>3</sub>COOH]/([H<sub>3</sub>O<sup>+</sup>][CH<sub>3</sub>COO<sup>-</sup>])", false));
            answerEntries.Add(new AnswerEntry("Ka = 1 / [CH<sub>3</sub>COOH]", false));
            answerEntries.Add(new AnswerEntry("Ka = [H<sub>3</sub>O<sup>+</sup>][CH<sub>3</sub>COO<sup>-</sup>]/[CH<sub>3</sub>COOH]", true));

            answerEntries.Add(new AnswerEntry("bromothymol blue (pH range 6-7.6)", false));
            answerEntries.Add(new AnswerEntry("methyl orange (pH range 3.2-4.4)", false));
            answerEntries.Add(new AnswerEntry("phenolphthalein (pH range 8.2-10)", true));

            answerEntries.Add(new AnswerEntry("1:1", true));
            answerEntries.Add(new AnswerEntry("1:2", false));
            answerEntries.Add(new AnswerEntry("1:3", false));

            answerEntries.Add(new AnswerEntry("0.01 mol", true));
            answerEntries.Add(new AnswerEntry("0.02 mol", false));
            answerEntries.Add(new AnswerEntry("0.03 mol", false));

            answerEntries.Add(new AnswerEntry("0.01 mol", true));
            answerEntries.Add(new AnswerEntry("0.1 mol", false));
            answerEntries.Add(new AnswerEntry("0.2 mol", false));

            answerEntries.Add(new AnswerEntry("0.001 M", false));
            answerEntries.Add(new AnswerEntry("1 M", true));
            answerEntries.Add(new AnswerEntry("10 M", false));

            answerEntries.Add(new AnswerEntry("Sample A = 1 M", true));
            answerEntries.Add(new AnswerEntry("Sample B = 0.9 M", false));
            answerEntries.Add(new AnswerEntry("I don't like pickles", false));

            globalAudioController = GameObject.Find("Sounds").GetComponent<GlobalAudioController>();

        }


        void Update()
        {
            updateVisibility();
        }

        private void updateVisibility()
        {
            if (Statemachine.Instance.startQuestioning && !Statemachine.Instance.introducing)
            {
                initButtons();
                QuestionsObject.SetActive(true);
                Statemachine.Instance.startQuestioning = false;
            }
        }

        private void initButtons()
        {
            switch (Statemachine.Instance.questions)
            {
                case 11:
                    question.text = "Which equation represents the neutralization reaction between the vinegar and sodium hydroxide?";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "Vinegar is an aqueous solution of acetic acid or CH<sub>3</sub>COOH, which after adding sodium hydroxide (NaOH) is neutralized to form as products water (H<sub>2</sub>O) and sodium acetate (CH<sub>3</sub>COONa)";
                    num = 0;
                    break;
                case 12:
                    question.text = "The smaller the value of the dissociation constant (Ka), the __________ the acid.";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The magnitude of Ka or acid dissociation constant indicates the tendency of the acid to ionize in water: the larger the value of Ka, the stronger the acid.";
                    num = 1;
                    break;
                case 13:
                    question.text = "What is the correct expression of the dissociation constant or Ka of acetic acid in water?";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The dissociation of a weak acid in water is characterized by an equilibrium equation. In this experiment, the equilibrium equation is the following: CH<sub>3</sub>COOH + NaOH <-> CH<sub>3</sub>COONa + H<sub>2</sub>O. Therefore, the expression is c), because water is the solvent, it is omitted from the equilibrium-constant.";
                    num = 2;
                    break;
                case 21:
                    question.text = "Which indicator is more suitable to detect the end point of the titration between acetic acid and sodium hydroxide?";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "No explanation provided.";
                    num = 3;
                    break;
                case 22:
                    question.text = "Select the molar ratio of the reaction between the acetic acid and sodium hydroxide:\n x CH<sub>3</sub>COOH + x NaOH → CH<sub>3</sub>COONa + H<sub>2</sub>O";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The equation shows that for every mole of acetic acid and sodium hydroxide, one mole of sodium acetate is formed along with one mole of water.";
                    num = 4;
                    break;
                case 23:
                    question.text = "Select the correct amount of NaOH (mol) that reacted in the titration experiment. \nIf you are missing information, check the logbook!";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The necessary equation can be found in the logbook as well as the concentration of the standard solution (0.5 mol/L). The volume used untill equivalence point was 21.6 mL.";
                    num = 5;
                    break;
                case 24:
                    question.text = "Based on the amount of NaOH (mol) used and the molar ratio of the equation, what is the amount of acetic acid that reacted during the titration experiment?";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The amount of acetic acid in moles is equivalent to that of NaOH since the molar ratio is 1:1.";
                    num = 6;
                    break;
                case 25:
                    question.text = "Finally, determine the concentration of acetic acid (analyte).\nIf you are missing information, check the logbook!";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The necessary equation can be found in the logbook, as well as the initial volume of the analyte (10 mL). The amount of acetic acid that reacted during the experiment was 0.01 mol.";
                    num = 7;
                    break;
                case 26:
                    question.text = "Which vinegar will you use for pickling your vegetables?";
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "Sample A is the best option as the concentration of acetic acid in this vinegar is higher. For culinary reasons, experts may also consider other aspects such as flavor but chemistry also helps!";
                    num = 8;
                    break;
            }

            for (var i = num * 3; i < 3 + num * 3; i++)
            {
                answers[i % 3].GetComponentInChildren<TMP_Text>().text = answerEntries[i].answerText;
                int buttonIndex = i;
                //answers[i % 3].onClick.AddListener(() => ValidateEntry(buttonIndex));
            }
        }

        private void ValidateEntry(int answerIndex)
        {
            AnalyticsResult result = Analytics.CustomEvent("AnswerQuestion",
                    new Dictionary<string, object>
                        {
                            { "QuestionNumber",Statemachine.Instance.questions},
                            { "Correct", answerEntries[answerIndex].isCorrect},
                            { "id", ApplicationSettings.idCode }
                        });
            //Debug.Log("analytic result: " + result);

            if (answerEntries[answerIndex].isCorrect)
            {
                answerIndex = answerIndex % 3;
                for (var i = 0; i < 3; i++)
                {
                    var tempCol = answers[i].colors;
                    tempCol.disabledColor = new Color((float)(200.0 / 255), (float)(200.0 / 255), (float)(200.0 / 255), (float)(128.0 / 255));
                    answers[i].colors = tempCol;
                    answers[i].interactable = false;
                }

                correct = true;

                //other colors
                var tempColor = answers[answerIndex].colors;
                tempColor.disabledColor = Color.green;
                answers[answerIndex].colors = tempColor;

                if(num == 3)
                {
                    setExplanation(true);
                }
                ExplanationObject.SetActive(true);
                Vector3 pos = answers[answerIndex].GetComponent<RectTransform>().position;
                ExplanationObject.transform.position = pos + new Vector3(0, 350, 0);

                Statemachine.Instance.stateNumber += 5;

                playQuestionSound(CorrectSound);
            }
            else
            {
                answerIndex = answerIndex % 3;
                //disable all
                for (var i = 0; i < 3; i++)
                {
                    answers[i].interactable = false;
                }

                correct = false;

                var tempColor = answers[answerIndex].colors;
                tempColor.disabledColor = Color.red;
                answers[answerIndex].colors = tempColor;

                if (num == 3)
                {
                    setExplanation(false);
                }
                ExplanationObject.SetActive(true);
                Vector3 pos = answers[answerIndex].GetComponent<RectTransform>().position;
                ExplanationObject.transform.position = pos + new Vector3(0, 350, 0);

                playQuestionSound(ErrorSound);
            }
        }

        public void ValidateEntry2(string answer)
        {
            int answerIndex;
            bool answered = Int32.TryParse(answer, out answerIndex);
            AnalyticsResult result = Analytics.CustomEvent("AnswerQuestion",
                    new Dictionary<string, object>
                        {
                            { "QuestionNumber",Statemachine.Instance.questions},
                            { "Correct", answerEntries[num*3 + answerIndex].isCorrect},
                            { "id", ApplicationSettings.idCode }
                        });
            //Debug.Log("analytic result: " + result);

            if (answerEntries[num*3 + answerIndex].isCorrect)
            {
                //answerIndex = answerIndex % 3;
                for (var i = 0; i < 3; i++)
                {
                    var tempCol = answers[i].colors;
                    tempCol.disabledColor = new Color((float)(200.0 / 255), (float)(200.0 / 255), (float)(200.0 / 255), (float)(128.0 / 255));
                    answers[i].colors = tempCol;
                    answers[i].interactable = false;
                }

                correct = true;

                //other colors
                var tempColor = answers[answerIndex].colors;
                tempColor.disabledColor = Color.green;
                answers[answerIndex].colors = tempColor;

                if (num == 3)
                {
                    setExplanation(true);
                }
                ExplanationObject.SetActive(true);
                Vector3 pos = answers[answerIndex].GetComponent<RectTransform>().position;
                ExplanationObject.transform.position = pos + new Vector3(0, 350, 0);

                Statemachine.Instance.stateNumber += 5;

                playQuestionSound(CorrectSound);
            }
            else
            {
                answerIndex = answerIndex % 3;
                //disable all
                for (var i = 0; i < 3; i++)
                {
                    answers[i].interactable = false;
                }

                correct = false;

                var tempColor = answers[answerIndex].colors;
                tempColor.disabledColor = Color.red;
                answers[answerIndex].colors = tempColor;

                if (num == 3)
                {
                    setExplanation(false);
                }
                ExplanationObject.SetActive(true);
                Vector3 pos = answers[answerIndex].GetComponent<RectTransform>().position;
                ExplanationObject.transform.position = pos + new Vector3(0, 350, 0);

                playQuestionSound(ErrorSound);
            }
        }

        public void closeExplanation()
        {
            ExplanationObject.SetActive(false);

            /*AnalyticsResult result = Analytics.CustomEvent("CloseQuestionExplanation",
                    new Dictionary<string, object>
                        {
                            { "QuestionNumber",Statemachine.Instance.questions},
                            { "id", ApplicationSettings.idCode }
                        });
            Debug.Log("analytic result: " + result);
            */
            for (var i = 0; i < 3; i++)
            {
                var tempColor = answers[i].colors;
                tempColor.disabledColor = new Color((float)(200.0 / 255), (float)(200.0 / 255), (float)(200.0 / 255), (float)(128.0 / 255));
                answers[i].colors = tempColor;
                answers[i].interactable = true;
            }
            if (correct)
            {

                questionsDone++;
                if (Statemachine.Instance.questions == 13 || Statemachine.Instance.questions == 26)
                {
                    if (Statemachine.Instance.questions == 13)
                    {
                        Statemachine.Instance.questions = 21;
                    }
                    Statemachine.Instance.questioning = false;
                    QuestionsObject.SetActive(false);
                    Statemachine.Instance.startIntro = true;
                }
                else
                {
                    Statemachine.Instance.questions++;
                    initButtons();
                    Logbook.GetComponent<LogbookController>().setReaction();
                }
                correct = false;
            }
            else
            {
                initButtons();
                correct = false;
            }

        }

        private void playQuestionSound(PlayableSounds sound)
        {
            globalAudioController.playSound(sound);
        }

        private void setExplanation(bool answeredCorrectly)
        {
            if(pHCalculator.calculator.getInd() == 1)
            {
                //user picked incorrectly
                if (answeredCorrectly)
                {
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "Indeed! Phenolphthalein is the suitable indicator. Next time make sure to choose an indicator that has the centre of its colour change range near the pH at the equivalence point. ";
                }
                else
                {
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "The indicator chosen for a titration should have the centre of its colour change range near the pH at the equivalence point. In this experiment, phenolphthalein would have been a better choice as its color range (8.2-10) is close to the equivalence point.";
                }
            }
            if (pHCalculator.calculator.getInd() == 2)
            {
                //user picked correctly
                if (answeredCorrectly)
                {
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "Correct! As it was seen in the experiment, the sample turned pink when approaching the end point of the titration!";
                }
                else
                {
                    ExplanationObject.GetComponentInChildren<TMP_Text>().text = "As in the experiment, the suitable indicator is phenolphthalein as it changes color in the end point of the titration.";
                }
            }
        }
    }
    public struct AnswerEntry
    {
        public string answerText { private set; get; }
        public bool isCorrect { private set; get; }

        public AnswerEntry(string answerText, bool isCorrect)
        {
            this.answerText = answerText;
            this.isCorrect = isCorrect;
        }
    }
}
