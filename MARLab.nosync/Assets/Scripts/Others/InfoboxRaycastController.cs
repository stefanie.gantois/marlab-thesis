﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Interaction;
using UnityEngine;

public class InfoboxRaycastController : MonoBehaviour
{

    public InfoBehaviour infoBehaviour;
    public int[] activeStates;
    public int timeInfoBoxOpen;
    private ObjectInteractionHandler objectInteractionHandler;
    public GameObject colliderObject;
    
    private Camera mainCamera;
    private Ray ray;
    private bool infoTriggered;

    private GameObject hitObject;
    void Start()
    {
        objectInteractionHandler = GetComponent<ObjectInteractionHandler>();
        mainCamera = Camera.main;
    }

    void Update()
    {
        //Close infobox if not in this state and return.
        if (!activeStates.Contains(Statemachine.Instance.stateNumber) || objectInteractionHandler.isGrabbed)
        {
            infoBehaviour.CloseInfo();
            return;
        }
        
        //Cast Ray from center of camera.
        ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

        //Check if ray hit something.
        var hits = Physics.RaycastAll(ray);
        foreach (RaycastHit hit in hits)
        {
            hitObject = hit.collider.gameObject;
            //Did it hit the target object? If so open the Infobox and close it after timeInfoBoxOpen seconds.
            if (hitObject == colliderObject && !infoTriggered)
            {
                infoTriggered = true;
                infoBehaviour.OpenInfoAndCloseAfterSeconds(timeInfoBoxOpen);
            }
        }
    }
}
