﻿using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;

public class SkipTutorialOnExplicitInteraction : MonoBehaviour
{
    public StartARScene startArScene;
    public GameObject onboardinScreen1;
    public GameObject loadingScreen;

    public void Fire()
    {
        onboardinScreen1.SetActive(false);
        loadingScreen.SetActive(true);
        startArScene.LoadArScene();
    }
}
