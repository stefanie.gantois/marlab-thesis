﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARScreenshot : MonoBehaviour
{
    private Texture2D m_Texture;
    private Pose aufbauPoseBeforeScreenshot;

    public RawImage targetImage;
    public ARCameraManager cameraManager;
    public GameObject arCameraHolder;
    public GameObject aufbau;
    public bool screenshotModeOn;
    public GameObject canvas;

    private void Start()
    {
        targetImage.rectTransform.sizeDelta = new Vector2(canvas.GetComponent<RectTransform>().rect.height,
            canvas.GetComponent<RectTransform>().rect.width);
    }

    /// <summary>
    /// Make a AR-Screenshot of the current view.
    /// </summary>
    public void MakeARScreenshot()
    {
        FreezeObjects();
        screenshotModeOn = true;
        // Get information about the device camera image.
        if (cameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
        {
            // If successful, launch a coroutine that waits for the image
            // to be ready, then apply it to a texture.
            StartCoroutine(ProcessImageAndPrepareForScreenshot(image));
            // It's safe to dispose the image before the async operation completes.
            image.Dispose();
        }
    }


    IEnumerator ProcessImageAndPrepareForScreenshot(XRCpuImage image)
    {
        //Reset the texture
        m_Texture = null;

        // Create the async conversion request.
        var request = image.ConvertAsync(new XRCpuImage.ConversionParams
        {
            // Use the full image.
            inputRect = new RectInt(0, 0, image.width, image.height),

            outputDimensions = new Vector2Int(image.width, image.height),

            // Color image format.
            outputFormat = TextureFormat.RGB24,

            // Flip across the Y axis.
            transformation = XRCpuImage.Transformation.MirrorX
        });

        // Wait for the conversion to complete.
        while (!request.status.IsDone())
            yield return null;

        // Check status to see if the conversion completed successfully.
        if (request.status != XRCpuImage.AsyncConversionStatus.Ready)
        {
            // Something went wrong.
            Debug.LogErrorFormat("Request failed with status {0}", request.status);

            //Unfreeze the objects when something went wrong.
            UnfreezeObjects();
            screenshotModeOn = false;

            // Dispose even if there is an error.
            request.Dispose();
            yield break;
        }

        // Image data is ready. Let's apply it to a Texture2D.
        var rawData = request.GetData<byte>();

        // Create a texture if necessary.
        if (m_Texture == null)
        {
            m_Texture = new Texture2D(
                request.conversionParams.outputDimensions.x,
                request.conversionParams.outputDimensions.y,
                request.conversionParams.outputFormat,
                false);
        }

        // Copy the image data into the texture.
        m_Texture.LoadRawTextureData(rawData);
        m_Texture.Apply();

        //Apply sprite to target Image.
        targetImage.texture = m_Texture;
        targetImage.gameObject.SetActive(true);

        //Adjust scaling of image, depending on used device.
        //Note: width and height of the image are reversed, because the CPU-Image is taken in landscape-orientation.
        
        //Determine aspect ratio of the CPU-Image and of the device screen
        float cpuImageAspectRatio = (float)m_Texture.width / m_Texture.height;
        float screenAspectRatio = (float)Screen.height / Screen.width;
        
        //Is either height or width needed to be to be adjusted?
        if (screenAspectRatio > cpuImageAspectRatio)
        {
            //Width needs to be adjusted.
            float targetImageAdjustedWidth = targetImage.rectTransform.rect.width / cpuImageAspectRatio;
            //Reminder: width and height of targetImage.RectTransform are reversed, because it's rotated.
            targetImage.rectTransform.sizeDelta = new Vector2(targetImage.rectTransform.rect.width,targetImageAdjustedWidth);
        }
        else if (screenAspectRatio < cpuImageAspectRatio)
        {
            //Height needs to be adjusted.
            float targetImageAdjustedHeight = targetImage.rectTransform.rect.height * cpuImageAspectRatio;
            targetImage.rectTransform.sizeDelta =
                new Vector2(targetImageAdjustedHeight, targetImage.rectTransform.rect.height);
        }

        // Need to dispose the request to delete resources associated
        // with the request, including the raw data.
        request.Dispose();
    }

    /// <summary>
    /// Freezes the the objects on screen.
    /// </summary>
    private void FreezeObjects()
    {
        aufbau.transform.SetParent(arCameraHolder.transform);
        aufbauPoseBeforeScreenshot = new Pose(aufbau.transform.position, aufbau.transform.rotation);
    }

    /// <summary>
    /// Unfreezes the objects on screen.
    /// </summary>
    private void UnfreezeObjects()
    {
        aufbau.transform.SetParent(null);
        aufbau.transform.position = aufbauPoseBeforeScreenshot.position;
        aufbau.transform.rotation = aufbauPoseBeforeScreenshot.rotation;
    }

    /// <summary>
    /// Re-enables regular AR-Mode.
    /// </summary>
    public void ReactivateARMode()
    {
        targetImage.gameObject.SetActive(false);
        UnfreezeObjects();
        screenshotModeOn = false;
    }
}