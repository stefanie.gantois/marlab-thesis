﻿using System;
using System.Linq;
using UnityEngine;

namespace Others
{
    public class DirectInfoRayCastTrigger : MonoBehaviour
    {
        public int[] activeStates;
        public GameObject colliderObject;
    
        private Camera mainCamera;
        private Ray ray;
        private bool infoTriggered = false;
        public static event Action UpdateInfoBox;

        private GameObject hitObject;
        void Start()
        {
            mainCamera = Camera.main;
        }

        void Update()
        {
            //Close infobox if not in this state and return.
            if (!activeStates.Contains(Statemachine.Instance.stateNumber))
            {
                return;
            }
        
            //Cast Ray from center of camera.
            ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

            //Check if ray hit something.
            var hits = Physics.RaycastAll(ray);
            foreach (RaycastHit hit in hits)
            {
                hitObject = hit.collider.gameObject;
                //Did it hit the target object? If so invoke UpdateInfoBox event.
                if (hitObject == colliderObject && !infoTriggered)
                {
                    infoTriggered = true;
                    UpdateInfoBox?.Invoke();
                }
            }
        }
    }
}
