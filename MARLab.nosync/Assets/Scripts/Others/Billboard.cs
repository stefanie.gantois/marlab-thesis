﻿using UnityEngine;

/// <summary>
/// This script faces an object towards the camera.
/// </summary>
public class Billboard : MonoBehaviour
{
    private Camera mainCamera;

    /// <summary>
    /// Search for the main camera of the scene and assign it at startup of the script
    /// </summary>
    private void Start()
    {
        mainCamera = Camera.main;
    }

    /// <summary>
    /// Orient the camera after all movement is completed this frame to avoid jittering
    /// </summary>
    void LateUpdate()
    {
        //Face object
        var cameraRotation = mainCamera.transform.rotation;
        transform.LookAt(transform.position + cameraRotation * Vector3.forward,
            cameraRotation * Vector3.up);
    }
}