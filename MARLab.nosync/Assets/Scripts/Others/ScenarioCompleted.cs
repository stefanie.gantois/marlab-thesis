﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioCompleted : MonoBehaviour
{
    public GameObject completionOverlay;
    public TextMeshProUGUI duration;
    public TextMeshProUGUI errors;
    public TextMeshProUGUI datas;
    public GameObject interactionButtons;
    public GameObject otherButtons;
    public GameObject crosshair;
    public static bool scenarioIsCompleted;
    public GameObject[] errorIndicator;
    public static List<string> errorHints = new List<string>();
    public GameObject improvementTips;
    public GameObject assistanceInstructions;
    private System.DateTime startTime;

    private void OnEnable()
    {
        startTime = System.DateTime.Now;
        Statemachine.Instance.ScenarioComplete += ShowCompletionOverlay;
    }

    private void OnDisable()
    {
        Statemachine.Instance.ScenarioComplete -= ShowCompletionOverlay;
    }

    private void ShowCompletionOverlay()
    {
        scenarioIsCompleted = true;
        completionOverlay.SetActive(true);
        interactionButtons.SetActive(false);
        otherButtons.SetActive(false);
        crosshair.SetActive(false);
        assistanceInstructions.SetActive(false);

        duration.text = GetDurationText();
        errors.text = ShowErrors();
        datas.text = GetDataTotal();
        ShowErrorIndicator();

    }

    private void ShowErrorIndicator()
    {
        if (Statemachine.Instance.errorTotal > 16)
        {
            errorIndicator[16].SetActive(true);
        }
        else
        {
            errorIndicator[Statemachine.Instance.errorTotal].SetActive(true);
        }
    }

    /// <summary>
    /// Adds a error hint to the container, if the same error hint has not already been added.
    /// </summary>
    public static void AddErrorHint(String hint)
    {
        if (errorHints.Contains(hint))
            return;
        errorHints.Add(hint);
    }

    private string GetDataTotal()
    {
        string dataString = "- You gathered <b>" + ResultsLibrary.InstanceLib.storedVolume.Count.ToString() + " datapoints</b>";
        return dataString;
    }

    private string ShowErrors()
    {
        string errorString = "- You made <b>" + Statemachine.Instance.errorTotal.ToString() + " mistakes</b>";
        return errorString;
    }

    private string GetTimeString()
    {
       System.TimeSpan endTime = System.DateTime.Now - startTime;
       string timeString = "- It took you ";
       if (endTime.Minutes == 1)
       {
           timeString += "<b>" + endTime.Minutes + " minute and </b>";
       }
       else if (endTime.Minutes > 1)
       {
           timeString += "<b>" + endTime.Minutes + " minutes and </b>";
       }

       if (endTime.Seconds == 1)
       {
           timeString += "<b>" + endTime.Seconds + " second. </b>";
       }
       else
       {
           timeString += "<b>" + endTime.Seconds + " seconds.</b>";
       }
       return timeString;
    }

    private string GetDurationText()
    {
        duration.text = GetTimeString();
        return duration.text;
    }
}
