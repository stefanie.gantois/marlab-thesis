﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
    public void loadGrabScene()
    {
        SceneManager.LoadScene("Scenes/Grabbing");
    }
    
    public void loadTouchScene()
    {
        SceneManager.LoadScene("Scenes/Touch");
    }

    public void loadAnnotationScene()
    {
        SceneManager.LoadScene("Scenes/Annotations");
    }
}
