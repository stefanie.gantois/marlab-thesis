﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableThisComponentAfterXSeconds : MonoBehaviour
{
    [Range(0.5f,10.0f)]
    public float seconds = 1.5f;
    // Start is called before the first frame update
    private void Start()
    {
        StartCoroutine(DisableComponent());
    }

    private IEnumerator DisableComponent()
    {
        yield return new WaitForSeconds(seconds);
        this.gameObject.SetActive(false);
    }
}
