﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;

public enum StateForPlacement
{
    nothing,
    grabPieces,
    placeSyringe,
    trash
}



public class ObjectInsideCollider : MonoBehaviour
{
    public StateForPlacement stateForPlacement = StateForPlacement.nothing;
    public string combinedWithName = "";
    
    private bool colliderSleeps = false;
    private bool collisionWasCheckedThisFrame = false;

    private void FixedUpdate()
    {
        collisionWasCheckedThisFrame = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Return if the collision was already checked
        if (collisionWasCheckedThisFrame) return;

        //Check if this collider should be sleeping
        if (colliderSleeps) return;
        
        //Return if the colliding object is not an AR_Interactable
        if (!collision.transform.CompareTag("AR_InteractableCollider")) return;
        
        //sleep the collider so no further checks are performed and wake it up delayed in a coroutine
        colliderSleeps = true;
        StartCoroutine(WakeColliderUp());

        //Handle the collision if the respective statemachine state is active
        var collidedARObject = Interaction.InteractionController.FindARInteractable(collision.gameObject);
        if (Statemachine.Instance.stateForPlacement != this.stateForPlacement)
        {
            if (this.combinedWithName.Equals("Arbeitsfläche")){
                if (collidedARObject.GetComponent<AR_Interactable>().InteractableName.Equals("Spritze") 
                    || collidedARObject.GetComponent<AR_Interactable>().InteractableName.Equals("Kanüle")
                    || collidedARObject.GetComponent<AR_Interactable>().InteractableName.Equals("Spritzenverschluss"))
                {
                    collidedARObject.GetComponent<Interaction.ObjectInteractionHandler>().Combine(this.combinedWithName);
                }
            }
            if (this.combinedWithName.Equals("Kanülenabwurfbehälter") || this.combinedWithName.Equals("Mülleimer"))
            {
                collidedARObject.GetComponent<Interaction.ObjectInteractionHandler>().Combine(this.combinedWithName);
            }
            return;
        }
        collidedARObject.GetComponent<Interaction.ObjectInteractionHandler>().Combine(this.combinedWithName);
        collisionWasCheckedThisFrame = true;
    }

    private IEnumerator WakeColliderUp()
    {
        yield return new WaitForSeconds(1.0f);
        colliderSleeps = false;
    }
}
