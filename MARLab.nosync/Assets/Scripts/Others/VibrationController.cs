﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

public class VibrationController : MonoBehaviour
{
    public float pulseSpeed = 0.75f;
    public bool isVibrating;
    
    public void StartHeartbeat()
    {
        InvokeRepeating(nameof(Heartbeat), 0f, pulseSpeed);
        isVibrating = true;
        //StartCoroutine(StopAfterSeconds(duration));
    }
    
    public void Heartbeat()
    {
        MMVibrationManager.Haptic(HapticTypes.RigidImpact,false,true,this);
    }
    
    private IEnumerator StopAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        CancelInvoke(nameof(Heartbeat));
    }

    public void StopHeartbeat()
    {
        CancelInvoke(nameof(Heartbeat));
    }

}
