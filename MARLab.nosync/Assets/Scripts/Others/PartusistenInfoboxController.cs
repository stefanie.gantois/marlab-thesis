﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartusistenInfoboxController : MonoBehaviour
{
    public InfoBehaviour info;
    private bool wasAlreadyOpened;

    public void OpenInfoBoxAndDisable()
    {
        if (wasAlreadyOpened) return;
        info.OpenInfoAndCloseAfterSeconds(10);
        wasAlreadyOpened = true;
    }
}
