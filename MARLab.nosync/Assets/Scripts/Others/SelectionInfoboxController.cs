﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionInfoboxController : MonoBehaviour
{

    private InfoBehaviour infoBehaviour;
    private bool infoActive = false;
    private Camera mainCamera;

    void Start()
    {
        infoBehaviour = GetComponent<InfoBehaviour>();
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Statemachine.Instance.stateNumber != 10)
        {
            infoBehaviour.CloseInfo();
            return;
        }
        Ray ray = mainCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            GameObject hitObject = hit.collider.gameObject;
            if (hitObject.name == "Tablett" && !infoActive)
            {
                infoBehaviour.OpenInfo();
            }
        }
    }
    
    private IEnumerator SetInfoActiveTrueAfterSeconds(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        infoActive = true;
    }
}
