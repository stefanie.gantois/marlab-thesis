﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;

namespace Others
{
    public class LeaveAndResetARScene : MonoBehaviour
    {
        public ARSession arSessionObject;
        public GameObject loadingScreen;
        public GameObject performanceMonitor;

        public GameObject tempLogbook;
        public GameObject tempGraph;
        public GameObject questions;

        public void Start()
        {
            Debug.Log(performanceMonitor);
            //performanceMonitor = GameObject.Find("Performance Monitor");
            if (performanceMonitor == null)
            {
                performanceMonitor = FindObjectOfType<Tayx.Graphy.GraphyManager> ().gameObject;
            }
            Debug.Log(performanceMonitor);
        }

        /// <summary>
        /// Resets the static variables and returns to the main menu scene.
        /// </summary>
        public void LeaveAndReset()
        {
            performanceMonitor.SetActive(false);
            loadingScreen.SetActive(true);
            ResetStaticClassesAndVariables();
            arSessionObject.Reset();
            questions.SetActive(false);
            ReviewController.controller.enableReview();
            ReviewController.controller.setTemps(tempLogbook, tempGraph);
            StartCoroutine(StartMenuSceneDelayed());
        }

        private IEnumerator StartMenuSceneDelayed()
        {
            yield return new WaitForSeconds(0.2f);
            SceneManager.LoadScene("Scenes/Menu", LoadSceneMode.Single);
        }
        
        private void ResetStaticClassesAndVariables()
        {
            Statemachine.Instance.Reset();
            AssistanceController.errorActive = false;
            AssistanceController.successActive = false;
            CollisionController.intersectedObject = null;
            CollisionController.atleastOneIntersectionDetected = false;
            ScenarioCompleted.scenarioIsCompleted = false;
            InteractionController.originalMaterials = new Dictionary<GameObject, Material>();
            ScenarioCompleted.errorHints = new List<string>();

            InstructionDict.InstanceInstructionDict.Reset();
            //ResultsLibrary.InstanceLib.Reset();
            pHCalculator.calculator.Reset();


        }
    }
}
