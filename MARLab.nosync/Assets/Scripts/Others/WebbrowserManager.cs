﻿using UnityEngine;

public class WebbrowserManager : MonoBehaviour
{
    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }
}
