﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer
{
    public string answerText { private set; get; }
    public string response { private set; get; }
    public bool isCorrectAnswer { private set; get;}
    public bool isDisabled { set; get;}
    public string nextState { set; get;}

    public Answer(string answerText, string response, bool isCorrectAnswer, bool isDisabled)
    {
        this.answerText = answerText;
        this.response = response;
        this.isCorrectAnswer = isCorrectAnswer;
        this.isDisabled = isDisabled;
        this.nextState = "Next";
    }
    
    public Answer(string answerText, string response, bool isCorrectAnswer, bool isDisabled, string nextState)
    {
        this.answerText = answerText;
        this.response = response;
        this.isCorrectAnswer = isCorrectAnswer;
        this.isDisabled = isDisabled;
        this.nextState = nextState;
    }
}
