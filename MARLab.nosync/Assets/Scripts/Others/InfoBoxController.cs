﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InfoBoxController : MonoBehaviour
{
    private List<InfoBehaviour> infos = new List<InfoBehaviour>();
    void Start()
    {
        infos = FindObjectsOfType<InfoBehaviour>().ToList();
    }
    void Update()
    {
        Ray ray = GetComponent<Camera>().ViewportPointToRay(new Vector3(0.5f,0.5f,0f));
        if(Physics.Raycast(ray, out RaycastHit hit))
        {
            GameObject hitObject = hit.collider.gameObject;
            if (hitObject.CompareTag("InfoBox"))
            {
                OpenInfo(hitObject.GetComponent<InfoBehaviour>());
            }
        }
        else
        {
            CloseAll();
        }

        void OpenInfo(InfoBehaviour desiredInfo)
        {
            foreach (InfoBehaviour info in infos)
            {
                if (info == desiredInfo)
                {
                    info.OpenInfo();
                }
                else
                {
                    info.CloseInfo();
                }
            }
        }

        void CloseAll()
        {
            foreach(InfoBehaviour info in infos)
            {
                info.CloseInfo();
            }
        }
    }
}
