﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartARScene : MonoBehaviour
{
    /// <summary>
    /// Loads the (main) AR scene
    /// </summary>
    public void LoadArScene()
    {
        ResultsLibrary.InstanceLib.Reset();
        StartCoroutine(StartARSceneDelayed());
    }
    
    private IEnumerator StartARSceneDelayed()
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene("Scenes/AR_Scenario1", LoadSceneMode.Single);
    }
}
