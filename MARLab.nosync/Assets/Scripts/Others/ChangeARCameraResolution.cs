﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ChangeARCameraResolution : MonoBehaviour
{
    public ResolutionModes cameraResolution;
    public ARCameraManager cameraManager;
    bool init = false;
    NativeArray<XRCameraConfiguration> configurations = new NativeArray<XRCameraConfiguration>();


 
    void OnEnable() {
        cameraManager.frameReceived += OnCameraFrameReceived;
    }
 
    void OnDisable() {
        cameraManager.frameReceived -= OnCameraFrameReceived;
    }

    void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
    {
        if (!init)
        {
            cameraManager.subsystem.currentConfiguration =
                cameraManager.GetConfigurations(Allocator.Temp)[(int)cameraResolution];
            init = true;
        }
    }
    
    private void GetAvailableConfig()
    {
        configurations = cameraManager.GetConfigurations(Allocator.Temp);
 
        foreach(XRCameraConfiguration config in configurations)
        {
            Debug.Log(config);
        }
    }
}

public enum ResolutionModes : int
{
    _640x480 = 0,
    _1280x720 = 1,
    _1920x1080 = 2,
}
