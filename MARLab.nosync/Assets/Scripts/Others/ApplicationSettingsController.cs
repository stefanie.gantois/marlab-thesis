﻿using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;

/// <summary>
/// The ApplicationSettingsController handles the inputs from the static ApplicationSettings class that is used to hold
/// application options from the menus.
/// </summary>
public class ApplicationSettingsController : MonoBehaviour
{
    public GameObject performanceMonitor;
    
    void Start()
    {
        Debug.Log(performanceMonitor);
        //performanceMonitor = GameObject.Find("Performance Monitor");
        if (performanceMonitor == null)
            {
                performanceMonitor = FindObjectOfType<Tayx.Graphy.GraphyManager>().gameObject;
            }
        PerformanceMonitor();
    }

    /// <summary>
    /// Activates the performance monitor in the AR scene based on if the setting is set in the settings menu
    /// </summary>
    private void PerformanceMonitor()
    {
        performanceMonitor.SetActive(ApplicationSettings.displayPerformanceMonitor == true);
        Debug.Log(ApplicationSettings.displayPerformanceMonitor);
    }
}
