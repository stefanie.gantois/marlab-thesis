﻿using System.Collections.Generic;
using System;

[System.Serializable]
public class RegularQuestion : Question
{
   
    public List<Answer> answers { get; private set; }
    private const int maxQuestionAmount = 6;

    //Throws exception if constructor is called with more than 6 answers.
    public RegularQuestion(string questionText, List<Answer> answers)
    {        
        if(answers.Count > maxQuestionAmount)
        {
            throw new TooManyAnswersException("Amount of answers cannot be higher than 6.");
        }
        this.questionText = questionText;
        this.answers = answers;
    }
}

public class TooManyAnswersException : Exception
{
    public TooManyAnswersException(string message) : base(message)
    {
    }
}
