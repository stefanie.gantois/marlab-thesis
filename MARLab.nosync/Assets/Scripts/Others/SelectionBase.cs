﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class SelectionBase : MonoBehaviour
{
    //Editor script to always select this object when a child is selected in the Scene view.
}
