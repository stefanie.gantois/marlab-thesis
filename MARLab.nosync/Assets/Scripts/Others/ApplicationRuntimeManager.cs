﻿using Static;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Others
{
    public class ApplicationRuntimeManager : MonoBehaviour
    {
        /// <summary>
        /// Quits the whole Application
        ///
        /// When in Editor, just stops the preview
        /// </summary>
        public void QuitApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

        /// <summary>
        /// Starts a Training with the provided ID
        /// </summary>
        /// <param name="trainingID"></param>
        public void StartTraining(int trainingID)
        {
            switch (trainingID)
            {
                case 1:
                    SceneManager.LoadScene("Scenes/Tutorial_Scenario1", LoadSceneMode.Single);
                    break;
                case 2:
                    SceneManager.LoadScene("Scenes/Tutorial_Scenario1", LoadSceneMode.Single);
                    ReviewController.controller.SetShow(true);
                    break;
                /*
                case 3:
                    SceneManager.LoadScene("Scenes/AR_Scenario3", LoadSceneMode.Single);
                    break;
                */
                default:
                    Debug.LogError("ApplicationRuntimeManager: Training with this ID could not be found.");
                    break;
            }
        }
    }
}
