﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBackToMenuScene : MonoBehaviour
{
    /// <summary>
    /// Returns the application back to the start menu scene
    /// </summary>
    public void BackToMenu()
    {
        SceneManager.LoadScene("Scenes/Menu", LoadSceneMode.Single);
    }
}
