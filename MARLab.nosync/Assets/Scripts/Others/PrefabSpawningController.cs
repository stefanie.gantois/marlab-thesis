﻿using System;
using System.Collections.Generic;
using Static;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Random = UnityEngine.Random;

namespace Others
{
    public enum MinPlanesize{small, middle, large};

    public class PrefabSpawningController : MonoBehaviour
    {
        [Header("Script References:")]
        public Camera arCamera;
        public GameObject aRSessionOrigin;
        public ARPlaneManager arPlaneManager;
        public ARAnchorManager arAnchorManager;
        public ARRaycastManager arRaycastManager;

        [Header("Prefabs:")]
        public GameObject spawnedPrefab;
        public GameObject infinityPlanePrefab;

        [Header("Options:")]
        public Material spawningMaterial;
        //[Range(0.0f, 2.0f)]
        //public float minPlanearea =0.5f;
        //public enum MinPlanesize{small, middle, large};
        //public MinPlanesize MinPlanearea;

        [HideInInspector]
        public bool infinityPlaneWasSpawned = false;
        [HideInInspector]
        public bool objectWasSpawned = false;
        [HideInInspector]
        public bool materialsWereReset = false;
        [HideInInspector]
        public bool placementPoseIsValid = false;


        private GameObject instantiatedPrefab;
        private GameObject instantiatedInfinityPlanePrefab;
        private Pose placementPose;
        private ARPlane plane;
        private String instructionTextWhenPrefabSpawned = "Put on protective gloves";
        private bool isLabeling = false;
        private bool isValidPlaneSize = false;
        private float minPlanearea;

        private Dictionary<GameObject, Material[]> originalMaterials = new Dictionary<GameObject, Material[]>();

        public static event Action prefabSpawned;
        public static event Action RepositionPrefab;


        private void Start()
        {
            arRaycastManager = FindObjectOfType<ARRaycastManager>();
            //Instantiate the prefab 2m behind the camera
            instantiatedPrefab = Instantiate(spawnedPrefab, new Vector3(0, 0, -2), Quaternion.identity);
            MakeMaterialsInvisibleForPlacement();

            switch(Static.ApplicationSettings.planeSize)
            {
                case MinPlanesize.small:
                minPlanearea = 0.2f;
                Debug.Log("sizeS");
                break;

                case MinPlanesize.middle:
                minPlanearea = 0.8f;
                Debug.Log("sizeM");
                break;

                case MinPlanesize.large:
                minPlanearea = 2.0f;
                Debug.Log("sizeL");
                break;

                default:
                Debug.LogError("Error");
                break;
            }
       }

        private void Update()
        {
            if(!objectWasSpawned)
            {
                PositionPrefab();

                //Spawn the construction with a touch on the screen
                if (Input.touchCount < 1) return;
                Touch touch = Input.GetTouch(0);
                //If Touch has been registered and placement pose has been declared valid, spawn the prefab
                if (touch.phase == TouchPhase.Began && placementPoseIsValid)
                {
                    SpawnPrefab();
                }
            }
            else if(infinityPlaneWasSpawned && objectWasSpawned && !materialsWereReset)
            {
                //Invoke the Event that the prefab was spawned
                prefabSpawned?.Invoke();
                //Reset the materials back to original
                ResetMaterialsToOriginal();
                materialsWereReset = true;
                var points = aRSessionOrigin.GetComponent<ARPointCloudManager>().trackables;
                aRSessionOrigin.GetComponent<ARPointCloudManager>().enabled = false;
                foreach(var pts in points)
                {
                    pts.gameObject.SetActive(false);
                }
            }
        }

        private void PositionPrefab()
        {
            if (Camera.current == null) return;
            var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
            var hits = new List<ARRaycastHit>();

            arRaycastManager.Raycast(screenCenter, hits, TrackableType.PlaneWithinBounds);

          //  placementPoseIsValid = hits.Count > 0;
            if (hits.Count > 0)
            {
                plane = arPlaneManager.GetPlane(hits[0].trackableId);

                if (plane.size.x * plane.size.y >= minPlanearea && plane.subsumedBy == null && plane.alignment == PlaneAlignment.HorizontalUp)
                {
                    placementPose = hits[0].pose;
                    placementPoseIsValid = true;
                }
                else
                {
                    instantiatedPrefab.transform.SetPositionAndRotation(new Vector3(0, 0, -2), Quaternion.identity);
                    placementPoseIsValid = false;
                    return;
                }
                //Set the new position and rotation to the object
                instantiatedPrefab.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
                //Let the object face the camera
                instantiatedPrefab.transform.LookAt(arCamera.transform);


                //Delete every rotation except the y rotation (to have it face towards the ground
                Quaternion newRotation = Quaternion.Euler(0, instantiatedPrefab.transform.rotation.eulerAngles.y, 0);
                //Set the new position and rotation to the object
                instantiatedPrefab.transform.SetPositionAndRotation(placementPose.position, newRotation);

            }
            else
            {
                instantiatedPrefab.transform.SetPositionAndRotation(new Vector3(0, 0, -2), Quaternion.identity);
                placementPoseIsValid = false;
            }
        }

        public void SpawnPrefab()
        {
            //Add an anchor attached to this plane for better tracking
            ARAnchor planeAnchor = arAnchorManager.AttachAnchor(plane, placementPose);

            //Instantiate the infinityPlanePrefab at this position
            instantiatedInfinityPlanePrefab = Instantiate(infinityPlanePrefab, placementPose.position, placementPose.rotation, planeAnchor.gameObject.transform);
            //Register that the infinity plane was spawned
            infinityPlaneWasSpawned = true;

            instantiatedPrefab.transform.SetParent(instantiatedInfinityPlanePrefab.transform);
            instantiatedPrefab.transform.localPosition = new Vector3(instantiatedPrefab.transform.localPosition.x, 0f , instantiatedPrefab.transform.localPosition.z);
            instantiatedPrefab.transform.localRotation = Quaternion.Euler(0, instantiatedPrefab.transform.localRotation.eulerAngles.y, 0);

            //Register that the prefab was positioned
            objectWasSpawned = true;
            //Statemachine.Instance.instruction = "Führen Sie eine Flächendesinfektion durch.";

            if(ApplicationSettings.scenarioMode == ScenarioMode.trainingMode)
			    Statemachine.Instance.instruction = StringDict.InstanceStringDict.lookupString(StoredTexts.disinfectWorkspace);
            else if (ApplicationSettings.scenarioMode == ScenarioMode.examMode)
                Statemachine.Instance.instruction =
                    StringDict.InstanceStringDict.lookupString(StoredTexts.disinfectWorkspaceExamMode);

			Statemachine.Instance.instruction = instructionTextWhenPrefabSpawned;
			//In case this isn't the initial spawning of the prefab, but a reposition, re-apply the labeling state before the reposition was initiated.
            Statemachine.Instance.labeling = isLabeling;
            //Statemachine.Instance.questioning = true;
            //Statemachine.Instance.startQuestioning = true;
            if (Statemachine.Instance.stateNumber == 0)
            {
                Statemachine.Instance.startIntro = true;
            }
            
        }

        public GameObject getSpawnedObject()
        {
            return instantiatedPrefab;
        }

        /// <summary>
        /// Replaces all materials on the spawned prefab with a transparent material and stores the reference
        /// to the original one to reset them later.
        /// </summary>
        private void MakeMaterialsInvisibleForPlacement()
        {
            //Get all the renderers in the childs of the spawned prefab
            Renderer[] objectRenderers = instantiatedPrefab.GetComponentsInChildren<Renderer>();
            foreach (Renderer objectRenderer in objectRenderers)
            {

                //Store the reference to the object and its original material to reset them after placement
                originalMaterials.Add(objectRenderer.gameObject, objectRenderer.materials);

                //Replace all the materials with the transparent spawning material
                Material[] thisRenderersMaterials = new Material[objectRenderer.materials.Length];
                for (int j = 0; j < objectRenderer.materials.Length; j++)
                {
                    thisRenderersMaterials[j] = spawningMaterial;
                }
                objectRenderer.materials = thisRenderersMaterials;
            }
        }

        /// <summary>
        /// Resets the materials of objects to their original ones.
        /// </summary>
        private void ResetMaterialsToOriginal()
        {
            foreach (KeyValuePair<GameObject, Material[]> obj in originalMaterials)
            {
                obj.Key.GetComponent<Renderer>().materials = obj.Value;
            }
        }

        public void OnRepositionButtonPressed()
        {
            if (objectWasSpawned == false)
            {
                Debug.LogWarning("PrefabSpawningController: Tried to reset preafb while already spawning/positioning it. Terminating function.");
                return;
            }

            //Unparent the instantiated prefab from the infinity plane and hide it behind the camera.
            instantiatedPrefab.transform.parent = null;
            instantiatedPrefab.transform.SetPositionAndRotation(new Vector3(0, 0, -2), Quaternion.identity);

            //Reset the originalMaterials container and remake the instantiated prefab transparent for placement.
            originalMaterials = new Dictionary<GameObject, Material[]>();
            MakeMaterialsInvisibleForPlacement();
            materialsWereReset = false;

            //Destroy the now unused infinity plane.
            Destroy(instantiatedInfinityPlanePrefab);

            //Set these booleans accordingly.
            infinityPlaneWasSpawned = false;
            objectWasSpawned = false;
            //isValidPlaneSize = false;

            //Safe the current instruction text. The Statemachine instruction text is set in SpawnPrefab().
            instructionTextWhenPrefabSpawned = Statemachine.Instance.instruction;

            //Invoke the Reposition Prefab event.
            RepositionPrefab?.Invoke();

            //In case the syringe labeling is happening right now, save this to enable it again when the prefab has been repositioned. Also set labeling to false, so labeling gets disabled for now.
            isLabeling = Statemachine.Instance.labeling;
            Statemachine.Instance.labeling = false;
        }
    }
}
