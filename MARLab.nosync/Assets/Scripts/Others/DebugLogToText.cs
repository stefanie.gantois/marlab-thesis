﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Static;

public class DebugLogToText : MonoBehaviour
{
         private string filePath;

         public void Awake()
         {
           filePath = Application.persistentDataPath+"/"+ApplicationSettings.idCode+".txt";
           Debug.Log("ExternalLoggerComponent -> Awake");
         }

         public void OnEnable()
         {
           Debug.Log("ExternalLoggerComponent -> OnEnable");

           Application.logMessageReceivedThreaded += HandleLog;
         }

         public void OnDisable()
         {
           Debug.Log("ExternalLoggerComponent -> OnDisable");

           Application.logMessageReceivedThreaded -= HandleLog;
         }

         public void HandleLog(string logString, string stackTrace, LogType type)
         {
           string logType = "None";

            switch(type)
            {
              case LogType.Error:
              logType = "Error: ";
              break;

              case LogType.Warning:
              logType = "Warning: ";
              break;
              case LogType.Log:
              logType = "Log: ";
              break;

              case LogType.Assert:
              logType = "Assert: ";
              break;

              case LogType.Exception:
              logType = "Exception: ";
              break;

              default:
              logType = "Other: ";
              break;
           }

           if(ApplicationSettings.loggingData)
           {
            StreamWriter writer = new StreamWriter(filePath, true);
            writer.WriteLine(System.DateTime.Now +" "+ logType + logString);
            writer.Close();
           }
         }
}
