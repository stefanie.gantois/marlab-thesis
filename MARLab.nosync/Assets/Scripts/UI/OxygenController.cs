﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class OxygenController : MonoBehaviour
{
    public TextMeshProUGUI oxy;
    public int oxySaturation;
    public bool oxyActive = false;

    void Update()
    {
        if (oxyActive)
        {
            StartCoroutine(ChangeOxy());
        }
    }

    private IEnumerator ChangeOxy()
    {
        oxyActive = false;
        yield return new WaitForSeconds(2f);
        int newValue = Random.Range(oxySaturation - 3, oxySaturation + 3);
        oxy.text = newValue + "%";
        oxyActive = true;
    }

    public void SetSaturation(int saturation)
    {
        oxySaturation = saturation;
    }
    
    public void SetOxyActive(bool isActive)
    {
        oxyActive = isActive;
    }
}
