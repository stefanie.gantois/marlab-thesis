﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Ilumisoft.VisualStateMachine;
using System.Linq;
using System.Threading;
using TMPro;
using UnityEngine.EventSystems;


public class QuestionnaireController : MonoBehaviour
{
    public GameObject regularQuestionUI;
    public GameObject inputFieldQuestionUI;
    private RegularQuestion currentRegularQuestion;
    private InputFieldQuestion currentInputFieldQuestion;
    public List<Button> buttons;
    public List<Button> inputFieldButtons;
    public TextMeshProUGUI regularQuestionText;
    public TextMeshProUGUI inputFieldQuestionText;
    private AudioSource audioSource;
    public AudioClip correctAnswerSound;
    public AudioClip wrongAnswerSound;
    public Color defaultDisabledButtonColor;
    public Color correctButtonColor = new Color(0f / 255, 255f / 255, 0f / 255);
    public Color wrongButtonColor = new Color(1f, 0f, 0f);
    public bool isLoopMode;
    public StateMachine stateMachine;
    private bool isFirstInput = true;
    public TimerController timer;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InitRegularQuestionButtons();
        //InitTestQuestions();
    }

    private void InitTestQuestions()
    {
        List<Answer> answers = new List<Answer>
        {
            new Answer("Langabnabeln des Kindes.", "Falsch", true, false),
            new Answer("Hinzuziehen eines Pädiaters.", "Falsch", false, false),
            new Answer("Informieren der Eltern, dass das Kind Unterstützung bei der Anpassung benötigt.", "Falsch", false, false),
            new Answer("Plusoxymetrie an der rechten Hand befestigen.", "Falsch", false, true)
        };
        Question q1 =
            new RegularQuestion(
                "Beurteilen Sie die Herzfrequenz des Neugeborenen. Die Atmung des Kindes ist flach und der Muskeltonus schlaff. Welche weitere Maßnahme sind vorzunehmen?",
                answers);
       // InitNewQuestion(q1);

        Question q2 = new InputFieldQuestion("Geben Sie die Atemfrequenz des Neugeborenen an: ", "60", "Richtig", "Falsch");

        InitNewQuestion(q2);

        /*List<Answer> answersQ1 = new List<Answer>();
        List<Answer> answersQ2 = new List<Answer>();
        List<Answer> answersQ3 = new List<Answer>();

        answersQ1.Add(new Answer("Richtige Antwort", "Response1", true));
        answersQ1.Add(new Answer("Falsche Antwort", "Response2", false));*

        answersQ2.Add(new Answer("Falsche Antwort", "Response1", false));
        answersQ2.Add(new Answer("Richtige Antwort", "Response2", true));
        answersQ2.Add(new Answer("Richtige Antwort", "Response3", true));
        answersQ2.Add(new Answer("Falsche Antwort", "Response4", false));

        answersQ3.Add(new Answer("Falsche Antwort", "Response1", false));
        answersQ3.Add(new Answer("Richtige Antwort", "Response2", true));
        answersQ3.Add(new Answer("Falsche Antwort", "Response3", false));
        answersQ3.Add(new Answer("Falsche Antwort", "Response4", false));
        answersQ3.Add(new Answer("Eine Antwort mit sehr viel Text um zu testen, ob die Schriftgröße und der Zeilenumbruch wie gewünscht funktionieren.", "Response5", true));
        answersQ3.Add(new Answer("Richtige Antwort", "Response6", true));

        Question test = new Question("eine Frage", answersQ1);
        Question test2 = new Question("noch eine Frage", answersQ2);
        Question test3 = new Question("Eine Dritte Frage mit sehr viel Text, umzu testen ob die Schriftgröße sich korrekt anpasst.", answersQ3);
        questions.Add(test);
        questions.Add(test2);
        questions.Add(test3);*/
    }

    public void InitNewQuestion(Question question)
    {
        timer.PauseTimer();
        if (question is RegularQuestion regularQuestion)
        {
            inputFieldQuestionUI.SetActive(false);
            regularQuestionUI.SetActive(true);
            currentRegularQuestion = regularQuestion;
            Shuffle(currentRegularQuestion.answers);
            ShowCurrentRegularQuestion();
        }
        else if (question is InputFieldQuestion inputFieldQuestion)
        {
            inputFieldQuestionUI.SetActive(true);
            regularQuestionUI.SetActive(false);
            currentInputFieldQuestion = inputFieldQuestion;
            ShowCurrentInputFieldQuestion();
        }
    }

    private void ShowCurrentInputFieldQuestion()
    {
        inputFieldQuestionText.text = currentInputFieldQuestion.questionText;
        isFirstInput = true;
    }
  
    //Reset buttons and show the current question with answers
    private void ShowCurrentRegularQuestion()
    {
        EnableAllButtons();
        //EventSystem.current.SetSelectedGameObject(null);
        var question = currentRegularQuestion;
        regularQuestionText.text = question.questionText;

        if (question.answers.Count < buttons.Count)
        {
            int diff = buttons.Count - question.answers.Count;
            for (int i = 1; i <= diff; i++)
            {
                Debug.Log(buttons.Count);
                buttons[buttons.Count - i].gameObject.SetActive(false);
            }
        }

        for (int i = 0; i < question.answers.Count; i++)
        {
            buttons[i].GetComponentInChildren<TextMeshProUGUI>().text = question.answers[i].answerText;
        }
    }

    // Initialize buttons with onClick-Event.
    private void InitRegularQuestionButtons()
    {
        for(int i = 0; i < buttons.Count; i++)
        { 
            Button button = buttons[i];
            int buttonIndex = i;
            button.onClick.AddListener( () => ValidateAnswer(buttonIndex) );
        }
    }

    //Validate Answers when a button get's clicked.
    private void ValidateAnswer(int i)
    {
        DisableAllButtons();
        var currentQ = currentRegularQuestion;
        if(currentQ.answers[i].isCorrectAnswer)
        {
            PlayCorrectAnswerSound();
            StartCoroutine(CorrectAnswerResponse(i));
        }
        else
        {
            PlayWrongAnswerSound();
            regularQuestionText.text = currentQ.answers[i].response;
            StartCoroutine(SetQuestionBackAfterDelay(i));
        }
    }

    private void DisableAllButtons()
    {
        foreach(Button b in buttons)
        {
            b.interactable = false;
        }
    }
    
    /// <summary>
    /// Enables all buttons, but disables those, who are marked as such in their Answer object.
    /// </summary>
    private void EnableAllButtons()
    {
        int index = 0;
        foreach (Answer a in currentRegularQuestion.answers)
        {
            buttons[index].interactable = !currentRegularQuestion.answers[index].isDisabled;
            buttons[index].gameObject.SetActive(true);
            index++;
        }
    }

    private void PlayCorrectAnswerSound()
    {
        audioSource.clip = correctAnswerSound;
        audioSource.Play();
    }

    private void PlayWrongAnswerSound()
    {
        audioSource.clip = wrongAnswerSound;
        audioSource.Play();
    }

    //Colors specified button red, shows response and resets to current questions after 3 seconds.
    private IEnumerator SetQuestionBackAfterDelay(int buttonIndex)
    {
        var tempColor = buttons[buttonIndex].colors;
        tempColor.disabledColor = Color.red;
        buttons[buttonIndex].colors = tempColor;
        yield return new WaitForSeconds(3f);
        tempColor.disabledColor = defaultDisabledButtonColor;
        buttons[buttonIndex].colors = tempColor;
        currentRegularQuestion.answers[buttonIndex].isDisabled = true;
        ShowCurrentRegularQuestion();
    }
    
    
    private IEnumerator SetInputFieldBackAfterDelay()
    {
        yield return new WaitForSeconds(3f);
        EnableInputFieldButtons();
        inputFieldQuestionText.text = currentInputFieldQuestion.questionText;
        isFirstInput = true;
    }

    ///Colors specified button green and initializes the next question.
    
    private IEnumerator CorrectAnswerResponse(int buttonIndex)
    {
        var tempColor = buttons[buttonIndex].colors;
        tempColor.disabledColor = Color.green;
        buttons[buttonIndex].colors = tempColor;
        yield return new WaitForSeconds(1f);
        regularQuestionUI.SetActive(false);
        Debug.Log(currentRegularQuestion.answers[buttonIndex].nextState);
        NotifyStatemachine(currentRegularQuestion.answers[buttonIndex].nextState);
        
        /* if (currentQuestionIndex >= questions.Count - 1)
        {
            if (isLoopMode)
            {
                currentQuestionIndex = 0;
                tempColor.disabledColor = defaultDisabledButtonColor;
                buttons[buttonIndex].colors = tempColor;
                ShowCurrentQuestion();
            }
            else
            {
                questionText.text = "Fragen zuende";
                HideAnswers();
            }
        }
        */
        
        tempColor.disabledColor = defaultDisabledButtonColor;
        buttons[buttonIndex].colors = tempColor;
        
    }

    void NotifyStatemachine(string nextState)
    {
        timer.StartTimer();
        stateMachine.TriggerByLabel(nextState);
    }

    private void HideAnswers()
    {
        foreach (var t in buttons)
        {
            t.gameObject.SetActive(false);
        }
    }
    
    private void Shuffle<T>(IList<T> list)
    {
        System.Random rng = new System.Random();
        int n = list.Count;  
        while (n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    /// <summary>
    /// Updates the question text with the given input.
    /// Is called by the input field number buttons.
    /// </summary>
    /// <param name="input">The value of the button pressed.</param>
    public void UpdateInput(int input)
    {
        if (isFirstInput)
        {
            isFirstInput = false;
            inputFieldQuestionText.text = input.ToString();
        }
        else
        {
            inputFieldQuestionText.text += input;
        }
    }

    /// <summary>
    /// Removes one Char of the input.
    /// Is called by the Remove Button of the input field.
    /// </summary>
    public void RemoveInput()
    {
        if (inputFieldQuestionText.text.Length > 0)
        {
            inputFieldQuestionText.text = inputFieldQuestionText.text.Remove(inputFieldQuestionText.text.Length - 1);
        }
    }

    public void ValidateInputFieldAnswer()
    {
        if (currentInputFieldQuestion.CorrectAnswer == inputFieldQuestionText.text)
        {
            inputFieldQuestionText.text = currentInputFieldQuestion.CorrectAnswerFeedback;
            NotifyStatemachine("Next");
            inputFieldQuestionUI.SetActive(false);
        }
        else
        {
            inputFieldQuestionText.text = currentInputFieldQuestion.WrongAnswerFeedback;
            DisableInputFieldButtons();
            StartCoroutine(SetInputFieldBackAfterDelay());
        }
    }

    private void DisableInputFieldButtons()
    {
        foreach (Button b in inputFieldButtons)
        {
            b.interactable = false;
        }
    }
    
    private void EnableInputFieldButtons()
    {
        foreach (Button b in inputFieldButtons)
        {
            b.interactable = true;
        }
    }
    

}

    


