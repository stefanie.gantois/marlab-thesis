﻿using System.Collections;
using System.Collections.Generic;
using Others;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;

namespace UI
{
    public class DirectInfoBoxController : MonoBehaviour
    {
        public GameObject InfoboxUI;
        public GameObject cartoon;
        public GameObject questionMark;
        public TextMeshProUGUI directInfotext;
        public AudioClip[] audioData;
        public AudioSource audioPlayer;
        public bool muted = true;
        private bool secondCallInSameState = false;

        private void OnEnable()
        {
            Statemachine.Instance.UpdateInfoBox += ShowInfobox;
            DirectInfoRayCastTrigger.UpdateInfoBox += ShowInfobox;
            muted = Static.ApplicationSettings.muted;
            
        }

        private void OnDisable()
        {
            Statemachine.Instance.UpdateInfoBox -= ShowInfobox;
            DirectInfoRayCastTrigger.UpdateInfoBox -= ShowInfobox;
        }

        /// <summary>
        /// Enables a direct info box and plays the corresponding soundclip.
        /// Also disables the direct info box, after soundclip is done playing.
        /// </summary>
        private void ShowInfobox()
        {
            int hintIndex = InstructionDict.InstanceInstructionDict.getFirstToDoHint();
            string text = InstructionDict.InstanceInstructionDict.lookUpHint(hintIndex);
            InfoboxUI.GetComponentInChildren<TextMeshProUGUI>().text = text;
            InfoboxUI.SetActive(true);

            cartoon.SetActive(true);
            questionMark.SetActive(false);

            AnalyticsResult result = Analytics.CustomEvent("ShowHint",
                new Dictionary<string, object>
                {
                        { "hintNumber",  hintIndex},
                        { "stateNumber", Statemachine.Instance.stateNumber},
                        { "id", ApplicationSettings.idCode}
                }
            );
            Debug.Log("analytic result: " + result);

            StartCoroutine(ShownCoroutine());
        }

        public void ShowTitrationInfobox()
        {
            InfoboxUI.GetComponentInChildren<TextMeshProUGUI>().text = "You should titrate a little longer. You have not reached the equivalence point yet. If you chose the correct indicator, you will notice the colorchange.";
            InfoboxUI.SetActive(true);

            cartoon.SetActive(true);
            questionMark.SetActive(false);

            AnalyticsResult result = Analytics.CustomEvent("ShowTitrationHint",
                new Dictionary<string, object>
                {
                        { "id", ApplicationSettings.idCode}
                }
            );
            Debug.Log("analytic result: " + result);

            StartCoroutine(ShownCoroutine());
        }

        /// <summary>
        /// Starts the soundclip and starts the coroutine to close the direct info box.
        /// </summary>
        private void PlayClip(AudioClip clip)
        {
            if (!muted)
            {
                const int delay = 2;
                audioPlayer.clip = clip;
                audioPlayer.Play();
                StartCoroutine(CloseAfterSeconds(audioPlayer.clip.length + delay));
            }
            else
            {
                StartCoroutine(CloseAfterSeconds(10));
            }
        }

        enum Clip
        {
            Disinfect,
            PrepareMedication,
            OpenPackages,
            OpenPartusisten,
            FillSyringe,
            Label
        }

        private IEnumerator ShownCoroutine()
        {
            yield return new WaitForSeconds(10);

            InfoboxUI.SetActive(false);
            cartoon.SetActive(false);
            questionMark.SetActive(true);
        }

        /// <summary>
        /// Closes the direct info box after given time.
        /// </summary>
        private IEnumerator CloseAfterSeconds(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            if (!audioPlayer.isPlaying)
            {
                InfoboxUI.SetActive(false);
                cartoon.SetActive(false);
                questionMark.SetActive(true);
            }
        }
    }
}