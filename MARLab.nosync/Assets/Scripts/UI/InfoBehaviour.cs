﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InfoBehaviour : MonoBehaviour
{
    private const float Speed = 6f;
    public Transform info;
    private Vector3 desiredScale = Vector3.zero;
    // Start is called before the first frame update
    // Update is called once per frame
    void Update()
    {
        info.localScale = Vector3.Lerp(info.localScale, desiredScale, Time.deltaTime * Speed);
    }

    /// <summary>
    /// Openes the attached infobox.
    /// </summary>
    public void OpenInfo()
    {
        desiredScale = Vector3.one;
    }

    /// <summary>
    /// Closes the attached infobox.
    /// </summary>
    public void CloseInfo()
    {
        desiredScale = Vector3.zero;
    }

    /// <summary>
    /// Openes the attached infobox and closes it after given time.
    /// </summary>
    /// <param name="seconds">The amount of time after which the infobox is closed again in seconds.</param>
    public void OpenInfoAndCloseAfterSeconds(int seconds)
    {
        OpenInfo();
        StartCoroutine(CloseAfterSeconds(seconds));
    }

    private IEnumerator CloseAfterSeconds(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        CloseInfo();
    }
}
