﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMuted : MonoBehaviour
{
    public void setMuted(bool muted)
    {
        Static.ApplicationSettings.muted = muted;
    }
}
