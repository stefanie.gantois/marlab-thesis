﻿using Others;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Video;
using Static;

namespace UI
{
    public class OnBoardingController : MonoBehaviour
    {
        [Header("References: ")]
        public ARCameraManager m_CameraManager;
        public ARSession session;
        public Text stateText;
        public Text reasonText;
        public PrefabSpawningController PrefabSpawningController;
        public GameObject videoFrameMoveDevice;
        public StreamVideo videoClipMoveDevice;
        public VideoPlayer videoPlayerMoveDevice;
        public GameObject videoFrameTapToPlace;
        public StreamVideo videoClipTapToPlace;
        public GameObject insufficientLight;
        public GameObject excessiveMotion;
        public GameObject insufficientFeatures;
        public GameObject defaultTracking;
        public Text otherReasons;
        public GameObject recordSymbol;

        bool displayTapToPlace = false;
        bool displayMoveDevice = true;
        private bool moveDeviceClipWasLoaded = false;
        private bool TapToPlaceClipWasLoaded = false;

        private int moveDeviceLoops = 0;

        /// <summary>
        /// Toggles the MoveDevice animation on and off
        /// </summary>
        /// <param name="toggleState">whether to toggle on or off</param>
        private void ToggleMoveDeviceAnimation(bool toggleState)
        {
            if (toggleState) //On
            {
                //Initial start, load the clip
                if (!moveDeviceClipWasLoaded)
                {
                    moveDeviceClipWasLoaded = true;
                    videoClipMoveDevice.StartVideo();
                    videoFrameMoveDevice.transform.localScale = Vector3.one;
                }
                else
                {
                    videoFrameMoveDevice.transform.localScale = Vector3.one;
                }
            }
            else // Off
            {
                videoFrameMoveDevice.transform.localScale = Vector3.zero;
                moveDeviceLoops = 0;
            }
        }

        /// <summary>
        /// Toggles the TapToPlace animation on and off
        /// </summary>
        /// <param name="toggleState">whether to toggle on or off</param>
        private void ToggleTapToPlaceAnimation(bool toggleState)
        {
            if (toggleState) //On
            {
                //Initial start, load the clip
                if (!TapToPlaceClipWasLoaded)
                {
                    TapToPlaceClipWasLoaded = true;
                    videoClipTapToPlace.StartVideo();
                    videoFrameTapToPlace.transform.localScale = Vector3.one;
                }
                else
                {
                    videoFrameTapToPlace.transform.localScale = Vector3.one;
                }
            }
            else // Off
            {
                videoFrameTapToPlace.transform.localScale = Vector3.zero;
            }
        }

        private void Update()
        {
            TrackingstateDependentOnboarding();
            ShowTrackingInformation();
        }

        void OnEnable()
        {
            if (m_CameraManager != null)
                m_CameraManager.frameReceived += FrameChanged;

            PrefabSpawningController.prefabSpawned += PlacedObject;
            PrefabSpawningController.RepositionPrefab += OnRepositionPrefab;

            ToggleMoveDeviceAnimation(true);

            recordSymbol.SetActive(ApplicationSettings.loggingData);


        }

        void OnDisable()
        {
            ToggleMoveDeviceAnimation(false);
            ToggleTapToPlaceAnimation(false);

            if (m_CameraManager != null)
                m_CameraManager.frameReceived -= FrameChanged;

            PrefabSpawningController.prefabSpawned -= PlacedObject;
            PrefabSpawningController.RepositionPrefab -= OnRepositionPrefab;
        }

        void FrameChanged(ARCameraFrameEventArgs args)
        {
            if (PrefabSpawningController.placementPoseIsValid && moveDeviceLoops>2)
            {
                ToggleMoveDeviceAnimation(false);
                ToggleTapToPlaceAnimation(true);

                displayTapToPlace = true;
                displayMoveDevice = false;
            }
            else if (!PrefabSpawningController.placementPoseIsValid)
            {
                ToggleTapToPlaceAnimation(false);
                ToggleMoveDeviceAnimation(true);

                displayTapToPlace = false;
                displayMoveDevice = true;

                videoPlayerMoveDevice.loopPointReached += EndReached;

            }
        }

        void EndReached(UnityEngine.Video.VideoPlayer vp)
        {
          moveDeviceLoops++;
        }

        /// <summary>
        /// The aufbau Prefab was spawned
        /// </summary>
        void PlacedObject()
        {
            //if (!displayTapToPlace) return;
            ToggleMoveDeviceAnimation(false);
            ToggleTapToPlaceAnimation(false);

            //Deactivate the videoframes
            videoFrameMoveDevice.SetActive(false);
            videoFrameTapToPlace.SetActive(false);

            //Clip onload automatically when the frame is disabled
            moveDeviceClipWasLoaded = false;
            TapToPlaceClipWasLoaded = false;

            //Display no animation
            displayMoveDevice = false;
            displayTapToPlace = false;
        }

        /// <summary>
        /// Starting the repositioningphase of the prefab spawning
        /// </summary>
        void OnRepositionPrefab()
        {
            //Activate the videoframes
            videoFrameMoveDevice.SetActive(true);
            videoFrameTapToPlace.SetActive(true);

            //Display the move device animation
            displayMoveDevice = true;
            displayTapToPlace = false;

            //Toggle the animation on
            ToggleMoveDeviceAnimation(true);
        }

        /// <summary>
        /// Function that is used to track the current reason for tracking problems
        /// </summary>
        void TrackingstateDependentOnboarding()
        {
            stateText.text = session.subsystem.trackingState.ToString();
            reasonText.text = session.subsystem.notTrackingReason.ToString();
        }

        /// <summary>
        /// Shows overlay dependend on the tracking status
        /// </summary>
        void ShowTrackingInformation()
        {

            insufficientLight.SetActive(false);
            insufficientFeatures.SetActive(false);
            excessiveMotion.SetActive(false);
            defaultTracking.SetActive(false);

            if(reasonText.text != "None"&&(displayMoveDevice || displayTapToPlace))
            {
              ToggleMoveDeviceAnimation(false);
              ToggleTapToPlaceAnimation(false);
              displayMoveDevice = false;
              displayTapToPlace = false;

            }

            switch (reasonText.text){
              case "InsufficientFeatures":
                  insufficientFeatures.SetActive(true);
                  Debug.Log("Lost tracking because of insufficient features");
              break;

              case "InsufficientLight":
                  insufficientLight.SetActive(true);
                  Debug.Log("Lost tracking because of insufficient light");
              break;

              case "ExcessiveMotion":
                  excessiveMotion.SetActive(true);
                  Debug.Log("Lost tracking because of excessive motion");
              break;

              default:
                  if(reasonText.text !="None")
                  {
                    defaultTracking.SetActive(true);
                    otherReasons.text = session.subsystem.notTrackingReason.ToString();
                    Debug.Log("Lost tracking because of " + reasonText.text);
                  }
              break;
            }
        }
    }
}
