﻿using System.Collections;
using System.Collections.Generic;
using Static;
using Tayx.Graphy.Utils.NumString;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AssistanceController : MonoBehaviour
{
    public static bool successActive;
    public static bool errorActive;
    
    private bool _successAnimationPlaying;
    private bool _errorAnimationPlaying;
    private readonly Color _initialColor = new Color(0.1215686f, 0.1215686f, 0.1215686f, 0.5882353f);
    
    [Header("Options: ")] public bool showAssistanceOverlay = true;
    public Color errorColor = Color.red;
    public Color successColor = Color.green;

    [Header("Object References: ")] public RectTransform assistanceOverlay;

    public Image progressLoadingBar;
    public TextMeshProUGUI progressLoadingBarText;

    public TextMeshProUGUI assistanceText;

    public Image successIcon;
    public Image errorIcon;
    public GameObject progressIcon;


    void Start()
    {
        //Switch assistance overlay on or off
        assistanceOverlay.gameObject.SetActive(showAssistanceOverlay);
    }
    
    void Update()
    {
        UpdateProgressBar();

        //if either a success or failure occurs, start the respective animation.
        if (successActive)
        {
            //set to false, so animation gets played just once.
            successActive = false;
            StartCoroutine(PlaySuccessAnimation());
        }
        else if (errorActive)
        {
            //set to false, so animation gets played just once.
            errorActive = false;
            StartCoroutine(PlayErrorAnimation());
        }
    }

    /// <summary>
    /// Updates percentage and fill of the progress bar.
    /// </summary>
    private void UpdateProgressBar()
    {
        //Only update the assistance overlay when active
        if (!showAssistanceOverlay) return;

        //Update the progress indicator
        //TODO: Actually get the max state value from statemachine
        int progressPercentage = (((Statemachine.Instance.stateNumber) * 100) / (Statemachine.Instance.maxstateNumber));
        progressLoadingBarText.text = progressPercentage + "%";
        progressLoadingBar.fillAmount = (progressPercentage.ToFloat() / 100);

        //Update the Assistance instructions
        assistanceText.text = Statemachine.Instance.instruction;
    }

    /// <summary>
    /// Starts the success animation.
    /// </summary>
    private IEnumerator PlaySuccessAnimation()
    {
        //indicates animation is playing.
        _successAnimationPlaying = true;

        //disable the progress icon while enabling the success icon.
        //disable also error icon, in case this method was called while PlayErrorAnimation was playing.
        progressIcon.SetActive(false);
        successIcon.enabled = true;
        errorIcon.enabled = false;

        //----Start the animation
        successIcon.color = successColor;
        yield return new WaitForSeconds(0.3f);
        successIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        successIcon.color = successColor;
        yield return new WaitForSeconds(0.3f);
        successIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        successIcon.color = successColor;
        yield return new WaitForSeconds(0.3f);
        successIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        //----End the animation

        //Animation done, so disable icon.
        successIcon.enabled = false;

        //In case the error animation was called while this one was playing, do not activate the progress Icon.
        if (!_errorAnimationPlaying)
        {
            progressIcon.SetActive(true);
        }

        //animation done playing
        _successAnimationPlaying = false;
    }

    /// <summary>
    /// Starts the error animation.
    /// </summary>
    private IEnumerator PlayErrorAnimation()
    {
        //indicates animation is playing.
        _errorAnimationPlaying = true;

        //disable the progress icon while enabling the success icon.
        //also, disable success icon, in case this method was called while PlaySuccessAnimation was playing.
        progressIcon.SetActive(false);
        errorIcon.enabled = true;
        successIcon.enabled = false;

        //----Start the animation
        errorIcon.color = errorColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = errorColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = errorColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = _initialColor;
        yield return new WaitForSeconds(0.3f);
        errorIcon.color = errorColor;
        yield return new WaitForSeconds(0.3f);
        //----End the animation

        //Animation done, so disable icon.
        errorIcon.enabled = false;

        //In case the success animation was called while this one was playing, do not activate the progress Icon.
        if (!_successAnimationPlaying)
        {
            progressIcon.SetActive(true);
        }

        //animation done playing.
        _errorAnimationPlaying = false;
    }
}