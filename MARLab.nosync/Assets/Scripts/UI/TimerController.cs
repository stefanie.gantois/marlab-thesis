﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimerController : MonoBehaviour
{
    public GameObject Timer;
    private TextMeshProUGUI TimerText;
    private float time;
    private bool timerOn;
    
    void Start()
    {
        timerOn = true;
        TimerText = Timer.GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        if (timerOn)
        {
            time += Time.deltaTime;
            UpdateGUI();
        }
    }

    void UpdateGUI()
    {
        var minutes = (int) time / 60;
        var seconds = (int) time % 60;

        TimerText.text = $"{minutes:00}:{seconds:00}";
    }

    public void PauseTimer()
    {
        timerOn = false;
    }

    public void StartTimer()
    {
        timerOn = true;
    }

    public void HideTimer()
    {
        Timer.SetActive(false);
    }

    public void ShowTimer()
    {
        Timer.SetActive(true);
    }

    public void ToggleTimer()
    {
        timerOn = !timerOn;
    }
}
