﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public class LocaliceTextMesh : MonoBehaviour
{
    [AddComponentMenu("Localize dropdown")]
    // Fields
    // =======
    public LocalizedOption options;

    // Properties
    // ===========
    public TMP_Text textfield;

    // Methods
    // ========
    private IEnumerator Start()
    {
        yield return PopulateTextField();
    }

    private void OnEnable() => LocalizationSettings.SelectedLocaleChanged += UpdateDropdownOptions;

    private void OnDisable() => LocalizationSettings.SelectedLocaleChanged -= UpdateDropdownOptions;

    private void OnDestroy() => LocalizationSettings.SelectedLocaleChanged -= UpdateDropdownOptions;

    public IEnumerator PopulateTextField()
    {
        var option = options;

        // If the option has text, fetch the localized version
        if (!option.text.IsEmpty)
        {
            var localizedTextHandle = option.text.GetLocalizedString();
            yield return localizedTextHandle;
            textfield.text = localizedTextHandle.Result;
        }
    }
    private void UpdateDropdownOptions(Locale locale)
    {
        var option = options;

        // Update the text
        if (!option.text.IsEmpty)
        {
            var localizedTextHandle = option.text.GetLocalizedString(locale);
            localizedTextHandle.Completed += (handle) =>
            {
                textfield.text = handle.Result;
            };
        }
    }
}
    [Serializable]
    public class LocalizedOption
    {
        public LocalizedString text;
    }
