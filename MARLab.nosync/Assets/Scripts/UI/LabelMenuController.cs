﻿using System;
using System.Collections.Generic;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LabelMenuController : MonoBehaviour
    {
        public List<Button> buttons;
        public List<LabelEntry> labelEntries = new List<LabelEntry>();

        public TMP_Text labelText;
        private bool isFirstLabel = true;
        public GameObject labelingMenu;
        public bool dateInLabel;
        private string todayDate;
        private int correctAnswerAmount;
        private int correctAnswerCounter = 0;
        
        void Start()
        {
            InitButtons();
            CountCorrectAnswers();
        }

        void Update()
        {    //As soon as labeling is true, activate the Menu.
            if (Statemachine.Instance.labeling && !ErrorOverlayController.ErrorOverlayIsActive())
            {
                labelingMenu.SetActive(true);
            }
            else
            {
                labelingMenu.SetActive(false);
            }
        }
        
        /// <summary>
        /// Initialize Label entries and corresponding buttons.
        /// </summary>
        /// 
        private void InitButtons()
        {
            //Fill List with entries
            labelEntries.Add(new LabelEntry("Dauer der Haltbarkeit: 24h", false));
            labelEntries.Add(new LabelEntry("[Aktuelle Uhrzeit]", true));
            labelEntries.Add(new LabelEntry("25 mcg Partusisten",
                true)); 
            //Check which of the two fluids is the correct one, and mark them as correct or not.
            if (Statemachine.Instance.notExpired.Equals("NaCl 0,9% 10ml"))
            {
                labelEntries.Add(new LabelEntry("4 ml NaCl", true));
                labelEntries.Add(new LabelEntry("4 ml G5", false));
            }
            else
            {
                labelEntries.Add(new LabelEntry("4 ml G5", true));
                labelEntries.Add(new LabelEntry("4 ml NaCl", false));
            }
            //Today and yesterday.
            labelEntries.Add(new LabelEntry("[Aktuelles Datum]", true));
            labelEntries.Add(new LabelEntry("[Handzeichen/Unterschrift]", true));
            //Add the OnClick-Event to the corresponding buttons.
            for (var i = 0; i < labelEntries.Count; i++)
            {
                buttons[i].GetComponentInChildren<TMP_Text>().text = labelEntries[i].labelText;
                int buttonIndex = i;
                buttons[i].onClick.AddListener(() => ValidateEntry(buttonIndex));
            }
        }

        /// <summary>
        /// increments correctAnswerAmount for each correct answer in labelEntries.
        /// </summary>
        /// 
        private void CountCorrectAnswers()
        {
            foreach(LabelEntry le in labelEntries)
            {
                if (le.isCorrect)
                {
                    correctAnswerAmount++;
                }
            }
        }

        /// <summary>
        /// Checks whether or not the pressed button was correct or not.
        /// </summary>
        /// 
        private void ValidateEntry(int buttonIndex)
        {    
            //If the chosen label element is correct, change color of button to green, make it non-interactable and add it to the label-text.
            if (labelEntries[buttonIndex].isCorrect)
            {
                buttons[buttonIndex].interactable = false;
                var tempColor = buttons[buttonIndex].colors;
                tempColor.disabledColor = Color.green;
                buttons[buttonIndex].colors = tempColor;
                correctAnswerCounter++;
                //Check for being the first part of the label-string. If it isn't add comma-separation, otherwise don't.
                if (isFirstLabel)
                    labelText.text += labelEntries[buttonIndex].labelText;
                else
                {
                    labelText.text += ", " + labelEntries[buttonIndex].labelText;
                }
                //Check if all the correct labels have been marked.
                if (correctAnswerCounter == correctAnswerAmount)
                {
                    Statemachine.Instance.labelingSuccessfull = true;
                    labelingMenu.SetActive(false);
                }
                isFirstLabel = false;
            }
            //When the chosen button was not correct, make it non-interactable and change color to red.
            else
            {
                ScenarioCompleted.AddErrorHint("Folgendes sollte als Beschriftung auf der Spritze vermerkt sein: aufgezogenen Substanzen, Uhrzeit und Datum, Ihr Kürzel/Unterschrift");
                Statemachine.Instance.errorCounter++;
                Statemachine.Instance.errorTotal++;
                Statemachine.Instance.infobox = "Zur Sicherung der Patient*innensicherheit ist es notwendig, dass die aufgezogenen Substanzen auf der Spritze vermerkt werden. Darüber hinaus sollten Datum und Uhrzeit vermerkt werden, damit die Haltbarkeit der aufgezogenen Lösung nicht überschritten wird. Zum Schluss sollten Sie ihr Kürzel oder Unterschrift hinterlassen, falls eine dritte Person die Injektion durchführt. (Beispiel: Notfalltokolyse, Partusisten 25 mikrogramm in 4 ml NaCl, 14.07.2016, 16:30, KL";
                buttons[buttonIndex].interactable = false;
                var tempColor = buttons[buttonIndex].colors;
                tempColor.disabledColor = Color.red;
                buttons[buttonIndex].colors = tempColor;
            }
        }
        
        /// <summary>
        /// function tests the input string on correct syntax for the label
        /// </summary>
        /// 
        private bool CorrectLabelInput(string toTest)
        {
            return PenLabelingRegexMatch.CheckLabel(toTest);
        }
        /// <summary>
        /// checks whether or not given source string is contained in toCheck string.
        /// </summary>
        /// 
        private static bool ContainsCaseinsensitive(string source, string toCheck)
        {
            return source?.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }

    public struct LabelEntry
    {
        
        public string labelText { private set; get; }
        public bool isCorrect { private set; get; }

        public LabelEntry(string labelText, bool isCorrect)
        {
            this.labelText = labelText;
            this.isCorrect = isCorrect;
        }
    }
}