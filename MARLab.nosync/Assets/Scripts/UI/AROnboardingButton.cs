﻿using System;
using Static;
using UnityEngine;
using UnityEngine.Events;

public class AROnboardingButton : MonoBehaviour
{
    public UnityEvent OnHelpButtonPressed;

    /// <summary>
    /// Show the Onboarding screens according to which interaction concept is currently active
    /// </summary>
    public void ShowOnboarding()
    {
        OnHelpButtonPressed.Invoke();
    }
}
