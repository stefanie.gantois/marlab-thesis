﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffScreenIndicator : MonoBehaviour
{
    private Vector3 targetPosition;
    public GameObject arrow;
    private RectTransform pointerRectTransform;
    public GameObject targetObject;
    [Range(0.0F, 200.0F)]
    public float vertialPadding = 100;

    public bool trackingEnabled;

    private void Awake()
    {
        pointerRectTransform = arrow.GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (!trackingEnabled)
        {
            arrow.SetActive(false);
            return;
        }
        Tracking();
    }

    public void StartTracking(GameObject target)
    {
        targetObject = target;
        trackingEnabled = true;
    }

    public void StopTracking()
    {
        targetObject = null;
        trackingEnabled = false;
    }

    public void PauseTracking()
    {
        if (targetObject == null)
        {
            throw new NoTargetObjectInitialized("Can't pause tracking, because, no targetObject has been initialized at this point.");
        }

        trackingEnabled = false;
    }
    
    public void ResumeTracking()
    {
        if (targetObject == null)
        {
            throw new NoTargetObjectInitialized("Can't resume tracking, because no targetObject has been initialized at this point.");
        }
        trackingEnabled = true;
    }

    private void Tracking()
    {
        
        Vector3 screenPos = Camera.main.WorldToScreenPoint(targetObject.transform.position);

        //Is targetObject on Screen?
        if (screenPos.z > 0 && screenPos.x > 0 && screenPos.x < Screen.width &&
            screenPos.y > 0 && screenPos.y < Screen.height)
        {
            //If targetObject is on screen, deactivate arrow
            arrow.SetActive(false);
        }
        //targetobject is Offscreen
        else
        {
            //If targetObject is Off screen activate arrow.
            arrow.SetActive(true);
            
            //Arrowposition flipped if object is behind us.
            if (screenPos.z < 0)
            {
                screenPos *= -1;
            }

            //Center of screen.
            Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;

            //subtract from screenPos - this makes the math a bit simpler. Gets added again later.
            screenPos -= screenCenter;

            //angle the arrow is pointing to
            float angle = Mathf.Atan2(screenPos.y, screenPos.x);
            angle -= 90 * Mathf.Deg2Rad;

            float cos = Mathf.Cos(angle);
            float sin = -Mathf.Sin(angle);

            float m = cos / sin;
            
            //Edge of the screen, with a tiny offset
            Vector3 screenBounds = new Vector3(screenCenter.x*0.8f,screenCenter.y-vertialPadding,0);

            //if cos > 0, than the arrow is on the upper side of the screen, elso bottom side
            if (cos > 0)
            {
                screenPos = new Vector3(screenBounds.y/m, screenBounds.y, 0);
            }
            else
            {
                screenPos = new Vector3(-screenBounds.y/m, -screenBounds.y, 0);
            }

            //if screenpos is out of bounds, the arrow needs to be moved to the appropriate side.
            if (screenPos.x > screenBounds.x)
            {    
                //arrow on right side of screen
                screenPos = new Vector3(screenBounds.x, screenBounds.x * m, 0);
            }
            else if (screenPos.x < -screenBounds.x)
            {
                //arrow on left side of screen
                screenPos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);
            }

            //re-add coordinate translation
            screenPos += screenCenter;

            //Apply position and rotation to arrow.
            pointerRectTransform.transform.position = screenPos;
            pointerRectTransform.transform.rotation = Quaternion.Euler(0, 0, angle*Mathf.Rad2Deg);
        }
    }
}

public class NoTargetObjectInitialized: Exception {
    public NoTargetObjectInitialized(string message): base(message) {
    }
}
