﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour {
    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public bool autoStartVideo = false;

    private void Start()
    {
        if(autoStartVideo) StartVideo();
    }

    public void StartVideo()
    {
        rawImage.enabled = false;
        StartCoroutine(PlayVideo());
    }
    
    private IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        rawImage.enabled = true;
    }
    
    //TODO: OnDisable braucht wahrscheinlich einen videoplayer.stop oder stopped das automatisch wenn das Objekt deaktiviert wird?
}