﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

public enum DropDownOptions
{
    scenarioMode,
    planesize
}

public class LocaliceDropdown : MonoBehaviour
{

    [AddComponentMenu("Localize dropdown")]
    // Fields
    // =======
    public List<LocalizedDropdownOption> options;

    private int selectedOptionIndex;
    public DropDownOptions dropdownType;

    // Properties
    // ===========
    public TMP_Dropdown Dropdown;

    // Methods
    // ========
    private IEnumerator Start()
    {
        yield return PopulateDropdown();
    }

    private void OnEnable() => LocalizationSettings.SelectedLocaleChanged += UpdateDropdownOptions;

    private void OnDisable() => LocalizationSettings.SelectedLocaleChanged -= UpdateDropdownOptions;

    private void OnDestroy() => LocalizationSettings.SelectedLocaleChanged -= UpdateDropdownOptions;

    private IEnumerator PopulateDropdown()
    {
        // Clear any options that might be present
        Dropdown.ClearOptions();
        Dropdown.onValueChanged.RemoveListener(UpdateSelectedOptionIndex);

        for (var i = 0; i < options.Count; ++i)
        {
            var option = options[i];
            var localizedText = string.Empty;

            // If the option has text, fetch the localized version
            if (!option.text.IsEmpty)
            {
                var localizedTextHandle = option.text.GetLocalizedString();
                yield return localizedTextHandle;

                localizedText = localizedTextHandle.Result;

                // If this is the selected item, also update the caption text
                if (i == selectedOptionIndex)
                {
                    UpdateSelectedText(localizedText);
                }
            }
            // Finally add the option with the localized content
            Dropdown.options.Add(new TMP_Dropdown.OptionData(localizedText));
        }

        // Update selected option, to make sure the correct option can be displayed in the caption
        if(dropdownType == DropDownOptions.scenarioMode)
            Dropdown.value = (int)Static.ApplicationSettings.scenarioMode;
        if (dropdownType == DropDownOptions.planesize)
            Dropdown.value = (int)Static.ApplicationSettings.planeSize;
        Dropdown.onValueChanged.AddListener(UpdateSelectedOptionIndex);
    }

    private void UpdateDropdownOptions(Locale locale)
    {
        // Updating all options in the dropdown
        // Assumes that this list is the same as the options passed on in the inspector window
        for (var i = 0; i < Dropdown.options.Count; ++i)
        {
            var optionI = i;
            var option = options[i];

            // Update the text
            if (!option.text.IsEmpty)
            {
                var localizedTextHandle = option.text.GetLocalizedString(locale);
                localizedTextHandle.Completed += (handle) =>
                {
                    Dropdown.options[optionI].text = handle.Result;

                        // If this is the selected item, also update the caption text
                        if (optionI == selectedOptionIndex)
                    {
                        UpdateSelectedText(handle.Result);
                    }
                };
            }
        }
    }

    private void UpdateSelectedOptionIndex(int index) => selectedOptionIndex = index;

    private void UpdateSelectedText(string text)
    {
        if (Dropdown.captionText != null)
        {
            Dropdown.captionText.text = text;
        }
    }
}
[Serializable]
public class LocalizedDropdownOption
{
    public LocalizedString text;

}
