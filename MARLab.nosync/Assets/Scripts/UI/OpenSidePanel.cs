﻿using System.Collections;
using System.Collections.Generic;
using Static;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UIElements;

public class OpenSidePanel : MonoBehaviour
{
    public GameObject panel;

    public void OpenPanel()
    {
        AnalyticsResult result = Analytics.CustomEvent("OpenSideMenu",
                new Dictionary<string, object>
                {
                    { "id", ApplicationSettings.idCode}
                }
            );
        Debug.Log("analytic result: " + result);
        if (panel != null)
        {
            Animator animator = panel.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("open");
                animator.SetBool("open", !isOpen);
            }
        }
    }
}