﻿using Static;
using Others;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ChangeApplicationSettings : MonoBehaviour
    {
        public TMP_Dropdown InputSystemDropdown;
        public TMP_Dropdown ScenarioModeDropdown;
        public TMP_Dropdown PlaneSizeDropdown;
        public Toggle DeveloperOptionsToggle;
        public Toggle LoggingOptionsToggle;
        public SaveManager savemanager;

        public void Start()
        {
            PlaneSizeDropdown.value = savemanager.state.planeSize;
            ScenarioModeDropdown.value = savemanager.state.szenarioMode;
            DeveloperOptionsToggle.isOn = savemanager.state.displayPerfMonitor;
            LoggingOptionsToggle.isOn = savemanager.state.logging;
        }

        /// <summary>
        /// Activates the performance monitor based on if the developer options are activated
        /// </summary>
        public void ChangeDeveloperOptions()
        {
            ApplicationSettings.displayPerformanceMonitor = DeveloperOptionsToggle.isOn;
            savemanager.state.displayPerfMonitor = DeveloperOptionsToggle.isOn;
            savemanager.Save();
        }

        /// <summary>
        /// Activates logging
        /// </summary>
        public void ChangeLogging()
        {
            ApplicationSettings.loggingData = LoggingOptionsToggle.isOn;
            savemanager.state.logging = LoggingOptionsToggle.isOn;
            savemanager.Save();
        }

        /// <summary>
        /// Activates the correct scneario mode based on the value selected in the settings menu
        /// </summary>
        public void ChangeScenarioModeBasedOnDropdown()
        {
            switch (ScenarioModeDropdown.value)
            {
                case 0:
                    ActiveTrainingMode();
                    break;
                case 1:
                    ActiveExamMode();
                    break;
            }
        }

        public void ChangePlaneSizeBasedOnDropdown()
        {
          switch (PlaneSizeDropdown.value)
          {
            case 0:
                ApplicationSettings.planeSize = MinPlanesize.small;
                savemanager.state.planeSize = (int)MinPlanesize.small;
                savemanager.Save();
                break;
            case 1:
                ApplicationSettings.planeSize = MinPlanesize.middle;
                savemanager.state.planeSize = (int)MinPlanesize.middle;
                savemanager.Save();
                break;
            case 2:
                ApplicationSettings.planeSize = MinPlanesize.large;
                savemanager.state.planeSize = (int)MinPlanesize.large;
                savemanager.Save();
                break;
          }
        }

        public void ActiveTrainingMode()
        {
            Debug.Log("training mode active");
            ApplicationSettings.scenarioMode = ScenarioMode.trainingMode;
            savemanager.state.szenarioMode = (int)ScenarioMode.trainingMode;
            savemanager.Save();
        }

        public void ActiveExamMode()
        {
            Debug.Log("exam mode active");
            ApplicationSettings.scenarioMode = ScenarioMode.examMode;
            savemanager.state.szenarioMode = (int)ScenarioMode.examMode;
            savemanager.Save();
        }
    }
}
