﻿using Michsky.UI.ModernUIPack;
using UnityEngine;

namespace UI
{
    public class ErrorOverlayController : MonoBehaviour
    {
        public ModalWindowManager ErrorOverlay;
        private static bool overLayIsActive;

        private void Update()
        {
            CloseAtTouch();
            if (Statemachine.Instance.errorCounter < 2) return;
            OpenErrorWindow();

            //Reset the ErrorCounter back to 1 error so the windows pops up at the next Error again
            Statemachine.Instance.errorCounter = 1; //TODO: this is kinda dirty...
        }

        private void CloseAtTouch()
        {
            if (ErrorOverlayIsActive()) //Error Overlay
            {
                if (Input.touchCount < 1)
                {
                    return;
                }
                if (Input.touchCount == 1)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        CloseErrorWindow();
                        return;
                    }
                }
                else if (Input.touchCount >= 2)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Ended)
                    {
                        CloseErrorWindow();
                        return;
                    }
                }
            }
        }

        private void OpenErrorWindow()
        {
            overLayIsActive = true;
            ErrorOverlay.titleText = "Hilfestellung:";
            ErrorOverlay.descriptionText = Statemachine.Instance.infobox;
            ErrorOverlay.UpdateUI();
            ErrorOverlay.OpenWindow();
            Debug.Log("Fehler: " + Statemachine.Instance.infobox);
        }

        public void CloseErrorWindow()
        {
            overLayIsActive = false;
            ErrorOverlay.CloseWindow();
        }

        public static bool ErrorOverlayIsActive()
        {
            return overLayIsActive;
        }
    }
}
