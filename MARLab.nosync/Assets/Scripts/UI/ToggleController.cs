﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ToggleController
{
    public static bool aRHilfestellung;
    public static bool aRPraxiswissen;

    public static void SwitchARHilfeStellung(bool switchValue)
    {
        aRHilfestellung = switchValue;
    }
    
    public static void SwitchARPraxiswissen(bool switchValue)
    {
        aRPraxiswissen = switchValue;
    }

}
