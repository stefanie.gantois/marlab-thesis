﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class HeartFrequencyController : MonoBehaviour
{
    public TextMeshProUGUI heartFrequencyText;
    public int heartFreq;
    public bool active = false;
    public int range = 0;

    void Update()
    {
        if (active)
        {
            StartCoroutine(ChangeHeartFrequency());
        }
    }

    private IEnumerator ChangeHeartFrequency()
    {
        active = false;
        yield return new WaitForSeconds(2f);
        int newValue = Random.Range(heartFreq - range, heartFreq + range);
        heartFrequencyText.text = newValue + " BPM";
        active = true;
    }

    public void SetHeartFrequency(int saturation)
    {
        heartFreq = saturation;
    }
    
    public void SetFreqActive(bool isActive)
    {
        active = isActive;
    }

    public void SetRange(int diffRange)
    {
        range = diffRange;
    }
    
}