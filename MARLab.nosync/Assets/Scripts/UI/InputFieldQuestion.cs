﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputFieldQuestion : Question
{
    public readonly string CorrectAnswer;
    public string CorrectAnswerFeedback;
    public string WrongAnswerFeedback;
    public InputFieldQuestion(string questionText, string correctAnswer, string correctAnswerFeedback, string wrongAnswerFeedback)
    {
        this.questionText = questionText;
        this.CorrectAnswer = correctAnswer;
        this.CorrectAnswerFeedback = correctAnswerFeedback;
        this.WrongAnswerFeedback = wrongAnswerFeedback;
    }
}