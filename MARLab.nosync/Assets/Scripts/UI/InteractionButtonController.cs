﻿using Interaction;
using Others;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class InteractionButtonController : MonoBehaviour
    {
        [Header("Script References: ")]
        public InteractionController interactionController;
        public TouchInputController touchInputController;
        public PrefabSpawningController spawningController;
        [Header("UI Elements: ")]
        public RectTransform ButtonHolder;
        public Button grabButton;
        public Button combineButton;
        public Button interactButton;
        public GameObject rotateButtonRight;
        public GameObject rotateButtonLeft;
        public GameObject syringeSlider;
        public GameObject titrationSlider;
        public GameObject GraphButton;
        [Header("Options: ")]
        [Range(0f, 2f)] public float gabbedObjectRotationSpeed = 0.2f;

        private bool versionAR = false;

        private void Start()
        {
            grabButton.GetComponentInChildren<TMP_Text>().text = StringDict.InstanceStringDict.lookupString(StoredTexts.buttonGrab);
        }

        // Update is called once per frame
        void Update()
        {
                UpdateButtons();
                TouchRotation();
                UpdateButtonVisibility();
        }

        /// <summary>
        /// Makes the buttons invisible when in the error overlay or the prefab is not spawned yet
        /// </summary>
        private void UpdateButtonVisibility()
        {
            if (spawningController.objectWasSpawned && !ErrorOverlayController.ErrorOverlayIsActive()
                && !Statemachine.Instance.labeling && !ScenarioCompleted.scenarioIsCompleted
                && !Statemachine.Instance.questioning && !Statemachine.Instance.introducing
                && !Statemachine.Instance.titrating)
            {
                ButtonHolder.gameObject.SetActive(true);
            }
            else
            {
                ButtonHolder.gameObject.SetActive(false);
            }
        }
        /// <summary>
        /// Updates the highlighting of the buttons, depending on whether they can be used or not.
        /// </summary>
        private void UpdateButtons()
        {
            //When an object is selected, that object is grabbable, or if a object is currently grabbed, make grab-button interactable. otherwise don't.
            if (interactionController.isSelectingObject &&
                !interactionController.tryedGrabbingObjectUnsuccessfully &&
                interactionController.selectedObject.GetComponent<ObjectInteractionHandler>().isGrabbable ||
                interactionController.isGrabbingObject)
            {
                grabButton.interactable = true;
            }
            else
            {
                grabButton.interactable = false;
            }

            if (Statemachine.Instance.titrating == true)
            {
                titrationSlider.SetActive(true);
                if (!Statemachine.Instance.ARVersion(ApplicationSettings.idCode))
                {
                    GraphButton.SetActive(true);
                }

            } else
            {
                titrationSlider.SetActive(false);
                //GraphButton.SetActive(false);
            }
            
            //Check if Object is currently grabbed
            if (interactionController.isGrabbingObject)
            {
                //check if currently grabbed Object is active, if so then release it and remove the rotation buttons.
                if (!interactionController.grabbedObject.activeSelf)
                {
                    GrabRelease();
                }
            }
            
            //When there is a object to select, make interactbutton interactable.
            interactButton.interactable = interactionController.isSelectingObject;

            //When grabbing object, and intersecting with another object or when selecting object make interactbutton interactable.
            if (interactionController.atleastOneIntersectionFromColliders && interactionController.isGrabbingObject || interactionController.isSelectingObject)
            {
                interactButton.interactable = true;
                if (interactionController.atleastOneIntersectionFromColliders)
                {
                    ColorBlock colorBlock = interactButton.GetComponent<Button>().colors;
                    colorBlock.normalColor = new Color32(255, 188, 0, 255);
                    interactButton.GetComponent<Button>().colors = colorBlock;
                }
                else
                {
                    ColorBlock colorBlock = interactButton.GetComponent<Button>().colors;
                    colorBlock.normalColor = new Color32(49, 86, 125, 200);
                    interactButton.GetComponent<Button>().colors = colorBlock;
                    if (!canInteract())
                    {
                        interactButton.interactable = false;
                    }
                }
            }
            else
            {
                interactButton.interactable = false;
                ColorBlock colorBlock = interactButton.GetComponent<Button>().colors;
                colorBlock.normalColor = new Color32(49, 86, 125, 200);
                interactButton.GetComponent<Button>().colors = colorBlock;
            }

            //Reset the tryed GrabbingObject
            interactionController.tryedGrabbingObjectUnsuccessfully = false;
            
        }
        
        /// <summary>
        /// Rotates the Grabbed Object via Touch.
        /// </summary>
        private void TouchRotation() {
            
            //When an object is grabbed and the labeling menu is not on screen, rotation is enabled. Rotation is also disabled, while the syringe is being filled.
            if (interactionController.isGrabbingObject && !Statemachine.Instance.labeling)
            {
                if (Input.touchCount < 1)
                {
                    return;
                }
                Touch touch = Input.GetTouch(0);
                RotateGabbedObject(touch);
            }
        }
        
        /// <summary>
        /// What happens when the "Greifen"-Button is pressed.
        /// </summary>
        public void GrabRelease()
        {
            if (interactionController.isGrabbingObject)
            {
                interactionController.ReleaseGrabbedObject();
                grabButton.GetComponentInChildren<TMP_Text>().text = StringDict.InstanceStringDict.lookupString(StoredTexts.buttonGrab);
            }
            else
            {
                grabButton.GetComponentInChildren<TMP_Text>().text = StringDict.InstanceStringDict.lookupString(StoredTexts.buttonRelease);
                interactionController.GrabObject();
            }
        }
        
        /// <summary>
        /// Rotates a grabbed Object around its Y Axis.
        /// </summary>
        /// <param name="direction">the direction of the corresponding button pressed.</param>
        public void RotateGabbedObject(float direction)
        {
            Quaternion yRotation = Quaternion.Euler(0f, direction * gabbedObjectRotationSpeed, 0f);
            interactionController.grabber.transform.rotation =
                yRotation * interactionController.grabber.transform.rotation;
        }
        
        private void RotateGabbedObject(Touch touch)
        {
            Quaternion yRotation = Quaternion.Euler(0f, -touch.deltaPosition.x * gabbedObjectRotationSpeed, 0f);
            interactionController.grabber.transform.rotation =
                yRotation * interactionController.grabber.transform.rotation;
        }

        private bool canInteract()
        {
            string name = interactionController.selectedObject.GetComponent<AR_Interactable>().InteractableName;
            if (name == "Handschuhbox" || name == "Glasses" || name == "Stirrer" || name == "BuretteStand" || name == "Titrant"
                || name == "Phenolphthalein" || name == "MethylOrange" || name == "PhenoDropper" || name == "MethylDropper")
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}
