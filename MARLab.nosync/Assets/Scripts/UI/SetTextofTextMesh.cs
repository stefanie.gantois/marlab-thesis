﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SetTextofTextMesh : MonoBehaviour
{
    public StoredTexts storedString;
    public TextMeshProUGUI textField;
    // Start is called before the first frame update
    void Start()
    {
        textField.text = StringDict.InstanceStringDict.lookupString(storedString);
    }
}
