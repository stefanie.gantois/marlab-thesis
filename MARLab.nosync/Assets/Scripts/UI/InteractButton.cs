﻿using System;
using Interaction;
using Static;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class InteractButton : MonoBehaviour
    {
        public InteractionController interactionController;
        public Slider slider;
        public TextMeshProUGUI buttonText;

        private void Start()
        {
            buttonText.text = StringDict.InstanceStringDict.lookupString(StoredTexts.interactDefault);
        }
        private void Update()
        {
            if (interactionController.isIntersecting)
            {
                buttonText.text = StringDict.InstanceStringDict.lookupString(StoredTexts.combineDefault);
                //buttonText.text = StringDict.InstanceStringDict.lookupString(interactionController.selectedObject.GetComponent<ObjectInteractionHandler>().combineString);
                buttonText.text = StringDict.InstanceStringDict.lookupString(setCombineString());
            }
            else if(interactionController.isSelectingObject || interactionController.isGrabbingObject)
            {
                buttonText.text = StringDict.InstanceStringDict.lookupString(StoredTexts.interactDefault);
                //buttonText.text = interactionController.selectedObject.GetComponent<ObjectInteractionHandler>().interactString;
                buttonText.text = StringDict.InstanceStringDict.lookupString(interactionController.selectedObject.GetComponent<ObjectInteractionHandler>().interactString);
            }
            else
            {
                buttonText.text = StringDict.InstanceStringDict.lookupString(StoredTexts.interactDefault);
            }
        }

        /// <summary>
        /// Calls either an interact or an combine, depending on the interactioncontroller context.
        /// </summary>
        public void ReleaseInteract()
        {
            
            //When grabbing Object and intersecting, Combine.
            if(interactionController.isGrabbingObject && interactionController.atleastOneIntersectionFromColliders)
            {
                interactionController.Combine();
            }
            else
                //Otherwise, it's always an interact.
            {
                interactionController.selectedObject.GetComponent<ObjectInteractionHandler>().Interact();
            }
        }

        public StoredTexts setCombineString()
        {
            StoredTexts result = StoredTexts.combineDefault;
            String nameGrabbedObject = interactionController.grabbedObject.GetComponent<AR_Interactable>().InteractableName;
            String nameIntersectedObject = interactionController.intersectedObject.GetComponent<AR_Interactable>().InteractableName;

            if (nameGrabbedObject.Equals("Funnel") || nameGrabbedObject.Equals("Burette"))
            {
                result = StoredTexts.attach;
            }
            if (nameGrabbedObject.Equals("Phenolphthalein") || nameGrabbedObject.Equals("MethylOrange")
                || nameGrabbedObject.Equals("PhenoDropper") || nameGrabbedObject.Equals("MethylDropper"))
            {
                result = StoredTexts.add;
            }
            if (nameGrabbedObject.Equals("Titrant"))
            {
                result = StoredTexts.fill;
            }
            if (nameGrabbedObject.Equals("Wash"))
            {
                result = StoredTexts.clean;
            }
            if (nameGrabbedObject.Equals("Erlenmeyer"))
            {
                result = StoredTexts.place;
            }
            if (nameGrabbedObject.Equals("BeakerEmpty"))
            {
                result = StoredTexts.fill;
            }
            if (nameGrabbedObject.Equals("Magnet"))
            {
                result = StoredTexts.add;
            }
            if (nameGrabbedObject.Equals("ProbeTip"))
            {
                result = StoredTexts.place;
            }

            if (nameIntersectedObject.Equals("JerryCanBlack") || nameIntersectedObject.Equals("JerryCanWhite"))
            {
                result = StoredTexts.waste;
            }

            return result;
        }
    }
}
