﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FaceCamera : MonoBehaviour
{
    // Start is called before the first frame update

    private Transform mainCamera;

    void Start()
    {
        mainCamera = Camera.main.transform;
    }

    void Update()
    {
        transform.LookAt(mainCamera);
    }
}
