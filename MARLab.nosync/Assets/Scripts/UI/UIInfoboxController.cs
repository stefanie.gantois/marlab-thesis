﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInfoboxController : MonoBehaviour
{
    public TextMeshProUGUI speechbubbleText;
    public GameObject speechbubble;
    void Update()
    {
        UpdateInfoBox();
    }

    private void UpdateInfoBox()
    {
        if (Statemachine.Instance.stateNumber == 80)
        {
            speechbubble.SetActive(true);
            //speechbubbleText.text = "Ziehen Sie die Lösung durch Aspiration auf. Ziehen Sie auch etwas Luft mit auf, damit die Aufziehkanüle leer wird. Danach halten Sie die Spritze mit dem Konus nach oben, klopfen sanft gegen den Zylinder, um Luftblasen nach oben steigen zu lassen. Drücken Sie den Kolben vorsichtig nach oben bis der Konus mit Flüssigkeit gefüllt ist.";
        }
        else
        {
            speechbubble.SetActive(false);
        }
    }
}
