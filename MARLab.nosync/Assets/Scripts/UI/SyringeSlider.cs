﻿using Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SyringeSlider : MonoBehaviour
    {
        public InteractionController interactionController;
        public GameObject slider;
        /// <summary>
        /// adjusts the syringe stamp according the the slider value.
        /// </summary>
        /// 
        public void ChangeSyringeValue() {
            interactionController.selectedObject.GetComponent<InjectionController>().ManipulateSyringeStampGameObject((int) slider.GetComponent<Slider>().value);
        }
    }
}
