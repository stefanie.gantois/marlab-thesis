﻿using Others;
using UnityEngine;

namespace UI
{
    public class AndroidBack : MonoBehaviour
    {
        public GameObject menu;
        public GameObject thisScreen;
        public bool isMainMenu;
        public ApplicationRuntimeManager applicationRuntimeManager;

        void Update()
        {
            if (Application.platform != RuntimePlatform.Android) return;
            if (!Input.GetKeyDown(KeyCode.Escape)) return;
            if (isMainMenu)
            {
                applicationRuntimeManager.QuitApplication();
            }
            else
            {
                menu.SetActive(true);
                thisScreen.SetActive(false);
            }
        }
    }
}