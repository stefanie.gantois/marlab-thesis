﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyringeCannulaPlugInfoboxController : MonoBehaviour
{
    public int timeInfoboxOpen;
    public InfoBehaviour infoBehaviour;
    
    private static bool infoboxOpened;

    public void OpenInfobox()
    {
        if (!infoboxOpened)
        {
            infoBehaviour.OpenInfoAndCloseAfterSeconds(timeInfoboxOpen);
            infoboxOpened = true;
        }
    }
}
