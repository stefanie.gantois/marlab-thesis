﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleChangeValue : MonoBehaviour
{
    public void SwitchARHilfestellung(bool switchValue)
    {
        ToggleController.aRHilfestellung = switchValue;
    }
    
    public void SwitchARPraxiswissen(bool switchValue)
    {
        ToggleController.aRPraxiswissen = switchValue;
    }
}
