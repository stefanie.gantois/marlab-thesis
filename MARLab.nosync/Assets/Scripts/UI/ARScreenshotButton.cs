﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARScreenshotButton : MonoBehaviour
{
    public bool arScreenshotModeActive = false;
    public Image buttonImage;
    public ARScreenshot arScreenshot;
    private Color oldColor;

    public void OnButtonPressed()
    {
        if (!arScreenshotModeActive)
        {
            arScreenshot.MakeARScreenshot();
            arScreenshotModeActive = true;
            oldColor = buttonImage.color;
            buttonImage.color = Color.red;
        }
        else
        {
            arScreenshot.ReactivateARMode();
            arScreenshotModeActive = false;
            buttonImage.color = oldColor;
        }
    }
}
