﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LanguageButton : MonoBehaviour
{
    public SaveManager savemanager;

    [Header("Options")]
    public bool buttonActive = true;
    public Language buttonLanguage;
    // Start is called before the first frame update
    void Start()
    {
        //Deactivate the Button interactive when the language button should not be active
        if (buttonActive == false)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            //Activate/Deactivate button based on selected language
            if(buttonLanguage != StringDict.selectedLanguage)
            {
                GetComponent<Button>().interactable = true;
                transform.GetChild(0).gameObject.SetActive(false);
            }
            else
            {
                GetComponent<Button>().interactable = false;
                transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    public void changeLanguage()
    {
        savemanager.state.selectedLanguage = (int)buttonLanguage;
        savemanager.Save();
        restartScene();
    }

    public void restartScene()
    {
        SceneManager.LoadScene("Menu");
    }
}
