﻿using Interaction;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class PointerListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        /// <summary>
        /// Rotate the currently grabbed object by holding a button down.
        /// </summary>
        /// 
        public float direction;
        public GameObject userInterfaceController;
        bool _pressed = false;
        public void OnPointerDown(PointerEventData eventData)
        {
            _pressed = true;
        }
 
        public void OnPointerUp(PointerEventData eventData)
        {
            _pressed = false;
        }
 
        void Update()
        {
            if (!_pressed)
                return;
            //As long as Button is held down, currently grabbed Object rotates.
            userInterfaceController.GetComponent<InteractionButtonController>().RotateGabbedObject(direction);
        }
    }
}