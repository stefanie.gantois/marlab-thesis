﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Static;

public class IDNumberController : MonoBehaviour
{
    public GameObject loggingID;
    public GameObject tutorial1;
    public TextMeshProUGUI inputFieldQuestionText;
    private bool isFirstInput = true;

    public GameObject review;

    public void OnEnable()
    {
        //if(!ApplicationSettings.loggingData || ApplicationSettings.idCode.Equals("000"))
        //if (!ApplicationSettings.loggingData)
        if(!ApplicationSettings.idCode.Equals("000"))
        {
            loggingID.SetActive(false);
            tutorial1.SetActive(true);
        }

       if (ReviewController.controller.GetShow())
       {
            loggingID.SetActive(false);
            review.SetActive(true);
       } else
       {
            review.SetActive(false);
       }
    }

    /// <summary>
    /// Updates the question text with the given input.
    /// Is called by the input field number buttons.
    /// </summary>
    /// <param name="input">The value of the button pressed.</param>
    public void UpdateInput(int input)
    {
        if (isFirstInput)
        {
            isFirstInput = false;
            inputFieldQuestionText.text = input.ToString();
        }
        else
        {
            inputFieldQuestionText.text += input;
        }
    }

    /// <summary>
    /// Removes one Char of the input.
    /// Is called by the Remove Button of the input field.
    /// </summary>
    public void RemoveInput()
    {
        if (inputFieldQuestionText.text.Length > 0)
        {
            inputFieldQuestionText.text = inputFieldQuestionText.text.Remove(inputFieldQuestionText.text.Length - 1);
        }
    }

    /// <summary>
    /// Saves ID in Application Settings
    /// </summary>
    public void SetIDNumber()
    {
        ApplicationSettings.idCode = inputFieldQuestionText.text;
    }
}
