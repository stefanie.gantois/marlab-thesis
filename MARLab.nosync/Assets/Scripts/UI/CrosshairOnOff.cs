﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;

public class CrosshairOnOff : MonoBehaviour
{
    public InteractionController interactionController;
    public GameObject cursor;
    public GameObject innerCursor;
    RectTransform cursor_rt;

    public void Start()
    {
        interactionController = interactionController.GetComponent<InteractionController>();
        cursor_rt =   cursor.GetComponent<RectTransform>();
    }

    public void Update()
    {
        if (interactionController.isGrabbingObject || Statemachine.Instance.questioning || Statemachine.Instance.introducing || Statemachine.Instance.titrating)
        {
            cursor.SetActive(false);
        }
        else
        {
            cursor.SetActive(true);
            cursor_rt.sizeDelta = getNewSize();
        }
    }

    private Vector2 getNewSize()
    {
        float distanceHit = interactionController.hit.distance;
        int sizeMax = 85;
        int sizeMin = 30;
        
        if(distanceHit>1) distanceHit = 1;

        float x=(sizeMin*(1-distanceHit))+(distanceHit*sizeMax);

        return new Vector2(x,x);
    }
}
