﻿
//Prepared by Jessica Lizeth Dominguez Alfaro
//Main Source for the equations :De Levie, R. (1993). Explicit expressions of the general form of the titration curve in terms of concentration: Writing a single closed-form expression for the titration curve for a variety of titrations without using approximations or segmentation. Journal of Chemical Education, 70(3), 209–217. https://doi.org/10.1021/ed070p209
//This script may serve to calculate the theorethical curves
//All the calculations are based on a list of generated pH


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitrationDataBase : MonoBehaviour
{

    public List<double> valuesVbVa;
    // //[H+]
    public List<double> valuesmProton;

    // //[OH-]
    public List<double> valuesmHydroxide;
    // //alphaA
    public List<double> valuesmAlphaA;

    //public TitrationScript titrationScript;
    //public WindowGraphScript WindowGraphScript;
  

    public double volumeb;
    public float totalVolumeinMl;
   

    public double[] valuespH;

    //dict pH vs. Vb
    public Dictionary<float, float> dictionary = new Dictionary<float, float>();

    //public TitrationDataBase(List<double> valuesVbVa, List<double> valuesmProton, List<double> valuesmHydroxide, List<double> valuesmAlphaA, double volumeb, float totalVolumeinMl, double[] valuespH, Dictionary<float, float> dictionary)
    //{
    //    this.valuesVbVa = valuesVbVa;
    //    this.valuesmProton = valuesmProton;
    //    this.valuesmHydroxide = valuesmHydroxide;
    //    this.valuesmAlphaA = valuesmAlphaA;
    //    this.volumeb = volumeb;
    //    this.totalVolumeinMl = totalVolumeinMl;
    //    this.valuespH = valuespH;
    //    this.dictionary = dictionary;
    //}

    public void SearchpHValue()
    {
        dictionary = WindowGraphScript.dictionary;
        totalVolumeinMl = VolumeManager.totalVolumeinMl;
        float y = (float)Math.Round(totalVolumeinMl, 2);
        //print("this is volume from the database" + y);

        //float pH = KeyByValue(dictionary, totalVolumeinMl);
        //print("this is the ph of this volume" + pH);

        if (dictionary.ContainsValue (y))
        {
            print("i found it");
        }
        else
        {
            //print("no esta"+dictionary[1f]);
           
        }


        //Read the volume that was added from burette to beaker
        //Search for this value in the list of Vb values (calculated by awake in the windowgraph script) or the range of values
        //Look up the index
        //save the ph read as a list

        //Use this list for displaying values  in the graph

        ////float addedVolume = VolumeManager.addedVolume;

        //Debug.Log("This is the added volume now" + addedVolume);
        //List<float> valueList = WindowGraphScript.valuesVbVa.ConvertAll(Convert.ToSingle);
        //volumeb = valueList.Find(x => x.Equals(addedVolume));
        //Debug.Log(volumeb);

    }

   // https://stackoverflow.com/questions/2444033/get-dictionary-key-by-value
    //public static T KeyByValue<T, W>(this Dictionary<T, W> dict, W val)
    //{
    //    T key = default;
    //    foreach (KeyValuePair<T, W> pair in dict)
    //    {
    //        if (EqualityComparer<W>.Default.Equals(pair.Value, val))
    //        {
    //            key = pair.Key;
    //            break;
    //        }
    //    }
    //    return key;
    //}


    
}
