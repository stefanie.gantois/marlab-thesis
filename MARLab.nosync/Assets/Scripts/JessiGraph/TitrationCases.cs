﻿

//Prepared by Jessica Lizeth Dominguez Alfaro
//Main Source for the equations :De Levie, R. (1993). Explicit expressions of the general form of the titration curve in terms of concentration: Writing a single closed-form expression for the titration curve for a variety of titrations without using approximations or segmentation. Journal of Chemical Education, 70(3), 209–217. https://doi.org/10.1021/ed070p209
//This script may serve to calculate the theorethical curves
//All the calculations are based on a list of generated pH

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitrationCases : MonoBehaviour
{
    public float cAcid;//"Unknown" for user
    public float cBase; //chosen by user
    public double constant_Ka;  //constant and characteristic of each species
    public double constant_Kw;
    public double constant_Kb;

    public float vAcid; //chosen by user
    public float vBase; //given during the experiment
    public float volumeTotal;
    public float CbVb;
    public float CaVa;

    //[H+]
    public float mProton;
    //log[H+] or logpH
    public float logmProton;
    //[OH-]
    public float mHydroxide;
    //log[OH-]
    public float logmHydroxide;

    //alphaHA
    public float mAlphaHA;

    //logHA
    public float logmHA;

    //alphaA
    public float mAlphaA;

    //logA
    public float logmA;

    //Vb/Va
    public float VbVa;

    public float pH;
    public float pOH;

    

    public float phi;//degree of completion CbVb/CaVa

    //Case of strong acid with Strong base
    private void Awake()
    {
        cAcid = 0.5f;
        cBase = 0.5f;
        vAcid = 10f;
        constant_Ka = Math.Pow(10, -4.76f);//Ka of acetic acid
        constant_Kw = Math.Pow(10, -14f);//Kw of water
        constant_Kb = constant_Kw / constant_Ka;
 
    }

    private void Update()
    {
        vBase = VolumeManager.totalVolumeinMl;//Read from the volume added by VolumeManager coming from burette+what is in the beaker
        Debug.Log("this is the Vbase" + vBase);//TODO this value is negative now but it should not be
        CbVb = vBase * cBase;
        //Debug.Log("this is " + CbVb);
        CaVa = vAcid * cAcid;
        //Debug.Log("this is "+ CaVa);
        volumeTotal = vBase + vAcid;
        phi = CbVb / CaVa;
        //print("this is phi" + phi);
        CalculateWeakAcid();
        
    }


    ///STRONG ACID vs STRONG BASE
    public void CalculateStrongAcid()
        {
            //At the start
            //Before equivalence, 0<phi>1
            if (CaVa > CbVb)
            {
                mProton = CaVa - CbVb / volumeTotal;
                logmProton = -Mathf.Log10(mProton);
                pH = Mathf.RoundToInt(logmProton);
                Debug.Log(pH);
            }

            //At the equivalence point, phi=1
           else if (CaVa == CbVb)//ph=7
            {

            double mProton = Math.Pow(10, -7f);
            logmProton = -Mathf.Log10(Convert.ToSingle(mProton));
            pH = Mathf.RoundToInt(logmProton);
            Debug.Log(pH);

            }

            //After equivalence point, phi>1
           else if (CaVa < CbVb)
            {
                mHydroxide = CbVb - CaVa / volumeTotal;
                logmHydroxide = -Mathf.Log10(mHydroxide);
                pOH = Mathf.RoundToInt(logmHydroxide);
                pH = 14 - pOH;
                Debug.Log(pH);
            }

        }


    //WEAK ACID vs STRONG BASE

    public void CalculateWeakAcid()
    {
        
        //Start
        if (phi == 0)
        {
            double mproton = Math.Sqrt(constant_Ka * cAcid);
            logmProton = -Mathf.Log10(Convert.ToSingle(mproton));
            pH = Mathf.RoundToInt(logmProton);
            Debug.Log(pH);
        }

        //Before equivalence, 0<phi>1, it considers that [A-] is proportional to phi and that [HA] is proportional to 1-phi
       else if (0 < phi && phi< 1)
        {
            double mProton = constant_Ka * (1 - phi) / phi;
            //double mProton = constant_Ka * (phi) / phi;

            logmProton = -Mathf.Log10(Convert.ToSingle(mProton));
            pH = Mathf.RoundToInt(logmProton);
            Debug.Log(pH);
        }

        ////Half of the equivalence point
        //if (phi == 0.5f)
        //{
        //    mProton = constant_Ka; 
        //    logmProton = -Mathf.Log10(mProton);
        //    pH = Mathf.RoundToInt(logmProton);
        //}

        //At the equivalence point, phi=1
        else if (phi==1)
        {
            //The responsable of  pH is the reaction of the weak base generated A-
            double mHydroxide = Math.Sqrt(constant_Kb * (CaVa / volumeTotal));
            logmHydroxide =-Mathf.Log10(Convert.ToSingle(mHydroxide));
            pOH = Mathf.RoundToInt(logmHydroxide);
            pH = 14 - pOH;
            Debug.Log(pH);
        }

        //After equivalence point, phi>1

        else if (phi>1)
        {
            mHydroxide = CbVb - CaVa / volumeTotal;
            logmHydroxide = -Mathf.Log10(mHydroxide);
            pOH = Mathf.RoundToInt(logmHydroxide);
            pH = 14 - pOH;
            Debug.Log(pH);
        }

        //else if (phi < 0)
        //{
        //    Debug.Log("This is a problem");
        //}

    }


    //WEAK BASE vs STRONG ACID
    //TODO


    //WEAK ACID vs WEAK BASE
    //TODO

}


