﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCountScript : MonoBehaviour
{

    public ParticleSystem particleSys;

    //Count the particles emitted


    void Start()
    {
        particleSys = GetComponent<ParticleSystem>();
    }

    //void Update()
    //{
    //    Debug.Log("Particles Count: " + particleSys.particleCount);
    //    Debug.Log("Particles Alive Count: " + GetAliveParticles());
    //}

    int GetAliveParticles()
    {
        ParticleSystem.Particle[] particles = new ParticleSystem.Particle[particleSys.particleCount];
        return particleSys.GetParticles(particles);
    }

}
