﻿
//Prepared by Jessica Lizeth Dominguez Alfaro
//Main Source for the equations :De Levie, R. (1993). Explicit expressions of the general form of the titration curve in terms of concentration: Writing a single closed-form expression for the titration curve for a variety of titrations without using approximations or segmentation. Journal of Chemical Education, 70(3), 209–217. https://doi.org/10.1021/ed070p209
//This script may serve to calculate the theorethical curves
//All the calculations are based on a list of generated pH

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;


public class TitrationScript : MonoBehaviour
{
    public float cAcid;//"Unknown" for user
    public float cBase; //chosen by user
    public double constant_Ka; //constant and characteristic of each species
    public float vAcid; //chosen by user
    public double vBase; //given during the experiment

    //[H+]
    public double mProton;
    //log[H+]
    public double logmProton;
    //[OH-]
    public double mHydroxide;
    //log[OH-]
    public double logmHydroxide;

    //alphaHA
    public double mAlphaHA;

    //logHA
    public double logmHA;

    //alphaA
    public double mAlphaA;

    //logA
    public double logmA;

    //Vb/Va
    public double VbVa;

    //pH values
    double[] valuespH;

    //To set the potence to multiply the value
    int value = 10;

    //[H+]
    List<double> valuesmProton = new List<double>();
    //[OH-]
    List<double> valuesmHydroxide = new List<double>();
    //log[H+]
    List<double> valuesLogmProton = new List<double>();
    //log[OH-]
    List<double> valuesLogmHydroxide = new List<double>();
    //alphaHA
    List<double> valuesmAlphaHA = new List<double>();
    //alphaA
    List<double> valuesmAlphaA = new List<double>();
    //logHA
    List<double> valuesLogmHA = new List<double>();
    //logA
    List<double> valuesLogmA = new List<double>();
    //Vb/Va
    List<double> valuesVbVa = new List<double>();

    //Vb
    List<double> valuesVb = new List<double>();

    //dict pH vs. Vb
    public Dictionary<float, float> dictionary = new Dictionary<float, float>();

    //private void Start()
    //{
    //    constant_Ka = Math.Pow(10, -4.76f);
    //    cAcid = 0.2f;
    //    cBase = 0.2f;

    //    //creating an array of pH values min 0, max 1, and step of 0.5
    //    double[] valuespH = GeneratepHvalues(2.8f, 14f, 0.1f);

    //    //double[] valuespH = Range(2.8, 14, 0.1).ToArray();
    //    Calculate_mproton(valuespH, valuesmProton); //[H+]
    //    //Calculate_mHydroxide(valuespH, valuesmHydroxide);//[OH-]
    //    //Calculate_logmProton(valuespH, valuesLogmProton);
    //    //Calculate_logmHydroxide(valuespH, valuesLogmHydroxide);
    //    //Calculate_mAlphaHA(valuesmProton, constant_Ka, valuesmAlphaHA);
    //    Calculate_mAlphaA(valuesmProton, constant_Ka, valuesmAlphaA);
    //    //Calculate_logmHA(valuesmAlphaHA, cAcid, valuesLogmHA);
    //    //Calculate_logmA(valuesmAlphaA, cAcid, valuesLogmA);
    //    //Calculate_VbVa(cAcid, cBase, valuesmProton, valuesmHydroxide, valuesmAlphaA, valuesVbVa);
    //    //print(valuesVbVa[1]);

    //}

    public static double[] GeneratepHvalues(float x, float y, float z)
    {
        //x min of the ph range
        //y max of the ph range
        //z resolution of measuring pH (ex. paper=1, ph meter=0.1f, etc..)
        double[] valuespH = Range(x, y, z).ToArray();
        return valuespH;
    }

    public List<double> Calculate_mproton(double[] valuespH, List<double>valuesmProton)
    {
        foreach (double valuepH in valuespH)
        {
            mProton = Math.Pow(value, -valuepH);

            valuesmProton.Add(mProton);

        }
        return valuesmProton;

    } //[H+]

    public List<double> Calculate_logmProton(double[] valuespH, List<double> valuesLogmProton)
    {
        foreach (double valuepH in valuespH)
        {
            logmProton = -1 * valuepH;
            valuesLogmProton.Add(logmProton);
            
        }
        return valuesLogmProton;
    } //log[H+]

    public List<double> Calculate_mHydroxide(double[] valuespH, List<double> valuesmHydroxide)
    { 
        foreach (double valuepH in valuespH)
        {
            mHydroxide= Math.Pow(value, (valuepH-14));
            valuesmHydroxide.Add(mHydroxide);  
        }
        return valuesmHydroxide;

    } //[OH-]

    public List<double> Calculate_logmHydroxide(double [] valuespH, List<double> valuesmLogHydroxide)
    {
        foreach (double valuepH in valuespH)
        {
            logmHydroxide = valuepH-14;
            valuesLogmHydroxide.Add(logmHydroxide);
        }
        return valuesmLogHydroxide;
    } //log [OH-]

    public List<double> Calculate_mAlphaHA(List<double> valuesmProton, double constant_Ka, List<double> valuesmAlphaHA)
    {
        
        foreach (double valueProton in valuesmProton)
        {
            mAlphaHA = valueProton / (valueProton + constant_Ka);
            
            valuesmAlphaHA.Add(mAlphaHA);

        }
        return valuesmAlphaHA;


    } //mAlphaHA

    public List<double> Calculate_mAlphaA(List<double> valuesmProton, double constant_Ka, List<double> valuesmAlphaA)
    {
        foreach (double valueProton in valuesmProton)
        {
            mAlphaA =  constant_Ka / (constant_Ka + valueProton);
            
            //print(mAlphaA);
            valuesmAlphaA.Add(mAlphaA);
        }

        return valuesmAlphaA;
        
    } //mAlphaA

    public List<double> Calculate_logmHA(List<double> valuesmAlphaHA, float cAcid, List<double> valuesLogmHA)
    {
        foreach (double mAlphaHA in valuesmAlphaHA)
        {   
            logmHA = Math.Log10(mAlphaHA * cAcid);
            valuesLogmHA.Add(logmHA);

        }
        return valuesLogmHA;

    } //log mAlphaHA
  
    public List<double> Calculate_logmA(List<double> valuesmAlphaA, float cAcid, List<double> valuesLogmA)
    {
        foreach (double valuemAlphaA in valuesmAlphaA)
        {
            logmA = Math.Log10(valuemAlphaA * cAcid);
            valuesLogmA.Add(logmA);

        }

        return valuesLogmA;

    }//log mAlphaA

    public List<double> Calculate_VbVa(float cAcid, float cBase, List<double> valuesmProton, List<double> valuesmHydroxide, List<double> valuesmAlphaA, List<double> valuesVbVa)
    {

        for (int i = 0; i < valuesmProton.Count; i++)
        {
            {
                VbVa = ((cAcid * valuesmAlphaA[i]) - valuesmProton[i] + valuesmHydroxide[i]) / (cBase + valuesmProton[i] - valuesmHydroxide[i]);
                valuesVbVa.Add(VbVa);
            }

        } 
        return valuesVbVa;
    }//Vb/VA

    public List<double> Calculate_Vb(float vAcid, List<double>valuesVbVa, List<double> valuesVb)
    {
        for (int i = 0; i < valuesVbVa.Count; i++)
        {
            {
                vBase = vAcid * valuesVbVa[i];
                valuesVb.Add(vBase);
            }

        }
        return valuesVb;
    }//Vb


    //Create a titration Array  or dictionary to relate all the values
    public Dictionary<float, float> Create_dictionary_pH_Vb(double[] valuespH, List<double> valuesVb, Dictionary<float, float> dictionary)
    {
        {
            //converting  ph values as keys
            List<float>keys= new List<float>();
            ConverttoFloat(valuespH, keys);

            //converting  valuesvb as values
            //List<float> values = valuesVb.ConvertAll(Convert.ToSingle);
            List<float> values = new List<float>();
            ConverttoFloatList(valuesVb, values);

            //Creating the dictionary
            dictionary = keys.Zip(values, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);

        }
        //print("the dictionary was created" + dictionary.Count);
        
        return dictionary;
        
    }

    //To create values of pH choosing a range, max and min
    public static IEnumerable<double> Range(double min, double max, double step)
    {
        double result = min;
        int decPlaces = BitConverter.GetBytes(decimal.GetBits((decimal)step)[3])[2];
        for (int i = 0; result < max; i++)
        {
            result = min + (step * i);
            yield return Math.Round(result, decPlaces);
        }
    }

    //Method to transform the double values of an array to float list with 2 decimals, not used all the values where repeated
    private IEnumerable<float> ConverttoFloat(double [] x, List<float> y)
    {
        double listvalue;
        foreach (double v in x)
        {
            listvalue=Math.Round(v, 1);
            //print("this is the value rounded" + v);
            //keys.Add(Convert.ToSingle(v));
            y.Add(Convert.ToSingle(listvalue));

        }
        return y;

       
    }

    //Method to transform the double values of a list to float list with 2 decimals, not used all the values where repeated
    private IEnumerable<float> ConverttoFloatList(List<double> x, List<float> y)
    {
        double listvalue;
        foreach (double v in x)
        {
            listvalue=Math.Round(v, 1);
            y.Add(Convert.ToSingle(listvalue));
            //keys.Add(Convert.ToSingle(valuespH[0]));
 
        }
        return y;

    }

}



//Tests
//print("this keys" + keys.Count);
//dictionary= Enumerable.Range(0, y.Count).ToDictionary(i => y[i], i => x[i]);
//valuespH.Zip(valuesVb, (key, date) => new { key, date }).GroupBy(x => x.key).ToDictionary(g => g.Key, g => g.Select(x => x.date).ToList());
//return dictionary;

//print("this values"+values.Count);
//List<float> keys = valuespH.OfType<float>().ToList();

//public List<float> PrepareValues(List<double> valuesVb, List<float> values)

//{
//    List<float> valuesVbSingle = valuesVb.ConvertAll(Convert.ToSingle);



//    foreach (float value in valuesVbSingle)
//    {
//        var x = Math.Round(value, 2);
//        //print("this is the value rounded" + x);
//        values.Add(x);
//    }
//    return values;

//}
