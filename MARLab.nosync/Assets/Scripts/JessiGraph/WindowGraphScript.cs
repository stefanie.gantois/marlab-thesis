﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using System;
using System.Linq;

public class WindowGraphScript : MonoBehaviour

{
    [SerializeField] private Sprite circleSprite;

    //References to graphContainer, labels and dash templates
    private RectTransform graphContainer;
    private RectTransform labelTemplateX;
    private RectTransform labelTemplateY;
    private RectTransform dashTemplateX;
    private RectTransform dashTemplateY;
    private List<GameObject> gameObjectList;


    //Test with list of titration script
    // //double[] valuespH;

    // //Vb/Va
    public  List<double> valuesVbVa = new List<double>();
    // //[H+]
    public  List<double> valuesmProton = new List<double>();

    // //[OH-]
    public  List<double> valuesmHydroxide = new List<double>();

    // //alphaA
    public  List<double> valuesmAlphaA = new List<double>();

    //Vb
    public List<double> valuesVb = new List<double>();

    //dict pH vs. Vb
    public static Dictionary<float, float> dictionary = new Dictionary<float, float>();

    public TitrationScript titrationScript;


    public float cAcid;//"Unknow" for user
    public float cBase; //chosen by userd
    public double constant_Ka; //constant 
    public float vAcid; //chosen by user
    public float vBase; //given during the experiment

   
    private void Awake()
    {
        //Create a list of pH values with a min, max and resolution
        double[] valuespH = TitrationScript.GeneratepHvalues(3.0f, 12.8f, 0.1f);//TODO make sure that the resolution (0.1f) does not give and error due to the number of elements in the list 
        constant_Ka = Math.Pow(10, -4.76f);//Ka of acetic acid, TODO test with pKa
        cAcid = 0.2f;
        cBase = 0.2f;
        vAcid = 100f;

        valuesmProton = titrationScript.Calculate_mproton(valuespH, valuesmProton);
        valuesmHydroxide= titrationScript.Calculate_mHydroxide(valuespH, valuesmHydroxide);
        valuesmAlphaA = titrationScript.Calculate_mAlphaA(valuesmProton, constant_Ka, valuesmAlphaA);
        valuesVbVa = titrationScript.Calculate_VbVa(cAcid, cBase, valuesmProton, valuesmHydroxide, valuesmAlphaA, valuesVbVa);
        valuesVb = titrationScript.Calculate_Vb(vAcid, valuesVbVa, valuesVb);
        dictionary = titrationScript.Create_dictionary_pH_Vb(valuespH, valuesVb, dictionary);

        //print("this is the number of elements in the dictionary" + dictionary.Count);

        //foreach (KeyValuePair<float, float> dic in dictionary)
        //{
        //    Debug.Log("this is the key or ph of dic " + dic.Key);
        //    Debug.Log("this is the value  or Vb of dic "+dic.Value);
        //}

        //reference to the graphContainer
        graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();

        //references for the labels to be duplicated
        labelTemplateX = graphContainer.Find("labelTemplateX").GetComponent<RectTransform>();
        labelTemplateY = graphContainer.Find("labelTemplateY").GetComponent<RectTransform>();

        dashTemplateX = graphContainer.Find("dashTemplateX").GetComponent<RectTransform>();
        dashTemplateY = graphContainer.Find("dashTemplateY").GetComponent<RectTransform>();

        //instantiate the list 
        gameObjectList = new List<GameObject>();

        //CreateCircle(new Vector2(200, 200));

        //List to plot
        //List<int> valueList = new List<int>() { 5, 98, 56, 45, 30, 22, 17, 15, 13, 17, 25, 37, 40, 36, 33, 80, 50, 4, 10, 11};

        //List<float> valueList = new List<float>() { };

        //convert list of Vb/Va as valueList used by the plot and convert them to float
        List<float> valueList = valuesVbVa.ConvertAll(Convert.ToSingle); //to plot Vb/Va
        //List<float> valueList = valuesVb.ConvertAll(Convert.ToSingle); //To plot Vb


        ////Test graph of pH
        //List < double> valueListo = valuespH.ToList();
        //List<float> valueList = valueListo.ConvertAll(Convert.ToSingle); //to plot Vb/Va


        //Convert the list of Vb/Va (double) the value list used by the graph
        //ConverttoFloat(valuesVbVa, valueList);

        //ShowGraph(valueList, -1, (float _i) => "vb/va" + (_i + 1), (float _f) => "pH" + Mathf.RoundToInt(_f));
        ShowGraph(valueList, -1);
        //print("this is the value"+valueList[15]);

        ////From CodeMonkey Utils to test random values
        //FunctionPeriodic.Create(() =>
        //{
        //    valueList.Clear();
        //    for (int i=0; i<15; i++)
        //    {
        //        valueList.Add(UnityEngine.Random.Range(0, 500));
        //    }
        //    ShowGraph(valueList, (int _i) => "Day" + (_i + 1), (float _f) => "$" + Mathf.RoundToInt(_f));

        //}, 0.5f);
    }

    private GameObject CreateCircle(Vector2 anchoredPosition)
    {
        //TODO check how to create the dot with a sphere instead of sprite
        GameObject gameObject = new GameObject("circle", typeof(Image));
        gameObject.transform.SetParent(graphContainer, false);
        gameObject.GetComponent<Image>().sprite = circleSprite;
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPosition;
        rectTransform.sizeDelta = new Vector2(11, 11);

        //Set anchor to lower left side
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        return gameObject;

    }

    //using a delegate to passed parameters to the method
    private void ShowGraph(List<float> valueList, int maxVisibleValueAmount=-1, Func<float, string> getAxisLabelX = null, Func<float, string> getAxisLabelY = null)

    {
        //defining the default case
        if (getAxisLabelX == null)
        {
            //getAxisLabelX = delegate (float _i) { return _i.ToString(); };//progress curve
            getAxisLabelX = delegate (float _f) { return Mathf.Round(_f).ToString(); };
        }

        if (getAxisLabelY == null)
        {
            //getAxisLabelY = delegate (float _i) { return Mathf.Round(_i).ToString(); };//progress curve

            getAxisLabelY = delegate (float _i) { return _i.ToString(); };

            //getAxisLabelY = delegate (float _f) { return  Mathf.RoundToInt(_f).ToString(); };
        }


        if (maxVisibleValueAmount <= 0)
        {
            maxVisibleValueAmount = valueList.Count;
        }

        foreach (GameObject gameObject in gameObjectList)
        {
            Destroy(gameObject);
        }
        //Clean list
        gameObjectList.Clear();


        //Define the graph height
        //float graphHeight = graphContainer.sizeDelta.y;//For the progress curve
        //float graphWidth = graphContainer.sizeDelta.x;

        float graphHeight = graphContainer.sizeDelta.x;
        //print("this is the height" +graphHeight);

        float graphWidth = graphContainer.sizeDelta.y;
        //print("this is the width" + graphWidth);


        //int maxVisibleValueAmount = 5;
        //the distance between each point in the x axis

        //float xSize = graphWidth / (maxVisibleValueAmount + 1);//For the progress curve
        float ySize = graphWidth / (maxVisibleValueAmount + 1);
        //float ySize = graphHeight / (maxVisibleValueAmount + 1);//test the limit of graph


        //Stablish the max values for the graph
        //float yMaximum = valueList[0];//For the progress curve
        //float yMinimum = valueList[0];//For the progress curve

        float xMaximum = valueList[0];
        //print("this is the xMax" + valueList[0]);
        float xMinimum = valueList[0];
        //print("this is the xMin" + xMinimum);

        ////To make the ymax according to the values received
        //foreach(int value in valueList)
        //{
        //    if (value> yMaximum)
        //    {
        //        yMaximum = value;
        //    }

        //    if (value< yMinimum)
        //    {
        //        yMinimum = value;
        //    }
        //}

        ////To make the values according to the ones that are visible
        ////Ensure that i is never out of range by going to negative values
        //for (int i = Mathf.Max(valueList.Count - maxVisibleValueAmount, 0); i < valueList.Count; i++)
        //{
        //    float value = valueList[i];

        //         if (value > yMaximum)
        //    {
        //        yMaximum = value;
        //    }

        //    if (value < yMinimum)
        //    {
        //        yMinimum = value;
        //    }
        //}


        //To make the values according to the ones that are visible
        //Ensure that i is never out of range by going to negative values
        for (int i = Mathf.Max(valueList.Count - maxVisibleValueAmount, 0); i < valueList.Count; i++)
        {
            float value = valueList[i];

            if (value > xMaximum)
            {
                xMaximum = value;
            }

            if (value < xMinimum)
            {
                xMinimum = value;
            }
        }


        //Create a buffer of 20% above the graph
        //yMaximum = yMaximum * 1.2f;

        //Create a buffer of 20% of the size of the difference between Ymax and Ymin

        //float yDifference = yMaximum - yMinimum;//For the progress curve
        float xDifference = xMaximum - xMinimum;

        //if (yDifference <= 0)//For the progress curve
        if (xDifference <= 0)
           {

            //yDifference = 5f;
            xDifference = 5f;

        }
        //yMaximum = yMaximum + (yDifference* 0.2f);//For the progress curve
        //yMinimum = yMinimum - (yDifference * 0.2f);//For the progress curve
        //yMinimum = 0f; //To start the graph at zero, For the progress curve


        xMaximum = xMaximum + (xDifference * 0.2f);
        xMinimum = xMinimum - (xDifference * 0.2f);
        xMinimum = 0f; //To start the graph at zero


        //Reference to previous gameobject
        GameObject lastCircleGameObject = null;

        //Create a variable to save the index value as i is not longer set to zero
        //int xIndex = 0;//For the progress curve
        int yIndex = 0;
        for (int i = Mathf.Max(valueList.Count - maxVisibleValueAmount, 0); i < valueList.Count; i++)
        {
            //float xPosition = xSize + (xIndex * xSize);//For the progress curve
            float yPosition = ySize + (yIndex * ySize);
          

            //provide a normalized value and reduce the value with the y min so that normalized value is correct
            //float yPosition = ((valueList[i]-yMinimum) / (yMaximum-yMinimum)) * graphHeight;//For the progress curve

            float xPosition = ((valueList[i] - xMinimum) / (xMaximum - xMinimum)) * graphHeight;
            //float xPosition = ((valueList[i] - xMinimum) / (xMaximum - xMinimum)) * graphWidth;//test


            //print("These are the values printed"+ (valueList[i] - yMinimum));//For the progress curve
            //print("These are the values printed" + (valueList[i] - xMinimum));

            GameObject circleGameObject = CreateCircle(new Vector2(xPosition, yPosition));

            //add gameobject
            gameObjectList.Add(circleGameObject);

            if (lastCircleGameObject != null) //if it is not the first dot
                {
                    GameObject dotConnectionGameObject = CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition);
                gameObjectList.Add(dotConnectionGameObject);
                }
            lastCircleGameObject = circleGameObject;

            //Create the separators
            RectTransform labelX = Instantiate(labelTemplateX);

            labelX.SetParent(graphContainer, false);//Tell who is the parent 
            labelX.gameObject.SetActive(true);//to activate in the inspector the gameobject

            labelX.anchoredPosition = new Vector2(xPosition, -7f);//set the position of the label, same for x but under the graph container -20f
            //labelX.anchoredPosition = new Vector2(yPosition, -7f);//progress curve


            //labelX.GetComponent<Text>().text = i.ToString();// set the text to the value of i
            labelX.GetComponent<Text>().text = getAxisLabelX(i);

            //TODO change the label instead of index, value of pH
            //print("this is the label of x" + getAxisLabelX(i));
            gameObjectList.Add(labelX.gameObject);



            //Create the dashTemplate X
            RectTransform dashX = Instantiate(dashTemplateX);
            dashX.SetParent(graphContainer, false);//Tell who is the parent 
            dashX.gameObject.SetActive(true);//to activate in the inspector the gameobject
                    //dashX.anchoredPosition = new Vector2(xPosition, -7f);//set the position of the label, same for x but under the graph container -20f
            dashX.anchoredPosition = new Vector2(yPosition, -7f);//set the position of the label, same for x but under the graph container -20f

            gameObjectList.Add(dashX.gameObject);

            //xIndex++;
            yIndex++;
            //print("this is yindex"+yIndex);
        }

        //Set Y separators
        //Choose how many do we need, TODO check if this number can be the # of measurement on each titration
        int separatorCount = 15;
        for (int i = 0; i <= separatorCount; i++)
        {
            RectTransform labelY = Instantiate(labelTemplateY);
            labelY.SetParent(graphContainer, false);//Tell who is the parent 
            labelY.gameObject.SetActive(true);//to activate in the inspector the gameobject
            float normalizedValue = i * 1f / separatorCount;

            labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphHeight);//progress curve
            //labelY.anchoredPosition = new Vector2(-7f, normalizedValue * graphWidth);//


            //labelY.GetComponent<Text>().text = Mathf.RoundToInt(normalizedValue * yMaximum).ToString();// set the text to the value of normalized value * y Maximum

            labelY.GetComponent<Text>().text = getAxisLabelY(xMinimum + (normalizedValue * (xMaximum-xMinimum)));// set the text to the value of normalized value * y Maximum
            //labelY.GetComponent<Text>().text = getAxisLabelY(yMinimum + (normalizedValue * (yMaximum - yMinimum)));// set the text to the value of normalized value * y Maximum

            gameObjectList.Add(labelY.gameObject);


            //Create the dashTemplate Y
            RectTransform dashY = Instantiate(dashTemplateY);
            dashY.SetParent(graphContainer, false);//Tell who is the parent 
            dashY.gameObject.SetActive(true);//to activate in the inspector the gameobject

            //dashY.anchoredPosition = new Vector2(-4f, normalizedValue * graphHeight);//progress curve
            dashY.anchoredPosition = new Vector2(-4f, normalizedValue * graphWidth);//set the position of the label, same for x but under the graph container -20f
            gameObjectList.Add(dashY.gameObject);
        }

    } 

    private GameObject CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB)
    {
        GameObject gameObject = new GameObject("dotConnection", typeof(Image));
        gameObject.transform.SetParent(graphContainer, false);
        gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);//white with transparency

        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

        //To set rotation of the connection
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);

        //horizontal bar
        rectTransform.sizeDelta = new Vector2(distance, 3f);

        //Set anchor to lower left side
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);

        //Position
        rectTransform.anchoredPosition = dotPositionA+dir*distance*0.5f;//place it in the middle

        //To rotate the connection
        rectTransform.localEulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVectorFloat(dir));

        return gameObject;
    }

    //private IEnumerable<int> ConvertpH(double[] valuespH, List<int> valueList)
    //{

    //    List<double> x = valuespH.OfType<double>().ToList();

    //    foreach (double v in x)


    //    {
    //        valueList.Add(Convert.ToInt32(x[0]));
    //    }

    //    return valueList;

    //}

    //Method to transform the double values of a list to float list with 2 decimals, not used all the values where repeated
    private IEnumerable<float> ConverttoFloat(List<double> x, List<float> y)
    {
        foreach (double v in x)
        {
            y.Add(Convert.ToSingle(x[0]));
            print(v);
            print(x);
        }
        return y;
    }

}
