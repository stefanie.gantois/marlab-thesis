﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopcockScript : MonoBehaviour
{

    public float angle = 90f;
    //public float range = 2f;

    Rigidbody myRigidbody;
    

    public float moveVertical;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody>();
       

    }

    // Update is called once per frame
    void Update()
    {
        moveVertical = Input.GetAxis("Vertical");

       
        //print("this is movevertical float" + moveVertical);
        //float moveHorizontal = Input.GetAxis("Horizontal");


        //Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        //myRigidbody.AddForce(movement * speed);

        ProcessRotation(moveVertical*angle);
    }

    private void ProcessRotation(float moveVertical)
    {
        
        transform.localRotation = Quaternion.Euler(moveVertical, 0f, 0f);
        print("opening stopcock");
    }
}

//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class PlayerMovement : MonoBehaviour
//{

//    public float speed = 5f;
//    Rigidbody myRigidbody;

//    // Start is called before the first frame update
//    void Start()
//    {
//        myRigidbody = GetComponent<Rigidbody>();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        float moveVertical = Input.GetAxis("Vertical");
//        float moveHorizontal = Input.GetAxis("Horizontal");


//        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

//        myRigidbody.AddForce(movement * speed);
//    }
//}
