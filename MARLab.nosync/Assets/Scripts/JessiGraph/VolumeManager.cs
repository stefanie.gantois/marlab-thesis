﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Relate the particle count with the volume released from the recipient
//Increase/Decrease accordingly the liquid mesh in the corresponding container


public class VolumeManager : MonoBehaviour
{
    public GameObject liquidMesh, container;
    Transform liquidMeshTransform;

    public float totalVolume;
    public float numberOfDrops;
    public  float addedVolume;// static to use it in the titrationdatabase finder



    public float factorVolume;//1 drop is equivalent to 0.05mL and 100ml is equivalent to 0.1 in the unity scale
    public static float totalVolumeinMl;

    //TODO if used equipment of different volumes this variable needs to be modified
    public float volumeContainer = 100f;
    public float maxVolumeContainer;
    public float minVolumeContainer;

    Vector3 originalScale;

    TitrationDataBase titrationDataBase;

    private void Start()
    {
        liquidMeshTransform = container.transform.Find("Liquid");//TODO change name to be unique for the liquid been added from burette
        originalScale = liquidMeshTransform.localScale;

        //To set a min and max of the volume TODO this does not work

            totalVolume = Mathf.Clamp(originalScale.y, minVolumeContainer, maxVolumeContainer);
            //print("this is the original scale" + originalScale.y);

            //Find the reference in the empty gameobject TitrationManager
            titrationDataBase = GameObject.Find("TitrationManager").GetComponent<TitrationDataBase>();//make the reference in the scene for the object
        

      

    }

    private void Update()
    {
        numberOfDrops = ParticleLauncher.drops;
        //print("this are the number of particles" + numberOfDrops);
        AdjustVolume();
        //titrationDataBase.SearchpHValue();
        //print("this is how much I added" + totalVolumeinMl); 
    }


    public void AdjustVolume()
    {
        //print("adding volume");
        //Changing Volume according to a particle count

        if (totalVolume > 0&& totalVolume<1)//This case applies for both beaker and burette
        { 
        liquidMeshTransform.localScale = new Vector3(liquidMeshTransform.localScale.x, totalVolume, liquidMeshTransform.localScale.z);
            ReadVolume();
        }

        else if (totalVolume > 1)//This case applies for the beakeror Erlenmeyer which scale is been increased
        {

          liquidMeshTransform.localScale = new Vector3(liquidMeshTransform.localScale.x, 1f, liquidMeshTransform.localScale.z);
            ReadVolume();
        }


        else if (totalVolume<=0)//This stops the liquid to grow further
        {
            liquidMeshTransform.localScale = new Vector3(liquidMeshTransform.localScale.x, 0f, liquidMeshTransform.localScale.z);
            ReadVolume();

        }
    }


    //Read the volume from particle counter 
    public void ReadVolume()
    {
        addedVolume = numberOfDrops * factorVolume; //0.05mL is equivalent to the volume added per drop by a normal burette, 0.1=10mL => the factor volume used in the inspector is 0.0005=0.05mL
        totalVolume += addedVolume;//increase the volume
        //print("the original scale is" + originalScale.y);
        totalVolumeinMl = totalVolume * 10 / originalScale.y;
        if (totalVolumeinMl < 0)
        {
            totalVolumeinMl = 0f;//TODO check if this is ok, the totalvolume is not added, maybe send a message and stop addition.
        }
        
        print("this is the total volume from readvolume function" + "  "+ totalVolumeinMl+"mL");
    }


    public void DeleteVolume()
    {
        //print("let's empty the container");
        liquidMeshTransform.localScale = originalScale;
    }

}

