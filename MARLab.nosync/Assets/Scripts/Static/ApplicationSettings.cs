﻿using System.Collections;
using System.Collections.Generic;
using Others;
using UnityEngine;

namespace Static
{
    public enum ScenarioMode
    {
        trainingMode,
        examMode
    }

    /// <summary>
    /// This static script hold information about application wide settings
    /// </summary>
    public static class ApplicationSettings{

        /// <summary>
        /// This setting handles if the performance monitor should be displayed in the AR scenes
        /// </summary>
        public static bool displayPerformanceMonitor = false;

        /// <summary>
        /// This Setting handles which mode the scenario is started: exam mode or training mode.
        /// </summary>
        public static ScenarioMode scenarioMode = ScenarioMode.trainingMode;

        /// <summary>
        /// This Setting handles the minimal plane size needed for tracking: small, middle, large.
        /// </summary>
        public static MinPlanesize planeSize = MinPlanesize.small;

        /// <summary>
        /// This Setting handles if the expert is muted.
        /// </summary>
        public static bool muted = true;

        /// <summary>
        /// This Setting saves the user ID.
        /// </summary>
        public static string idCode = "000";

        /// <summary>
        /// This Setting handles if logging is active.
        /// </summary>
        public static bool loggingData = true;



    }
}
