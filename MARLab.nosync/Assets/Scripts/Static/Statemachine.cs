﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Static;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;
using UnityEngine.Analytics;

/// <summary>
/// The Interaction type that was used with a state change
/// </summary>
public enum InteractionType
{
    Select, //Not used but implemented
    Deselect, //Not used but implemented
    Grab,
    Release, //Not used but implemented
    Interact,
    Combine
}

/// <summary>
/// The information stored for ever state that is registered
/// </summary>
public struct StateInformation
{
    public string primaryObjectName;
    public string secondaryObjectName; //only used for Combining objects
    public InteractionType interactionType;
    public string parameter;

    public StateInformation(string primaryObjectName, string secondaryObjectName, InteractionType interactionType,
        string parameter = " ")
    {
        this.primaryObjectName = primaryObjectName;
        this.secondaryObjectName = secondaryObjectName;
        this.interactionType = interactionType;
        this.parameter = parameter;
    }
}

/// <summary>
/// The statemachine that handles the possible interactions and error feedback of the application
/// </summary>
public class Statemachine
{
    private string randExp()
    {
        int expired = Random.Range(1, 3);

        switch (expired)
        {
            case 1:
                return nacl;
            case 2:
                return glucose;
            default:
                return nacl;
        }
    }

    public static Statemachine Instance = new Statemachine();


    private static List<StateInformation> stateHistory = new List<StateInformation>();

    public int stateNumber = 0;
    public int maxstateNumber = 120;
    public int errorCounter = 0;

    private bool[] progressCheck = new bool[10]
        {false, false, false, false, false, false, false, false, false, false};

    private bool acceptedStateChange = false;

    private string nacl = "NaCl 0,9% 10ml";
    private string glucose = "Glucose 10ml";

    public bool labeling = false;
    public bool labelingDone = false;
    public bool labelingSuccessfull = false;
    public GameObject labelingScript = null;
    public string notExpired;
    public int errorTotal = 0;
    public string infobox = null;
    //public string instruction = StringDict.InstanceStringDict.lookupString(StoredTexts.placePrefab);
    public string instruction = "Put on protective gloves";
    public string directinfobox = null;
    //public int progressProcent = 0;
    //public int progressStep = 1;
    //public const int progressStepMax = 19;
    public StateForPlacement stateForPlacement = StateForPlacement.grabPieces;
    public bool showOverlayBox = false;

    public event Action ScenarioComplete;
    public event Action UpdateInfoBox;

    private int tempState = 0;
    private System.DateTime startTime = System.DateTime.Now;

    //------------ Booleans visibilities ------------
    public bool showPhenol = false;
    public bool showMethyl = false;
    public int questions = 11; 
    public bool questioning = false;
    public bool startQuestioning = false;
    public bool introducing = false;
    public bool startIntro = false;
    public bool titrating = false;

    public bool reviewPossible = false;

    //---------- Boolean states ----------
    private bool gloves = false;
    private bool glasses = false;
    private bool buretteAttached = false;
    private bool buretteFilled = false;
    private bool erlenmeyerFilled = true;
    private bool indicatorAdded = false;
    private bool buretteRinsed = false;
    private bool beakerFilled = false;
    private bool funnelPlaced = false;
    private bool dropperFilled = false;
    private bool erlenmeyerOnStand = false;
    private bool flaskOpen = false;
    private bool magnetInErlen = false;
    private bool pinAdded = false;
    private bool buretteRead = false;

    public bool stoptTitrating = false;
    public bool cleaned = false;

    public bool[] thrownAway = new bool[4] {true, false, false, false};

    private int hintNext = 3; // show hint after 3 mistakes

    private Statemachine()
    {
        this.notExpired = randExp();
    }

    public bool requestStateChange(StateInformation stateInformation)
    {
        //each Combine triggers twice so the second pass doesn't need to be checked in the catalog again or added to the history
        if ((stateHistory.Count > 1) &&
            (stateHistory[stateHistory.Count - 1].secondaryObjectName == stateInformation.primaryObjectName) &&
            (stateHistory[stateHistory.Count - 1].primaryObjectName == stateInformation.secondaryObjectName))
            //this returns the result of the first combine check
            return acceptedStateChange;

        stateHistory.Add(stateInformation);

        acceptedStateChange = CheckStateCatalog(stateHistory[stateHistory.Count - 1]);

        PrintStateChangeRequest(stateHistory[stateHistory.Count - 1], acceptedStateChange);

        return acceptedStateChange;
    }

    /// <summary>
    /// Print the requested Stage to the log.
    /// </summary>
    /// <param name="stateInformation"></param>
    /// <param name="correct"></param>
    private void PrintStateChangeRequest(StateInformation stateInformation, bool correct)
    {
        string correctString = correct ? "CORRECT" : "WRONG";
        switch (stateInformation.interactionType)
        {
            case InteractionType.Combine:
                /*Debug.Log("Statemachine: Trying to COMBINE " + stateInformation.primaryObjectName + " and " +
                          stateInformation.secondaryObjectName + ". This was: " + correctString);
                */
                if (!correct)
                {
                    AnalyticsResult result = Analytics.CustomEvent("Combine",
                    new Dictionary<string, object>
                        {
                            { "Primary",stateInformation.primaryObjectName},
                            { "Secondary",stateInformation.secondaryObjectName},
                            { "Correct", correctString},
                            { "id", ApplicationSettings.idCode }
                        });
                    //Debug.Log("analytic result: " + result);
                }
                break;
            case InteractionType.Select:
                /*Debug.Log("Statemachine: Trying to SELECT " + stateInformation.primaryObjectName + ". This was: " +
                          correctString);*/
                break;
            case InteractionType.Deselect:
                /*Debug.Log("Statemachine: Trying to DESELECT " + stateInformation.primaryObjectName + ". This was: " +
                          correctString);*/
                break;
            case InteractionType.Grab:
                /*Debug.Log("Statemachine: Trying to GRAB " + stateInformation.primaryObjectName + ". This was: " +
                          correctString);*/
                break;
            case InteractionType.Release:
                /*Debug.Log("Statemachine: Trying to RELEASE " + stateInformation.primaryObjectName + ". This was: " +
                          correctString);*/
                break;
            case InteractionType.Interact:
                /*Debug.Log("Statemachine: Trying to INTERACT WITH " + stateInformation.primaryObjectName +
                          ". This was: " + correctString);*/
                if (!correct)
                {
                    AnalyticsResult result2 = Analytics.CustomEvent("Interact",
                    new Dictionary<string, object>
                        {
                            { "Primary",stateInformation.primaryObjectName},
                            { "Correct", correctString},
                            { "id", ApplicationSettings.idCode }
                        });
                    //Debug.Log("analytic result: " + result2);
                }
                break;
            default:
            {
                break;
            }
        }

        if(tempState != stateNumber)
        {
          System.TimeSpan deltaTime = System.DateTime.Now - startTime;
          //Debug.Log("Transitioning from state: " + tempState + " to state: " + stateNumber + " took: " + deltaTime);
          tempState = stateNumber;
        }
    }

    private bool CheckStateCatalog(StateInformation stateInformation)
    {
        //return true;
        bool returnValue = true;


        if (stateInformation.interactionType == InteractionType.Combine)
        {
            returnValue = false;

            if (gloves && glasses)
            {
                if (stateInformation.secondaryObjectName.Equals("Arbeitsfläche"))
                {
                    return true;
                }

                if (stateInformation.primaryObjectName.Equals("Burette") &&
                    stateInformation.secondaryObjectName.Equals("Stand"))
                {
                    if (!buretteAttached)
                    {
                        returnValue = true;
                        stateNumber += 5;
                        buretteAttached = true;
                        InstructionDict.InstanceInstructionDict.putDone(1);
                    }
                }

                if (stateInformation.primaryObjectName.Equals("Wash") &&
                    stateInformation.secondaryObjectName.Equals("BuretteStand"))
                {
                    if (buretteAttached)
                    {
                        buretteRinsed = true;
                        returnValue = true;
                        stateNumber += 5;
                        InstructionDict.InstanceInstructionDict.putDone(2);
                    }
                    else
                    {
                        //errorCounter++;
                        returnValue = false;
                    }
                }

                if (stateInformation.primaryObjectName.Equals("Funnel") &&
                    stateInformation.secondaryObjectName.Equals("BuretteStand"))
                {
                    if (buretteRinsed)
                    {
                        returnValue = true;
                        funnelPlaced = true;
                        stateNumber += 5;
                        InstructionDict.InstanceInstructionDict.putDone(3);
                    }
                    else
                    {
                        //infobox = "You have to rinse the burette to make sure no other chemicals are present in the burette.";
                        //UpdateInfoBox.Invoke();
                    }
                }

                if (stateInformation.primaryObjectName.Equals("Titrant") &&
                    stateInformation.secondaryObjectName.Equals("BeakerEmpty"))
                {
                    if (!beakerFilled && flaskOpen)
                    {
                        beakerFilled = true;
                        returnValue = true;
                        stateNumber += 5;
                        InstructionDict.InstanceInstructionDict.putDone(4);
                    }
                    else
                    {
                        //you cannot fill the same beaker twice
                        //allow it but nothing happens, after two or three times saying that is enough
                    }
                }

                if (stateInformation.primaryObjectName.Equals("BeakerEmpty") &&
                    (stateInformation.secondaryObjectName.Equals("BuretteStand") ||
                    stateInformation.secondaryObjectName.Equals("Funnel")))
                {
                    if (funnelPlaced && beakerFilled)
                    {
                        returnValue = true;
                        stateNumber += 5;
                        buretteFilled = true;
                        InstructionDict.InstanceInstructionDict.putDone(5);
                    }
                    else
                    {
                        //infobox = "You should use a funnel, to pour the titrant.";
                        //UpdateInfoBox.Invoke();
                        returnValue = false;
                    }
                }

                if (stateInformation.primaryObjectName.Equals("PhenoDropper") &&
                        stateInformation.secondaryObjectName.Equals("Erlenmeyer"))
                {
                    if (erlenmeyerFilled)
                    {

                        if (indicatorAdded)
                        {
                            returnValue = false;
                            //you should not add two indicators...
                        }
                        else
                        {
                            //deze is de juiste
                            returnValue = true;
                            indicatorAdded = true;
                            stateNumber += 5;
                            InstructionDict.InstanceInstructionDict.putDone(8);
                        }
                    }
                    else
                    {
                        //error
                    }
                }

                if (stateInformation.primaryObjectName.Equals("MethylDropper") &&
                    stateInformation.secondaryObjectName.Equals("Erlenmeyer"))
                {
                    if (erlenmeyerFilled)
                    {
                        if (indicatorAdded)
                        {
                            returnValue = false;
                            //you should not add two indicators...
                        }
                        else
                        {
                            //foute indicator
                            returnValue = true;
                            indicatorAdded = true;
                            stateNumber += 5;
                            InstructionDict.InstanceInstructionDict.putDone(8);
                        }
                    }
                }

                if ((stateInformation.primaryObjectName.Equals("MethylOrange") ||
                    stateInformation.primaryObjectName.Equals("Phenolphthalein")) &&
                    stateInformation.secondaryObjectName.Equals("Erlenmeyer"))
                {
                    returnValue = false;
                    //infobox = "You have to grab the top to add the indicator.";
                    //UpdateInfoBox.Invoke();
                }

                if (stateInformation.primaryObjectName.Equals("PhenoDropper") &&
                    stateInformation.secondaryObjectName.Equals("Phenolphthalein"))
                {
                    returnValue = true;
                }

                if (stateInformation.primaryObjectName.Equals("MethylDropper") &&
                    stateInformation.secondaryObjectName.Equals("MethylOrange"))
                {
                    returnValue = true;
                }

                if (stateInformation.primaryObjectName.Equals("Magnet") &&
                    stateInformation.secondaryObjectName.Equals("Erlenmeyer"))
                {
                    magnetInErlen = true;
                    returnValue = true;
                    stateNumber += 5;
                    InstructionDict.InstanceInstructionDict.putDone(9);
                }

                if (stateInformation.primaryObjectName.Equals("Erlenmeyer") &&
                    (stateInformation.secondaryObjectName.Equals("Stand") ||
                    stateInformation.secondaryObjectName.Equals("Stirrer")))
                {
                    if (buretteFilled && magnetInErlen)
                    {
                        if (indicatorAdded)
                        {
                            erlenmeyerOnStand = true;
                            returnValue = true;
                            stateNumber += 5;
                            InstructionDict.InstanceInstructionDict.putDone(10);
                        }
                        else
                        {
                            returnValue = false;
                            // can you add indicator later?
                        }
                    }
                    else
                    {
                        returnValue = false;
                        infobox = "The burette needs to be filled before putting the erlenmeyer below it. This is because we do not want titrant leaking into the Erlenmeyer.";
                        UpdateInfoBox.Invoke();
                    }
                }

                if (stateInformation.primaryObjectName.Equals("ProbeTip") &&
                    stateInformation.secondaryObjectName.Equals("ErlenStand"))
                {
                    if (erlenmeyerOnStand)
                    {
                        returnValue = true;
                        stateNumber += 5;
                        InstructionDict.InstanceInstructionDict.putDone(11);
                        pinAdded = true;
                    }
                }

                
                    if ((stateInformation.primaryObjectName.Equals("TitrantPouring")||
                        stateInformation.primaryObjectName.Equals("BeakerFilled") ||
                        stateInformation.primaryObjectName.Equals("BuretteStand")) &&
                    stateInformation.secondaryObjectName.Equals("JerryCanBlack"))
                    {
                        returnValue = true;
                        stateNumber += 5;
                    }
                    if (stateInformation.primaryObjectName.Equals("ErlenStand") &&
                    (stateInformation.secondaryObjectName.Equals("JerryCanBlack") ||
                    stateInformation.secondaryObjectName.Equals("JerryCanWhite")))
                    {
                        returnValue = true;
                        stateNumber += 5;
                    }
                
            }
        }

        if (stateInformation.interactionType == InteractionType.Interact)
        {
            returnValue = false;

            if (stateInformation.primaryObjectName.Equals("Handschuhbox"))
            {
                returnValue = true;
                if (glasses)
                {
                    InstructionDict.InstanceInstructionDict.putDone(0);
                    stateNumber += 5;
                }
                gloves = true;
            }

            if (stateInformation.primaryObjectName.Equals("Glasses"))
            {
                returnValue = true;
                if (gloves)
                {
                    InstructionDict.InstanceInstructionDict.putDone(0);
                    stateNumber += 5;
                }
                glasses = true;
            }

            if (stateInformation.primaryObjectName.Equals("PhenoDropper") ||
                stateInformation.primaryObjectName.Equals("Phenolphthalein"))
            {
                showPhenol = !showPhenol;
                returnValue = true;
            }

            if (stateInformation.primaryObjectName.Equals("MethylDropper") ||
                stateInformation.primaryObjectName.Equals("MethylOrange"))
            {
                showMethyl = !showMethyl;
                returnValue = true;
            }

            if (gloves && glasses)
            {
                if (stateInformation.primaryObjectName.Equals("BuretteStand"))
                {
                    if (buretteFilled)
                    {
                        returnValue = true;
                        InstructionDict.InstanceInstructionDict.putDone(6);
                        buretteRead = true;
                    }
                }

                if (stateInformation.primaryObjectName.Equals("Titrant"))
                {
                    returnValue = true;
                    flaskOpen = true;
                }

                if (stateInformation.primaryObjectName.Equals("Stirrer"))
                {
                    if (pinAdded && !funnelPlaced && buretteRead)
                    {
                        returnValue = true;
                        InstructionDict.InstanceInstructionDict.putDone(12);

                        titrating = true;
                        stateNumber += 5;
                        InstructionDict.InstanceInstructionDict.putDone(13);

                        startIntro = true;
                    }
                }
            }
        }

        if (stateInformation.interactionType == InteractionType.Grab)
        {
            if (stateInformation.primaryObjectName.Equals("FunnelStand"))
            {
                if (funnelPlaced && beakerFilled)
                {
                    InstructionDict.InstanceInstructionDict.putDone(7);
                }
                funnelPlaced = false;
            }
        }
        
        InstructionDict.InstanceInstructionDict.setAssistanceInstruction();
        if (!returnValue)
        {
            hintNext--;
            errorTotal++;
            if (hintNext == 0)
            {
                UpdateInfoBox.Invoke();
                hintNext = 3;
            }
        } else
        {
            hintNext = 3;
        }
        return returnValue;
    }

    public void Reset()
    {
        Instance = new Statemachine();
    }

    private StoredTexts StringToEnum(string toConvert)
    {
        StoredTexts result = StoredTexts.test;
        if (toConvert.Equals("Desinfektionsspender"))
        {
            result = StoredTexts.disinfectant;
        }
        else if (toConvert.Equals("Tupfer"))
        {
            result = StoredTexts.swap;
        }
        else if (toConvert.Equals("Desinfektionstücherspender"))
        {
            result = StoredTexts.disinfectantWipe;
        }
        else if (toConvert.Equals("Spritzen Verpackung"))
        {
            result = StoredTexts.syringePackage;
        }
        else if (toConvert.Equals("Spritzenverschluss Verpackung"))
        {
            result = StoredTexts.syringeStoppperPackage;
        }
        else if (toConvert.Equals("Kanülen Verpackung"))
        {
            result = StoredTexts.needlePackage;
        }
        else if (toConvert.Equals("Handschuhbox"))
        {
            result = StoredTexts.glovebox;
        }
        else if (toConvert.Equals("Spritze"))
        {
            result = StoredTexts.syringe;
        }
        else if (toConvert.Equals("Kanüle"))
        {
            result = StoredTexts.needle;
        }
        else if (toConvert.Equals("Spritzenschale"))
        {
            result = StoredTexts.syringeTray;
        }
        else if (toConvert.Equals("Spritzenverschluss"))
        {
            result = StoredTexts.syringeStopper;
        }
        else if (toConvert.Equals("NaCl 0,9% 10ml"))
        {
            result = StoredTexts.nacl;
        }
        else if (toConvert.Equals("Glucose 10ml"))
        {
            result = StoredTexts.glucose;
        }
        else if (toConvert.Equals("Aqua 10ml"))
        {
            result = StoredTexts.aqua;
        }
        else if (toConvert.Equals("Mülleimer"))
        {
            result = StoredTexts.bin;
        }
        else if (toConvert.Equals("Stift"))
        {
            result = StoredTexts.pen;
        }
        else if (toConvert.Equals("Kanülenabwurfbehälter"))
        {
            result = StoredTexts.needleBin;
        }
        else if (toConvert.Equals("Partusiten Verpackung"))
        {
            result = StoredTexts.partusistenPackage;
        }
        else if (toConvert.Equals("Partusiten"))
        {
            result = StoredTexts.partusisten;
        }
        else if (toConvert.Equals("Katheter Verpackung"))
        {
            result = StoredTexts.venousCannulaPackage;
        }

        return result;
    }

    /// <summary>
    /// Checks whether or not an object has already placed on the working space.
    /// </summary>
    /// <returns>Returns true if atleast one object has already been placed on the working space.</returns>
    private bool ObjectHasBeenPlaced()
    {
        foreach (bool check in progressCheck)
        {
            if (check)
                return true;
        }

        return false;
    }

    public void doneCleanUp()
    {
        bool done = true;
        foreach (bool check in thrownAway)
        {
            if (!check)
                done = false;
        }
        if (done)
        {
            cleaned = true;
            InstructionDict.InstanceInstructionDict.putDone(14);
            //startIntro = true;
            ScenarioComplete.Invoke();
        }
    }
    public bool ARVersion(string idCode)
    {
        int number;
        bool isParsable = Int32.TryParse(idCode, out number);
        if (isParsable)
        {
            if (number % 2 == 0)
            {
                return false;
            }
        }
        return true;
    }
}
