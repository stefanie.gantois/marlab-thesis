﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct StringLine
{
    public string gerText;
    public string engText;

    public StringLine(string gerText, string engText)
    {
        this.gerText = gerText;
        this.engText = engText;
    }
}

public enum Language
{
    eng,
    ger
}

public enum StoredTexts
{
    interactDefault,
    combineDefault,
    buttonRelease,
    buttonGrab,
    information,

    attach,
    clean,
    add,
    fill,
    place,
    open,
    stirring,
    reading,

    interactDisinfectant,
    interactGlovebox,
    combineGlovebox,
    interactOpen,
    combineDisinfectant,
    combineOpen,
    interactDisinfectantSwap,
    combineDisinfectantSwap,
    interactBin,
    combineBin,
    interactPlug,
    combinePlug,
    interactPen,
    combinePen,

    test,
    placePrefab,
    disinfectant,
    swap,
    disinfectantWipe,

    putOnGlovesSuccess,
    putOnGlassesSuccess,
    interactGlasses,
    syringePackage,
    syringeStoppperPackage,
    needlePackage,
    glovebox,

    waste,

    syringe,
    needle,
    syringeTray,
    syringeStopper,
    nacl,
    glucose,
    aqua,
    bin,
    pen,
    needleBin,
    partusistenPackage,
    partusisten,
    cathetherPackage,
    dontPlaceOnWorkspacePartOne,
    dontPlaceOnWorkspacePartTwo,
    throwAwayError,
    disinfectWorkspace,
    disinfectWorkspaceError,
    disinfectWorkspaceErrorHint,
    disinfectWorkspaceDone,
    collectMaterials,
    collectMaterialsError,
    wrongCarrierError,
    wrongCarrierErrorEnd,
    collectCathetherError,
    collectMaterialsMissingPartOne,
    materials,
    carrier,
    handDisinfection,
    handDisinfectionBeforeGrabbingPieces,
    handDisinfectionError,
    handDisinfectionErrorEnd,
    handDisinfectionSuccess,
    putOnGloves,
    putOnGlovesError,
    putOnGlovesErrorEnd,
    openPackages,
    disposeSwap,
    disposeSwapErrorEnd,
    disposeSwapError,
    openPackagesError,
    openMissingPackagesPartOne,
    packages,
    connectNeedle,
    connectNeedleError,
    removeNeedleCap,
    connectCarrier,
    connectCarrierError,
    useCarrier,
    useCarrierError,
    useCarrierErrorEnd,
    removeCarrier,
    removeCarrierError,
    connectMedication,
    connectMedicationError,
    useMedication,
    useMedicationError,
    useMedicationErrorEnd,
    removeMedication,
    disposeNeedle,
    connectPlug,
    disposeNeedleError,
    disposeNeedleErrorEnd,
    connectPlugError,
    labelSyringe,
    placeSyringeInTray,
    labelSyringeError,
    labelSyringeErrorEnd,
    cleanWorkspace,
    placeSyringeInTrayErrorEnd,
    cleanWorkspaceError,
    cleanWorkspaceMedicationError,
    cleanWorkspaceMedicationErrorEnd,
    endMessage,
    errors,
    uiStartTraining,
    uiCreateTraining,
    uiProjectInformation,
    uiSettings,
    uiAppname,
    combineConnect,
    combineBreak,
    uiTrainingOneName,
    uiTrainingOneDescription,
    uiTrainingTwoName,
    uiTrainingTwoDescription,
    uiTrainingThreeName,
    uiTrainingThreeDescription,
    uiTrainingStart,
    uiTrainingHead,
    uiInfoTitle,
    uiInfoTextPartOne,
    uiInfoTextPartTwo,
    uiInfoPartner,
    uiInfoMoreInformation,
    uiInfoButtonHompage,
    uiInfoButtonPublication,
    uiInfoHead,
    uiSettingsMode,
    uiSettingsModeTraining,
    uiSettingsDevOption,
    uiSettingsLanguage,
    uiSettingsHead,
    uiTutorialScenarioOneHeaderDescription,
    uiTutorialScenarioOneDescription,
    uiTutorialScenarioOneHeaderGrab,
    uiTutorialScenarioOneHelpGrab,
    uiTutorialScenarioOneHeaderUse,
    uiTutorialScenarioOneHelpUse,
    uiTutorialScenarioOneHeaderCombine,
    uiTutorialScenarioOneHelpCombine,
    uiTutorialLoading,
    uiOnboardingScan,
    uiOnboardingPlace,
    collectAquaError,
    collectExpiredError,
    partusistenThrownAwayTooEarly,
    disinfectWorkspaceExamMode,
    collectMaterialsExamMode,
    mindHygieneExamMode,
    prepareMaterialsExamMode,
    prepareTocolysisExamMode,
    postProcessingExamMode,
    labelSyringeExamMode,
    collectMissingMaterialsExamMode,
    openPackagesExamMode, 
    collectVenousCannula,
    venousCannulaPackage,
    prepareTocolysisExamModeStart,
    interactRemoveNeedleCap,
    interactPull,
    uiSettingsPlanesize,
    discardedExpired,
    directInfoBoxClean,
    directInfoBoxPackage,
    directInfoBoxGlas,
    directInfoBoxPull,
    directInfoBoxLabel,
    directInfoBoxMedication
}

public class StringDict
{
    Dictionary<StoredTexts, StringLine> storedStrings = new Dictionary<StoredTexts, StringLine>();

    public static Language selectedLanguage = Language.ger;
    public static StringDict InstanceStringDict = new StringDict();

    private StringDict()
    {
        storedStrings.Add(StoredTexts.interactDefault, new StringLine
            (
            "use",
            "use"
            ));
        storedStrings.Add(StoredTexts.combineDefault, new StringLine
            (
            "combine",
            "combine"
            ));

        storedStrings.Add(StoredTexts.buttonGrab, new StringLine
            (
            "grab",
            "Grab"
            ));
        storedStrings.Add(StoredTexts.buttonRelease, new StringLine
            (
            "put down",
            "put down"
            ));
        storedStrings.Add(StoredTexts.information, new StringLine
            (
            "Information",
            "Information"
            ));
        storedStrings.Add(StoredTexts.attach, new StringLine
            (
            "attach",
            "attach"
            ));
        storedStrings.Add(StoredTexts.clean, new StringLine
            (
            "clean",
            "clean"
            ));
        storedStrings.Add(StoredTexts.add, new StringLine
            (
            "add",
            "add"
            ));
        storedStrings.Add(StoredTexts.fill, new StringLine
            (
            "pour",
            "pour"
            ));
        storedStrings.Add(StoredTexts.place, new StringLine
            (
            "place",
            "place"
            ));
        storedStrings.Add(StoredTexts.open, new StringLine
            (
            "open",
            "open"
            ));
        storedStrings.Add(StoredTexts.stirring, new StringLine
            (
            "Start stirring",
            "Start stirring"
            ));
        storedStrings.Add(StoredTexts.reading, new StringLine
            (
            "Read",
            "Read"
            ));

        storedStrings.Add(StoredTexts.interactDisinfectant, new StringLine
            (
            "Hände desinfizieren",
            "hand disinfection"
            ));
        storedStrings.Add(StoredTexts.combineDisinfectant, new StringLine
            (
            "Desinfizieren",
            "disinfect"
            ));
        storedStrings.Add(StoredTexts.interactGlovebox, new StringLine
            (
            "put on gloves",
            "put on gloves"
            ));
        storedStrings.Add(StoredTexts.interactGlasses, new StringLine
             (
             "put on glasses",
             "put on glasses"
             ));
        storedStrings.Add(StoredTexts.waste, new StringLine
             (
             "throw liquid away",
             "throw liquid away"
             ));
        storedStrings.Add(StoredTexts.combineGlovebox, new StringLine
            (
            "Kombinieren",
            "combine"
            ));
        storedStrings.Add(StoredTexts.interactOpen, new StringLine
            (
            "interact",
            "open"
            ));
        storedStrings.Add(StoredTexts.combineOpen, new StringLine
            (
            "Kombinieren",
            "combine"
            ));
        storedStrings.Add(StoredTexts.interactDisinfectantSwap, new StringLine
            (
            "Fläche desinfizieren",
            "desk disinfection"
            ));
        storedStrings.Add(StoredTexts.combineDisinfectantSwap, new StringLine
            (
            "Kombinieren",
            "combine"
            ));
        storedStrings.Add(StoredTexts.interactBin, new StringLine
            (
            "Benutzen",
            "use"
            ));
        storedStrings.Add(StoredTexts.combineBin, new StringLine
            (
            "Wegwerfen",
            "dispose"
            ));
        storedStrings.Add(StoredTexts.interactPlug, new StringLine
            (
            "Benutzen",
            "use"
            ));
        storedStrings.Add(StoredTexts.combinePlug, new StringLine
            (
            "Verschließen",
            "seal"
            ));
        storedStrings.Add(StoredTexts.interactPen, new StringLine
            (
            "Benutzen",
            "use"
            ));
        storedStrings.Add(StoredTexts.combinePen, new StringLine
            (
            "Beschriften",
            "label"
            ));

        storedStrings.Add(StoredTexts.disinfectant, new StringLine
            (
            "Desinfektionsspender",
            "disinfectent dispenser"
            ));
        storedStrings.Add(StoredTexts.swap, new StringLine
            (
            "Tupfer",
            "swab"
            ));
        storedStrings.Add(StoredTexts.disinfectantWipe, new StringLine
            (
            "Desinfektionstücherspender",
            "disinfecting tissue dispenser"
            ));
        storedStrings.Add(StoredTexts.syringePackage, new StringLine
            (
            "Spritzenverpackung",
            "syringe wrapping"
            ));
        storedStrings.Add(StoredTexts.syringeStoppperPackage, new StringLine
            (
            "Spritzenverschlussverpackung",
            "syringe closure wrapping"
            ));
        storedStrings.Add(StoredTexts.needlePackage, new StringLine
            (
            "Kanülenverpackung",
            "tube wrapping"
            ));
        storedStrings.Add(StoredTexts.glovebox, new StringLine
            (
            "Handschuhbox",
            "glove dispenser"
            ));
        storedStrings.Add(StoredTexts.syringe, new StringLine
            (
            "Spritze",
            "syringe"
            ));
        storedStrings.Add(StoredTexts.needle, new StringLine
            (
            "Kanüle",
            "hello"
            ));
        storedStrings.Add(StoredTexts.syringeTray, new StringLine
            (
            "Spritzenschale",
            "syringe tray"
            ));
        storedStrings.Add(StoredTexts.syringeStopper, new StringLine
            (
            "Spritzenverschluss",
            "syringe closure"
            ));
        storedStrings.Add(StoredTexts.nacl, new StringLine
            (
            "NaCl 0,9% 10ml",
            "Sodium Chloride NaCl 0,9% 10ml"
            ));
        storedStrings.Add(StoredTexts.glucose, new StringLine
            (
            "Glucose5% 10ml",
            "Glucose5% 10ml"
            ));
        storedStrings.Add(StoredTexts.aqua, new StringLine
            (
            "Aqua 10ml",
            "Water/Aqua 10 ml"
            ));
        storedStrings.Add(StoredTexts.bin, new StringLine
            (
            "Mülleimer",
            "bin"
            ));
        storedStrings.Add(StoredTexts.pen, new StringLine
            (
            "Stift",
            "pen"
            ));
        storedStrings.Add(StoredTexts.needleBin, new StringLine
            (
            "Kanülenabwurfbehälter",
            "tube disposal container"
            ));
        storedStrings.Add(StoredTexts.partusistenPackage, new StringLine
            (
            "Partusistenverpackung",
            "Partusisten wrapping"
            ));
        storedStrings.Add(StoredTexts.partusisten, new StringLine
            (
            "Partusisten",
            "Partusisten"
            ));
        storedStrings.Add(StoredTexts.cathetherPackage, new StringLine
            (
            "Katheterberpackung",
            "catheter wrapping"
            ));
        storedStrings.Add(StoredTexts.packages, new StringLine
            (
            "Verpackungen",
            "wrappings"
            ));
        storedStrings.Add(StoredTexts.placePrefab, new StringLine
            (
            "Platzieren Sie den Aufbau.",
            "Place the setup."
            ));
        storedStrings.Add(StoredTexts.dontPlaceOnWorkspacePartOne, new StringLine
            (
            "Aus Hygienegründen darf die" + "\"",
            "For hygienic reasons " + "\""
            ));
        storedStrings.Add(StoredTexts.dontPlaceOnWorkspacePartTwo, new StringLine
            (
            "\"" + " nicht auf die Arbeitsfläche gelegt werden.",
            "\"" + "  may not be placed on the desk."
            ));
        storedStrings.Add(StoredTexts.throwAwayError, new StringLine
            (
            "Das Aufräumen des Arbeitsplatzes, sowie das Entsorgen der verbrauchten Materialien sollte erst am Ende gemacht werden.",
            "Cleaning the area and disposing of materials should be done last."
            ));
        storedStrings.Add(StoredTexts.disinfectWorkspace, new StringLine
            (
            "Führen Sie eine Flächendesinfektion durch.",
            "Disinfect the desk."
            ));
        storedStrings.Add(StoredTexts.disinfectWorkspaceError, new StringLine
            (
            "Um die Verunreinigung von Medikamenten zu vermeiden, sollte zunächst eine Wischdesinfektion der Arbeitsfläche mit einem Flächendesinfektionsmittel erfolgen.",
            "The desk should be cleaned with a wiping disinfectant first to avoid a contamination of the medication and materials used."
            ));
        storedStrings.Add(StoredTexts.disinfectWorkspaceErrorHint, new StringLine
            (
            "Die Arbeitsfläche sollte vor dem Platzieren der Gegenstände desinfiziert werden.",
            "The desk should be disinfected before placing the materials."
            ));
        storedStrings.Add(StoredTexts.disinfectWorkspaceDone, new StringLine
            (
            "Sie haben die Arbeitsfläche desinfiziert.",
            "You have disinfected the desk."
            ));
        storedStrings.Add(StoredTexts.collectMaterials, new StringLine
            (
            "Wählen Sie die benötigten Materialien vom Tablett aus und platzieren Sie diese ungeöffnet auf der Arbeitsfläche",
            "Choose the needed materials from the tray and place the unopened packages on the desk. "
            ));
        storedStrings.Add(StoredTexts.collectMaterialsError, new StringLine
            (
            "Wählen Sie folgende Materialien aus und platzieren Sie diese ungeöffnet auf der Arbeitsfläche: Spritze, Spritzenverschluss, Aufziehkanüle, Tupfer, Partusisten, Trägerlösung.",
            "Choose the following materials and place the unopened packages on the desk: syringe, syringe closure, tube, swab, Partusisten, carrier solution."
            ));
        storedStrings.Add(StoredTexts.wrongCarrierError, new StringLine
            (
            "Folgende Trägerlösungen können zur Herstellung der gebrauchsfertigen Injektionslösung verwendet werden: Glukoselösung 5 %, physiologische Kochsalzlösung, Ringerlösung, Ringer-Lactat-Lösung, Xylitlösung 5 %, Xylitlösung 10 %. Achten Sie auch auf das Verfallsdatum der Trägerlösung!",
            "These carrier solutions can be used to make a ready-for-use solution for injection: glucose solution 5%, physiological saline solution, Ringer's solution, Ringer's lactate solution, xylitol solution 5%, xylitol solution 10%. Mind the expiration date of the carrier solution!"
            ));
        storedStrings.Add(StoredTexts.wrongCarrierErrorEnd, new StringLine
            (
            "Für die Trägerlösung einer Notfalltokolyse können folgende Injektionslösungen verwendet werden: Glukoselösung 5 %, physiologische Kochsalzlösung, Ringerlösung, Ringer-Lactat-Lösung, Xylitlösung 5 %, Xylitlösung 10 %. Achten Sie außerdem auf das Verfallsdatum der Trägerlösung.",
            "These injection solutions can be used as carrier solutions for an emergency tocolysis: glucose solution 5%, physiological saline solution, Ringer's solution, Ringer's lactate solution, xylitol solution 5%, xylitol solution 10%. Mind the expiration date of the carrier solution!"
            ));
        storedStrings.Add(StoredTexts.collectCathetherError, new StringLine
            (
            "Eine Venenverweilkanüle wird für die Notfalltokolyse nicht benötigt.",
            "A catheter is not needed for an emergency tocolysis."
            ));
        storedStrings.Add(StoredTexts.collectMaterialsMissingPartOne, new StringLine
            (
            "Es fehlen noch",
            "You still need"
            ));
        storedStrings.Add(StoredTexts.materials, new StringLine
            (
            "Materialien",
            "materials"
            ));
        storedStrings.Add(StoredTexts.carrier, new StringLine
            (
            "Trägerlösung",
            "carrier solution"
            ));
        storedStrings.Add(StoredTexts.handDisinfection, new StringLine
            (
            "Führen Sie eine korrekte Händedesinfektion durch.",
            "Properly disinfect your hands."
            ));
        storedStrings.Add(StoredTexts.
            handDisinfectionBeforeGrabbingPieces, new StringLine
            (
            "Sich die Hände bereits vor der Auswahl der Utensilien zu desinfizieren ist löblich. Zwingend notwendig ist dies jedoch erst vor dem Öffnen dieser!",
            "It's praiseworthy to desinfect your hands even before selecting the utils. Nontheless, it is only mandatory before opening the selected utils!"
            ));
        storedStrings.Add(StoredTexts.handDisinfectionError, new StringLine
            (
            "Bevor Arzneimittel vorbereitet werden, ist eine hygienische Händedesinfektion erforderlich",
            "Before prepping medicinal products, a hygienic hand disinfection is required."
            ));
        storedStrings.Add(StoredTexts.handDisinfectionErrorEnd, new StringLine
            (
            "Vor dem Auspacken der Medikamente, muss eine Handdesinfektion erfolgen.",
            "Hands must be disinfected before opening the medicinal products."
            ));
        storedStrings.Add(StoredTexts.handDisinfectionSuccess, new StringLine
            (
            "Indikation zur Händedesinfektion: Vor Patient*innenkontakt Vor einer aseptischen Tätigkeit Nach Kontakt mit potentiell infektiösen Material Nach Patient*innenkontakt Nach Kontakt mit der unmittelbaren Patient*innenumgebung Vor der Vorbereitung und Durchführung von Medikamenten / Injektionen",
            "Reasons for hand disinfection: before contact with patients, before an aseptic taks, after contact with potentially infectious material, after contact with patients, after contact with patient environment, before prepping and performing medication/injections."
            ));
        storedStrings.Add(StoredTexts.putOnGloves, new StringLine
            (
            "Zum Eigenschutz werden unsterile Handschuhe benötigt.",
            "Non-sterile gloves are used for self-protection."
            ));
        storedStrings.Add(StoredTexts.putOnGlovesError, new StringLine
            (
            "Handschuhe als Eigenschutzmaßnahme beachten, so dass Hautkontakt mit dem Medikament vermieden wird (auch i.v.-Medikamente können transdermal aufgenommen werden)",
            "Mind the gloves for self-protection in order to avoid skin contact with the medicinal products (i. v.-medications can be transdermally absorbed."
            ));
        storedStrings.Add(StoredTexts.putOnGlovesErrorEnd, new StringLine
            (
            "Um Hautkontakt mit dem Medikamenten zu vermeiden, sollten unsterile Handschuhe angelegt werden.",
            "In order to avoid skin contact with medicinal products, non-sterile gloves should be used."
            ));
        storedStrings.Add(StoredTexts.putOnGlovesSuccess, new StringLine
            (
            "You have put on gloves.",
            "You have put on gloves."
            ));
        storedStrings.Add(StoredTexts.putOnGlassesSuccess, new StringLine
            (
            "You have put on glasses.",
            "You have put on glasses."
            ));
        storedStrings.Add(StoredTexts.openPackages, new StringLine
            (
            "Öffnen Sie die Verpackungen der ausgewählten Materialien.",
            "Open the selected materials' packages."
            ));
        storedStrings.Add(StoredTexts.disposeSwap, new StringLine
            (
            "Entsorgen Sie den Tupfer mit dem Ampullenkopf in dem Kanülenabwurfbehälter.",
            "Dispose of the swap and the ampoule tip into the tube disposal box."
            ));
        storedStrings.Add(StoredTexts.disposeSwapErrorEnd, new StringLine
            (
            "Der Ampullenkopf der Glasampulle sollte direkt in dem Kanülenabwurfbehälter entsorgt werden.",
            "The glass ampoule tip should be disposed of immediately into the tube disposal box."
            ));
        storedStrings.Add(StoredTexts.disposeSwapError, new StringLine
            (
            "Zum Selbstschutz sollten Sie den Tupfer mit dem Ampullenkopf sofort in den Kanülenabwurfbehälter werfen.",
            "For your own protection you should dispose of the swab and the ampoule tip immediately into the tube disposal box."
            ));
        storedStrings.Add(StoredTexts.openPackagesError, new StringLine
            (
            "Öffnen Sie die Verpackungen Ihrer Materialien an der Stempelseite und belassen Sie diese in geöffneter Verpackung. Zum Brechen einer Glasampulle muss ein Tupfer verwendet werden. Es wird empfohlen alle Verpackungen zu öffnen, bevor Sie fortfahren.",
            "Open the packages from the stamped side and leave the products in their open wrapping. A swab must be used to break a glass ampoule. It is advised to open all packages before continuing."
            ));
        storedStrings.Add(StoredTexts.openMissingPackagesPartOne, new StringLine
            (
            "Öffnen Sie die folgenden ",
            "Open the following packages"
            ));
        storedStrings.Add(StoredTexts.connectNeedle, new StringLine
            (
            "Wählen Sie die Spritze und konnektieren Sie diese mit der Aufziehkanüle",
            "Select the syringe and connect it to the drain tube."
            ));
        storedStrings.Add(StoredTexts.connectNeedleError, new StringLine
            (
            "Wählen Sie die Spritze und konnektieren Sie diese mit der Aufziehkanüle, um fortfahren zu können.",
            "Select the syrince and connect it to the drain tube to continue."
            ));
        storedStrings.Add(StoredTexts.removeNeedleCap, new StringLine
            (
            "Entfernen Sie die Schutzkappe von der Aufziehkanüle.",
            "Remove the protective cap from the drain tube."
            ));
        storedStrings.Add(StoredTexts.connectCarrier, new StringLine
            (
            "Wählen Sie die Spritze und konnektieren Sie diese mit der Trägerlösung",
            "Select the syringe and connect it to the carrier solution."
            ));
        storedStrings.Add(StoredTexts.connectCarrierError, new StringLine
            (
            "Bedenken Sie immer zuerst die Trägerlösung aufzuziehen, danach folgt das Medikament.",
            "Remember to always draw up the carrier solution first, then the medicin."
            ));
        storedStrings.Add(StoredTexts.useCarrier, new StringLine
            (
            "Ziehen Sie die Spritze mit 4 ml Trägerlösung auf.",
            "Draw up 4ml of carrier solution into the syringe."
            ));
        storedStrings.Add(StoredTexts.useCarrierError, new StringLine
            (
            "Die Spritze muss mit der Trägerlösung aufgezogen werden. Überprüfen Sie erneut die Dosierungsmenge (4ml).",
            "Carrier solution must be drawn up into the syringe. Double-check the dosage (4ml)."
            ));
        storedStrings.Add(StoredTexts.useCarrierErrorEnd, new StringLine
            (
            "Beachten Sie die richtige Menge der Trägerlösung: 4ml.",
            "Mind the correct dosage of carrier solution: 4ml."
            ));
        storedStrings.Add(StoredTexts.removeCarrier, new StringLine
            (
            "Entfernen Sie die Trägerlösung und stellen Sie diese zurück auf die Arbeitsfläche.",
            "Remove the carrier solution and place it back on the desk."
            ));
        storedStrings.Add(StoredTexts.removeCarrierError, new StringLine
            (
            "Die Trägerlösung muss zuerst entfernt werden, bevor sie etwas Neues aufziehen können.",
            "The carrier solution has to be removed before you can draw up something else."
            ));
        storedStrings.Add(StoredTexts.connectMedication, new StringLine
            (
            "Wählen Sie die Spritze und konnektieren Sie diese mit dem Medikament",
            "Select the syringe and connect it to the medication."
            ));
        storedStrings.Add(StoredTexts.useMedication, new StringLine
            (
            "Ziehen Sie mit der Spritze 1ml von dem Medikament auf.",
            "Draw up 1ml of medication into the syringe."
            ));
        storedStrings.Add(StoredTexts.useMedicationError, new StringLine
            (
            "Überprüfen Sie erneut die Dosierungsmenge (1ml Partusisten).",
            "Reconsider the dosage (1ml Partusisten)."
            ));
        storedStrings.Add(StoredTexts.useMedicationErrorEnd, new StringLine
            (
            "Beachten Sie die richtige Menge des Medikaments: 1ml Partusisten",
            "Mind the correct dosage: 1ml Partusisten"
            ));
        storedStrings.Add(StoredTexts.removeMedication, new StringLine
            (
            "Entfernen Sie das Medikament von der Kanüle.",
            "Remove the medication from the drain tube."
            ));
        storedStrings.Add(StoredTexts.disposeNeedle, new StringLine
            (
            "Entsorgen Sie die Kanüle direkt im Kanülenabwurfbehälter.",
            "Dispose of the tube directly into the tube disposal box."
            ));
        storedStrings.Add(StoredTexts.connectPlug, new StringLine
            (
            "Wählen Sie die Spritze und konnektieren Sie diese mit dem Kombistopfen.",
            "Select the syringe and connect it to the sealing plug."
            ));
        storedStrings.Add(StoredTexts.disposeNeedleError, new StringLine
            (
            "Entsorgen Sie die Aufziehkanüle direkt in den Kanülenabwurfbehälter, als Schutz vor Eigenverletzung.",
            "Dispose of the drain tube directly into the tube disposal bin, to protect yourself from injuring."
            ));
        storedStrings.Add(StoredTexts.disposeNeedleErrorEnd, new StringLine
            (
            "Die benutzte Kanüle sollte als Schutz vor Eigenverletzung immer sofort in den Kanülenabwurfbehälter entsorgt werden.",
            "The used tube should immediately be disposed of into the tube disposal bin to protect yourself from injury."
            ));
        storedStrings.Add(StoredTexts.connectPlugError, new StringLine
            (
            "Verbinden Sie die Spritze hygienisch sachgemäß mit dem Kombistopfen.",
            "Connect the syringe with the sealing plug in a hygienically appropriate manner."
            ));
        storedStrings.Add(StoredTexts.labelSyringe, new StringLine
            (
            "Beschriften Sie die Spritze mit Wirkstoff, Zusammensetzung, Datum, Uhrzeit und Ihrem Kürzel, damit die Injektionslösung identifizierbar bleibt.",
            "Label the syringe with substance, compostion, date, time and your signature, so that the injection solution can be identified later."
            ));
        storedStrings.Add(StoredTexts.placeSyringeInTray, new StringLine
            (
            "Legen Sie die Spritze in die Spritzenschale.",
            "Place the syringe in the syringe tray."
            ));
        storedStrings.Add(StoredTexts.labelSyringeError, new StringLine
            (
            "Zur Wahrung der Patient*innensicherheit ist es notwendig, dass die aufgezogenen Substanzen auf der Spritze vermerkt werden. Darüber hinaus sollten Datum und Uhrzeit vermerkt werden, damit die Haltbarkeit der aufgezogenen Lösung nicht überschritten wird. Zum Schluss sollten Sie Ihr Kürzel oder Unterschrift hinterlassen, falls eine dritte Person die Injektion durchführt. (Beispiel: Notfalltokolyse, Partusisten 25 mikrogramm in 4 ml NaCl, 14.07.2016, 16:30, KL)",
            "For patient safety it is required to note the substances on the syringe. Moreover, date and time should be noted to monitor the expiration time of the solution. Lastly, leave your signature in case someone else performs the injection. (Example: Emergeny tocolysis, Partusisten 25 microgram in 4ml NaCl, 14/07/2016, 4:30pm, KL)"
            ));
        storedStrings.Add(StoredTexts.labelSyringeErrorEnd, new StringLine
            (
            "Denken Sie nach der Zubereitung der Notfalltokolyse daran, die Spritze zu beschriften.",
            "After preparing the emergency tocolysis, remember to label the syringe."
            ));
        storedStrings.Add(StoredTexts.cleanWorkspace, new StringLine
            (
            "Räumen Sie Ihren Arbeitsplatz auf.",
            "Clean up the work area."
            ));
        storedStrings.Add(StoredTexts.placeSyringeInTrayErrorEnd, new StringLine
            (
            "Die fertig hergestellte Spritze sollte nicht auf die Arbeitsfläche, sondern auf das Tablet abgelegt werden.",
            "The fully prepared syringe should be placed on the tray instead of the desk."
            ));
        storedStrings.Add(StoredTexts.cleanWorkspaceError, new StringLine
            (
            "Räumen Sie Ihren Arbeitsplatz auf und entsorgen Sie bitte den Müll.",
            "Clean up your work area and dispose of the waste."
            ));
        storedStrings.Add(StoredTexts.cleanWorkspaceMedicationError, new StringLine
            (
            "Die Partusistenglasampulle gehört in den Kanülenabwurfbehälter.",
            "The glasbottle of Partusisten goes into the tube disposal bin."
            ));
        storedStrings.Add(StoredTexts.cleanWorkspaceMedicationErrorEnd, new StringLine
            (
            "Die Partusistenglasampulle muss in den Kanülenabwurfbehälter entsorgt werden, nicht in den Mülleimer.",
            "The glasbottle of Partusisten is to be disposed of into the tube disposal box instead of the bin."
            ));
        storedStrings.Add(StoredTexts.endMessage, new StringLine
            (
            "Gratulation! Sie haben erfolgreich eine Notfalltokolyse zubereitet. Mit ",
            "Well done! You have successfully prepared an emergency tocolysis. You made"
            ));
        storedStrings.Add(StoredTexts.errors, new StringLine
            (
            " Fehlern.",
            " mistakes."
            ));

        storedStrings.Add(StoredTexts.uiStartTraining, new StringLine
            (
            "Augmented Reality \nTraining starten",
            "Augmented Reality \nStart practising"
            ));
        storedStrings.Add(StoredTexts.uiCreateTraining, new StringLine
            (
            "Augmented Reality \nTraining erstellen",
            "Augmented Reality \nCreate scenario"
            ));
        storedStrings.Add(StoredTexts.uiProjectInformation, new StringLine
            (
            "Heb@AR: \nProjekt Informationen",
            "Heb@AR: \nProject information"
            ));
        storedStrings.Add(StoredTexts.uiSettings, new StringLine
            (
            "Einstellungen",
            "Settings"
            ));
        storedStrings.Add(StoredTexts.uiAppname, new StringLine
            (
            "Heb@AR",
            "Heb@AR"
            ));
        storedStrings.Add(StoredTexts.combineConnect, new StringLine
            (
            "Aufstecken",
            "plug in"
            ));
        storedStrings.Add(StoredTexts.combineBreak, new StringLine
            (
            "Aufbrechen",
            "break open"
            ));
        storedStrings.Add(StoredTexts.uiTrainingOneName, new StringLine
            (
            "Vorbereitung einer Notfalltokolyse",
            "Preparing an emergency tocolysis"
            ));
        storedStrings.Add(StoredTexts.uiTrainingOneDescription, new StringLine
            (
            "Schwierigkeit: Anfänger\nTrainingsdauer: 5 Minuten\nVorerfahrung: /\nTrainingsart: Training@Home",
            "Difficulty: Beginner\nPractise duration: 5 minutes\nExperience: /\nPractice type: Practise@Home"
            ));
        storedStrings.Add(StoredTexts.uiTrainingTwoName, new StringLine
            (
            "Vorbereitung einer Schwangeren auf eine Sectio caesarea",
            "Preparing a pregnant woman for a Caesarean section"
            ));
        storedStrings.Add(StoredTexts.uiTrainingTwoDescription, new StringLine
            (
            "Schwierigkeit: Fortgeschrittene\nTrainingsdauer: 20 Minuten\nVorerfahrung: Modul HK 08 & HK 10\nTrainingsart: SkillsLab Training",
            "Difficulty: Advanced\nPractise duration: 20 minutes\nExperience: Modul HK 08 & HK 10\nPractice type: SkillsLab Training"
            ));
        storedStrings.Add(StoredTexts.uiTrainingThreeName, new StringLine
            (
            "Reanimation eines Neugeborenen",
            "Resuscitation of an infant"
            ));
        storedStrings.Add(StoredTexts.uiTrainingThreeDescription, new StringLine
            (
            "Schwierigkeit: Fortgeschrittene\nTrainingsdauer: 15 Minuten\nVorerfahrung: Modul HK 10\nTrainingsart: SkillsLab & Training@Home",
            "Difficulty: Advanced\nPractise duration: 15 minutes\nExperience: Modul HK 10\nPractice type: SkillsLab & Practise@Home"
            ));
        storedStrings.Add(StoredTexts.uiTrainingStart, new StringLine
            (
            "Starten...",
            "start..."
            ));
        storedStrings.Add(StoredTexts.uiTrainingHead, new StringLine
            (
            "Training Auswählen",
            "Select practise type"
            ));
        storedStrings.Add(StoredTexts.uiInfoTitle, new StringLine
            (
            "Heb@AR | Augmented-Reality-gestütztes Lernen in der hochschulischen Hebammenausbildung",
            "Heb@AR | AR supported learning in higher midwife education"
            ));
        storedStrings.Add(StoredTexts.uiInfoTextPartOne, new StringLine
            (
            "Während des Studiums zur Hebamme und der damit einhergehenden Vorbereitung auf die Berufsbefähigung, wie auch im späteren Berufsleben, ist zur Vermeidung mütterlicher und kindlicher Morbidität und Mortalität neben dem Erwerb fachlicher Kompetenzen das Management geburtshilflicher Notfälle unabdingbar.\n \nNotfälle und verschiedene Komplikationen kommen nur selten in der Praxis vor. Dies führt dazu, dass das Management und die Abläufe zumeist erst während der ersten Berufsjahre sicherer werden.Vor diesem Hintergrund ist die Entwicklung von fachdidaktischen Konzepten notwendig, die diese Lernziele adäquat unterstützen. Hierbei stehen die Organisation und Durchführung der Versorgung unter Zuhilfenahme geeigneter Technologien nach vorangegangener Prüfung auf deren Notwendigkeit sowie dieAnleitung eines adäquaten und effizienten Notfallmanagements im Fokus. Dadurch soll die Sicherheit von Mutter und Kind / Kindern innerhalb der geburtshilflichen Versorgung erhöht werden.",
            "During university education for midwives and the associated professional qualification it is of high importance to foster the skills to manage obstretic emergencies to avoid maternal and child morbidity and mortality. Emergencies and various complications rarely occur in practical experience. This means that management and processes usually only become more confident during the first years of working. Therefore, the development of didactic concepts that meet these learning objectives is necessary. These will combine the implementation of treatment processes with the help of suitable technological support, which offers needed additional value, with instructions for adequate and efficient emergency management. In this way, maternal and child safety in obstretical care can be enhanced."
	          ));
        storedStrings.Add(StoredTexts.uiInfoTextPartTwo, new StringLine
            (
            "An dieser Stelle setzt das Forschungsvorhaben an. Für den primärqualifizierenden Bachelorstudiengang Hebammenkunde an der Hochschule für Gesundheit Bochum (hsg Bochum) soll ein vollständiges digitales Lehr-/Lernkonzept unter Einsatz von ortsunabhängigen, mehrbenutzerfähigen Augmented Reality (AR) Trainingssimulationen entwickelt, evaluiert und implementiert werden.\n\nEs werden modellhaft AR - Szenarien zu den Themen “Vorbereiten einer Notfalltokolyse”, “Reanimation eines Neugeborenen“ und “Vorbereitung einer Schwangeren auf eine Sectio caesarea (Kaiserschnitt)” entwickelt.Die Ergebnisse können auf weitere Module im Studiengang sowie auf andere Studiengänge übertragen werden.Die spätere Verwendung der notfallbezogenen Trainingsszenarien ist für berufstätige Hebammen sowie Wiedereinsteiger*innen im Sinne der Continuing Education über die kooperierende Fachgesellschaft Deutsche Gesellschaft für Hebammenwissenschaft e.V. (DGHWi) geplant.",
            "This is where the research project sets is focus. For the graduate study programm for midwifery at the University of Applied Health Sciences (hsg) Bochum we will develop, evaluate and implement a fully digital learning concept using location-independent, multi-user supported, Augmented-Reality (AR)-practise simulations.\n\nModel AR scenarios on the subjects of “preparing an emergency tocolysis“, “resuscitation of an infant“ and “Preparing a pregnant woman for a Caesarian section“ will be implemented. Results can be transfeerd to other modules in the course as well as to other courses. Later use of the emergency-related practise scenarios is planned for midwives on the job as will as returners to the profession according to the concept of continuing education, via the cooperating German Society for Midwifery Science (DGHWi)."
            ));
        storedStrings.Add(StoredTexts.uiInfoPartner, new StringLine
            (
            "Projektpartner:",
            "Project partners:"
            ));
        storedStrings.Add(StoredTexts.uiInfoMoreInformation, new StringLine
            (
            "Weitere Informationen:",
            "More information:"
            ));
        storedStrings.Add(StoredTexts.uiInfoButtonHompage, new StringLine
            (
            "Projekt Homepage",
            "Project website"
            ));
        storedStrings.Add(StoredTexts.uiInfoButtonPublication, new StringLine
            (
            "Publikationen",
            "Publications"
            ));
        storedStrings.Add(StoredTexts.uiInfoHead, new StringLine
            (
            "Projekt Heb@AR Info",
            "Project info Heb@AR"
            ));
        storedStrings.Add(StoredTexts.uiSettingsMode, new StringLine
            (
            "Szenariomodus:",
            "Scenario mode"
            ));
        storedStrings.Add(StoredTexts.uiSettingsModeTraining, new StringLine
            (
            "Trainingsmodus",
            "Practise mode"
            ));
        storedStrings.Add(StoredTexts.uiSettingsPlanesize, new StringLine
            (
            "Flächengröße:",
            "Plane size"
            ));
        storedStrings.Add(StoredTexts.uiSettingsDevOption, new StringLine
            (
            "Entwickleroptionen:",
            "Developer settings"
            ));
        storedStrings.Add(StoredTexts.uiSettingsLanguage, new StringLine
            (
            "Sprache:",
            "Language:"
            ));
        storedStrings.Add(StoredTexts.uiSettingsHead, new StringLine
            (
            "Einstellungen",
            "Settings"
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHeaderDescription, new StringLine
            (
            "Augmented Reality Training@Home:\nVorbereitung einer Notfalltokolyse",
            "Augmented Reality Training@Home:\nPreparing of emergency tokolysis"
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneDescription, new StringLine
            (
            "Es ist 06:30h am Morgen. Sie befinden sich im Frühdienst. Bei der routinemäßigen Überprüfung der geburtshilflich relevanten Geräte und Materialien zu Schichtbeginn fällt Ihnen auf, dass die vorbereitete Notfalltokolyse bereits 36 Stunden alt ist und verworfen werden muss. Sie entschließen sich eine neue Notfalltokolyse vorzubereiten.",
            ""
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHeaderGrab, new StringLine
            (
            "Objekte “Greifen“",
            "Objects “Grab“"
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHelpGrab, new StringLine
            (
            "Um Objekte zu greifen, nähern Sie sich dem Objekt, bewegen Sie den weißen Cursor über das Objekt und drücken Sie den “Greifen“ Button.\n\nNun kann das Objekt bewegt werden.Drücken Sie den “Ablegen“ Button um das Objekt fallen zu lassen.",
            "to grab an object, go near an object, move the white cursor over the object and press the “Grab“ button.\n\n Now you can move the object. Press the “Put down“ button to let the object go."
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHeaderUse, new StringLine
            (
            "Objekte “Benutzen“",
            "Objects “Use“"
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHelpUse, new StringLine
            (
            "Um Objekte zu benutzen, nähern Sie sich dem Objekt, bewegen Sie den weißen Cursor über das Objekt und drücken Sie den “Benutzen“ Button.\n\nObjekte können auch benutzt werden, wenn diese bereits gegriffen sind.",
            "To use an object, go near an object, move the white cursor over the object an pree the “Use“ button.\n\nObjects can also be used while grabbed."
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHeaderCombine, new StringLine
            (
            "Objekte “Kombinieren“",
            "Objects “Combine“"
            ));
        storedStrings.Add(StoredTexts.uiTutorialScenarioOneHelpCombine, new StringLine
            (
            "Um Objekte zu kombinieren, verbinden Sie ein bereits gegriffenes Objekt mit einem weitern und drücken Sie anschließend dann den “Kombinieren“ Button.",
            "To Combine objects, overlap an already grabbed object with another object and press the “combine“ button."
            ));
        storedStrings.Add(StoredTexts.uiTutorialLoading, new StringLine
            (
            "Loading...",
            "Loading..."
            ));
        storedStrings.Add(StoredTexts.uiOnboardingScan, new StringLine
            (
            "Move the device over the workspace to detect planes.",
            "Move the device over the workspace to detect planes."
            ));
        storedStrings.Add(StoredTexts.uiOnboardingPlace, new StringLine
            (
            "Tap on the screen to place the materials at the shown location.",
            "Tap on the screen to place the materials at the shown location."
            ));
        storedStrings.Add(StoredTexts.collectAquaError, new StringLine(
            "Folgende Trägerlösungen können zur Herstellung der gebrauchsfertigen Injektionslösung verwendet werden: Glukoselösung 5 %, physiologische Kochsalzlosung, Ringerlösung, Ringer-Lactat - Losung, Xylitlösung 5 %, Xylitlösung 10 %.",
            ""
            ));
        storedStrings.Add(StoredTexts.collectExpiredError, new StringLine(
            "Zur Grundlage der Vorbereitung gehört es das Verfallsdatum zu überprüfen.",
            ""
            ));
        storedStrings.Add(StoredTexts.partusistenThrownAwayTooEarly, new StringLine(
            "Legen Sie die Ampulle des Medikaments auf das Spritzentablett, sodass die Injektionslösung identifizierbar bleibt. Erst nachdem Sie das Klebeetikett beschriftet haben, kann die Ampulle im Abwurfbehälter entsorgt werden.",
            ""
            ));
        storedStrings.Add(StoredTexts.disinfectWorkspaceExamMode, new StringLine(
            "Führen Sie die Reinigung der Arbeitsfläche durch.",
            ""
            ));
        storedStrings.Add(StoredTexts.collectMaterialsExamMode, new StringLine(
            "Bereiten Sie nun alle benötigten Utensilien vor und legen diese auf die Arbeitsfläche ab.",
            ""
            ));
        storedStrings.Add(StoredTexts.mindHygieneExamMode, new StringLine(
            "Beachten Sie die Hygiene.",
            ""
            ));
        storedStrings.Add(StoredTexts.prepareMaterialsExamMode, new StringLine(
            "Bereiten Sie nun alle Materialien vor.",
            ""
            ));
        storedStrings.Add(StoredTexts.prepareTocolysisExamMode, new StringLine(
            "Bereiten sie die Notfalltokolyse zu.",
            ""
            ));
        storedStrings.Add(StoredTexts.postProcessingExamMode, new StringLine(
            "Nachbereitung: Entsorgen Sie die Kanüle und verschließen Sie Ihre Spritze anschließend.",
            ""
            ));
        storedStrings.Add(StoredTexts.labelSyringeExamMode, new StringLine(
            "Beschriften Sie nun die zubereitete Notfalltokolyse. Danach können Sie Ihren Platz aufräumen.",
            ""
            ));
        storedStrings.Add(StoredTexts.collectMissingMaterialsExamMode, new StringLine(
            "Es fehlen noch folgende Materialien",
            ""
            ));
        storedStrings.Add(StoredTexts.openPackagesExamMode, new StringLine(
            "Öffnen Sie noch folgende Verpackungen",
            ""
            ));
        storedStrings.Add(StoredTexts.venousCannulaPackage, new StringLine(
            "Venenkanüle Verpackung",
            "hello"
            ));
        storedStrings.Add(StoredTexts.collectVenousCannula, new StringLine(
            "Eine Venenkanüle wird für die Notfalltokolyse nicht benötigt.",
            "dumb"
            ));
        
        storedStrings.Add(StoredTexts.prepareTocolysisExamModeStart, new StringLine
            (
            "Nun können Sie mit der Zubereitung der Notfalltokolyse starten.",
            "Now you can start with the preparation of an emergency tocolysis"
            ));
        storedStrings.Add(StoredTexts.interactRemoveNeedleCap, new StringLine
            (
            "Entfernen",
            "Remove"
            ));
        storedStrings.Add(StoredTexts.interactPull, new StringLine
            (
            "Aufziehen",
            "Pull"
            ));
        storedStrings.Add(StoredTexts.discardedExpired, new StringLine
            (
            "Sie haben das abgelaufene Medikament entsorgt. Sehr gut!",
            "You tossed the expired Medication. Good!"
            ));
        storedStrings.Add(StoredTexts.directInfoBoxClean, new StringLine
            (
            "Welcome to the lab!",
            "Welcome to the lab!"
            ));
        storedStrings.Add(StoredTexts.directInfoBoxPackage, new StringLine
            (
            "You need to choose an indicator, but choose wisely!",
            "You need to choose an indicator, but choose wisely!"
            ));
        storedStrings.Add(StoredTexts.directInfoBoxGlas, new StringLine
            (
            "Zum Eigenschutz sollte ein Tupfer bei Glasampullen verwendet werden. Es wird immer von der Sollbruchstelle weg aufgebrochen.",
            "For your own safety you should use a dab on glass vials. Always break in the direction away from the defined breaking point."
            ));
        storedStrings.Add(StoredTexts.directInfoBoxPull, new StringLine
            (
            "Damit eine korrekte Dosierung erfolgt, sollte immer Luft mitaufgezogen werden, so dass sich keine Flüssigkeit in der Aufziehkanüle befindet. ",
            "To ensure correct dosage, always draw some air into the syringe, so that no liquid is left in the tube."
            ));
        storedStrings.Add(StoredTexts.directInfoBoxLabel, new StringLine
            (
            "Die Skalierung der Spritze sollte nicht mit dem Etikett überklebt werden, so dass diese sichtbar bleibt.  ",
            "The syringes scale may not be covered by the label."
            ));
        storedStrings.Add(StoredTexts.directInfoBoxMedication, new StringLine
            (
            "You need to choose an indicator, but choose wisely!",
            "You need to choose an indicator, but choose wisely!"
            ));
        

        //StringDict.InstanceStringDict.lookupString(StoredTexts.test);
    }
    public string lookupString(StoredTexts toLookUpText)
    {
        string result = "no text in dictionary found";
        if (storedStrings.ContainsKey(toLookUpText))
        {
            StringLine foundDictEntry = storedStrings[toLookUpText];
            switch (selectedLanguage)
            {
                case Language.eng:
                    result = foundDictEntry.engText;
                    break;
                case Language.ger:
                    result = foundDictEntry.gerText;
                    break;
            }
        }
        return result;

    }
}
