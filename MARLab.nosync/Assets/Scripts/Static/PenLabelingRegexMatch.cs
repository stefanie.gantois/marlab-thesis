﻿using System;

namespace Static
{
    public static class PenLabelingRegexMatch
    {
        /// <summary>
        /// Check if everything we need is in this string.
        ///
        /// Cheat code to bypass this at runtime: "skipit"
        /// </summary>
        /// <param name="labelText">text to check</param>
        /// <returns>True if the label contains everything, false if something is missing</returns>
        public static bool CheckLabel(string labelText)
        {
            //Bypass labeling for development purposes
            if (labelText == "skipit" || labelText == "Skipit")
            {
                return true;
            }
            
            //Check name
            if (!labelText.ContainsCaseinsensitive("Notfalltokolyse"))
            {
                return false;
            }
            
            //Check Med
            if(!labelText.ContainsCaseinsensitive("Partusisten") && !labelText.Contains("Fenoterolhydrobromid"))
            {
                return false;
            }
            if (!labelText.ContainsCaseinsensitive("25"))
            {
                return false;
            }
            
            //Check Carrier solution
            if (!labelText.ContainsCaseinsensitive("NaCl")
                && !labelText.ContainsCaseinsensitive("G-5") 
                && !labelText.ContainsCaseinsensitive("G5"))
            {
                return false;
            }
            if (!labelText.ContainsCaseinsensitive("4"))
            {
                return false;
            }
            
            //Check date
            //....no, too much regex stuff.
            
            //Everything in the string: return true!
            return true;
        }

        private static bool ContainsCaseinsensitive(this string source, string toCheck)
        {
            return source?.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}