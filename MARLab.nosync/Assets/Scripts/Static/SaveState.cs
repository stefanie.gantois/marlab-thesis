﻿public class SaveState
{
    public int selectedLanguage = 1;
    public bool displayPerfMonitor = false;
    public int szenarioMode = 1;
    public int planeSize = 1;
    public bool logging = true;
}
