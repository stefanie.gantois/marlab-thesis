﻿using UnityEngine;

namespace Static
{
    /// <summary>
    /// Normalizes stuff
    /// </summary>
    public static class Normalizer
    {
        /// <summary>
        /// Normalizes a TouchPosition so movement is device independent.
        /// </summary>
        /// <param name="deviceSpecificTouchPosition">the device/resolution specific touchposition</param>
        /// <returns>a normalized position (0 to 1 on both axis)</returns>
        public static Vector2 NormalizeTouchPosition(Vector2 deviceSpecificTouchPosition)
        {
            Vector2 touchPosition = deviceSpecificTouchPosition;
            Vector2 screen = new Vector2(Screen.width, Screen.height);
            Vector2 normalizedScreenPosition = new Vector2(touchPosition.x / screen.x, touchPosition.y / screen.y);

            return normalizedScreenPosition;
        }
    }
}
