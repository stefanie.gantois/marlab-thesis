﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using UnityEngine.Localization.Settings;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public SaveManager SaveManagerInstance { set; get; }
    public SaveState state;

    public void Awake()
    {
        //DontDestroyOnLoad(gameObject);
        SaveManagerInstance = this;
        Load();
        Debug.Log(Serialize<SaveState>(state));
        setConfigs();
    }

    public IEnumerator Start()
    {
        yield return LocalizationSettings.InitializationOperation;
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[state.selectedLanguage];
    }

    public void Save()
    {
        PlayerPrefs.SetString("save", Serialize<SaveState>(state));
    }

    public void Load()
    {
        if (PlayerPrefs.HasKey("save"))
        {
            try
            {
                state = Deserialize<SaveState>(PlayerPrefs.GetString("save"));
            }
            catch
            {
                PlayerPrefs.DeleteKey("save");
                Load();
            }
        }
        else
        {
            state = new SaveState();
            Save();
            Debug.Log("No savefile found, create new one.");
        }
    }

    private string Serialize<T>(T toSerialize)
    {
        XmlSerializer xml = new XmlSerializer(typeof(T));
        StringWriter writer = new StringWriter();
        xml.Serialize(writer, toSerialize);
        return writer.ToString();
    }

    private T Deserialize<T>(string toDeserialize)
    {
        XmlSerializer xml = new XmlSerializer(typeof(T));
        StringReader reader = new StringReader(toDeserialize);
        return (T)xml.Deserialize(reader);
    }

    private void setConfigs()
    {
        StringDict.selectedLanguage = (Language)state.selectedLanguage;
        Static.ApplicationSettings.displayPerformanceMonitor = state.displayPerfMonitor;
        Static.ApplicationSettings.scenarioMode = (Static.ScenarioMode)state.szenarioMode;
        Static.ApplicationSettings.planeSize = (Others.MinPlanesize)state.planeSize;
        Static.ApplicationSettings.loggingData = state.logging;
        Debug.Log("Selected Language: " + state.selectedLanguage + "\ndisplay Performance Monitor: " + state.displayPerfMonitor + "\n Szenario Mode: " + state.szenarioMode);
    }
}
