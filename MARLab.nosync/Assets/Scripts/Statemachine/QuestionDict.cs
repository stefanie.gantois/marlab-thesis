﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionDict : MonoBehaviour
{
    private Dictionary<string, Question> questionDictionary = new Dictionary<string, Question>();
    public QuestionnaireController questionnaireController;

    void Awake()
    {
        InitDictionary();
    }

    void InitDictionary()
    {
        questionDictionary.Add("Begin", new RegularQuestion("Was wollen Sie als erstes tun?",new List<Answer>{
            new Answer("Die Kollegin zur Geburt befragen", "Es handelt sich um eine akute Situation. Sie dürfen keine Zeit verlieren!", false, false), 
            new Answer("abtrocknen mit frischen warmen Handtüchern", "Richtig: Unmittelbar nach der Geburt muss das NG abgetrocknet und warmgehalten werden.", true, false),
            new Answer("Den 1 min APGAR erheben","Direkt nach der Geburt sollte jedes NG in Bezug auf potentiell notwendige Reanimationsmaßnahmen/die Anpassung unterstützende Maßnahmen eingeschätzt werden. Der APGAR-Score ist ungeeignet, um den Bedarf und das Ausmaß von Reanimationsmaßnahmen direkt nach der Geburt festzulegen, da innerhalb weniger Sekunden nach der Geburt grundsätzliche Maßnahmen eingeleitet werden müssen.", false, false)
        }));
        questionDictionary.Add("Checkup Breathing", new RegularQuestion("Mit welchen Aspekt wollen Sie starten?",new List<Answer>{
            new Answer("Atmung", "Die Atmung ist ein wichtiger klinischer Parameter. Es empfiehlt sich die Beurteilung eines NG immer mit diesem Parameter zu beginnen, denn ein NG mit einer suffizienten Atmung zeigt in der Regel keine regelwidrige Herzfrequenz auf.", true, false), 
            new Answer("Herzfrequenz", "Die Herzfrequenz ist ein sehr wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen klinischen Parameter zu beginnen.", false, false),
            new Answer("Muskeltonus","Der Muskeltonus ist ein  wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen  klinischen Parameter zu beginnen.", false, false),
            new Answer("Hautkolorit", "Das Hautkolorit kann Hinweise auf die O2-Versorgung des NG geben. Allerdings zur Beurteilung der Oxygenierung ist die Beurteilung des Hautkolorits ein schlechter Parameter. Hierzu sollte besser ein Pulsoxymeter verwendet werden.", false, false)
        }));
        questionDictionary.Add("Stethoskop or Inspect", new RegularQuestion("Was wollen Sie als nächstes tun?",new List<Answer>{
            new Answer("Beurteilung der Atmung durch Hand auf den Brustkorb auflegen", "Dieses Vorgehen ist zu ungenau, da Sie damit zwar bestimmen können, ob das Neugeborene (regelmäßig) atmet (Atemfrequenz &  - Rhytmus). Allerdings nicht ob die Atemexkursionen seitengleich sind und die Atemtiefe ist nur eingeschänkt mit dieser Technik beurteilbar. Bei Neugeborenen die eine Unterstützung der Anpassung benötigen, wird mit dieser Maßnahme zu viel Zeit verloren.", false, false), 
            new Answer("mit Stethoskop abhören", "Was wollen Sie als erstes tun?", true, false, "Stethoskop"),
            new Answer("Atemwege inspizieren","Was wollen Sie als erstes tun?", true, false, "Inspect")
        }));
        questionDictionary.Add("Breath Frequency", new InputFieldQuestion("Geben Sie die ermittelte Atemfrequenz an: ", "60", "Sie haben die Atemfrequenz korrekt ermittelt", "Schade, da ist etwas schiefgelaufen. Die korrekte Atemfequenz beträgt 60 Atemzüge/minRM"));
        questionDictionary.Add("Free?", new RegularQuestion("Sind die Atemwege frei?",new List<Answer>{
            new Answer("Ja", "Korrekt!", true, false), 
            new Answer("Nein","Schade, da ist Ihnen ein Fehler unterlaufen. Die Atemwege sind frei und nicht verlegt.", false, false)
        }));
        questionDictionary.Add("Inspect", new RegularQuestion("Was wollen Sie als nächstes tun?",new List<Answer>{
            new Answer("Beurteilung der Atmung durch Hand auf den Brustkorb auflegen", "Dieses Vorgehen ist zu ungenau, da Sie damit zwar bestimmen können, ob das Neugeborene (regelmäßig) atmet (Atemfrequenz &  - Rhytmus). Allerdings nicht ob die Atemexkursionen seitengleich sind und die Atemtiefe ist nur eingeschänkt mit dieser Technik beurteilbar. Bei Neugeborenen die eine Unterstützung der Anpassung benötigen, wird mit dieser Maßnahme zu viel Zeit verloren.", false, false), 
            new Answer("mit Stethoskop abhören", "Was wollen Sie als erstes tun?", true, true),
            new Answer("Atemwege inspizieren","Was wollen Sie als erstes tun?", true, false)
        }));
        questionDictionary.Add("Stethoskop", new RegularQuestion("Was wollen Sie als nächstes tun?",new List<Answer>{
            new Answer("Beurteilung der Atmung durch Hand auf den Brustkorb auflegen", "Dieses Vorgehen ist zu ungenau, da Sie damit zwar bestimmen können, ob das Neugeborene (regelmäßig) atmet (Atemfrequenz &  - Rhytmus). Allerdings nicht ob die Atemexkursionen seitengleich sind und die Atemtiefe ist nur eingeschänkt mit dieser Technik beurteilbar. Bei Neugeborenen die eine Unterstützung der Anpassung benötigen, wird mit dieser Maßnahme zu viel Zeit verloren.", false, false), 
            new Answer("mit Stethoskop abhören", "Was wollen Sie als erstes tun?", true, false),
            new Answer("Atemwege inspizieren","Was wollen Sie als erstes tun?", true, true)
        }));
        questionDictionary.Add("Checkup Frequency", new RegularQuestion("Welchen Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", "Die Herzfrequenz ist der zuverlässigste klinische Parameter, um den Zustand eines Neugeborenen nach der Geburt zu beurteilen, und zeigt zudem am sensitivsten den Erfolg von unterstützenden Maßnahmen an.", true, false),
            new Answer("Muskeltonus", "Der Muskeltonus ist ein wichtiger klinischer Parameter. Es empfiehlt  sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen klinischen Parameter fortzufahren.", false, false),
            new Answer("Hautkolorit", "Das Hautkolorit kann Hinweise auf die O2 Versorgung des NG geben. Allerdings zu Beurteilung der Oxygenierung ist die Beurteilung des Hautkolorits ein schlechter Parameter. Hierzu sollte besser ein Pulsoxymeter verwendet werden.", false, false)
        }));
        questionDictionary.Add("Oxymetrie or Stethoskop", new RegularQuestion("Welche Maßnahme ergreifen Sie zur Beurteilung der Herzfrequenz?", new List<Answer>{
            new Answer("Pulsoxymetrie an der rechten Hand befestigen", "Korrekt, Die Pulsoxymetrie-Messung erfolgt immer präduktal.", true, false,"Oxymetrie"),
            new Answer("HF auskultieren mit dem Stethoskop", "HF auskultieren mit dem Stethoskop", true, false,"Stethoskop"),
            new Answer("Pulsoxymetrie am Fuß befestigen", "Die Pulsoxymetrie-Messung erfolgt immer präduktal. Aus diesem Grund kann diese nur an der rechten Hand gemessen werden.", false, false),
            new Answer("HF am Nabelschnurgrund oder in der Leiste Tasten", "Das Tasten des Pulses an der Basis der Nabelschnur ist bei einer HF < 100/min nicht zuverlässig.", false, false)
        }));
        questionDictionary.Add("Oxymetrie", new RegularQuestion("Welche Maßnahme ergreifen Sie zur Beurteilung der Herzfrequenz?", new List<Answer>{
            new Answer("Pulsoxymetrie an der rechten Hand befestigen", "Korrekt, Die Pulsoxymetrie-Messung erfolgt immer präduktal.", true, false),
            new Answer("HF auskultieren mit dem Stethoskop", "HF auskultieren mit dem Stethoskop", true, true),
            new Answer("Pulsoxymetrie am Fuß befestigen", "Die Pulsoxymetrie-Messung erfolgt immer präduktal. Aus diesem Grund kann diese nur an der rechten Hand gemessen werden.", false, false),
            new Answer("HF am Nabelschnurgrund oder in der Leiste Tasten", "Das Tasten des Pulses an der Basis der Nabelschnur ist bei einer HF < 100/min nicht zuverlässig.", false, false)
        }));
        questionDictionary.Add("Stethoskop Heart", new RegularQuestion("Welche Maßnahme ergreifen Sie zur Beurteilung der Herzfrequenz?", new List<Answer>{
            new Answer("Pulsoxymetrie an der rechten Hand befestigen", "Korrekt, Die Pulsoxymetrie-Messung erfolgt immer präduktal.", true, true),
            new Answer("HF auskultieren mit dem Stethoskop", "HF auskultieren mit dem Stethoskop", true, false),
            new Answer("Pulsoxymetrie am Fuß befestigen", "Die Pulsoxymetrie-Messung erfolgt immer präduktal. Aus diesem Grund kann diese nur an der rechten Hand gemessen werden.", false, false),
            new Answer("HF am Nabelschnurgrund oder in der Leiste Tasten", "Das Tasten des Pulses an der Basis der Nabelschnur ist bei einer HF < 100/min nicht zuverlässig.", false, false)
        }));
        questionDictionary.Add("Heart Frequency", new InputFieldQuestion("Bitte geben Sie an welche Herzfrequenz Sie ermittelt haben: ", "80", "Super, Sie haben die HF korrekt ermittelt.", "Schade, da ist etwas schiefgelaufen. Die korrekte HF beträgt 80 bpmRM"));
        questionDictionary.Add("Checkup Body", new RegularQuestion("Welchen Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", " ", false, true),
            new Answer("Muskeltonus", "Korrekt! Der Muskeltonus ist ein wichtiger klinischer Parameter, allerdings gibt ein andere Parameter am sensitivsten den Erfolg von unterstützenden Maßnahmen an. Aus diesem Grund sollte der Muskeltonus als letzter Faktor beurteilt werden.", true, false),
            new Answer("Hautkolorit", "Das Hautkolorit kann Hinweise auf die O2 Versorgung des NG geben. Allerdings zu Beurteilung der Oxygenierung ist die Beurteilung des Hautkolorits ein schlechter Parameter. Hierzu sollte besser ein Pulsoxymeter verwendet werden.", false, false)
        }));
        questionDictionary.Add("Bodylanguage", new RegularQuestion("Welche Maßnahme ergreifen Sie zur Beurteilung des Muskeltonus?", new List<Answer>{
            new Answer("Beurteilung Grundspannung des Körpers", " ", true, false),
            new Answer("Gesichtsbewegungen von Marta", "Gesichtsbewegungen bzw. Grimassieren können Vorstufen von schreien sein. Dies ist ein Teilaspekt der Bestimmung des APGAR. In diesem Fall wird zu viel Zeit verloren diese zusätzlich zu erheben.", false, false)
        }));
        questionDictionary.Add("Muscle Tone", new RegularQuestion("Wie ist der Tonus von Marta?", new List<Answer>{
            new Answer("Normaler Tonus", "Bei einem normalen Tonus sind Spontanbewegungen sichtbar. Dies ist bei Marta leider nicht der Fall.", false, false),
            new Answer("Reduzierter Tonus", "Sehr gut, der Tonus ist aufgrund der fehlenden Grundspannung in Armen und Beinen reduziert.", true, false),
            new Answer("schlaffer Tonus", "Ein ausgeprägt hypotones NG ist meistens auch bewusstlos und benötigt unbedingt respiratorische Unterstützung. Marta zeigt glücklicherweise Beugebewegungen in den Gliedmaßen .", false, false)
        }));
        questionDictionary.Add("Group", new RegularQuestion("Sehr gut, Sie haben nun alle wichtigen Parameter erhoben. Bitte ordnen Sie ihre Befunde in die passende Gruppe ein.", new List<Answer>{
            new Answer("Gruppe 1", "Ein Neugeborenes der Gruppe 1 sollte nicht von der Mutter getrennt werden. Denn es hat eine suffiziente Atmung/Schreien und einen normotonen Muskeltonus. Darüberhinaus beträgt die HF in dieser Gruppe >100/min. Ein Kind dieser Gruppe benötigt keine weiteren unterstützenden Maßnahmen", false, false),
            new Answer("Gruppe 2", "Korrekt! Folgende Werte haben Sie erhoben: Insuffiziente Spontanatmung (Stöhnen) reduzierter Tonus, HF < 100/min", true, false),
            new Answer("Gruppe 3", "Ein Neugeborenes der Gruppe 3 weißt Insuffiziente Spontantanatmung/ keine Atmung oder Apnoen, einen schkaffen Tonus, Bradykardie oder eine nicht nachweisbare HF und eine ausgeprägte Blässe, aufgrund der Fehlenden Perfusion aus. Diese Befunde wurden bei Marta nicht erhoben.", false, false)
        }));
        questionDictionary.Add("React?", new RegularQuestion("Müssen weitere Maßnahmen eingeleitet werden?", new List<Answer>{
            new Answer("Ja, sofort.", "Ja, sofort.", true, false),
            new Answer("Nein, noch ist der Zustand nicht lebensbedrohlich, sodass kuscheln und Körperwärme der Mutter am Besten ist", "Ein schnelles handeln ist dieser Situation wichtig! Neugeborene der Gruppe zwei und drei benötigen sofortige Unterstützung der Anpassung!", false, false)
        }));
        questionDictionary.Add("Which action", new RegularQuestion("Was tun Sie als nächstes?", new List<Answer>{
            new Answer("Ein*e Pädiater*in informieren", "Ein*e Pädiater*in informieren", true, false),
            new Answer("Das Neugeborene beatmen", "Bevor Sie das Neugeborene beatmen können ist es notwendig ggf. geschlossene oder verlegte Atemwege zu öffnen", false, false),
            new Answer("Das Neugeborene zur Anregung der Atmung absaugen", "Hinweis: NG müssen nur abgesaugt werden, wenn die Atemwege verlegt sind (beispielsweise durch Mekonium, Blutkoagel, zähen Schleim oder Vernix). Wird „unnötig“ abgesaugt, wird wertvolle Zeit verloren.", false, false)
        }));
        questionDictionary.Add("First Aid", new RegularQuestion("Was tun Sie als nächstes?", new List<Answer>{
            new Answer("Atemwege öffnen (Esmarch-Handgriff)", "Atemwege öffnen (Esmarch-Handgriff)", true, false),
            new Answer("Sauerstoff vor Nase und Mund des Neugeborenen halten", "Eine Beatmung von reifen Neugeborenen sollte immer mit Raumluft begonnen werden. Hohe Sauerstoffkonzentrationen sind mit einer erhöhten Mortalität und verzögerten Einsetzten der Spontanatmung verbunden. Zudem können Sich die Lungen nicht entfalten, hierfür ist die initiale Beatmung notwendig.", false, false),
            new Answer("Das Neugeborene beatmen", "Bevor Sie das Neugeborene beatmen können ist es notwendig ggf. geschossene Atemwege zu öffnen", false, false)
        }));
        questionDictionary.Add("Second Aid", new RegularQuestion("Was tun Sie nun?", new List<Answer>{
            new Answer("Vorbereitungen zur Beatmung treffen", "Vorbereitungen zur Beatmung treffen", true, false),
            new Answer("Vorbereitungen zur Herzdruckmassage treffen", "Eine Thorax-Kompression/ Herzdruckmassage kann nur wirksam sein, wenn die Lunge zuvor erfolgreich belüftet wurde.", false, false)
        }));
        questionDictionary.Add("Checkup Breathing 2", new RegularQuestion("Nun sollte erneut der Zustand von Marta beurteilt werden. Womit wollen Sie starten?", new List<Answer>{
            new Answer("Atmung", "Die Atmung ist ein wichtiger klinischer Parameter. Es empfiehlt sich die Beurteilung eines NG immer mit diesem Parameter zu beginnen, denn ein NG mit einer suffizienten Atmung zeigt in der Regel keine regelwidrige Herzfrequenz auf.", true, false),
            new Answer("Herzfrequenz", "Die Herzfrequenz ist ein sehr wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen klinischen Parameter zu beginnen.", false, false),
            new Answer("Muskeltonus","Der Muskeltonus ist ein  wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen  klinischen Parameter zu beginnen.", false, false),
            new Answer("Beurteilung des Zustandes mittels Petrussa-Index", "Mittels des Petrussa-Index kann der Reifezustand des Neugeborenen durch sechs Kriterien (Ohrform, Haut, Brustwarzen, Hoden, Labden und Fußsohlen) beruteilt werden. Das Ermitteln der Gestationswoche ist in der Notsituation zweitrangig und es wird kostbare Zeit verloren.", false, false)
        }));
        questionDictionary.Add("Check Breathing", new RegularQuestion("Wie wollen Sie die Atmung von Marta beurteilen?", new List<Answer>{
            new Answer("NG mit Stethoskop abhören", " ", true, false),
            new Answer("Zur Unterstützung der Atmung das NG auf den Bauch legen mit 15° Kopfhochlage", "Bauchlage mit 15° Kopfhochlage kann die Häufigkeit und Schwere von Apnoen reduzieren. Allerdings können keine weiteren Maßnahmen in Bauchlage initiert werden, so dass diese zum aktuellen Zeitpunkt hinderlich ist.", false, false)
        }));
        questionDictionary.Add("Breath Frequency 2", new InputFieldQuestion("Bitte geben Sie an welche Atemfrequenz Sie ermittelt haben: ", "13", "Sie haben die Atemfrequenz korrekt ermittelt.", "Schade, da ist etwas schiefgelaufen. Die korrekte Atemfrequenz beträgt 13 Atemzüge/min"));
        questionDictionary.Add("Heart Frequency 2", new RegularQuestion("Welchen Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", "Die Herzfrequenz ist ein sehr wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen klinischen Parameter zu beginnen.", true, false),
            new Answer("Muskeltonus","Der Muskeltonus ist ein  wichtiger klinischer Parameter. Es empfiehlt sich allerdings die Beruteilung der Vitalfunktion  mit einem anderen  klinischen Parameter zu beginnen.", false, false),
            new Answer("Beurteilung des Zustandes mittels Petrussa-Index", "Mittels des Petrussa-Index kann der Reifezustand des Neugeborenen durch sechs Kriterien (Ohrform, Haut, Brustwarzen, Hoden, Labden und Fußsohlen) beruteilt werden. Das Ermitteln der Gestationswoche ist in der Notsituation zweitrangig und es wird kostbare Zeit verloren.", false, false)
        }));
        questionDictionary.Add("EKG/Oxymetrie", new RegularQuestion("Da es durch die Maskenbeatmung bisher zu keinem zufriedenstellenden Anstieg der HF gekommen ist, sollten Sauerstoffkonzentration und HF  kontinuierlich gemessen werden. Was wollen Sie tun?", new List<Answer>{
            new Answer("EKG befestigen", "Laut ERC Leitlinie wird angeregt, dass Neugeborene, die Reanimationsmaßnahmen benötigen, ein EKG zur schnellen und sicheren Bestimmung der Herzfrequenz zu verwenden.", true, false),
            new Answer("HF weiterhin über die Pulsoxymetrie ableiten", " Laut ERC Leitlinie wird angeregt, dass Neugeborene, die Reanimationsmaßnahmen benötigen, ein EKG zur schnellen und sicheren  Bestimmung der Herzfrequenz zu verwenden.", false, false)
        }));
        questionDictionary.Add("Checkup Body 2", new RegularQuestion("Welchen Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", " ", false, true),
            new Answer("Muskeltonus", "Korrekt! Der Muskeltonus ist ein wichtiger klinischer Parameter, allerdings gibt ein andere Parameter am sensitivsten den Erfolg von unterstützenden Maßnahmen an. Aus diesem Grund sollte der Muskeltonus als letzter Faktor beurteilt werden.", true, false),
            new Answer("Beurteilung des Zustandes mittels Petrussa-Index", "Mittels des Petrussa-Index kann der Reifezustand des Neugeborenen durch sechs Kriterien (Ohrform, Haut, Brustwarzen, Hoden, Labden und Fußsohlen) beruteilt werden. Das Ermitteln der Gestationswoche ist in der Notsituation zweitrangig und es wird kostbare Zeit verloren.", false, false)
        }));
        questionDictionary.Add("Muscle Tone 2", new RegularQuestion("Wie ist der Tonus von Marta?", new List<Answer>{
            new Answer("Normaler Tonus", "Bei einem normalen Tonus sind Spontanbewegungen sichtbar. Dies ist bei Marta leider nicht der Fall.", false, false),
            new Answer("Reduzierter Tonus", "Sehr gut, der Tonus ist aufgrund der fehlenden Grundspannung in Armen und Beinen reduziert.", true, false),
            new Answer("schlaffer Tonus", "Ein ausgeprägt hypotones NG ist meistens auch bewusstlos und benötigt unbedingt respiratorische Unterstützung. Marta zeigt glücklicherweise Beugebewegungen in den Gliedmaßen .", false, false)
        }));
        questionDictionary.Add("Group 2", new RegularQuestion("Neugeborenen: Ordnen Sie Ihre erhobenen Befunde ein. Welche Maßnahme ergreifen Sie als nächstes?", new List<Answer>{
            new Answer("Kreislaufunterstützung durch Thoraxkompression & Beatmung", "Bevor Sie diese Intervention anwenden, sollten Sie zunächst andere Maßnahmen probiert haben.", false, false),
            new Answer("30 Beatmungen mit Raumluft (Inspirationszeit 1 Sek., bis suffiziente Spontanatmung einsetztKreislaufunterstützung", "Gute Entscheidung! Sollte die Herzfrequenz ansteigen, kann diese Maßnahme beendet werden. Sinkt die HF weiterhin benötigt Marta Maßnahmen der Reanimation", true, false),
            new Answer("5 Beatmungen mit Raumluft: Inspirationszeit 2-3 sek", "Da sich Martas Thorax unter der intialen Beatmung deutlich bewegt hat und die HF >60 spm liegt, ist keine Wiederholung notwendig", false, false),
            new Answer("Keine weiteren akuten Maßnahmen notwendig, Marta weiter überwachen", "Marta zeigt eine insuffiziente Spontanatmung, einen reduzierten Muskeltonus und eine Herzfrequenz <100/min. Es beseht dringender Handlungsbedarf!", false, false)
        }));
        questionDictionary.Add("Group 2 2", new RegularQuestion("Neugeborenen: Ordnen Sie Ihre erhobenen Befunde ein. Welche Maßnahme ergreifen Sie als nächstes?", new List<Answer>{
            new Answer("Kreislaufunterstützung durch Thoraxkompression & Beatmung", "Bevor Sie diese Intervention anwenden, sollten Sie zunächst andere Maßnahmen probiert haben.", false, false),
            new Answer("30 Beatmungen mit Raumluft (Inspirationszeit 1 Sek., bis suffiziente Spontanatmung einsetztKreislaufunterstützung", "Martas Herzfrequenz ist <60 spm, daraus leitet sich eine andere Maßnahme ab.", false, false),
            new Answer("5 Beatmungen mit Raumluft: Inspirationszeit 2-3 sek", "Korrekt, da die HF von Marta unter 60 spM liegt.", true, false),
            new Answer("Keine weiteren akuten Maßnahmen notwendig, Marta weiter überwachen", "Marta zeigt eine insuffiziente Spontanatmung, einen reduzierten Muskeltonus und eine Herzfrequenz <100/min. Es beseht dringender Handlungsbedarf!", false, false)
        }));
        questionDictionary.Add("Breathing 3", new RegularQuestion("Entscheidung: Nun sollte erneut der Zustand von Marta beurteilt werden. Mit welchen Aspekt wollen Sie starten?", new List<Answer>{
            new Answer("Atmung", "Atmung", true, false),
            new Answer("Herzfrequenz", "Damit Sie nichts vergessen empfiehlt es sich die Beurteilung eines Neugeborenen nach einem gleichbleibenden Schema vorzunehmen. Expert*innen empfehlen: Als erstes die Atmung, dann die Herzfrequenz und zu letzt den Muskeltonus", false, false),
            new Answer("Muskeltonus", " Damit Sie nichts vergessen empfiehlt es sich die Beurteilung eines Neugeborenen nach einem gleichbleibenden Schema vorzunehmen. Expert*innen empfehlen: Als erstes die Atmung, dann die Herzfrequenz und zu letzt den Muskeltonus", false, false)
        }));
        questionDictionary.Add("Breathing 3 1", new RegularQuestion("Entscheidung: Nun sollte erneut der Zustand von Marta beurteilt werden. Mit welchen Aspekt wollen Sie starten?", new List<Answer>{
            new Answer("Atmung", "Atmung", true, false),
            new Answer("Herzfrequenz", "Damit Sie nichts vergessen empfiehlt es sich die Beurteilung eines Neugeborenen nach einem gleichbleibenden Schema vorzunehmen. Expert*innen empfehlen: Als erstes die Atmung, dann die Herzfrequenz und zu letzt den Muskeltonus", false, false),
            new Answer("Muskeltonus", " Damit Sie nichts vergessen empfiehlt es sich die Beurteilung eines Neugeborenen nach einem gleichbleibenden Schema vorzunehmen. Expert*innen empfehlen: Als erstes die Atmung, dann die Herzfrequenz und zu letzt den Muskeltonus", false, false)
        }));
        questionDictionary.Add("Listen to Breathing 3", new InputFieldQuestion("Geben Sie an welche Atemfrequenz Sie ermittelt haben: ", "25", "Sie haben die Atemfrequenz korrekt ermittelt.", "Schade, da ist etwas schiefgelaufen. Die korrekte Atemfrequenz beträgt 25 Atemzüge/min"));
        questionDictionary.Add("Listen to Breathing 3 1", new InputFieldQuestion("Geben Sie an welche Atemfrequenz Sie ermittelt haben: ", "4", "Sie haben die Atemfrequenz korrekt ermittelt.", "Schade, da ist etwas schiefgelaufen. Die korrekte Atemfrequenz beträgt 4 Atemzüge/min"));
        questionDictionary.Add("Heart Frequency 3", new RegularQuestion("Welchen Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", "", false, true),
            new Answer("Herzfrequenz", "Da Sie bereits die Pulsoxymetrie und ein EKG zur Hilfe genommen haben können SIe nun die HF dort ablesen.", true, false),
            new Answer("Hautkolorit", "Der Muskeltonus ist ein wichtiger klinischer Parameter, allerdings gibt ein andere Parameter am sensitivsten den Erfolg von unterstützenden Maßnahmen an. Aus diesem Grund sollte der Muskeltonus als letzter Faktor beurteilt werden.", false, false)
        }));
        questionDictionary.Add("Muscle Tone 3", new RegularQuestion("Welche Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", " ", false, true),
            new Answer("Muskeltonus", "Muskeltonus", true, false)
        }));
        questionDictionary.Add("Muscle Tone 3 1", new RegularQuestion("Welche Faktor wollen Sie als nächstes beurteilen?", new List<Answer>{
            new Answer("Atmung", " ", false, true),
            new Answer("Herzfrequenz", " ", false, true),
            new Answer("Muskeltonus", "Muskeltonus", true, false)
        }));
        questionDictionary.Add("Checkup Body 3", new RegularQuestion("Wie beurteilen Sie den Tonus?", new List<Answer>{
            new Answer("Normaler Tonus", "Richtig", true, false),
            new Answer("reduzierter Tonus", "Bei einem reduzierten Tonus zeigen die Arme und Beine eine Grundspannung. Diese ist bei Marta nicht mehr sichtbar.", false, false),
            new Answer("Schlaffer Tonus", "Ein ausgeprägt hypotones NG ist meistens auch bewusstlos und benötigt unbedingt respiratorische Unterstützung. Marta zeigt glücklicherweise Beugebewegungen in den Gliedmaßen.", false, false)
        }));
        questionDictionary.Add("Checkup Body 3 1", new RegularQuestion("Wie beurteilen Sie den Tonus?", new List<Answer>{
            new Answer("Normaler Tonus", " Bei einem normalen Tonus sind Spontanbewegungen sichtbar. Dies ist bei Marta leider nicht der Fall", false, false),
            new Answer("reduzierter Tonus", "Bei einem reduzierten Tonus zeigen die Arme und Beine eine Grundspannung. Diese ist bei Marta nicht mehr sichtbar.", false, false),
            new Answer("Schlaffer Tonus", "Richtig", true, false)
        }));
        questionDictionary.Add("Action", new RegularQuestion("Entscheidung :Ordnen Sie Ihre erhobenen Befunde ein. Welche Maßnahme ergreifen Sie als nächstes?", new List<Answer>{
            new Answer("Kreislaufunterstützung durch Thoraxkompression", "Korrekt, Marta benötigt Maßnahmen der Reanimation. Sie sollten nun alle Schritte zur Durchführung der Thoraxkompression/ Beatmung durchführen.", true, false),
            new Answer("Erneut 30 Beatmungen mit Raumluft (Inspirationszeit 1 Sek., bis suffiziente Spontanatmung einsetzt", "Da trotz der bereits ergriffenen Maßnahmen, die HF weiterhin <60/Min liegt besteht dringender Handlungsbedarf für erweiterte Maßnahmen.", false, false),
            new Answer("Keine weiteren akuten Maßnahmen notwendig, Marta weiter überwachen", "Marta zeigt eine insuffiziente Spontanatmung, einen reduzierten Muskeltonus und eine Herzfrequenz <60/min. Es beseht dringender Handlungsbedarf!", false, false)
        }));
        questionDictionary.Add("Reanimation Ratio", new RegularQuestion("In welchem Verhältnis führen Sie die Reanimation durch?", new List<Answer>{
            new Answer("30:2 (30 Kompressionen: 2 Beatmungen)", "Das ist der Rhythmus, um bei einem Erwachsenen auf den Herzstillstand zu reagieren.", false, false),
            new Answer("3:1 (3 Herzdruckmassage: 1 Beatmung)", "Korrekt!", true, false)
        })); 
        questionDictionary.Add("Reanimation Rate", new RegularQuestion("Welche Kompressionsfrequenz wählen Sie?", new List<Answer>{
            new Answer("100/min", "100/min ist etwas zu langsam. Die physiologische Herzfrequenz bei einem Neugeborenen liegt zwischen 120-160/min.", false, false),
            new Answer("120/min", "Der Rhythmus muss „in Fleisch und Blut“ übergehen. Im Hinterkopf können Sie ein Lied mit dem richtigen Takt ablaufen Lassen: Beispielsweise: Staying alive;", true, false)
        }));
        questionDictionary.Add("Group 3", new RegularQuestion("Wie gehen Sie weiter vor?", new List<Answer>{
            new Answer("Alle 30 sek. HF beurteilen", "Martas Zustand scheint sich zu stabilisieren. Es ist nun wichtig, dass Sie Marta weiterhin überwachen. Denken Sie daran mit Marta empathisch zu sprechen!", true, false),
            new Answer("Zugang und Medikamentengabe erwägen", "Diese Maßnahme wäre erst indiziert, sollte die HF trotz der Herzdruckmassage weiterhin bei <60/Minute liegen oder nicht feststellbar sein.", false, false),
            new Answer("Keine weiteren Maßnahmen Notwendig.", "Martas Zustand scheint sich stabilisiert zu haben, dennoch benötigt Sie weiterhin Ihre Aufmerksamkeit.", false, false)
        }));
        questionDictionary.Add("Done", new RegularQuestion("Erfolgreich durchgeführt!", new List<Answer>{
            new Answer("Noch einmal", "Noch einmal", true, false)
        }));



        //Copy for as pattern
        questionDictionary.Add("Test", new RegularQuestion(" ", new List<Answer>{
            new Answer(" ", " ", true, false),
            new Answer(" ", " ", false, false),
            new Answer(" ", " ", false, false),
            new Answer(" ", " ", false, false)
        }));

        questionDictionary.Add("TestInput", new InputFieldQuestion(" ", " ", " ", " "));
    }

    public void SetNewQuestion(string questionKey)
    {
        if (questionDictionary.ContainsKey(questionKey))
        {
            Debug.Log(questionDictionary[questionKey].questionText);
            questionnaireController.InitNewQuestion(questionDictionary[questionKey]);
        }
        else
        {
            Debug.Log("Key " + questionKey + " not in questionDictionary.");
        }
    }
}