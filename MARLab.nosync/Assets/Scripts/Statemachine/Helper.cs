﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ilumisoft.VisualStateMachine;
using Others;
using UnityEngine.Events;


public enum ChosenPath
{
    red,
    green,
    blue
}

public class Helper : MonoBehaviour
{
    public int rightAnswerCounter = 0;
    public ChosenPath path;
    public GameObject ReaController;
    public int skippingPoint = 0;

    [HideInInspector]
    public GameObject ReaPrefab;

    private void OnEnable()
    {
        PrefabSpawningController.prefabSpawned += FindPrefab;
    }

    private void OnDisable()
    {
        PrefabSpawningController.prefabSpawned -= FindPrefab;
    }

    void FindPrefab()
    {
        //TODO: Check for Nullpointer
        
        ReaPrefab = GameObject.Find("Rea_Scenario(Clone)");
        ReaController.GetComponent<NeugeborenesInspectorController>().newborn =
            ReaPrefab.GetComponentInChildren<Neugeborenes>();
        ReaController.GetComponent<NeugeborenesInspectorController>().rea =
            ReaPrefab.GetComponentInChildren<ScenarioInteractionController>();
        ReaController.GetComponent<ReanimationseinheitInspektorController>().rea =
            ReaPrefab.GetComponentInChildren<Reanimationseinheit>();
        ReaController.SetActive(true);
        GetComponent<StateMachine>().TriggerByLabel("Next");
    }

    public void resetAnswerCounter()
    {
        rightAnswerCounter = 0;
    }

    public void incrementAnswerCounter()
    {
        rightAnswerCounter++;
    }

    public void answerCounterEqual(int countToReach)
    {
        if (rightAnswerCounter == countToReach)
            GetComponent<StateMachine>().TriggerByLabel("Yes");
        else
            GetComponent<StateMachine>().TriggerByLabel("No");
    }

    public void chosenPathColour(int tryThis)
    {
        if ((ChosenPath)tryThis == path)
        {
            switch (path)
            {
                case ChosenPath.blue:
                    GetComponent<StateMachine>().TriggerByLabel("Blue");
                    break;
                case ChosenPath.green:
                    GetComponent<StateMachine>().TriggerByLabel("Green");
                    break;
            }
        }else
        {
            GetComponent<StateMachine>().TriggerByLabel("Red");
        }
    }

    public void setPathColour(ChosenPath setPath)
    {
        path = setPath;
    }

    public void checkSkippingPoint()
    {
        switch (skippingPoint)
        {
            case 0:
                GetComponent<StateMachine>().TriggerByLabel("Custom");
                break;
            case 1:
                GetComponent<StateMachine>().TriggerByLabel("Start");
                break;
            case 2:
                GetComponent<StateMachine>().TriggerByLabel("After first Iteration");
                break;
            case 3:
                GetComponent<StateMachine>().TriggerByLabel("After Second Iteration");
                break;
        }
    }

}
