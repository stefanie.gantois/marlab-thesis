﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class needleInjection : MonoBehaviour
{
    public GameObject trash;
    public GameObject syringe;

    private GameObject arSessionOrigin;
    private bool firstCombine = false;


    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Kanülenabwurfbehälter":
                gameObject.SetActive(false);
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                firstCombine = true;
                break;
        }
    }
    public void Interact()
    {
        syringe.GetComponent<Interaction.ObjectInteractionHandler>().interact.Invoke();
    }
}
