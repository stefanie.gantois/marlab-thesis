﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayableSounds
{
    none,
    failure,
    Grab,
    OpenCardboard,
    OpenGlassDrug,
    OpenDrug,
    OpenPackaging,
    Release,
    SpritzeAufziehen,
    success,
    WasteBin,
    Writing,
    Rear
}

public class GlobalAudioController : MonoBehaviour
{
    public AudioSource failure;
    public AudioSource Grab;
    public AudioSource OpenCardboard;
    public AudioSource OpenDrug;
    public AudioSource OpenGlassDrug;
    public AudioSource OpenPackaging;
    public AudioSource Release;
    public AudioSource SpritzeAufziehen;
    public AudioSource success;
    public AudioSource WasteBin;
    public AudioSource Writing;
    public AudioSource Rear;

    public void playSound(PlayableSounds sound)
    {
        switch (sound)
        {
            case PlayableSounds.failure:
                failure.Play();
                break;
            case PlayableSounds.Grab:
                Grab.Play();
                break;
            case PlayableSounds.OpenCardboard:
                OpenCardboard.Play();
                break;
            case PlayableSounds.OpenDrug:
                OpenDrug.Play();
                break;
            case PlayableSounds.OpenGlassDrug:
                OpenGlassDrug.Play();
                break;
            case PlayableSounds.OpenPackaging:
                OpenPackaging.Play();
                break;
            case PlayableSounds.Release:
                Release.Play();
                break;
            case PlayableSounds.SpritzeAufziehen:
                SpritzeAufziehen.Play();
                break;
            case PlayableSounds.success:
                success.Play();
                break;
            case PlayableSounds.WasteBin:
                WasteBin.Play();
                break;
            case PlayableSounds.Writing:
                Writing.Play();
                break;
            case PlayableSounds.Rear:
                Rear.Play();
                break;
            case PlayableSounds.none:
                break;
        }
    }
}
