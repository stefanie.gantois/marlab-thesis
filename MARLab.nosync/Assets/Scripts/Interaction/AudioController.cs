﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioController : MonoBehaviour
{
    //public PlayableSounds Grab = PlayableSounds.Grab;
    //public PlayableSounds Release = PlayableSounds.Release;
    //public PlayableSounds Mistake = PlayableSounds.failure;
    //public PlayableSounds Success = PlayableSounds.success;

    public PlayableSounds SelectSound;
    public PlayableSounds DeselectSound;
    public PlayableSounds GrabSound;
    public PlayableSounds ReleaseSound;
    public PlayableSounds InteractSound;
    public PlayableSounds CombineSound;
    public PlayableSounds DropSound;
    public PlayableSounds ErrorSound;

    private GlobalAudioController globalAudioController;

    void Start()
    {
        globalAudioController = GameObject.Find("Sounds").GetComponent<GlobalAudioController>();
        GetComponent<Interaction.ObjectInteractionHandler>().select.AddListener(playSoundSelect);
        GetComponent<Interaction.ObjectInteractionHandler>().deselect.AddListener(playSoundDeselect);
        GetComponent<Interaction.ObjectInteractionHandler>().grab.AddListener(playSoundGrab);
        GetComponent<Interaction.ObjectInteractionHandler>().release.AddListener(playSoundRelease);
        GetComponent<Interaction.ObjectInteractionHandler>().error.AddListener(playSoundError);
        GetComponent<Interaction.ObjectInteractionHandler>().combine.AddListener(playSoundCombine);
        GetComponent<Interaction.ObjectInteractionHandler>().interact.AddListener(playSoundInteract);
    }

    private void playSoundSelect()
    {
        //SelectSound.Play();
    }
    private void playSoundDeselect()
    {
        //DeselectSound.Play();
    }
    private void playSoundGrab()
    {
        globalAudioController.playSound(GrabSound);
    }
    private void playSoundRelease()
    {
        globalAudioController.playSound(ReleaseSound);
    }
    public void playSoundInteract()
    {
            globalAudioController.playSound(InteractSound);
    }
    public void playSoundCombine(string combinedWith)
    {
        //Debug.Log("combined with: " + combinedWith);
        if (combinedWith.Equals("Magnet") || combinedWith.Equals("Erlenmeyer"))
        {
            globalAudioController.playSound(DropSound);
        }
        globalAudioController.playSound(CombineSound);
    }
    private void playSoundError()
    {
        globalAudioController.playSound(ErrorSound);
    }

    public void playOtherSound(PlayableSounds sound)
    {
        globalAudioController.playSound(sound);
    }
}
