﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class NeedlebinInjectionController : MonoBehaviour
{
    public GameObject needle;
    public GameObject drug;
    public GameObject swap;

    private GameObject arSessionOrigin;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Tupfer":
                swap.SetActive(true);
                
                break;
            case "Spritze":
                drug.SetActive(true);
                break;
            case "Kanüle":
                needle.SetActive(true);
                break;
        }
    }
}
