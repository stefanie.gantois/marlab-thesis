﻿using UnityEngine;

namespace Interaction
{
    /// <summary>
    /// This script handles the opening and closing on packaging objects
    /// </summary>
    public class ObjectPackagingController : MonoBehaviour
    {
        [Header("Packaging References: ")]
        public GameObject openPackaging;
        public GameObject closedPackaging;

        [Header("Sound References: ")]
        public PlayableSounds openingSound;
        public PlayableSounds closingSound;

        [HideInInspector]
        public bool isOpen { get; private set; } = false;
        
        /// <summary>
        /// Opens the packaging
        /// </summary>
        public void Open()
        {
            //Return if the package is already open
            if (isOpen) return;
            
            //Open the package
            closedPackaging.SetActive(false);
            openPackaging.SetActive(true);
            
            //Play the opening sound
            GetComponent<AudioController>().playOtherSound(openingSound);
            
            //Set the state as opened
            isOpen = true;
        }
   
        /// <summary>
        /// Closes the packaging
        /// </summary>
        public void Close()
        {
            //Return if the package is already open
            if (!isOpen) return;
            
            //Close the package
            openPackaging.SetActive(false);
            closedPackaging.SetActive(true);

            //Play the closing sound
            GetComponent<AudioController>().playOtherSound(closingSound);

            //Set the state as closed
            isOpen = false;
        }
    
    }
}
