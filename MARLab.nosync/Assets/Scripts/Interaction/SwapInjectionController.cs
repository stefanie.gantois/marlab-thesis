﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class SwapInjectionController : MonoBehaviour
{
    public GameObject partusitenHead;
    public GameObject trash;

    private GameObject arSessionOrigin;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Partusiten":
                partusitenHead.SetActive(true);
                break;
            case "Kanülenabwurfbehälter":
                gameObject.SetActive(false);
                partusitenHead.SetActive(false);         
                //this.transform.parent = trash.transform;
                //transform.GetComponent<Interaction.RigidbodyController>().DisablePhysics();
                //transform.SetParent(trash.transform);
                //transform.localPosition= new Vector3(0.0f, 0.0f, 0.0f);
                //transform.localEulerAngles= new Vector3(0f, -0f, 0f);
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
        }
    }
}
