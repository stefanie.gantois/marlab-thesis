﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Interaction
{
    [RequireComponent(typeof(OutlineController))]
    [RequireComponent(typeof(RigidbodyController))]
    [RequireComponent(typeof(AudioController))]

    public class ObjectInteractionHandler : MonoBehaviour
    {
        //public bool isInteractable = true; //Not used yet
        //public bool isCombinable = true; //Not used yet
        public bool isGrabbable = true;
        
        /// <summary>
        /// SELECT & DESELECT
        /// </summary>
        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        [HideInInspector] public UnityEvent select;
#pragma warning restore 0649

        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        [HideInInspector] public UnityEvent deselect;
#pragma warning restore 0649

        [HideInInspector] public bool isSelected = false;


        /// <summary>
        /// GRAB & RELEASE
        /// </summary>
        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        [HideInInspector] public UnityEvent grab;
#pragma warning restore 0649

        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        [HideInInspector] public UnityEvent release;
#pragma warning restore 0649

        [HideInInspector] public bool isGrabbed = false;


        /// <summary>
        /// INTERACT & COMBINE
        /// </summary>
        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        public UnityEvent interact;
#pragma warning restore 0649

        // Declare a custom class 
        [System.Serializable]
        public class CustomUnityEvent : UnityEvent<string>
        {
        }

        [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        public CustomUnityEvent combine;
#pragma warning restore 0649
        
        
        
        /// <summary>
        /// Error
        /// </summary>
         [SerializeField]
        //Suppress "no value assigned" warning, as this is intended behaviour
#pragma warning disable 0649
        [HideInInspector] public UnityEvent error;
#pragma warning restore 0649

        public StoredTexts interactString;
        public StoredTexts combineString;


        public void Select()
        {
                    if (gameObject.activeSelf)
                    {
                        select.Invoke();
                        isSelected = true;
                        return;
                    }
        }

        public void Deselect()
        {
                    if (gameObject.activeSelf)
                    {
                        deselect.Invoke();
                        isSelected = false;
                        return;
                    }
        }

        public void Interact(string parameter = " ")
        {
                    if (gameObject.activeSelf)
                    {
                        if (Statemachine.Instance.requestStateChange(new StateInformation(
                        this.transform.GetComponent<AR_Interactable>().InteractableName,
                        null, InteractionType.Interact, parameter)))
                        {
                            interact.Invoke();
                        }
                        else
                        {
                            error.Invoke();
                        }
                        return;
                    }
        }

        public void Combine(string combinedWithName)
        {
                    if (gameObject.activeSelf)
                    {
                        if (Statemachine.Instance.requestStateChange(new StateInformation(
                            this.transform.GetComponent<AR_Interactable>().InteractableName,
                            combinedWithName, InteractionType.Combine)))
                        {
                            CollisionController.atleastOneIntersectionDetected = false;
                            CollisionController.intersectedObject = null;
                            combine.Invoke(combinedWithName);
                        }
                        else
                        {
                            error.Invoke();
                        }
                        return;
                    }
        }

        public bool Grab()
        {
                    if (gameObject.activeSelf)
                    {
                        if (!isGrabbable)
                        {
                            error.Invoke();
                            return false;
                        }
                        else if (Statemachine.Instance.requestStateChange(new StateInformation(
                            this.transform.GetComponent<AR_Interactable>().InteractableName, null, InteractionType.Grab)))
                        {
                            grab.Invoke();
                            isGrabbed = true;
                            return true;
                        }
                        else
                        {
                            error.Invoke();
                            return false;
                        }
                    }
                    return false;
        }

        public void Release()
        {
                    if (gameObject.activeSelf)
                    {
                        release.Invoke();
                        isGrabbed = false;
                        return;
                    }
        }

        public void Error()
        {
            error.Invoke();
        }
    }
}