﻿using System;
using Michsky.UI.ModernUIPack;
using Others;
using Static;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Interaction
{
    public class TouchInputController : MonoBehaviour
    {
        [Header("Script References: ")] public InteractionController interactionController;
        public ErrorOverlayController errorOverlayController;
        public PrefabSpawningController objectPlacement;

        public Button grabButton;
        public Button combineButton;
        public Button interactButton;
        public GameObject rotateButtonRight;
        public GameObject rotateButtonLeft;
        public GameObject syringeSlider;
        
        [Header("Options: ")] [Range(0f, 1f)] public float holdRecognitionThreshold = 0.25f;
        [Range(0f, 2f)] public float gabbedObjectRotationSpeed = 0.2f;

        private GameObject initiallySelectedGameObject;
        private readonly float[] touchTimesPassed = new float[2];

        /// <summary>
        /// Continuously checks the current touch phases and triggers actions in the InteractionController based on
        /// short presses, long presses, touch movements or touch releases
        /// </summary>
        private void Update()
        {
            if (/*objectPlacement.infinityPlaneWasSpawned &&*/ !objectPlacement.objectWasSpawned
            ) //Initial Spawning of the construct
            {
                if (Input.touchCount < 1) return;

                //Spawn the construction with a touch on the screen
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Ended && objectPlacement.placementPoseIsValid)
                {
                    objectPlacement.SpawnPrefab();
                }
            }
            else if (ErrorOverlayController.ErrorOverlayIsActive()) //Error Overlay
            {
                if (Input.touchCount < 1)
                {
                    return;
                }
                else if (Input.touchCount == 1)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        errorOverlayController.CloseErrorWindow();
                        return;
                    }
                }
                else if (Input.touchCount >= 2)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Ended)
                    {
                        errorOverlayController.CloseErrorWindow();
                        return;
                    }
                }
            }
            else //AR Interaction
            {
                if (!Statemachine.Instance.labeling && !Statemachine.Instance.questioning && !Statemachine.Instance.introducing)
                {
                    //Check how many touch interactions are registered
                    switch (Input.touchCount)
                    {
                        case 1: //Only one finger is used.
                        {
                            FirstTouch();
                            break;
                        }
                        case 2: //Two Fingers are used.
                        {
                            FirstTouch();
                            SecondaryTouch();
                            break;
                        }
                        default:
                            //No touch or too many touches - no interaction
                            break;
                    }
                }
            }
        }
       


        /// <summary>
        /// Handle the first touch on the screen
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Unkown Touchphase</exception>
        private void FirstTouch()
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                //Touch Begins
                case TouchPhase.Began:
                    //Count the time for the very first frame
                    touchTimesPassed[0] += Time.deltaTime;

                    //Store the game initially selected gameObject
                    if (interactionController.isSelectingObject)
                    {
                        initiallySelectedGameObject = interactionController.selectedObject;
                    }

                    break;
                //Touch continues this frame but is stationary
                case TouchPhase.Stationary:
                {
                    //Add up the touch duration
                    touchTimesPassed[0] += Time.deltaTime;

                    //If no object is currently grabbed, one is selected that was also initially selected when the
                    //touch started and the timer passed the threshold, grab the object
                    if (touchTimesPassed[0] >= holdRecognitionThreshold
                        && interactionController.isSelectingObject
                        && interactionController.selectedObject == initiallySelectedGameObject
                        && !interactionController.isGrabbingObject
                        && !interactionController.tryedGrabbingObjectUnsuccessfully)
                    {
                        interactionController.GrabObject();
                    }

                    break;
                }
                //Touch continues this frame and is moving
                case TouchPhase.Moved:
                {
                    //Add up the touch duration
                    touchTimesPassed[0] += Time.deltaTime;

                    //If no object is currently grabbed, one is selected that was also initially selected when the
                    //touch started and the timer passed the threshold, grab the object
                    if (touchTimesPassed[0] >= holdRecognitionThreshold
                        && interactionController.isSelectingObject
                        && interactionController.selectedObject == initiallySelectedGameObject
                        && !interactionController.isGrabbingObject
                        && !interactionController.tryedGrabbingObjectUnsuccessfully)
                    {
                        interactionController.GrabObject();
                    }

                    //If an object is grabbed (therefore the timmer passed the threshold) and movement is detected, this is Y axis rotation
                    if (interactionController.isGrabbingObject)
                    {
                        RotateGabbedObject(touch);
                    }

                    break;
                }
                //Touch ended this frame
                case TouchPhase.Ended:
                {
                    //Fire an interaction when this wasnt a hold
                    if (touchTimesPassed[0] < holdRecognitionThreshold && interactionController.isSelectingObject)
                    {
                        interactionController.Interact();
                    }
                    else if (interactionController.isGrabbingObject) //Release the object if this was a hold
                    {
                        interactionController.ReleaseGrabbedObject();
                    }

                    //Reset the touchTimer
                    touchTimesPassed[0] = 0;

                    //Reset the tryedGrabbingObject
                    interactionController.tryedGrabbingObjectUnsuccessfully = false;
                    break;
                }
                case TouchPhase.Canceled:
                    //On Cancelled Touch just reset the timer but dont fire an interaction
                    touchTimesPassed[0] = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Handle the second touch on the screen
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Unkown Touchphase</exception>
        private void SecondaryTouch()
        {
            Touch touch = Input.GetTouch(1);

            switch (touch.phase)
            {
                //Touch Begins
                case TouchPhase.Began:
                    //Count the time for the very first frame
                    touchTimesPassed[1] += Time.deltaTime;

                    break;
                //Touch continues this frame but is stationary
                case TouchPhase.Stationary:
                {
                    //Add up the touch duration
                    touchTimesPassed[1] += Time.deltaTime;

                    break;
                }
                //Touch continues this frame and is moving
                case TouchPhase.Moved:
                {
                    //Add up the touch duration
                    touchTimesPassed[1] += Time.deltaTime;

                    //If an object is grabbed (therefore the timmer passed the threshold) and movement is detected, this is Y axis rotation
                    if (interactionController.isGrabbingObject)
                    {
                        RotateGabbedObject(touch);
                    }

                    break;
                }
                //Touch ended this frame
                case TouchPhase.Ended:
                {
                    //Fire an interaction when this wasnt a hold
                    if (touchTimesPassed[1] < holdRecognitionThreshold && interactionController.isSelectingObject)
                    {
                        //Combine with the second touch when currently intersecting with an object fire an interaction otherwise
                        if (interactionController.atleastOneIntersectionFromColliders)
                        {
                            interactionController.Combine();
                        }
                        else
                        {
                            interactionController.Interact();
                        }
                    }

                    //Reset the touchTimer
                    touchTimesPassed[1] = 0;
                    break;
                }
                case TouchPhase.Canceled:
                    //On Cancelled Touch just reset the timer but dont fire an interaction
                    touchTimesPassed[1] = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Rotates a grabbed Object around its Y Axis.
        /// </summary>
        /// <param name="touch">The touch that triggeres the rotation</param>
        public void RotateGabbedObject(Touch touch)
        {
            Quaternion yRotation = Quaternion.Euler(0f, -touch.deltaPosition.x * gabbedObjectRotationSpeed, 0f);
            interactionController.grabber.transform.rotation =
                yRotation * interactionController.grabber.transform.rotation;
        }

    }
}