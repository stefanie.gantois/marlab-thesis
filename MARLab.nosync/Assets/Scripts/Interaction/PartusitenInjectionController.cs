﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartusitenInjectionController : MonoBehaviour
{
    public ObjectDrugController objectDrugController;
    public GameObject partusiten;
    public GameObject trash;
    public GameObject syringe;

    private GameObject arSessionOrigin;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Tupfer":
                objectDrugController.Open();
                break;
            case "Kanülenabwurfbehälter":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(-0.0028f, -0.0257f, -0.0183f);
                transform.localEulerAngles = new Vector3(0f, -81.32301f, 0f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
        }
    }
    public void Interact()
    {
        syringe.GetComponent<Interaction.ObjectInteractionHandler>().interact.Invoke();
    }
}
