﻿using System;
using Static;
using Tayx.Graphy.Utils.NumString;
using UI;
using UnityEngine;

namespace Interaction
{
    public class InjectionController : MonoBehaviour
    {
        public GameObject syringe;
        public GameObject syringeStamp;
        public GameObject needle;
        public GameObject needleRigidbody;
        public GameObject needleProtection;
        public GameObject combinationPlug;
        public GameObject drug;
        public GameObject carrierSolutionNaCl;
        public GameObject carrierSolutionGlucose;
        public GameObject date;

        public PlayableSounds garbageSound;
        public PlayableSounds grabSound;
        public PlayableSounds rearSound;
        public PlayableSounds writingLabel;

        private Transform saveMainParentCarrier;
        private Transform saveMainParentDrug;
        private Transform saveMainNeedle;


        private int interactionStep = 0;

        private Vector2 touchStartingPosition;
        private Vector2 normalizedTouchDistance;
        private int millilitersPulled;
        private Quaternion savedRotation;

        public void CombineWith(string objectName)
        {
            switch (objectName)
            {
                case "NaCl 0,9% 10ml":
                    saveMainParentCarrier = carrierSolutionNaCl.transform.parent;
                    carrierSolutionNaCl.transform.parent = syringe.transform;
                    carrierSolutionNaCl.transform.localPosition = new Vector3(-0.0013f, -0.0071f, 0.0834f);
                    carrierSolutionNaCl.transform.localEulerAngles = new Vector3(90f, 180f, 0f);
                    carrierSolutionNaCl.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    break;
                case "Glucose 10ml":
                    saveMainParentCarrier = carrierSolutionGlucose.transform.parent;
                    carrierSolutionGlucose.transform.parent  = syringe.transform;
                    carrierSolutionGlucose.transform.localPosition = new Vector3(-0.0013f, -0.0071f, 0.0834f);
                    carrierSolutionGlucose.transform.localEulerAngles = new Vector3(90f, 180f, 0f);
                    carrierSolutionGlucose.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    break;
                case "Partusiten":
                    saveMainParentDrug = drug.transform.parent;
                    drug.transform.parent  = syringe.transform;
                    drug.transform.localPosition = new Vector3(0.0003f, -0.0065f, 0.0751f);
                    drug.transform.localEulerAngles = new Vector3(90f, 180f, 0f);
                    drug.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    break;
                case "Kanüle":
                    //needle.SetActive(true);
                    saveMainNeedle = needle.transform.parent;
                    needle.transform.parent  = syringe.transform;
                    needle.transform.localPosition = new Vector3(0.0f, -0.0071f, 0.0875f);
                    needle.transform.localEulerAngles = new Vector3(0f, 90f, 0f);
                    needleRigidbody.GetComponent<MeshCollider>().enabled = false;
                    needle.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactRemoveNeedleCap;
                    break;
                case "Kanülen Verpackung":
                    //needle.SetActive(true);
                    saveMainNeedle = needle.transform.parent;
                    needle.transform.parent  = syringe.transform;
                    needle.transform.localPosition = new Vector3(0.0f, -0.0071f, 0.0875f);
                    needle.transform.localEulerAngles = new Vector3(0f, 90f, 0f);
                    needle.GetComponent<ObjectInteractionHandler>().isGrabbable = false;
                    needleRigidbody.GetComponent<MeshCollider>().enabled = false;
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactRemoveNeedleCap;
                    break;
                case "Spritzenverschluss":
                    combinationPlug.SetActive(true);
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    break;
                case "Spritzenverschluss Verpackung":
                    combinationPlug.SetActive(true);
                    GetComponent<AudioController>().playOtherSound(grabSound);
                    break;
                case "Kanülenabwurfbehälter":
                    switch (interactionStep)
                    {
                        case 0: //remove needle protection
                            Interact();
                            break;
                        case 5: //remove needle
                            Interact();
                            needle.gameObject.SetActive(false);
                            needle.transform.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke("Kanülenabwurfbehälter");
                            break;
                        default:
                            break;
                    }
                    break;
                case "Mülleimer":
                    break;
                case "Spritzenschale":
                    break;
                default:
                    break;
            }
        }

        public void Interact()
        {
            var localPosition = Vector3.zero;
        
            switch (interactionStep)
            {
                case 0: //remove needle protection
                    needleProtection.SetActive(false);
                    GetComponent<AudioController>().playOtherSound(garbageSound);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactPull;
                    interactionStep++;
                    break;
                case 1: //pull carrier solution
                    interactionStep++;
                    GetComponent<AudioController>().playOtherSound(rearSound);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactRemoveNeedleCap;
                    break;
                case 2: //remove carrier solution
                    interactionStep++;
                    needleRigidbody.GetComponent<MeshCollider>().isTrigger = true;
                    carrierSolutionNaCl.transform.parent = saveMainParentCarrier;
                    carrierSolutionGlucose.transform.parent = saveMainParentCarrier;
                    carrierSolutionNaCl.transform.GetComponent<Interaction.ObjectInteractionHandler>().Release();
                    carrierSolutionGlucose.transform.GetComponent<Interaction.ObjectInteractionHandler>().Release();
                    carrierSolutionNaCl.GetComponent<ObjectInteractionHandler>().isGrabbable = true;
                    carrierSolutionGlucose.GetComponent<ObjectInteractionHandler>().isGrabbable = true;
                    InteractionController.RemoveGlowMaterial(carrierSolutionGlucose);
                    InteractionController.RemoveGlowMaterial(carrierSolutionNaCl);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactPull;
                    needleRigidbody.GetComponent<MeshCollider>().isTrigger = false;
                    break;
                case 3: //pull drug
                    interactionStep++;
                    GetComponent<AudioController>().playOtherSound(rearSound);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactRemoveNeedleCap;
                    break;
                case 4: //remove drug
                    interactionStep++;
                    needleRigidbody.GetComponent<MeshCollider>().isTrigger = true;
                    drug.transform.parent = saveMainParentDrug;
                    drug.transform.GetComponent<Interaction.ObjectInteractionHandler>().Release();
                    drug.GetComponent<ObjectInteractionHandler>().isGrabbable = true;
                    InteractionController.RemoveGlowMaterial(drug);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactRemoveNeedleCap;
                    needleRigidbody.GetComponent<MeshCollider>().isTrigger = false;
                    break;
                case 5: //remove needle
                    interactionStep++;
                    needleRigidbody.GetComponent<MeshCollider>().enabled = true;
                    needle.transform.parent = saveMainNeedle;
                    needle.transform.GetComponent<Interaction.ObjectInteractionHandler>().Release();
                    needle.GetComponent<ObjectInteractionHandler>().isGrabbable = true;
                    InteractionController.RemoveGlowMaterial(needle);
                    GetComponent<ObjectInteractionHandler>().interactString = StoredTexts.interactDefault;
                    break;
                case 6:
                    GetComponent<AudioController>().playOtherSound(writingLabel);
                    GetComponent<OutlineController>().ActivateSuccessIndicator();
                    date.SetActive(true);
                    break;
            }
        }

        /// <summary>
        /// Manipulates the actual syringe gameobject to represent the filling level
        /// </summary>
        /// <param name="halfmilliliters">halfmilliliters to show</param>
        public void ManipulateSyringeStampGameObject(int halfmilliliters)
        {
            Vector3 syringePosition = syringeStamp.transform.localPosition;
            
            switch (halfmilliliters)
            {
                case 0:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, 0.0115f);
                   break;
                case 1:
                    syringePosition = new Vector3(syringePosition.x, syringePosition.y, 0.0046f);
                    break;
                case 2:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0023f);
                   break;
                case 3:
                    syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0090f);
                    break;
                case 4:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0157f);
                   break;
                case 5:
                    syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0225f);
                    break;
                case 6:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0293f);
                   break;
                case 7:
                    syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0362f);
                    break;
                case 8:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0431f);
                   break;
                case 9:
                    syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0500f);
                    break;
                case 10:
                   syringePosition = new Vector3(syringePosition.x, syringePosition.y, -0.0568f);
                   break;
            }
            
            syringeStamp.transform.localPosition = syringePosition;
        }
    }
}
