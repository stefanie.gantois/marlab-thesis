﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlaceObjects : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    [SerializeField]
    private bool deleteLastPlacedObject = true;

    [SerializeField]
    private bool onlyAllowPlacingOnce = true;
    [HideInInspector]
    public bool contructIsReady = false;

    private bool objectwasPlaced;
    private GameObject lastPlacedObject;
    
    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public GameObject spawnedObject { get; private set; }

    /// <summary>
    /// Invoked whenever an object is placed in on a plane.
    /// </summary>
    public static event Action onPlacedObject;

    ARRaycastManager m_RaycastManager;
    private ARAnchorManager anchorManager;

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    private void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        anchorManager = GetComponent<ARAnchorManager>();
    }

    private void Update()
    {
        //Return from the function when the object was placed and should only be placed once
        if (onlyAllowPlacingOnce && objectwasPlaced) return;
        
        if (Input.touchCount <= 0) return;
        Touch touch = Input.GetTouch(0);

        if (touch.phase != TouchPhase.Began) return;
        if (!m_RaycastManager.Raycast(touch.position, s_Hits, TrackableType.PlaneWithinPolygon)) return;
        Pose hitPose = s_Hits[0].pose;

        //Delete the last placed Object
        if (deleteLastPlacedObject && lastPlacedObject != null)
        {
            GameObject.Destroy(lastPlacedObject);
            lastPlacedObject = null;
        }

        //Spawn a new object including an AR anchor to ensure good tracking
        ARAnchor anchor = anchorManager.AddAnchor(hitPose);
        spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation, anchor.gameObject.transform);

        //Set the new object as the last spawned object
        if(deleteLastPlacedObject) lastPlacedObject = spawnedObject;
        
        //An object was placed
        objectwasPlaced = true;
        StartCoroutine(SetObjectWasPlacedDelayed());

        if (onPlacedObject != null)
        {
            onPlacedObject();
        }
    }

    private IEnumerator SetObjectWasPlacedDelayed()
    {
        yield return new WaitForSeconds(1.0f);
        contructIsReady = true;
    }

    public GameObject getSpawnedObject()
    {
        return lastPlacedObject;
    }
}
