﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AquaThrowawayController : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject trash;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Mülleimer":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(-0.0152f, -0.0689f, 0.0582f);
                transform.localEulerAngles = new Vector3(0f, -14.675f, 0f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
        }
    }
}
