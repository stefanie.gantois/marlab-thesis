﻿using System;
using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;
using Object = System.Object;

public class CollisionController : MonoBehaviour
{
    public static GameObject intersectedObject; //TODO: This is wrong for sure
    public static bool atleastOneIntersectionDetected;
    public BoxCollider boxCollider;
    public GameObject grabbedObject;


    private void OnTriggerStay(Collider other)
    {
        //If the other collider is not an Boxcollider or if there already is an intersection, do nothing.
        if (!(other is BoxCollider) || atleastOneIntersectionDetected) return;
        
        
        
        //Is the other object the grabbed object?
        grabbedObject = FindGrabbedObject(other.gameObject);
        if (grabbedObject == null) return;

        //Is this object a child of the grabbed object?
        if (gameObject.transform.IsChildOf(grabbedObject.transform)) return;

        //Debug.Log(FindARInteractable(gameObject).name + " Kollision mit " + FindARInteractable(other.gameObject).name);
        
        //Set intersectedObject and isIntersecting.
        intersectedObject = FindARInteractable(gameObject);
        atleastOneIntersectionDetected = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (!(other is BoxCollider) || !atleastOneIntersectionDetected) return;

        //Debug.Log(FindARInteractable(gameObject).name + "Verlässt Kollision mit " + FindARInteractable(other.gameObject).name);
        InteractionController.ResetIntersectedObjectShaderAndSelection(FindARInteractable(this.gameObject));
        InteractionController.ResetIntersectedObjectShaderAndSelection(FindARInteractable(other.gameObject));
        
        atleastOneIntersectionDetected = false;
    }
    
    private static GameObject FindARInteractable(GameObject childObject, string tag = "AR_Interactable")
    {
        Transform t = childObject.transform;
        while (t.parent != null)
        {
            if (t.parent.CompareTag(tag))
            {
                return t.parent.gameObject;
            }

            t = t.parent.transform;
        }
        //Return null if there is none
        return null;
    }

    private static GameObject FindGrabbedObject(GameObject other)
    {
        Transform t = other.transform;
        while (t.parent != null)
        {
            if (t.parent.CompareTag("AR_Interactable"))
            {
                if (t.parent.GetComponent<ObjectInteractionHandler>().isGrabbed)
                    return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null;
    }
}