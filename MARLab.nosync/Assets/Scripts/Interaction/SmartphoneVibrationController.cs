﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartphoneVibrationController : MonoBehaviour
{
   public void Vibrate()
   {
      Handheld.Vibrate();
   }
}
