﻿using System;
using UnityEngine;

namespace Interaction
{
    /// <summary>
    /// The LocalRigidbodyController makes AR_InteractableColliders kinematic after their physics were executed
    /// </summary>
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class LocalRigidbodyController : MonoBehaviour
    {
        [Header("Options:")]
        public bool kinematicWhenStatic = true;

        private Rigidbody thisRigidbody;
        private int kinematicFrameCounter = 0;
    
        // Start is called before the first frame update
        void Start()
        {
            thisRigidbody = this.GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update()
        {
            //Make the object kinematic if it is sleeping right now (therefore not acted on by physics) and didnt last fram 
            if (!kinematicWhenStatic) return;

            //Return if this rigidbody is currently kinematic
            if (thisRigidbody.isKinematic == true) return;

            //Use the kinematicFrameCounter to skip the first x frames (1 should be sufficient?!), reset it afterwards
            kinematicFrameCounter++;
            if (kinematicFrameCounter < 2) return;
            kinematicFrameCounter = 0;

            //Check if the object is currently moving, return if it is still moving
            if (thisRigidbody.velocity.sqrMagnitude  > 0.001f) return;

            //Make Object kinematic
            this.thisRigidbody.isKinematic = true;

            //Reset the parent/child offset that was caused by the rigidbody falling down
            GameObject arInteractableCollider = this.gameObject;
            GameObject arInteractable = FindARInteractable(arInteractableCollider);
            arInteractable.transform.position = arInteractableCollider.transform.position;
            arInteractable.transform.rotation = arInteractableCollider.transform.rotation;
            arInteractableCollider.transform.localPosition = Vector3.zero;
            arInteractableCollider.transform.localRotation = Quaternion.identity;
        }

        private static GameObject FindARInteractable(GameObject childObject, string tag = "AR_Interactable")
        {
            Transform t = childObject.transform;
            while (t.parent != null)
            {
                if (t.parent.CompareTag(tag))
                {
                    return t.parent.gameObject;
                }
                t = t.parent.transform;
            }
            //Return null if there is none
            return null;
        }
    }
}
