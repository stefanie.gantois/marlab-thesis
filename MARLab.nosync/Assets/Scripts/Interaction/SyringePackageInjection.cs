﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyringePackageInjection : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject trash;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Mülleimer":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(-0.0418f, 0.0742f, 0.0011f);
                transform.localEulerAngles = new Vector3(0.293f, 168.981f, 99.9f);
                transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
        }
    }

    public void Interact()
    {
        GetComponent<Interaction.ObjectPackagingController>().Open();
    }
}
