﻿using Michsky.UI.ModernUIPack;
using Others;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Interaction
{
    public class InteractionController : MonoBehaviour
    {
        [Header("Script References: ")]
        public PrefabSpawningController objectPlacementController;

        [Header("Object References: ")]
        //The main camera in the scene
        public Camera arCamera;
        //The reference object to attach objects from the scene to
        public GameObject grabber;



        //The object that is currently selected, combined or grabbed by the user
        [HideInInspector]
        public GameObject grabbedObject; //Grabbed by the user
        [HideInInspector]
        public bool isGrabbingObject;
        [HideInInspector]
        public bool tryedGrabbingObjectUnsuccessfully;
        [HideInInspector]
        public GameObject selectedObject; //Selected (aimed at) by the user
        private GameObject lastSelectedObject;
        [HideInInspector]
        public bool isSelectingObject;
        [HideInInspector]
        public GameObject intersectedObject; //User trys to combine grabbedObject with this object
        [HideInInspector]
        public bool atleastOneIntersectionFromColliders;
        [HideInInspector]
        public bool isIntersecting;
        public Material transparentMaterial;
        public Material glowMaterial;

        public static Material staticGlowMaterial;

        public RaycastHit hit;

        private static bool originalMaterialsSaved = false;

        public static Dictionary<GameObject, Material> originalMaterials = new Dictionary<GameObject, Material>();

        private void Awake()
        {
            staticGlowMaterial = glowMaterial;
        }
        // Update is called once per frame
        private void Update()
        {
            //Check if there is an object selected right now
            if (!isGrabbingObject)
            {
                //Select Objects through raycasting
                SelectARInteractable();
            }
            else
            {
                //Combine Objects through overlapping them
                CombineARInteractables();
            }
        }

        private void CombineARInteractables()
        {
            //Deactivate all the box colliders of the CollisionControllers belonging to grabbed object and it's children
            DeactivateAllBoxColliderTrigger(grabbedObject);

            //Update isIntersectingObject
            atleastOneIntersectionFromColliders = CollisionController.atleastOneIntersectionDetected;

            if (atleastOneIntersectionFromColliders)
            {
                isIntersecting = true;

                //Get the intersected object
                intersectedObject = CollisionController.intersectedObject;

                ChangeToCombineMaterials();
                originalMaterialsSaved = true;

                //Select intersected object
                intersectedObject.GetComponent<ObjectInteractionHandler>().Select();

                //Deselect the grabbed object
                grabbedObject.GetComponent<ObjectInteractionHandler>().Deselect();
            }
            else
            {
                isIntersecting = false;

                if (originalMaterialsSaved)
                {
                    ResetIntersectedObjectShaderAndSelection(intersectedObject);
                }
            }
        }

        private void SelectARInteractable()
        {

            if (!objectPlacementController.objectWasSpawned) return;

            //Check if the ray hits anything at all, return when no hit
            Ray ray = arCamera.ViewportPointToRay(new Vector3(0.5f,0.5f,0f));
            if (!Physics.Raycast(ray, out hit))
            {
                //Reset the Selection state
                if (selectedObject != null)
                {
                    selectedObject.GetComponent<ObjectInteractionHandler>().Deselect();
                    RemoveGlowMaterial(selectedObject);
                }
                selectedObject = null;
                isSelectingObject = false;
                return;
            }

            //Check if the hit object hit is an AR_Interactable
            if (!hit.transform.CompareTag("AR_InteractableCollider"))
            {
                //Reset the Selection state
                if (selectedObject != null)
                {
                    selectedObject.GetComponent<ObjectInteractionHandler>().Deselect();
                    RemoveGlowMaterial(selectedObject);
                }
                selectedObject = null;
                isSelectingObject = false;
                return;
            }

            if (hit.distance >= 0.4)
            {
                //Reset the Selection state
                if (selectedObject != null)
                {
                    selectedObject.GetComponent<ObjectInteractionHandler>().Deselect();
                    RemoveGlowMaterial(selectedObject);
                }
                selectedObject = null;
                isSelectingObject = false;
                return;
            }
            //Make the selected object to the lastSelected one
            lastSelectedObject = selectedObject != null ? selectedObject : null;

            //Finds the AR_Interactable that is the parent (distance unknown) of the AR_InteractableCollider that was hit
            selectedObject = FindARInteractable(hit.transform.gameObject);

            //An AR_InteractableCollider is selected
            isSelectingObject = true;

            AddGlowMaterial(selectedObject);

            //If an object is hit, its state is set to selected
            selectedObject.GetComponent<ObjectInteractionHandler>().Select();

            //Return from this function if the selected object is the same as last frame
            if(lastSelectedObject != null) if(selectedObject == lastSelectedObject) return;

            //Deselect the last selected object if there was one
            if (lastSelectedObject != null)
            {
                lastSelectedObject.GetComponent<ObjectInteractionHandler>().Deselect();
                RemoveGlowMaterial(lastSelectedObject);
            }


        }

        //Grab the object that is currently selected by the raycast
        public void GrabObject()
        {
            //Important: This method assumes there is an object selected!

            //Invokes the Grab events on the grabbed object and returns if that failed
            if (tryedGrabbingObjectUnsuccessfully) return;
            if (hit.distance >= 0.4) return;
            if (!selectedObject.GetComponent<ObjectInteractionHandler>().Grab())
            {
                tryedGrabbingObjectUnsuccessfully = true;
                return;
            }

            //Store the currently grabbed object
            grabbedObject = selectedObject;

            //Set the parent to the camera (so it follows the cameras movement)
            grabbedObject.transform.parent = grabber.transform;

            //Is grabbing an object
            isGrabbingObject = true;

            RemoveGlowMaterial(grabbedObject);
            grabbedObject.GetComponent<ObjectInteractionHandler>().Deselect();

            if (intersectedObject != null)
            {
                ResetIntersectedObjectShaderAndSelection(intersectedObject);
            }

            //Activate all BoxCollider Trigger for the grabbed Object.
            //ActivateAllBoxColliderTrigger(grabbedObject.GetComponentsInChildren<CollisionController>());
        }

        //Release the currently gabbed object
        public void ReleaseGrabbedObject()
        {
            //Return if no object is currently grabbed
            if (!isGrabbingObject) return;


            if (originalMaterialsSaved)
            {
                ResetMaterialsToOriginal();
                originalMaterialsSaved = false;
            }

            //Deactivate the trigger for the BoxColliders
            ActivateAllBoxColliderTrigger(grabbedObject);

            //Set the parent back to be a child of the anchored object that was originally spawned
            grabbedObject.transform.parent = objectPlacementController.getSpawnedObject().transform;

            //Invokes the Release events on the grabbed object
            grabbedObject.GetComponent<ObjectInteractionHandler>().Release();

            //Is no longer grabbing an object
            isGrabbingObject = false;
            isIntersecting = false;

            //Reset the rotation of the grabber (which might be modified by the GrabbedRotationController)
            grabber.transform.localRotation = Quaternion.identity;
        }

        public void Interact()
        {
            //Important: This method assumes there is an object selected!
            //It also assumes that ARInteractables have an ObjectInteraktionhandler

            selectedObject.GetComponent<ObjectInteractionHandler>().Interact();
        }

        public void Combine()
        {
            //Important: This method assumes there is an object selected and it is currently intersecting with another.
            //This is always the case because the TouchInputController would otherwise not fire the function

            //It also assumes that ARInteractables have an ObjectInteraktionhandler

            selectedObject.GetComponent<ObjectInteractionHandler>().Combine(intersectedObject.GetComponent<AR_Interactable>().InteractableName);
            intersectedObject.GetComponent<ObjectInteractionHandler>().Combine(selectedObject.GetComponent<AR_Interactable>().InteractableName);
        }


        /// <summary>
        /// Find the parent of the collider object that was hit which is tagged as "AR_Interactable"
        /// </summary>
        /// <param name="childObject"></param>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static GameObject FindARInteractable(GameObject childObject, string tag = "AR_Interactable")
        {
            Transform t = childObject.transform;
            while (t.parent != null)
            {
                if (t.parent.CompareTag(tag))
                {
                    return t.parent.gameObject;
                }
                t = t.parent.transform;
            }
            //Return null if there is none
            return null;
        }

        /// <summary>
        /// Replaces all materials on the grabbed Object with a material for combining and stores the reference
        /// to the original one to reset them later.
        /// </summary>
        private void ChangeToCombineMaterials()
        {
            //Grabbed object get's combine shader
            Renderer[] objectRenderers = grabbedObject.GetComponentsInChildren<Renderer>();

            RemoveGlowMaterial(grabbedObject);
            foreach (Renderer objectRenderer in objectRenderers)
            {
                if (originalMaterials.ContainsKey(objectRenderer.gameObject)) return;
                //Store the reference to the object and its original material to reset them after placement
                originalMaterials.Add(objectRenderer.gameObject, objectRenderer.material);

                //Replace the material with the combine material
                objectRenderer.material = transparentMaterial;
            }
            AddGlowMaterial(intersectedObject);
        }

        /// <summary>
        /// Resets the materials of objects to their original ones.
        /// </summary>
        private static void ResetMaterialsToOriginal()
        {
            foreach (KeyValuePair<GameObject, Material> obj in originalMaterials)
            {
                obj.Key.GetComponent<Renderer>().material = obj.Value;
            }
            originalMaterials = new Dictionary<GameObject, Material>();
        }

        /// <summary>
        /// Activates the Box-Collider-Trigger of each of the collision controllers.
        /// </summary>
        public static void ActivateAllBoxColliderTrigger(GameObject objectToActivate)
        {
            var collisionControllers = objectToActivate.GetComponentsInChildren<CollisionController>();
            foreach (var collisionController in collisionControllers)
            {
                collisionController.boxCollider.isTrigger = true;
            }
        }

        /// <summary>
        /// Deactivates the Box-Collider-Trigger of each of the collision controllers.
        /// </summary>
        public static void DeactivateAllBoxColliderTrigger(GameObject objectToDeactivate)
        {
            var collisionControllers = objectToDeactivate.GetComponentsInChildren<CollisionController>();
            foreach (CollisionController collisionController in collisionControllers)
            {
                collisionController.boxCollider.isTrigger = false;
            }
        }

        /// <summary>
        /// Resets the shaders and deselects the intersected object and selects the grabbed object.
        /// </summary>
        public static void ResetIntersectedObjectShaderAndSelection(GameObject intersectedObject)
        {
            if (originalMaterialsSaved)
            {
                ResetMaterialsToOriginal();
                originalMaterialsSaved = false;
            }
            RemoveGlowMaterial(intersectedObject);
            intersectedObject.GetComponent<ObjectInteractionHandler>().Deselect();
        }

        public static void  AddGlowMaterial(GameObject objectToChange)
        {
            Renderer[] renderers = objectToChange.GetComponentsInChildren<Renderer>(); //ToDo: This is actually quite the expensive method to do on every select/deselect

            foreach (Renderer objectRenderer in renderers)
            {
                //Ignore the info boxes (Direkthinweise)
                if (objectRenderer.gameObject.CompareTag("InfoBox")) return;

                var materials = objectRenderer.sharedMaterials.ToList();
                if(!materials.Contains(staticGlowMaterial))
                    materials.Add(staticGlowMaterial);

                objectRenderer.materials = materials.ToArray();
            }
        }

        public static void RemoveGlowMaterial(GameObject objectToChange)
        {
            if (objectToChange == null)
            {
                Debug.Log("objectToChange is null");
            }
            Renderer[] renderers = objectToChange.GetComponentsInChildren<Renderer>();

            foreach (Renderer objectRenderer in renderers)
            {
                //Ignore the info boxes (Direkthinweise)
                if (objectRenderer.gameObject.CompareTag("InfoBox")) return;

                var materials = objectRenderer.sharedMaterials.ToList();
                if(materials.Contains(staticGlowMaterial))
                    materials.Remove(staticGlowMaterial);
                objectRenderer.materials = materials.ToArray();
            }

        }
    }
}
