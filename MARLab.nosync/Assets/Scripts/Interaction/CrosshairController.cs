﻿using System;
using Others;
using UnityEngine;

namespace Interaction
{
    public class CrosshairController : MonoBehaviour
    {
        [Header("Object References: ")]
        public RectTransform crosshair;
    
        [HideInInspector]
        public bool contextButtonsAreActive;

        private void Start()
        {
            DeactivateCrosshair();
        }

        private void OnEnable()
        {
            PrefabSpawningController.prefabSpawned += ActivateCrosshair;
            PrefabSpawningController.RepositionPrefab += DeactivateCrosshair;
        }

        private void OnDisable()
        {
            PrefabSpawningController.prefabSpawned -= ActivateCrosshair;
            PrefabSpawningController.RepositionPrefab -= DeactivateCrosshair;
        }


        public void ActivateCrosshair()
        {
            crosshair.gameObject.SetActive(true);
        }
        
        public void DeactivateCrosshair()
        {
            crosshair.gameObject.SetActive(false);
        }
        
        public void DeactivateContextButtons()
        {
            crosshair.transform.GetChild(1).gameObject.SetActive(false);
            contextButtonsAreActive = false;
        }
    
        public void ActivateContextButtons()
        {
            crosshair.transform.GetChild(1).gameObject.SetActive(true);
            contextButtonsAreActive = true;
        }
        
    }
}
