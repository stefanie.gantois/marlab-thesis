﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DrugDateController : MonoBehaviour
{
    public enum ExpirationDate
    {
        LastEightMonths,
        LastMonth,
        ThisMonth,
        OneMonth,
        NineMonths,
        OneYear,
    }
    public TextMeshPro[] dateTexts;
    
    public ExpirationDate expirationDate;

    public bool expired;
    
    // Start is called before the first frame update
    private void Start()
    {
        var dateTodayStruct = DateTime.Now;
        if(Statemachine.Instance.notExpired.Equals(transform.GetComponent<AR_Interactable>().InteractableName))
            expirationDate = ExpirationDate.NineMonths;
        else if(transform.GetComponent<AR_Interactable>().InteractableName.Equals("NaCl 0,9% 10ml") || transform.GetComponent<AR_Interactable>().InteractableName.Equals("Glucose 10ml"))
            expirationDate = ExpirationDate.LastEightMonths;
        //Generate the wanted date string
        string dateString;
        
        switch (expirationDate)
        {
            case ExpirationDate.LastEightMonths:
                //Add the year
                dateString = dateTodayStruct.AddMonths(-8).Year + " - ";

                //Add the month
                if (dateTodayStruct.AddMonths(-8).Month < 10)
                {
                    dateString += "0" + dateTodayStruct.AddMonths(-8).Month;
                }
                else
                {
                    dateString += dateTodayStruct.AddMonths(-8).Month;
                }
                break;
            case ExpirationDate.LastMonth:
                //Add the year
                dateString = dateTodayStruct.AddMonths(-1).Year + " - ";
                
                //Add the month
                if (dateTodayStruct.AddMonths(-1).Month < 10)
                {
                    dateString += "0" + dateTodayStruct.AddMonths(-1).Month;
                }
                else
                {
                    dateString += dateTodayStruct.AddMonths(-1).Month;
                }
                break;
            case ExpirationDate.ThisMonth:
                //Add the year
                dateString = dateTodayStruct.Year + " - ";
                
                //Add the month
                if (dateTodayStruct.Month < 10)
                {
                    dateString += "0" + dateTodayStruct.Month;
                }
                else
                {
                    dateString += dateTodayStruct.Month;
                }
                break;
            case ExpirationDate.OneMonth:
                //Add the year
                dateString = dateTodayStruct.AddMonths(1).Year + " - ";
                
                //Add the month
                if (dateTodayStruct.AddMonths(1).Month < 10)
                {
                    dateString += "0" + dateTodayStruct.AddMonths(1).Month;
                }
                else
                {
                    dateString += dateTodayStruct.AddMonths(1).Month;
                }
                break;
            case ExpirationDate.NineMonths:
                //Add the year
                dateString = dateTodayStruct.AddMonths(9).Year + " - ";

                //Add the month
                if (dateTodayStruct.AddMonths(9).Month < 10)
                {
                    dateString += "0" + dateTodayStruct.AddMonths(9).Month;
                }
                else
                {
                    dateString += dateTodayStruct.AddMonths(9).Month;
                }
                break;
            case ExpirationDate.OneYear:
                //Add the year
                dateString = dateTodayStruct.AddYears(1).Year + " - ";
                
                //Add the month
                if (dateTodayStruct.AddYears(1).Month < 10)
                {
                    dateString += "0" + dateTodayStruct.AddYears(1).Month;
                }
                else
                {
                    dateString += dateTodayStruct.AddYears(1).Month;
                }
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        //Add the string to the Object
        foreach (var dateText in dateTexts)
        {
            dateText.text = dateString;
        }
    }
}
