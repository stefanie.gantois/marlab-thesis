﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateAmbubeutel : MonoBehaviour
{
    public GameObject ambuBeutel;
    public GameObject billBoard;

    // Update is called once per frame
    void Update()
    {
       ambuBeutel.SetActive(ToggleController.aRHilfestellung);
       billBoard.SetActive(ToggleController.aRPraxiswissen);
    }
}
