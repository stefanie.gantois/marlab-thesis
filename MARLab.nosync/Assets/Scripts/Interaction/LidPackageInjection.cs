﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LidPackageInjection : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject toAttach;
    public GameObject trash;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Mülleimer":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(-0.0334f, 0.0345f, 0.0255f);
                transform.localEulerAngles = new Vector3(-2.391f, 70f, 0f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
            case "Spritze":
                toAttach.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke("Spritze");
                break;
        }
    }

    public void Interact()
    {
        GetComponent<Interaction.ObjectPackagingController>().Open();
    }
}
