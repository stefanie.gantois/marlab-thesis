﻿using System;
using UnityEngine;

namespace Interaction
{
    public class ObjectLerpingController : MonoBehaviour
    {
        public enum RotationType
        {
            freeRotation,
            rotationInRelationToPlane
        }

        public enum CameraOffset
        {
            staticOffset,
            groupedSizeOffset,
            dynamicSizeOffset
        }
    
        [Header("Object References: ")]
        public GameObject grabber;

        [Header("Options: ")]
        [Range(0.0f, 0.5f)]
        public float movementSpeed = 0.1f;
        public CameraOffset cameraOffsetType = CameraOffset.groupedSizeOffset;
        [Range(0.0f, 1f)]
        public float defaultStaticOffset = 0.2f;

        //Should the Slerping be done freely or only on the Y axis
        public RotationType rotationType = RotationType.rotationInRelationToPlane;

        //Private Variables
        private GameObject grabbedObject;
        private Camera arCamera;
        //private Collider[] hitColliders;

        private void Start()
        {
            arCamera = Camera.main;
        
            ChangeOffsetToCamera(defaultStaticOffset);
        }

        private void ChangeOffsetToCamera(float offset)
        {
            //Set the grabber to the desired offset location
            grabber.transform.localPosition = new Vector3(0,0,offset);
        }

        private void FixedUpdate()
        {
            //Check if an object is selected
            if (grabber.transform.childCount == 0) return;
        
            //Store the selected object
            grabbedObject = grabber.transform.GetChild(0).gameObject;

            switch (cameraOffsetType)
            {
                case CameraOffset.groupedSizeOffset:
                    //Change according to what the AR_Interactable is described as (large, very large, small...)
                    AR_Interactable interactable = grabbedObject.GetComponent<AR_Interactable>();
                    switch (interactable.objectsize)
                    {
                        case ObjectSize.verySmall:
                            ChangeOffsetToCamera(0.075f);
                            break;
                        case ObjectSize.small:
                            ChangeOffsetToCamera(0.1f);
                            break;
                        case ObjectSize.medium:
                            ChangeOffsetToCamera(0.2f);
                            break;
                        case ObjectSize.large:
                            ChangeOffsetToCamera(0.3f);
                            break;
                        case ObjectSize.veryLarge:
                            ChangeOffsetToCamera(0.4f);
                            break;
                        case ObjectSize.defaultSize:
                            ChangeOffsetToCamera(0.2f);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    break;
                case CameraOffset.dynamicSizeOffset:
                    break;
                case CameraOffset.staticOffset:
                    //nothing. Keep the default static offset
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            /*
            hitColliders = Physics.OverlapSphere(grabber.transform.position, 0.025f);
            foreach (var hitCollider in hitColliders)
            {
                //Check if this is an AR_Interactable
                if (hitCollider.transform.CompareTag("AR_InfinityPlane")) return;
            }
            */

            ClipIntoPlane();

            //Lerp the object into the position of the grabber
            Vector3 smoothLerpedPosition = Vector3.Lerp(grabbedObject.transform.position, grabber.transform.position, movementSpeed);
            grabbedObject.transform.position = smoothLerpedPosition;
            //Make the object face the camera
            Quaternion smoothSlerpedRotation = Quaternion.Lerp(grabbedObject.transform.rotation, grabber.transform.rotation, movementSpeed);
            
            //Rotate the object
            if (rotationType == RotationType.rotationInRelationToPlane)
            {
                //Delete the lerped values for X and Z to always have it rotated towards the ground
                Quaternion correctedRotation = Quaternion.Euler(0, smoothSlerpedRotation.eulerAngles.y, 0);
                grabbedObject.transform.rotation = correctedRotation;
            }
            else
            {
                grabbedObject.transform.rotation = smoothSlerpedRotation;
            }
        }
        
        private void ClipIntoPlane()
        {
            Ray ray = arCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
            RaycastHit[] hits = Physics.RaycastAll(ray, 2.0F);

            //Return if the raycast hit nothing at all
            if (hits.Length == 0) return;

            //Check which index is the infinityplane
            int hitIndex = Array.FindIndex(hits, rHit => rHit.transform.CompareTag("AR_InfinityPlane"));

            //Return if the infinityplane was not hit at all
            if (hitIndex == -1) return;

            //Utilize the correct hit = the Infinity plane
            RaycastHit hit = hits[hitIndex];
            if (hit.distance < 0.2f)
            {
                ChangeOffsetToCamera(hit.distance);
            }
        }
    }
}
