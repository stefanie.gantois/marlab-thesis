﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carrierInjection : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject trash;
    public GameObject syringe;

    private int interactionStep = 0;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Mülleimer":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(-0.0152f, -0.0689f, 0.0582f);
                transform.localEulerAngles = new Vector3(0f, -14.675f, 0f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                break;
        }
    }

    public void Interact()
    {
        switch (interactionStep)
        {
            case 0:
                GetComponent<ObjectDrugController>().Open();
                interactionStep++;
                break;
            case 1:
                syringe.GetComponent<Interaction.ObjectInteractionHandler>().interact.Invoke();
                interactionStep++;
                break;
        }
    }
}
