﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Interaction
{
    [RequireComponent(typeof(ObjectInteractionHandler))]
    public class ResetLostObjectController : MonoBehaviour
    {
        private Transform interactable;
        private Vector3 startPosition;
        private Vector3 startRotation;
        private Transform aufbauTransform;
    
        private void Start()
        {
            //Store the transform of this object and its local start position/rotation
            interactable = this.gameObject.transform;
            startPosition = interactable.localPosition;
            startRotation = interactable.localRotation.eulerAngles;
            
            //Store the transform of the aufbau
            aufbauTransform = GameObject.FindWithTag("Aufbau").transform;
            
            //Listen to this objects ObjectInteractionHandler events
            GetComponent<ObjectInteractionHandler>().release.AddListener(RestoreObjectIfLost);
        }

        /// <summary>
        /// Resets the object if it was lost either because it was release too far away or is in free fall
        /// </summary>
        private void RestoreObjectIfLost()
        {
            //if (CheckReleaseDistance()) return;
            StartCoroutine(CheckDistanceDelayed());
        }

        /// <summary>
        /// Checks after secondsUntilfreefallDistanceCheck if the object is too far away from the aufbau
        /// </summary>
        /// <returns>nothing</returns>
        private IEnumerator CheckDistanceDelayed()
        {
            for (int i = 0; i < 3; i++)
            {
                //Wait for 1 second
                yield return new WaitForSeconds(1);

                //As soon as something is 1.5 meters or further away from the initial prefab fire this
                if (Vector3.Distance(aufbauTransform.position, interactable.position) >= 1.5f)
                {
                    //Reset the position and rotation
                    interactable.localPosition = startPosition;
                    interactable.localRotation = Quaternion.Euler(startRotation);
                
                    //Output the result to the console
                    Debug.Log("ResetLostObjectController: Object "
                              + interactable.GetComponent<AR_Interactable>().InteractableName
                              + " was too far away (Distance: "
                              +Vector3.Distance(aufbauTransform.position, interactable.position)
                              + "). It was reset to its initial position.");
                    
                    //Break the coroutine if this was executed
                    yield break;
                }   
            }
        }
    }
}
