﻿using Interaction;
using UnityEngine;

public enum ObjectSize
{
    defaultSize,
    verySmall,
    small,
    medium,
    large,
    veryLarge
}

/// <summary>
/// The AR_Interactable scripts hold information about the AR Object
/// </summary>
[RequireComponent(typeof(SelectionBase))]
[RequireComponent(typeof(ObjectInteractionHandler))]
[RequireComponent(typeof(RigidbodyController))]
public class AR_Interactable : MonoBehaviour
{
    //Name of the AR Interactable
    public string InteractableName;

    //The size of the object
    public ObjectSize objectsize = ObjectSize.defaultSize;
    private ObjectInteractionHandler interactionHandler;
}