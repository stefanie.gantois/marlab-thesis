﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class TrashbinInjectionController : MonoBehaviour
{
    public GameObject syringePackage;
    public GameObject needlePackage;
    public GameObject combinationPlugPackage;
    public GameObject carrierSolutionPackage;
    public GameObject needleProtection;

    private GameObject arSessionOrigin;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Spritzen Verpackung":
                syringePackage.SetActive(true);
                break;
            case "Kanülen Verpackung":
                needlePackage.SetActive(true);
                break;
            case "Spritzenverschluss Verpackung":
                combinationPlugPackage.SetActive(true);
                break;
            case "Spritze":
                carrierSolutionPackage.SetActive(true);
                break;
            case "Kanüle":
                //needleProtection.SetActive(true);
                break;
        }
    }
}
