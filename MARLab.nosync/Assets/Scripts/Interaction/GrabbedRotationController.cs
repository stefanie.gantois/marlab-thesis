﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;

public class GrabbedRotationController : MonoBehaviour
{
    [Header("Script References: ")]
    public InteractionController interactionController;

    [Header("Options: ")]
    [Range(0f,2f)]
    public float rotationSpeed = 0.2f;
    
    void Update()
    {
        if (!interactionController.isGrabbingObject || Input.touchCount <= 0) return;
        Touch touch = Input.GetTouch(0);

        if (touch.phase != TouchPhase.Moved) return;
        
        Quaternion yRotation = Quaternion.Euler(0f, -touch.deltaPosition.x * rotationSpeed, 0f);
        interactionController.grabber.transform.rotation =
            yRotation * interactionController.grabber.transform.rotation;
    }
}
