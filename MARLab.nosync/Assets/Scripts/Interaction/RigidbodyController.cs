﻿using System;
using UnityEngine;

namespace Interaction
{
    /// <summary>
    /// The RigidbodyController handles the activation and deactivation of physics on the child Rigidbodies
    /// </summary>
    public class RigidbodyController : MonoBehaviour
    {
        [Header("References: ")]
        public GameObject[] rigidbodies;

        void Start()
        {
            GetComponent<Interaction.ObjectInteractionHandler>().grab.AddListener(DeactivatePhysics);
            GetComponent<Interaction.ObjectInteractionHandler>().release.AddListener(ActivatePhysics);
        }

        public void ActivatePhysics()
        {
            //Disable physics for the object
            foreach (var rb in rigidbodies)
            {
                rb.GetComponent<Rigidbody>().isKinematic = false;
            }
        
            //Note: Objetcs use the LocalRigidbodyControllers to make themselfs kinematic again after a while
        }

        public void DeactivatePhysics()
        {
            //Disable physics for the object
            foreach (var rb in rigidbodies)
            {
                rb.GetComponent<Rigidbody>().isKinematic = true;
            }
        }

        public void DisablePhysics()
        {
            foreach (var rb in rigidbodies)
            {
                rb.GetComponent<Rigidbody>().isKinematic = true;
                rb.GetComponent<Rigidbody>().detectCollisions = false;
            }
        }
    }
}
