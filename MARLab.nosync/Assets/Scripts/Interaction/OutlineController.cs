﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineController : MonoBehaviour
{
    [Header("References: ")]
    public Outline[] outlines;

    [Header("Options: ")]
    public Color errorColor = Color.red;
    public Color successColor = Color.green;
    
    private bool feedbackOutlineIsActive = false;

    void Start()
    {
        GetComponent<Interaction.ObjectInteractionHandler>().select.AddListener(ActivateOutlines);
        GetComponent<Interaction.ObjectInteractionHandler>().deselect.AddListener(DeactivateOutlines);
        GetComponent<Interaction.ObjectInteractionHandler>().error.AddListener(ActivateErrorIndicator);
        GetComponent<Interaction.ObjectInteractionHandler>().combine.AddListener(ActivateSuccessIndicator);
        GetComponent<Interaction.ObjectInteractionHandler>().interact.AddListener(ActivateSuccessIndicator);
    }


    /// <summary>
    /// Activate the Outline of the object
    /// </summary>
    public void ActivateOutlines()
    {
        if(!Statemachine.Instance.titrating && !Statemachine.Instance.questioning)
            ToggleOutlines(true);
    }
    
    /// <summary>
    /// Deactivate the Outline of the object
    /// </summary>
    public void DeactivateOutlines()
    {
        if (feedbackOutlineIsActive == true )
        {
            StartCoroutine(ToggleOutlinesDelayed(false));
        }
        else
        {
            ToggleOutlines(false);
        }
    }

    /// <summary>
    /// Toggle Outline on and off
    /// </summary>
    private void ToggleOutlines(bool toggle)
    {
        foreach (var ol in outlines)
        {
            ol.enabled = toggle;
        }
    }

    /// <summary>
    /// Toggle the outline delayed if an outline animation is currently playing
    /// </summary>
    /// <param name="toggle"></param>
    /// <returns></returns>
    private IEnumerator ToggleOutlinesDelayed(bool toggle)
    {
        yield return new WaitUntil(() => feedbackOutlineIsActive == false);
        ToggleOutlines(toggle);
    }
    
    /// <summary>
    /// Make Outline invisible without deactivating it
    /// </summary>
    /// <param name="outlineWidth"></param>
    private void ChangeOutLineVisibility(float outlineWidth)
    {
        foreach (var ol in outlines)
        {
            ol.OutlineWidth = outlineWidth;
        }
    }

    /// <summary>
    /// Play Error outline animation in coroutine
    /// </summary>
    public void ActivateErrorIndicator()
    {
        if (feedbackOutlineIsActive == true || !this.gameObject.activeInHierarchy) return;
        StartCoroutine(playErrorOutlineSequence());
    }

    private IEnumerator playErrorOutlineSequence()
    {
        //Starts failure animation for the icons in Assisstance Instrucions Overlay.
        AssistanceController.errorActive = true;
        
        //Indicate that the feedback outline animation ist currently playing
        feedbackOutlineIsActive = true;
        
        //Store the original outline colors
        Color[] initialColors = new Color[outlines.Length];
        for (int i = 0; i < initialColors.Length; i++)
        {
            initialColors[i] = outlines[i].OutlineColor;
        }

        //Store the original outline width
        float initialOutlineWith = outlines[0].OutlineWidth;
        
        //Change the outline color for the error feedback
        foreach (var ol in outlines)
        {
            ol.OutlineColor = errorColor;
        }

        //----Start the animation
        ToggleOutlines(true);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(0);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(0);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(0);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        //----End the animation

        //Restore the original outline colors
        for (int i = 0; i < initialColors.Length; i++)
        {
            outlines[i].OutlineColor = initialColors[i];
        }

        yield return new WaitForSeconds(0.01f);
        ToggleOutlines(false);

        feedbackOutlineIsActive = false;
    }
    
    /// <summary>
    /// Play Success outline animation in coroutine
    /// </summary>
    public void ActivateSuccessIndicator()
    {
        if (feedbackOutlineIsActive == true || !this.gameObject.activeInHierarchy) return;
        StartCoroutine(playSuccessOutlineSequence());
    }

    public void ActivateSuccessIndicator(string CombinedWith)
    {
        if (feedbackOutlineIsActive == true || !this.gameObject.activeInHierarchy) return;
        StartCoroutine(playSuccessOutlineSequence());
    }


    private IEnumerator playSuccessOutlineSequence()
    {
        //Starts success animation for the icons in Assisstance Instrucions Overlay.
        AssistanceController.successActive = true;
        
        //Indicate that the feedback outline animation ist currently playing
        feedbackOutlineIsActive = true;
        
        //Store the original outline colors
        Color[] initialColors = new Color[outlines.Length];
        for (int i = 0; i < initialColors.Length; i++)
        {
            initialColors[i] = outlines[i].OutlineColor;
        }
        
        //Store the original outline width
        float initialOutlineWith = outlines[0].OutlineWidth;
        
        //Change the outline color for the success feedback
        foreach (var ol in outlines)
        {
            ol.OutlineColor = successColor;
        }

        //----Start the animation
        ToggleOutlines(true);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(0);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(0);
        yield return new WaitForSeconds(0.3f);
        ChangeOutLineVisibility(initialOutlineWith);
        yield return new WaitForSeconds(0.3f);
        //----End the animation

        //Restore the original outline colors
        for (int i = 0; i < initialColors.Length; i++)
        {
            outlines[i].OutlineColor = initialColors[i];
        }
        yield return new WaitForSeconds(0.01f);
        ToggleOutlines(false);
        feedbackOutlineIsActive = false;
    }
}

