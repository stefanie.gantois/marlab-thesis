﻿using System.Data;
using Static;
using TMPro;
using UnityEngine;

namespace Interaction
{
    public class penLabeling : MonoBehaviour
    {
        private string label = "";
        public GameObject toLabel;
        public GameObject mainObjectOfLabel;

        private void Start()
        {
            //creates reference in statemachine to this object
            Statemachine.Instance.labelingScript = gameObject;
        }

        private void Update()
        {
            if (Statemachine.Instance.labeling)
            {
                if (Statemachine.Instance.labelingSuccessfull)
                {
                    if (Statemachine.Instance.notExpired.Equals("NaCl 0,9% 10ml"))
                    {
                        label = "Notfalltokolyse, Partusisten 25 Mikrogramm in 4 ml NaCl, " +
                                System.DateTime.Now.ToString("dd.MM.yy") + ", " + System.DateTime.Now.ToString("HH:mm");
                    }
                    else
                    {
                        label = "Notfalltokolyse, Partusisten 25 Mikrogramm in 4 ml GL, " +
                                System.DateTime.Now.ToString("dd.MM.yy") + ", " + System.DateTime.Now.ToString("HH:mm");
                    }
                    toLabel.SetActive(true);
                    Statemachine.Instance.requestStateChange(new StateInformation("pen",null, InteractionType.Interact, "true"));
                    mainObjectOfLabel.GetComponent<InjectionController>().Interact();
                    toLabel.GetComponent<TextMeshPro>().text = label;
                    Statemachine.Instance.labeling = false;
                }
            }
        }

        /// <summary>
        /// function tests the input string on correct syntax for the label
        /// </summary>
        /// 
        private bool correctLabelInput(string toTest)
        {
            return PenLabelingRegexMatch.CheckLabel(toTest);
        }
    }
}
