﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDrugController : MonoBehaviour
{
    [Header("Drug References: ")]
    public GameObject openDrug;
    public GameObject closeDrug;
    
    [Header("Sound References: ")]
    public PlayableSounds openingSound;
    public PlayableSounds closingSound;
    
    [HideInInspector]
    public bool isOpen { get; private set; } = false;

    /// <summary>
    /// Opens the packaging
    /// </summary>
    public void Open()
    {
        //Return if the package is already open
        if (isOpen) return;
            
        //Open the package
        closeDrug.SetActive(false);
        openDrug.SetActive(true);

        //Play the opening sound
        GetComponent<AudioController>().playOtherSound(openingSound);

        //Set the state as opened
        isOpen = true;
    }
   
    /// <summary>
    /// Closes the packaging
    /// </summary>
    public void Close()
    {
        //Return if the package is already open
        if (!isOpen) return;
            
        //Close the package
        openDrug.SetActive(false);
        closeDrug.SetActive(true);

        //Play the closing sound
        GetComponent<AudioController>().playOtherSound(closingSound);

        //Set the state as closed
        isOpen = false;
    }
}
