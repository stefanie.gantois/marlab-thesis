﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedlePackageInjection : MonoBehaviour
{
    private GameObject arSessionOrigin;
    public GameObject toAttach;
    public GameObject trash;

    private void Start()
    {
        arSessionOrigin = GameObject.Find("ArSessionOrigin");
    }

    public void CombineWith(string objectName)
    {
        switch (objectName)
        {
            case "Arbeitsfläche":
                GetComponent<OutlineController>().ActivateSuccessIndicator();
                break;
            case "Mülleimer":
                gameObject.SetActive(false);
                /*
                transform.SetParent(trash.transform);
                transform.localPosition = new Vector3(0.0074f, 0.0807f, 0.0517f);
                transform.localEulerAngles = new Vector3(0f, 113.335f, -104.432f);
                */
                trash.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke(transform.GetComponent<AR_Interactable>().InteractableName);
                        
                break;
            case "Spritze":
                toAttach.GetComponent<Interaction.ObjectInteractionHandler>().combine.Invoke("Spritze");
                break;
        }
    }

    public void Interact()
    {
        GetComponent<Interaction.ObjectPackagingController>().Open();
    }
}
