﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LogController : MonoBehaviour
{
    public static LogController controller = new LogController();
    LogData logData = new LogData();

    private class LogData
    {
        public int userId;
        public float ts;
    }

    private LogController()
    {
        logData.userId = 5;
        logData.ts = Time.time;
    }

    public void printJson(int id2, float ts2)
    {
        logData.userId = id2;
        logData.ts = ts2;
        string json = JsonUtility.ToJson(logData);
        Debug.Log(json);
        File.AppendAllText(Application.dataPath + "logdata.json", json);
    }


}
