﻿using UnityEngine;

namespace Tracking
{
    /// <summary>
    /// This Script instantiates an infinitely large plane after loading the prefab
    /// </summary>
    public class InstantiateInfinityPlane : MonoBehaviour
    {
        public GameObject infinitePlanePrefab;
        
        void Start()
        {
            GameObject spawnedPrefab = Instantiate(infinitePlanePrefab, new Vector3(0, 0, 0), Quaternion.identity);
            spawnedPrefab.transform.SetParent(this.transform);
            spawnedPrefab.transform.localPosition = new Vector3(0,0,0 );
            spawnedPrefab.transform.localRotation = Quaternion.identity;
        }
    }
}
