﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// The infinityPlaneManager spawns an infinitely large plane when the first plane is found on the device to
/// make the interaction easier and not rely on actual detected planed by ARCore oder ARKit
/// </summary>
public class infinityPlaneManager : MonoBehaviour
{
    [Header("Managers: ")]
    public ARPlaneManager planeManager;
    public ARPointCloudManager pointCloudManager;
    public ARSessionOrigin arSessionOrigin;

    [Header("Prefabs: ")]
    public GameObject infinityPlanePrefab;
    
    private bool infinityPlaneWasCreated = false;
    private Camera arCamera;

    private void Start()
    {
        arCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Return if the infinity plane was created already
        if (infinityPlaneWasCreated) return;
        
        //Return if no planes are currently found
        if (planeManager.trackables.count < 0) return;
        
        foreach (var plane in planeManager.trackables)
        {
            //Go through all the planes to find the first one that is not already subsumed by another
            if (plane.subsumedBy != null) continue;
            
            //Spawn the infinityplane at the center of the found plane and make it face the camera on the y axis
            Vector3 position = plane.center;
            Quaternion rotation = Quaternion.LookRotation(arCamera.transform.position) * Quaternion.Euler(0, 90, 0);
            Instantiate(infinityPlanePrefab, position, rotation, arSessionOrigin.transform);
                
            //End the loop when the correct plane was found
            break;
        }
    }
}
