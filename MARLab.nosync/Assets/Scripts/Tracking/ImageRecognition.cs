﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ImageRecognition : MonoBehaviour
{
    private ARTrackedImageManager _arTrackedImageManager;
    private VibrationController _vibController;
    public GameObject vibController;
    public GameObject monitor;
    public GameObject info;
    public GameObject holder;
    public GameObject spawnedHolder;
    public GameObject aufbau;
    public bool prefabSpawned;
    private static Guid imageID;

    private void Awake()
    {
        _arTrackedImageManager = FindObjectOfType<ARTrackedImageManager>();
        _vibController = vibController.GetComponent<VibrationController>();
    }
    
    
    public void OnEnable()
    {
        imageID = _arTrackedImageManager.referenceLibrary[0].guid;
        _arTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }
    
    public void OnDisable()
    {
        _arTrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs args)
    {
        foreach(ARTrackedImage trackedImage in args.added)
        {
            if (trackedImage.referenceImage.guid == imageID)
            {
                spawnedHolder = Instantiate(holder, trackedImage.transform);
                prefabSpawned = true;
                _vibController.StartHeartbeat();
            }
        }
    }
}
