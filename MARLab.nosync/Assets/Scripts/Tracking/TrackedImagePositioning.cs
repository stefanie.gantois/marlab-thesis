﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackedImagePositioning : MonoBehaviour
{
    public ImageRecognition imageRecognition;
    public ARScreenshot arScreenshot;
    public GameObject trackedImagePrefab;

    private bool objectsActive;
    void Update()
    {
        if (trackedImagePrefab.activeSelf && !arScreenshot.screenshotModeOn)
        {
            trackedImagePrefab.transform.position = imageRecognition.spawnedHolder.transform.position;
            trackedImagePrefab.transform.rotation = imageRecognition.spawnedHolder.transform.rotation;
        }
        else if(imageRecognition.prefabSpawned && !trackedImagePrefab.activeSelf)
        {
            trackedImagePrefab.SetActive(true);
        }
    }
}
