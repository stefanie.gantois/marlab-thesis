# MAR Lab

This repository holds the code for the MARLab application developed for the master's thesis "Mobile augmented reality prototype of a titration experiment for chemical engineering education" submitted for the degree of Master of Science in Engineering: Computer Science.
This thesis was established under the supervision of Prof. dr. K. Verbert and Prof. dr. ir. P. Van Puyvelde. The mentors involved were Dr. ir. R. De Croon and J. L. Dominguez Alfaro.

You also find an executable .apk-file to install the application to your android phone.
You can, however, also download the application through the Google Play Store: https://play.google.com/store/apps/details?id=com.thesisAR.MARLab

This thesis builds on the TrainAR framework of Blattgerste et al. which was accessed through the repository for the BMBF Project "HebAR", in which the usage of Augmented Reality for academic midwifery education is explored.
